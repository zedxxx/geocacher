unit u_ObjectPool;

interface

uses
  SysUtils;

type
  TPooledObject = class(TObject)
  private
    _Next: TPooledObject;
  end;

  TObjectPool = class(TObject)
  private
    FRoot: TPooledObject;
    FCount: Integer;
    FCapacity: Integer;
    FLock: IReadWriteSync;
  public
    procedure Clean;
    function Pull: TPooledObject;
    procedure Push(AObject: TPooledObject);
  public
    constructor Create(const ACapacity: Integer = 1024);
    destructor Destroy; override;
  end;

implementation

uses
  u_Synchronizer;

{ TObjectPool }

constructor TObjectPool.Create(const ACapacity: Integer);
begin
  inherited Create;
  FRoot := nil;
  FCount := 0;
  FCapacity := ACapacity;
  FLock := GSync.SyncVariable.Make;
end;

destructor TObjectPool.Destroy;
begin
  Clean;
  FLock := nil;
  inherited Destroy;
end;

procedure TObjectPool.Clean;
var
  VObject: TPooledObject;
begin
  if FCount = 0 then begin
    Exit;
  end;

  FLock.BeginWrite;
  try
    while FRoot <> nil do begin
      VObject := FRoot;
      FRoot := VObject._Next;
      VObject.Free;
      Dec(FCount);
    end;
  finally
    FLock.EndWrite;
  end;
end;

function TObjectPool.Pull: TPooledObject;
begin
  Result := nil;

  if FRoot <> nil then begin
    FLock.BeginWrite;
    try
      if FRoot <> nil then begin
        Result := FRoot;
        FRoot := Result._Next;
        Dec(FCount);
      end;
    finally
      FLock.EndWrite;
    end;
  end;

  if Result <> nil then begin
    Result._Next := nil;
  end;
end;

procedure TObjectPool.Push(AObject: TPooledObject);
var
  VDoFree: Boolean;
begin
  VDoFree := False;

  FLock.BeginWrite;
  try
    if FCount < FCapacity then begin
      AObject._Next := FRoot;
      FRoot := AObject;
      Inc(FCount);
    end else begin
      VDoFree := True;
    end;
  finally
    FLock.EndWrite;
  end;

  if VDoFree then begin
    AObject.Free;
  end;
end;

end.
