unit u_GlobalLog;

interface

{$IFDEF DEBUG}
  {$DEFINE USE_JCL_DEBUG}
{$ENDIF}

uses
  SyncObjs,
  Classes,
  SysUtils;

type
  TGlobalLog = class
  private
    FLock: TCriticalSection;
    FFileStream: TFileStream;
    FPrepareFailed: Boolean;
    function PrepareStream: Boolean;
    procedure Write(const AMsg: string);
    function GetCallStack: string;
    {$IFDEF USE_JCL_DEBUG}
    procedure OnExceptNotify(ExceptObj: TObject; ExceptAddr: Pointer; OSException: Boolean);
    {$ENDIF}
  public
    procedure ShowErrorMessage(const AMsg: string);
    procedure ShowExceptionMessage(const E: Exception; const AMsg: string = '');
    procedure Error(const AMsg: string; const E: Exception = nil);
  public
    constructor Create;
    destructor Destroy; override;
  end;

var
  GLog: TGlobalLog = nil;

implementation

uses
  Windows,
  {$IF (CompilerVersion >= 23.0)} // XE2 and up
  System.UITypes,
  {$IFEND}
  Dialogs,
  {$IFDEF USE_JCL_DEBUG}
  JclDebug,
  JclHookExcept,
  IdException,
  {$ENDIF}
  u_FileSystemTools;

const
  CFileName = 'GeoCacher.log';

function IsMessageToIgnore(const AMsg: string): Boolean;
const
  CIgnoreIndyMessages: array [0..1] of string = (
    'EIdSocketError: Socket Error # 10053\r\nSoftware caused connection abort.',
    'EIdSocketError: Socket Error # 10054\r\nConnection reset by peer.'
  );
var
  I: Integer;
begin
  Result := False;
  for I := Low(CIgnoreIndyMessages) to High(CIgnoreIndyMessages) do begin
    if AMsg = CIgnoreIndyMessages[I] then begin
      Result := True;
      Break;
    end;
  end;
end;

function BeautifyStr(const AStr: string): string;
var
  VTmp: string;
begin
  VTmp := AStr;
  repeat
    Result := StringReplace(VTmp, '  ', ' ', [rfReplaceAll]);
    if Result = VTmp then begin
      Break;
    end else begin
      VTmp := Result;
    end;
  until False;
  Result := StringReplace(Result, #13#10, '\r\n', [rfReplaceAll]);
  Result := StringReplace(Result, #13, '\r', [rfReplaceAll]);
  Result := StringReplace(Result, #10, '\n', [rfReplaceAll]);
end;

{ TGlobalLog }

constructor TGlobalLog.Create;
begin
  inherited Create;
  FLock := TCriticalSection.Create;
  FFileStream := nil;
  FPrepareFailed := False;
  {$IFDEF USE_JCL_DEBUG}
  JclStackTrackingOptions := JclStackTrackingOptions + [stRAWMode];
  JclStartExceptionTracking;
  JclAddExceptNotifier(Self.OnExceptNotify);
  {$ENDIF}
end;

destructor TGlobalLog.Destroy;
begin
  {$IFDEF USE_JCL_DEBUG}
  JclStopExceptionTracking;
  JclRemoveExceptNotifier(Self.OnExceptNotify);
  {$ENDIF}
  FreeAndNil(FFileStream);
  FreeAndNil(FLock);
  inherited;
end;

function TGlobalLog.PrepareStream: Boolean;
var
  VFileName: string;
begin
  Result := False;

  if FPrepareFailed then begin
    Exit;
  end;

  Result := (FFileStream <> nil);
  if Result then begin
    Exit;
  end;

  try
    VFileName := PathToFullPath('.\') + CFileName;
    FFileStream := TFileStream.Create(VFileName, fmCreate);
    FreeAndNil(FFileStream);
    FFileStream := TFileStream.Create(VFileName, fmOpenWrite or fmShareDenyNone);
    Result := True;
  except
    on E: Exception do begin
      FPrepareFailed := True;
      ShowExceptionMessage(E, 'Failed to create log file: ' + VFileName);
    end;
  end;
end;

procedure TGlobalLog.ShowExceptionMessage(const E: Exception; const AMsg: string);
var
  VMsg: string;
begin
  Error(AMsg, E); // write to log

  if AMsg <> '' then begin
    VMsg := AMsg + #13#10 + #13#10;
  end else begin
    VMsg := '';
  end;
  VMsg := VMsg + E.ClassName + ': ' + E.Message;
  MessageDlg(VMsg, mtError, [mbOk], 0);
end;

procedure TGlobalLog.ShowErrorMessage(const AMsg: string);
begin
  Error(AMsg, nil); // write to log
  MessageDlg(AMsg, mtError, [mbOk], 0);
end;

procedure TGlobalLog.Error(const AMsg: string; const E: Exception);
var
  VMsg: string;
begin
  try
    if E <> nil then begin
      VMsg := E.ClassName + ': ' + E.Message;
      if AMsg <> '' then begin
        VMsg := AMsg + '  ' + VMsg;
      end;
    end else begin
      VMsg := AMsg;
    end;

    VMsg := BeautifyStr(VMsg);
    if IsMessageToIgnore(VMsg) then begin
      Exit;
    end;

    Write('{ERR} ' + VMsg + #13#10 + GetCallStack);
  except
    // ignore
  end;
end;

procedure TGlobalLog.Write(const AMsg: string);
var
  VMsg: UTF8String;
  VTimeStamp: string;
  VThreadID: string;
begin
  FLock.Acquire;
  try
    if (FFileStream <> nil) or PrepareStream then begin
      VTimeStamp := FormatDateTime('yyyy-mm-dd hh:nn:ss.zzz', Now);
      VThreadID := Format('%0.5d', [GetCurrentThreadID]);
      VMsg := UTF8Encode(Format('[:%s] %s %s' + #13#10, [VThreadID, VTimeStamp, AMsg]));
      FFileStream.WriteBuffer(VMsg[1], Length(VMsg) * SizeOf(VMsg[1]));
    end;
  finally
    FLock.Release;
  end;
end;

{$IFDEF USE_JCL_DEBUG}
procedure TGlobalLog.OnExceptNotify(
  ExceptObj: TObject;
  ExceptAddr: Pointer;
  OSException: Boolean
);
var
  E: Exception;
  VList: TStringList;
  VMsg, VText: string;
begin
  if (ExceptObj is EIdSilentException) or (ExceptObj is EAbort) then begin
    Exit;
  end;
  VList := TStringList.Create;
  try
    JclLastExceptStackListToStrings(VList, True, True, True, True);
    VText := '<stack_trace>' + #13#10 + VList.Text + '</stack_trace>' + #13#10;
    if Pos('SetThreadName', VText) > 0 then begin
      Exit;
    end;
    if ExceptObj is Exception then begin
      E := ExceptObj as Exception;
      VMsg := BeautifyStr(E.ClassName + ': ' + E.Message);
      if IsMessageToIgnore(VMsg) then begin
        Exit;
      end;
      VText := '{EXC} ' + VMsg + #13#10 + VText;
    end;
    Write(VText);
  finally
    FreeAndNil(VList);
  end;
end;

function TGlobalLog.GetCallStack: string;
var
  I: Integer;
  VTmp: string;
  VStackList: TJclStackInfoList; // JclDebug.pas
  VList: TStringList;
begin
   VStackList := JclCreateStackList(False, 0, Caller(0, False));
   try
     VList := TStringList.Create;
     try
       VStackList.AddToStrings(VList, True, True, True, True);

       // clean
       for I := VList.Count - 1 downto 0 do begin
         VTmp := VList.Strings[I];
         if
           (Pos(Self.ClassName, VTmp) > 0) or
           (Pos('GetCurrentStack', VTmp) > 0) or
           (Pos('JclDebug', VTmp) > 0)
         then begin
           VList.Delete(I);
         end;
       end;

       Result := '<call_stack>' + #13#10 + VList.Text + '</call_stack>' + #13#10;
     finally
       VList.Free;
     end;
   finally
     VStackList.Free;
   end;
end;
{$ELSE}
function TGlobalLog.GetCallStack: string;
begin
  Result := '';
end;
{$ENDIF}

end.
