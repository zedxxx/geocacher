unit u_ServerActivityStatNotifier;

interface

uses
  ExtCtrls,
  t_ServerActivityStat,
  i_AppNotifier,
  i_GuiEventDispatcher;

type
  TServerActivityStatNotifier = class(TObject)
  private
    FTimer: TTimer;
    FStatLast: TServerActivityStatRec;
    FStatCurr: TServerActivityStatRec;
    FAppNotifier: IAppNotifier;
    FGuiEventDispatcher: IGuiEventDispatcher;
    procedure OnTimer(Sender: TObject);
  public
    constructor Create(
      const AAppNotifier: IAppNotifier;
      const AGuiEventDispatcher: IGuiEventDispatcher;
      const AIntervalMillisec: Cardinal = 250
    );
    destructor Destroy; override;
  end;

implementation

uses
  SysUtils,
  u_HttpServer;

{ TServerActivityStatNotifyer }

constructor TServerActivityStatNotifier.Create(
  const AAppNotifier: IAppNotifier;
  const AGuiEventDispatcher: IGuiEventDispatcher;
  const AIntervalMillisec: Cardinal
);
begin
  inherited Create;

  FAppNotifier := AAppNotifier;
  FGuiEventDispatcher := AGuiEventDispatcher;

  FStatLast := cServerActivityStatRecEmpty;

  FTimer := TTimer.Create(nil);
  FTimer.OnTimer := Self.OnTimer;
  FTimer.Interval := AIntervalMillisec;
  FTimer.Enabled := True;
end;

destructor TServerActivityStatNotifier.Destroy;
begin
  FreeAndNil(FTimer);
  inherited Destroy;
end;

procedure TServerActivityStatNotifier.OnTimer(Sender: TObject);
begin
  if not GetHttpServerStat(FStatCurr) then begin
    Exit;
  end;

  if
    // check for unfinished requests
    (FStatCurr.TransferBegin <> FStatCurr.TransferEnd) or
    (FStatCurr.RequestBegin <> FStatCurr.RequestEnd) or
    // check for requests count from last check
    (FStatCurr.RequestBegin <> FStatLast.RequestBegin) or
    (FStatCurr.RequestEnd <> FStatLast.RequestEnd) or
    (FStatCurr.TransferBegin <> FStatLast.TransferBegin) or
    (FStatCurr.TransferEnd <> FStatLast.TransferEnd)
  then begin
    FAppNotifier.HttpActivityOn;
  end else begin
    FAppNotifier.HttpActivityOff;
  end;

  if
    (FGuiEventDispatcher <> nil) and
    ( (FStatCurr.InConnCount <> FStatLast.InConnCount) or
      (FStatCurr.OutConnCount <> FStatLast.OutConnCount) )
  then begin
    FGuiEventDispatcher.SetConnCountStat(
      FStatCurr.InConnCount,
      FStatCurr.OutConnCount
    );
  end;

  FStatLast := FStatCurr;
end;

end.
