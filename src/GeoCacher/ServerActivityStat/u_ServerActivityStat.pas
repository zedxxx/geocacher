unit u_ServerActivityStat;

interface

uses
  FastInterlocked,
  t_ServerActivityStat;

type
  {$A8}
  TServerActivityStat = class(TObject)
  private
    FStat: TServerActivityStatRec;
  public
    // requests
    procedure RequestBegin; inline;
    procedure RequestEnd; inline;

    // tcp activity
    procedure TransferBegin; inline;
    procedure TransferEnd; inline;

    // outgoing connections
    procedure RemoteConnected; inline;
    procedure RemoteDisconnected; inline;

    // incoming connections
    procedure LocalConnected; inline;
    procedure LocalDisconnected; inline;
  public
    constructor Create;
    procedure GetStatRec(out ARec: TServerActivityStatRec); inline;
  end;

implementation

{ TServerActivityStat }

constructor TServerActivityStat.Create;
begin
  inherited Create;
  FStat := cServerActivityStatRecEmpty;
end;

procedure TServerActivityStat.GetStatRec(out ARec: TServerActivityStatRec);
begin
  ARec.InConnCount := FStat.InConnCount;
  ARec.OutConnCount := FStat.OutConnCount;
  
  ARec.RequestBegin := FStat.RequestBegin;
  ARec.RequestEnd := FStat.RequestEnd;

  ARec.TransferBegin := FStat.TransferBegin;
  ARec.TransferEnd := FStat.TransferEnd;
end;

procedure TServerActivityStat.LocalConnected;
begin
  TFastInterlocked.Inc(FStat.InConnCount);
end;

procedure TServerActivityStat.LocalDisconnected;
begin
  TFastInterlocked.Dec(FStat.InConnCount);
end;

procedure TServerActivityStat.RemoteConnected;
begin
  TFastInterlocked.Inc(FStat.OutConnCount);
end;

procedure TServerActivityStat.RemoteDisconnected;
begin
  TFastInterlocked.Dec(FStat.OutConnCount);
end;

procedure TServerActivityStat.RequestBegin;
begin
  TFastInterlocked.Inc(FStat.RequestBegin);
end;

procedure TServerActivityStat.RequestEnd;
begin
  TFastInterlocked.Inc(FStat.RequestEnd);
end;

procedure TServerActivityStat.TransferBegin;
begin
  TFastInterlocked.Inc(FStat.TransferBegin);
end;

procedure TServerActivityStat.TransferEnd;
begin
  TFastInterlocked.Inc(FStat.TransferEnd);
end;

end.
