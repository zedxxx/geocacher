unit t_ServerActivityStat;

interface

type
  {$A8}
  TServerActivityStatRec = record
    InConnCount: Integer;
    OutConnCount: Integer;

    RequestBegin: Integer;
    RequestEnd: Integer;

    TransferBegin: Integer;
    TransferEnd: Integer;
  end;

const
  cServerActivityStatRecEmpty: TServerActivityStatRec = (
    InConnCount   : 0;
    OutConnCount  : 0;
    RequestBegin  : 0;
    RequestEnd    : 0;
    TransferBegin : 0;
    TransferEnd   : 0;
  );

implementation

end.
