unit u_AppTray;

interface

uses
  Windows,
  SysUtils,
  ExtCtrls,
  Graphics,
  t_AppConfig,
  i_AppConfig,
  i_AppNotifier,
  i_Listener,
  u_AppTrayMenu;

type
  TAppTray = class
  private
    type TTrayIconImageId = (tiBlack, tiBlue, tiGreen);
  private
    FTray: TTrayIcon;
    FIcons: array [TTrayIconImageId] of TIcon;

    FAppTrayMenu: TAppTrayMenu;

    FIsBusy: Boolean;
    FDefaultIcon: TTrayIconImageId;

    FAppConfig: IAppConfig;
    FAppNotifier: IAppNotifier;

    FConfigChangeListener: IListener;
    FLanguageChangeListener: IListener;

    function GetDefaultIconId(const AConfig: PAppConfigRec): TTrayIconImageId;

    procedure OnClick(Sender: TObject);
    procedure OnConfigChange(const AConfigStatic: IInterface);
    procedure OnLanguageChange;

    procedure LoadTrayIcons;
    procedure SetIconImage(const AImageId: TTrayIconImageId);
  public
    procedure SetBusy;
    procedure SetIdle;

    procedure RefreshIcon;
    procedure RefreshMenu;
  public
    constructor Create(
      const AAppConfig: IAppConfig;
      const AAppNotifier: IAppNotifier
    );
    destructor Destroy; override;
  end;

implementation

uses
  u_AppVersionInfo,
  u_ListenerByEvent;

{$R TrayIcon\TrayIcon.res}

{ TAppTray }

constructor TAppTray.Create(
  const AAppConfig: IAppConfig;
  const AAppNotifier: IAppNotifier
);
begin
  inherited Create;

  FAppConfig := AAppConfig;
  FAppNotifier := AAppNotifier;

  FAppTrayMenu := TAppTrayMenu.Create(AAppConfig, AAppNotifier);

  FTray := TTrayIcon.Create(nil);

  FIsBusy := False;

  LoadTrayIcons;
  FDefaultIcon := GetDefaultIconId(FAppConfig.GetStatic.RecPtr);
  SetIconImage(FDefaultIcon);

  FTray.PopupMenu := FAppTrayMenu.PopupMenu;
  FTray.Hint := 'GeoCacher' + ' ' + GetAppVersionText;
  FTray.Visible := True;
  FTray.OnClick := Self.OnClick;

  FConfigChangeListener := TNotifyEventListener.Create(Self.OnConfigChange);
  FAppConfig.ChangeNotifier.Add(FConfigChangeListener);

  FLanguageChangeListener := TNotifyNoMmgEventListener.Create(Self.OnLanguageChange);
  FAppConfig.LanguageManager.ChangeNotifier.Add(FLanguageChangeListener);
end;

destructor TAppTray.Destroy;
var
  I: TTrayIconImageId;
begin
  if Assigned(FAppConfig) and Assigned(FConfigChangeListener) then begin
    FAppConfig.ChangeNotifier.Remove(FConfigChangeListener);
    FConfigChangeListener := nil;
  end;

  if Assigned(FAppConfig) and Assigned(FLanguageChangeListener) then begin
    FAppConfig.ChangeNotifier.Remove(FLanguageChangeListener);
    FLanguageChangeListener := nil;
  end;

  for I := Low(TTrayIconImageId) to High(TTrayIconImageId) do begin
    FreeAndNil(FIcons[I]);
  end;

  FreeAndNil(FAppTrayMenu);
  FreeAndNil(FTray);

  inherited Destroy;
end;

function TAppTray.GetDefaultIconId(const AConfig: PAppConfigRec): TTrayIconImageId;
begin
  if
    (AConfig.WorkMode <> wmInetAndCacheRW) or
    (AConfig.FlatFileCache.AllowReadAnyVersion)
  then begin
    Result := tiBlue;
  end else begin
    Result := tiBlack;
  end;
end;

procedure TAppTray.LoadTrayIcons;
const
  CIconName: array [TTrayIconImageId] of string = ('BLACK', 'BLUE', 'GREEN');
var
  I: TTrayIconImageId;
begin
  for I := Low(TTrayIconImageId) to High(TTrayIconImageId) do begin
    FIcons[I] := TIcon.Create;
    FIcons[I].LoadFromResourceName(hInstance, CIconName[I]);
  end;
end;

procedure TAppTray.OnClick(Sender: TObject);
begin
  if FAppConfig.IsGeoGuiAvailable then begin
    FAppNotifier.RunGeoGui;
  end else begin
    FAppNotifier.CloseApp;
  end;
end;

procedure TAppTray.OnConfigChange(const AConfigStatic: IInterface);
var
  VStatic: IAppConfigStatic;
  VConfig: PAppConfigRec;
  VDefaultIconOld: TTrayIconImageId;
begin
  VStatic := AConfigStatic as IAppConfigStatic;
  Assert(VStatic <> nil);

  VConfig := VStatic.RecPtr;

  VDefaultIconOld := FDefaultIcon;
  FDefaultIcon := GetDefaultIconId(VConfig);

  if
    (VDefaultIconOld <> FDefaultIcon) or
    (VConfig.ShowTrayIcon <> FTray.Visible)
  then begin
    FAppNotifier.RefreshTrayIcon;
  end;
end;

procedure TAppTray.OnLanguageChange;
begin
  FAppNotifier.RefreshTrayMenu;
end;

procedure TAppTray.SetIconImage(const AImageId: TTrayIconImageId);
begin
  Assert(MainThreadID = GetCurrentThreadID);
  FTray.Icon := FIcons[AImageId];
  FIsBusy := AImageId <> FDefaultIcon;
end;

procedure TAppTray.SetBusy;
begin
  if not FIsBusy then begin
    SetIconImage(tiGreen);
  end;
end;

procedure TAppTray.SetIdle;
begin
  if FIsBusy then begin
    SetIconImage(FDefaultIcon);
  end;
end;

procedure TAppTray.RefreshIcon;
begin
  if FIsBusy then begin
    SetIconImage(tiGreen);
  end else begin
    SetIconImage(FDefaultIcon);
  end;

  FTray.Visible := FAppConfig.GetStatic.RecPtr.ShowTrayIcon;
end;

procedure TAppTray.RefreshMenu;
begin
  FAppTrayMenu.Refresh;
end;

end.
