unit u_AppTrayMenu;

interface

uses
  SysUtils,
  Menus,
  i_AppConfig,
  i_AppNotifier;

type
  TAppTrayMenu = class
  private
    FMenu: TPopupMenu;

    FAppConfig: IAppConfig;
    FAppNotifier: IAppNotifier;

    function NewItem(
      const ACaption: string;
      const ATag: Integer = 0;
      const AEnabled: Boolean = True;
      const ADefault: Boolean = False;
      const ARadioGroup: Byte = 0
    ): TMenuItem;

    procedure BuildMenu;

    procedure OnPopup(Sender: TObject);
    procedure OnItemClick(Sender: TObject);
  public
    constructor Create(
      const AAppConfig: IAppConfig;
      const AAppNotifier: IAppNotifier
    );
    destructor Destroy; override;

    procedure Refresh;
    property PopupMenu: TPopupMenu read FMenu;
  end;

implementation

uses
  gnugettext,
  t_AppConfig,
  i_LanguageManager,
  u_LanguageMenuItem;

type

{
  Language >
  -
  Open GeoGui
  -
  Inet Only
  Inet + Cache RW
  Inet + Cache RO
  Inet + Cache WO
  Cache Only
  -
  Any Version
  Apply dbRoot Patch
  -
  Exit
}

  TMenuItemTagId = (
    mtNone = 0,

    mtOpenGeoGui,

    mtInetOnly,
    mtInetAndCacheRW,
    mtInetAndCacheRO,
    mtInetAndCacheWO,
    mtCacheOnly,

    mtAnyVersion,
    mtApplyDbRootPatch,

    mtExit
  );

{ TAppTrayMenu }

constructor TAppTrayMenu.Create(
  const AAppConfig: IAppConfig;
  const AAppNotifier: IAppNotifier
);
begin
  inherited Create;

  FAppConfig := AAppConfig;
  FAppNotifier := AAppNotifier;

  FMenu := TPopupMenu.Create(nil);
  FMenu.OnPopup := Self.OnPopup;

  BuildMenu;
end;

destructor TAppTrayMenu.Destroy;
begin
  FreeAndNil(FMenu);
  inherited Destroy;
end;

function TAppTrayMenu.NewItem(
  const ACaption: string;
  const ATag: Integer;
  const AEnabled: Boolean;
  const ADefault: Boolean;
  const ARadioGroup: Byte
): TMenuItem;
begin
  Result := TMenuItem.Create(FMenu);
  with Result do begin
    Enabled := AEnabled;
    Caption := ACaption;
    Tag := ATag;
    if Tag > 0 then begin
      OnClick := Self.OnItemClick;
    end;
    RadioItem := (ARadioGroup > 0);
    GroupIndex := ARadioGroup;
    Default := ADefault;
  end;
end;

procedure TAppTrayMenu.BuildMenu;

  procedure _AddLang;
  var
    I: Integer;
    VItem: TMenuItem;
    VLanguageManager: ILanguageManager;
  begin
    VItem := NewItem('Language'); // do not localize

    VLanguageManager := FAppConfig.LanguageManager;
    for I := 0 to VLanguageManager.LanguageList.Count - 1 do begin
      TLanguageMenuItem.Create(FMenu, VItem, VLanguageManager, I);
    end;

    FMenu.Items.Add(VItem);
  end;

  procedure _AddSep;
  begin
    FMenu.Items.Add( NewItem('-') );
  end;

  procedure _AddItem(const ACaption: string; const ATagId: TMenuItemTagId;
    const AEnabled: Boolean = True; const ADefault: Boolean = False);
  begin
    FMenu.Items.Add( NewItem(ACaption, Integer(ATagId), AEnabled, ADefault) );
  end;

  procedure _AddRadioItem(const ACaption: string; const ATagId: TMenuItemTagId;
    const AGroup: Integer);
  begin
    FMenu.Items.Add( NewItem(ACaption, Integer(ATagId), True, False, AGroup) );
  end;

var
  VIsGeoGuiAvailable: Boolean;
begin
  VIsGeoGuiAvailable := FAppConfig.IsGeoGuiAvailable;
  _AddLang;
  _AddSep;
  _AddItem( _('Open GeoGui'), mtOpenGeoGui, VIsGeoGuiAvailable, VIsGeoGuiAvailable);
  _AddSep;
  _AddRadioItem( _('Internet Only'), mtInetOnly, 1);
  _AddRadioItem( _('Internet and Cache'), mtInetAndCacheRW, 1);
  _AddRadioItem( _('Internet and Cache RO'), mtInetAndCacheRO, 1);
  _AddRadioItem( _('Internet and Cache WO'), mtInetAndCacheWO, 1);
  _AddRadioItem( _('Cache Only'), mtCacheOnly, 1);
  _AddSep;
  _AddItem( _('Any Version'), mtAnyVersion);
  _AddItem( _('Apply dbRoot Patch'), mtApplyDbRootPatch);
  _AddSep;
  _AddItem( _('Exit'), mtExit, True, not VIsGeoGuiAvailable);
end;

procedure TAppTrayMenu.OnItemClick(Sender: TObject);
var
  VItem: TMenuItem;
  VTagId: TMenuItemTagId;
  VConfig: PAppConfigRec;
  VStatic: IAppConfigStatic;
begin
  if not (Sender is TMenuItem) then begin
    Assert(False);
    Exit;
  end;

  VItem := TMenuItem(Sender);
  VTagId := TMenuItemTagId(VItem.Tag);

  VStatic := FAppConfig.GetStatic;
  VConfig := VStatic.RecPtr;

  case VTagId of
    mtOpenGeoGui: FAppNotifier.RunGeoGui;

    mtInetOnly: FAppConfig.SetWorkMode(wmInetOnly);
    mtInetAndCacheRW: FAppConfig.SetWorkMode(wmInetAndCacheRW);
    mtInetAndCacheRO: FAppConfig.SetWorkMode(wmInetAndCacheRO);
    mtInetAndCacheWO: FAppConfig.SetWorkMode(wmInetAndCacheWO);
    mtCacheOnly: FAppConfig.SetWorkMode(wmCacheOnly);

    mtAnyVersion: FAppConfig.SetFlatFileAnyVersion(not VConfig.FlatFileCache.AllowReadAnyVersion);
    mtApplyDbRootPatch: FAppConfig.SetUseDbRootPatch(not VConfig.DbRoot.AllowPatch);

    mtExit: FAppNotifier.CloseApp;
  end;
end;

procedure TAppTrayMenu.OnPopup(Sender: TObject);
var
  I: Integer;
  VItem: TMenuItem;
  VTagId: TMenuItemTagId;
  VConfig: PAppConfigRec;
  VStatic: IAppConfigStatic;
begin
  VStatic := FAppConfig.GetStatic;
  VConfig := VStatic.RecPtr;

  for I := 0 to FMenu.Items.Count - 1 do begin
    VItem := FMenu.Items[I];
    VTagId := TMenuItemTagId(VItem.Tag);
    case VTagId of
      mtInetOnly         : VItem.Checked := VConfig.WorkMode = wmInetOnly;
      mtInetAndCacheRW   : VItem.Checked := VConfig.WorkMode = wmInetAndCacheRW;
      mtInetAndCacheRO   : VItem.Checked := VConfig.WorkMode = wmInetAndCacheRO;
      mtInetAndCacheWO   : VItem.Checked := VConfig.WorkMode = wmInetAndCacheWO;
      mtCacheOnly        : VItem.Checked := VConfig.WorkMode = wmCacheOnly;
      mtAnyVersion       : VItem.Checked := VConfig.FlatFileCache.AllowReadAnyVersion;
      mtApplyDbRootPatch : VItem.Checked := VConfig.DbRoot.AllowPatch;
    end;
  end;
end;

procedure TAppTrayMenu.Refresh;
begin
  FMenu.Items.Clear;
  BuildMenu;
end;

end.
