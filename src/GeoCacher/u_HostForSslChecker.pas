unit u_HostForSslChecker;

interface

uses
  flcASCII,
  flcStdTypes,
  flcUtils,
  flcStrings,
  flcDataStructs,
  t_AppConfig;

type
  THostForSslChecker = class(TObject)
  private
    FSslEnabled: Boolean;
    FForceEnabled: Boolean;

    FHosts: TIntegerDictionaryA;
    FHostsPatterns: TAnsiStringArray;

    procedure LoadHosts(const AHostsMask, AHostsExcludeMask: TArrayOfString);

    class function GetHostNorm(const AHost: string): RawByteString; inline;
    class procedure DeleteDefaultPortNumber(var AHost: RawByteString); inline;
  public
    constructor Create(const AConfig: PSslInterceptConfigRec);
    destructor Destroy; override;

    function IsEnabled(const AHost: string; const APort: Integer): Boolean;
  end;

implementation

uses
  SysUtils,
  flcStringPatternMatcher;

{ THostForSslChecker }

constructor THostForSslChecker.Create(const AConfig: PSslInterceptConfigRec);
begin
  inherited Create;

  FSslEnabled := AConfig.Enabled;
  FForceEnabled := False;

  FHosts := TIntegerDictionaryA.Create;
  FHosts.DuplicatesAction := ddIgnore;

  FHostsPatterns := TAnsiStringArray.Create;

  LoadHosts(AConfig.HostsMask, AConfig.HostsExcludeMask);
end;

destructor THostForSslChecker.Destroy;
begin
  FreeAndNil(FHosts);
  FreeAndNil(FHostsPatterns);
  inherited Destroy;
end;

class procedure THostForSslChecker.DeleteDefaultPortNumber(var AHost: RawByteString);
var
  I: Integer;
begin
  I := Length(AHost) - 4 + 1;
  if (I > 0) and StrMatchB(AHost, ':443', I) then begin
    SetLength(AHost, I-1);
  end;
end;

class function THostForSslChecker.GetHostNorm(const AHost: string): RawByteString;
begin
  if IsAsciiString(AHost) then begin
    Result := RawByteString(AHost);
    AsciiConvertLowerB(Result);
  end else begin
    raise Exception.Create(ClassName + ': Non ASCII chars in host name: ' + AHost);
  end;
end;

procedure THostForSslChecker.LoadHosts(const AHostsMask, AHostsExcludeMask: TArrayOfString);
const
  cMatchPatternCharset: ByteCharSet = ['*', '?', '[', ']'];
var
  I: Integer;
  VHost: RawByteString;
begin
  for I := 0 to Length(AHostsMask) - 1 do begin
    VHost := GetHostNorm(AHostsMask[I]);
    DeleteDefaultPortNumber(VHost);

    if VHost = '*' then begin
      FForceEnabled := True;
      Continue;
    end;

    if PosCharSetA(cMatchPatternCharset, VHost) = 0 then begin
      FHosts.Add(VHost, 0);
    end else begin
      FHostsPatterns.AppendItem(VHost);
    end;
  end;
end;

function THostForSslChecker.IsEnabled(const AHost: string; const APort: Integer): Boolean;
var
  I: Integer;
  VHost: RawByteString;
begin
  Result := (APort = 443) and FForceEnabled;

  if
    Result or
    not FSslEnabled or
    ( (FHostsPatterns.Count = 0) and (FHosts.Count = 0) )
  then begin
    Exit;
  end;

  VHost := GetHostNorm(AHost);
  if APort <> 443 then begin
    VHost := VHost + AnsiChar(':') + IntToStringA(APort);
  end;

  if FHosts.Count > 0 then begin
    Result := FHosts.HasKey(VHost);
    if Result then begin
      Exit;
    end;
  end;

  for I := 0 to FHostsPatterns.Count - 1 do begin
    Result := StrEqualPatternA(FHostsPatterns.Item[I], VHost);
    if Result then begin
      Break;
    end;
  end;
end;

end.
