unit u_TcpTunnel;

interface

uses
  IdGlobal,
  IdIOHandler,
  IdComponent,
  IdTCPClient,
  IdTCPConnection,
  FastInterlocked,
  t_AppConfig,
  u_ServerActivityStat,
  u_HttpContext;

type
  TTcpTunnel = class(TObject)
  private
    FStat: TServerActivityStat;
    FLocalConnection: TIdTCPConnection;

    FRemoteClient: TIdTCPClient;
    FRemoteConnection: TIdTCPConnection;

    procedure Disconnect;
    procedure OnStatus(
      ASender: TObject;
      const AStatus: TIdStatus;
      const AStatusText: string
    );
    function CopyBuffer(
      const ASource: TIdIOHandler;
      const ADest: TIdIOHandler;
      var ANetData: TIdBytes
    ): Integer; inline;
  public
    procedure Connect(
      const AHost: string;
      const APort: Integer;
      const AProxyInfo: PProxyInfo
    );
    procedure Run(const ACtx: THttpContext);
  public
    constructor Create(
      const AStat: TServerActivityStat;
      const ALocalConnection: TIdTCPConnection;
      const ARemoteConnection: TIdTCPConnection = nil
    );
    destructor Destroy; override;
  end;

implementation

uses
  SysUtils,
  IdStack,
  IdStackConsts,
  u_WebClientFactory;

{ TTcpTunnel }

constructor TTcpTunnel.Create(
  const AStat: TServerActivityStat;
  const ALocalConnection: TIdTCPConnection;
  const ARemoteConnection: TIdTCPConnection
);
begin
  Assert(AStat <> nil);

  inherited Create;

  FStat := AStat;
  FLocalConnection := ALocalConnection;

  FRemoteClient := nil;
  FRemoteConnection := ARemoteConnection;
end;

destructor TTcpTunnel.Destroy;
begin
  Disconnect;
  FreeAndNil(FRemoteClient);
  inherited Destroy;
end;

procedure TTcpTunnel.OnStatus(
  ASender: TObject;
  const AStatus: TIdStatus;
  const AStatusText: string
);
begin
  case AStatus of
    hsConnected: FStat.RemoteConnected;
    hsDisconnected: FStat.RemoteDisconnected;
  end;
end;

procedure TTcpTunnel.Connect(
  const AHost: string;
  const APort: Integer;
  const AProxyInfo: PProxyInfo
);
begin
  FRemoteClient := TWebClientFactory.CreateTcpClient(AHost, APort, AProxyInfo);
  FRemoteClient.OnStatus := Self.OnStatus;
  if Assigned(FRemoteClient.IOHandler) then begin
    FRemoteClient.IOHandler.OnStatus := Self.OnStatus;
  end;
  FRemoteClient.Connect;
  FRemoteClient.CheckForGracefulDisconnect(True);

  FRemoteConnection := FRemoteClient as TIdTCPConnection;
end;

procedure TTcpTunnel.Disconnect;
begin
  try
    if Assigned(FRemoteConnection) then begin
      FRemoteConnection.Disconnect;
      FRemoteConnection := nil;
    end;
  except
    //ToDo: log error
  end;
end;

function TTcpTunnel.CopyBuffer(
  const ASource: TIdIOHandler;
  const ADest: TIdIOHandler;
  var ANetData: TIdBytes
): Integer;
begin
  FStat.TransferBegin;
  try
    Result := ASource.InputBuffer.Size;
    ASource.InputBuffer.ExtractToBytes(ANetData, Result, False, -1);
    ADest.Write(ANetData, Result);
  finally
    FStat.TransferEnd;
  end;
end;

procedure TTcpTunnel.Run(const ACtx: THttpContext);
var
  VCount: Integer;
  VDoRead: Boolean;
  VNetData: TIdBytes;
  VSrc, VDest: TIdIOHandler;
  VSrcHandle, VDestHandle: TIdStackSocketHandle;
  VReadList, VDataAvailList: TIdSocketList;
begin
  try
    if FRemoteConnection.Connected and FLocalConnection.Connected then begin
      VReadList := nil;
      VDataAvailList := nil;
      try
        VReadList := TIdSocketList.CreateSocketList;
        VDataAvailList := TIdSocketList.CreateSocketList;

        VDoRead := False;

        VSrc := FLocalConnection.IOHandler;
        VSrcHandle := FLocalConnection.Socket.Binding.Handle;

        VDest := FRemoteConnection.IOHandler;
        VDestHandle := FRemoteConnection.Socket.Binding.Handle;

        SetLength(VNetData, IndyMax(VSrc.RecvBufferSize, VDest.RecvBufferSize));

        VReadList.Add(VSrcHandle);
        VReadList.Add(VDestHandle);
        repeat
          try
            if VDoRead and VSrc.InputBufferIsEmpty and VDest.InputBufferIsEmpty then begin
              if VReadList.SelectReadList(VDataAvailList, IdTimeoutInfinite) then begin
                //1.LConnectionHandle
                if VDataAvailList.ContainsSocket(VSrcHandle) then begin
                  // TODO:
                  // WSAECONNRESET (Exception [EIdSocketError] Socket Error # 10054 Connection reset by peer)
                  VSrc.CheckForDataOnSource(0);
                end;
                //2.LOutBoundHandle
                if VDataAvailList.ContainsSocket(VDestHandle) then begin
                  VDest.CheckForDataOnSource(0);
                end;
              end;
            end;

            // client -> server
            if not VSrc.InputBufferIsEmpty then begin
              VCount := CopyBuffer(VSrc, VDest, VNetData);
              Inc(ACtx.Counters.UpBody, VCount);
            end;

            // server -> client
            if not VDest.InputBufferIsEmpty then begin
              VCount := CopyBuffer(VDest, VSrc, VNetData);
              Inc(ACtx.Counters.DownBody, VCount);
            end;

            VSrc.CheckForDisconnect(False);
            if not VSrc.Connected then begin
              Break;
            end;

            VDest.CheckForDisconnect(False);
            if not VDest.Connected then begin
              Break;
            end;

            VDoRead := True;
          except
            // ToDo: log error
            Break;
          end;
        until False;
      finally
        FreeAndNil(VReadList);
        FreeAndNil(VDataAvailList);
      end;
    end;
  finally
    Disconnect;
  end;
end;

end.

