unit t_FileInfo;

interface

type
  {$IFNDEF UNICODE}
  UnicodeString = WideString;
  {$ENDIF}

  TFileInfoRec = record
    IsExists: Boolean;
    IsLongName: Boolean;
    IsWithMetaData: Boolean;
    LastModified: TDateTime;
    FileNameS: string;
    FileNameW: UnicodeString;
  end;
  PFileInfoRec = ^TFileInfoRec;
  
const
  cFileInfoRecEmpty: TFileInfoRec = (
    IsExists: False;
    IsLongName: False;
    IsWithMetaData: False;
    LastModified: 0;
    FileNameS: '';
    FileNameW: '';
  );

implementation

end.
