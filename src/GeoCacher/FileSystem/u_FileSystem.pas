unit u_FileSystem;

interface

uses
  Windows,
  SysUtils,
  t_FileInfo;

type
  TFileSystem = record
    // Windows
    class function CreateFileX(
      const AInfo: PFileInfoRec;
      const dwDesiredAccess: DWORD;
      const dwShareMode: DWORD;
      const lpSecurityAttributes: PSecurityAttributes;
      const dwCreationDisposition: DWORD;
      const dwFlagsAndAttributes: DWORD;
      const hTemplateFile: THandle
    ): THandle; static;

    class procedure GetFileAttributesX(
      const AInfo: PFileInfoRec
    ); static;

    class procedure Win32Check(
      const AResult: Boolean
    ); inline; static;

    class function RenameFileX(
      const AOldName: string;
      const ANewName: string
    ): Boolean; static;

    // SysUtils
    class function FileExistsX(const AFileName: string): Boolean; static;
    class function DirectoryExistsX(const ADirectory: string): Boolean; static;
    class function CreateDirX(const ADir: string): Boolean; static;
    class function ForceDirectoriesX(ADir: string): Boolean; static;
  end;

  EFileSystem = class(Exception);

const
  INVALID_FILE_ATTRIBUTES = $FFFFFFFF;

implementation

uses
  SysConst;

function _IsLongName(const AName: string): Boolean; inline;
begin
  Result := Length(AName) >= (MAX_PATH - 32);
end;

procedure CheckFileName(const AInfo: PFileInfoRec); inline;
begin
  if AInfo.FileNameS = '' then begin
    raise EFileSystem.Create('File name is empty!');
  end;
  if not AInfo.IsLongName and _IsLongName(AInfo.FileNameS) then begin
    AInfo.IsLongName := True;
    AInfo.FileNameW := '\\?\' + AInfo.FileNameS;
  end;
end;

{ TFileSystem }

class procedure TFileSystem.Win32Check(const AResult: Boolean);
begin
  if not AResult then begin
    RaiseLastOSError(GetLastError);
  end;
end;

class function TFileSystem.CreateFileX(
  const AInfo: PFileInfoRec;
  const dwDesiredAccess: DWORD;
  const dwShareMode: DWORD;
  const lpSecurityAttributes: PSecurityAttributes;
  const dwCreationDisposition: DWORD;
  const dwFlagsAndAttributes: DWORD;
  const hTemplateFile: THandle
): THandle;
begin
  CheckFileName(AInfo);

  if AInfo.IsLongName then begin
    Result :=
      Windows.CreateFileW(
        PWideChar(AInfo.FileNameW),
        dwDesiredAccess,
        dwShareMode,
        lpSecurityAttributes,
        dwCreationDisposition,
        dwFlagsAndAttributes,
        hTemplateFile
      );
  end else begin
    Result :=
      Windows.CreateFile(
        PChar(AInfo.FileNameS),
        dwDesiredAccess,
        dwShareMode,
        lpSecurityAttributes,
        dwCreationDisposition,
        dwFlagsAndAttributes,
        hTemplateFile
      );
  end;
  
  TFileSystem.Win32Check(Result <> INVALID_HANDLE_VALUE);
end;

class function TFileSystem.RenameFileX(const AOldName, ANewName: string): Boolean;
var
  VFlag: DWORD;
  VOld, VNew: UnicodeString;
begin
  VFlag := MOVEFILE_REPLACE_EXISTING or MOVEFILE_WRITE_THROUGH;
  if _IsLongName(AOldName) or _IsLongName(ANewName) then begin
    VOld := '\\?\' + AOldName;
    VNew := '\\?\' + ANewName;
    Result := Windows.MoveFileExW(PWideChar(VOld), PWideChar(VNew), VFlag);
  end else begin
    Result := Windows.MoveFileEx(PChar(AOldName), PChar(ANewName), VFlag);
  end;
end;

procedure _SetupAttr(const AInfo: PFileInfoRec; const AAttr: DWORD); inline;
begin
  AInfo.IsExists := (AAttr and FILE_ATTRIBUTE_DIRECTORY = 0);
  AInfo.IsWithMetaData := AInfo.IsExists and (AAttr and FILE_ATTRIBUTE_SYSTEM <> 0);
end;

procedure _SetupTime(
  const AInfo: PFileInfoRec;
  const ALastWriteTime: PFileTime;
  const ACreationTime: PFileTime
); inline;
var
  VPtr: PFileTime;
  VSysTime: TSystemTime;
begin
  VPtr := ALastWriteTime;
  if (VPtr.dwLowDateTime = 0) or (VPtr.dwHighDateTime = 0) then begin
    VPtr := ACreationTime;
    if (VPtr.dwLowDateTime = 0) or (VPtr.dwHighDateTime = 0) then begin
      raise EFileSystem.Create('Read file time filed: ' + AInfo.FileNameS);
    end;
  end;
  TFileSystem.Win32Check( FileTimeToSystemTime(VPtr^, VSysTime) );
  AInfo.LastModified := SystemTimeToDateTime(VSysTime);
end;

class procedure TFileSystem.GetFileAttributesX(
  const AInfo: PFileInfoRec
);
var
  VHandle: THandle;
  VLastError: Cardinal;
  VAttrData: TWin32FileAttributeData;
  VFindData: TWin32FindData;
  VFindDataW: TWin32FindDataW;
begin
  CheckFileName(AInfo);

  if AInfo.IsLongName then begin
    AInfo.IsExists :=
      Windows.GetFileAttributesExW(
        PWideChar(AInfo.FileNameW),
        GetFileExInfoStandard,
        @VAttrData
      );
  end else begin
    AInfo.IsExists :=
      Windows.GetFileAttributesEx(
        PChar(AInfo.FileNameS),
        GetFileExInfoStandard,
        @VAttrData
      );
  end;

  if AInfo.IsExists then begin
    _SetupAttr(AInfo, VAttrData.dwFileAttributes);
    _SetupTime(AInfo, @(VAttrData.ftLastWriteTime), @(VAttrData.ftCreationTime));
  end else begin
    // file is locked/share_exclusive or we got an access denied
    VLastError := GetLastError;

    AInfo.IsExists :=
      (VLastError <> ERROR_FILE_NOT_FOUND) and
      (VLastError <> ERROR_PATH_NOT_FOUND) and
      (VLastError <> ERROR_INVALID_NAME);

    if AInfo.IsExists then begin
      if AInfo.IsLongName then begin
        VHandle :=
          Windows.FindFirstFileW(
            PWideChar(AInfo.FileNameW),
            VFindDataW
          );
      end else begin
        VHandle :=
          Windows.FindFirstFile(
            PChar(AInfo.FileNameS),
            VFindData
          );
      end;
      if VHandle <> INVALID_HANDLE_VALUE then begin

        Windows.FindClose(VHandle);

        if AInfo.IsLongName then begin
          _SetupAttr(AInfo, VFindDataW.dwFileAttributes);
          _SetupTime(AInfo, @VFindDataW.ftLastWriteTime, @VFindDataW.ftCreationTime);
        end else begin
          _SetupAttr(AInfo, VFindData.dwFileAttributes);
          _SetupTime(AInfo, @VFindData.ftLastWriteTime, @VFindData.ftCreationTime);
        end;
      end else begin
        // file exists but we cant read it's info
        RaiseLastOSError;
      end;
    end else begin
      if VLastError = ERROR_INVALID_NAME then begin
        RaiseLastOSError(VLastError);
      end;
    end;
  end;
end;

class function TFileSystem.FileExistsX(const AFileName: string): Boolean;
var
  VFileInfo: TFileInfoRec;
begin
  VFileInfo := cFileInfoRecEmpty;
  VFileInfo.FileNameS := AFileName;
  GetFileAttributesX(@VFileInfo);
  Result := VFileInfo.IsExists;
end;

class function TFileSystem.DirectoryExistsX(const ADirectory: string): Boolean;
var
  VAttr: DWORD;
  VDirectoryW: UnicodeString;
begin
  if _IsLongName(ADirectory) then begin
    VDirectoryW := '\\?\' + ADirectory;
    VAttr := Windows.GetFileAttributesW(PWideChar(VDirectoryW));
  end else begin
    VAttr := Windows.GetFileAttributes(PChar(ADirectory));
  end;

  Result :=
    (VAttr <> INVALID_FILE_ATTRIBUTES) and
    (VAttr and FILE_ATTRIBUTE_DIRECTORY <> 0);
end;

class function TFileSystem.CreateDirX(const ADir: string): Boolean;
var
  VDirW: UnicodeString;
begin
  if _IsLongName(ADir) then begin
    VDirW := '\\?\' + ADir;
    Result := Windows.CreateDirectoryW(PWideChar(VDirW), nil);
  end else begin
    Result := Windows.CreateDirectory(PChar(ADir), nil);
  end;
end;

class function TFileSystem.ForceDirectoriesX(ADir: string): Boolean;
var
  E: EInOutError;
begin
  Result := True;

  if ADir = '' then begin
    E := EInOutError.CreateRes(@SCannotCreateDir);
    E.ErrorCode := 3;
    raise E;
  end;
  
  ADir := ExcludeTrailingPathDelimiter(ADir);

  if
    (Length(ADir) < 3) or
    TFileSystem.DirectoryExistsX(ADir) or
    (ExtractFilePath(ADir) = ADir)
  then begin
    Exit; // avoid 'xyz:\' problem.
  end;

  Result :=
    TFileSystem.ForceDirectoriesX(ExtractFilePath(ADir)) and
    TFileSystem.CreateDirX(ADir);
end;

end.
