unit u_FileSystemStorage;

interface

uses
  Windows,
  Classes,
  SysUtils,
  t_FileInfo,
  i_FileSystemStorage,
  u_FileSystem;

type
  TFileSystemStorage = class(TInterfacedObject, IFileSystemStorage)
  private
    FLock: IReadWriteSync;
  private
    { IFileSystemStorage }
    procedure GetInfo(
      const AFileName: string;
      out AFileInfo: TFileInfoRec
    );
    procedure ReadMetadata(
      const AFileInfo: PFileInfoRec;
      out AMetaData: TFileMetaData
    );
    procedure Read(
      const AStream: TMemoryStream;
      const AFileInfo: PFileInfoRec;
      out AMetaData: TFileMetaData
    );
    procedure Write(
      const AData: Pointer;
      const ASize: Int64;
      const AFileInfo: PFileInfoRec;
      const AMetaData: TFileMetaData = nil
    );
  public
    constructor Create;
  end;

  EFileSystemStorage = class(Exception);

implementation

uses
  u_Synchronizer;

procedure _FileNameNormalize(const AFileInfo: PFileInfoRec); inline;
var
  I: Integer;
  VFileName: string;
begin
  VFileName := AFileInfo.FileNameS;
  I := Length(VFileName);
  Assert(I > 1);
  if
    ( (VFileName[I-1] <> '#') and (VFileName[I] <> '_') ) and
    ( (VFileName[I] = '\') or TFileSystem.DirectoryExistsX(VFileName) )
  then begin
    AFileInfo.FileNameS := AFileInfo.FileNameS + '#_';
    if AFileInfo.IsLongName then begin
      AFileInfo.FileNameW := AFileInfo.FileNameW + '#_';
    end;
  end;
end;

{ TFileSystemStorage }

constructor TFileSystemStorage.Create;
begin
  inherited Create;
  FLock := GSync.SyncStd.Make;
end;

procedure TFileSystemStorage.GetInfo(
  const AFileName: string;
  out AFileInfo: TFileInfoRec
);
begin
  AFileInfo := cFileInfoRecEmpty;
  AFileInfo.FileNameS := AFileName;

  FLock.BeginRead;
  try
    _FileNameNormalize(@AFileInfo); // ToDo: use optimistic logic
    TFileSystem.GetFileAttributesX(@AFileInfo);
  finally
    FLock.EndRead;
  end;
end;

procedure TFileSystemStorage.Read(
  const AStream: TMemoryStream;
  const AFileInfo: PFileInfoRec;
  out AMetaData: TFileMetaData
);
var
  I: Integer;
  VLen: Byte;
  VCount: Word;
  VStartPos: Int64;
  VPtr: PAnsiChar;
  VHandle: THandle;
  VFileStream: THandleStream;
begin
  FLock.BeginRead;
  try
    _FileNameNormalize(AFileInfo); // ToDo: use optimistic logic

    VHandle :=
      TFileSystem.CreateFileX(
        AFileInfo,
        GENERIC_READ,
        FILE_SHARE_READ,
        nil,
        OPEN_EXISTING,
        FILE_ATTRIBUTE_NORMAL,
        0
      );
    try
      VFileStream := THandleStream.Create(VHandle);
      try
        AStream.Clear;
        AStream.LoadFromStream(VFileStream);
      finally
        VFileStream.Free;
      end;
    finally
      TFileSystem.Win32Check( CloseHandle(VHandle) );
    end;
  finally
    FLock.EndRead;
  end;

  if AFileInfo.IsWithMetaData then begin
    // read Count
    VStartPos := AStream.Size - SizeOf(VCount);
    AStream.Position := VStartPos;
    AStream.ReadBuffer(VCount, SizeOf(VCount));

    SetLength(AMetaData, VCount);

    for I := VCount - 1 downto 0 do begin
      // read field len
      Dec(VStartPos, SizeOf(VLen));
      AStream.Position := VStartPos;
      AStream.ReadBuffer(VLen, SizeOf(VLen));

      // prepare dest string
      SetLength(AMetaData[I], VLen);

      if VLen = 0 then begin
        Continue;
      end;

      VPtr := @(AMetaData[I][1]);
      ZeroMemory(VPtr, VLen);

      // read field value
      Dec(VStartPos, VLen);
      AStream.Position := VStartPos;
      AStream.ReadBuffer(VPtr^, VLen);
    end;

    // truncate metadata
    AStream.Position := 0;
    AStream.Size := VStartPos;
  end;
end;

procedure TFileSystemStorage.ReadMetadata(
  const AFileInfo: PFileInfoRec;
  out AMetaData: TFileMetaData
);
var
  I: Integer;
  VLen: Byte;
  VCount: Word;
  VStartPos: Int64;
  VPtr: PAnsiChar;
  VHandle: THandle;
  VFileStream: THandleStream;
begin
  if not AFileInfo.IsWithMetaData then begin
    AMetaData := nil;
    Exit;
  end;

  FLock.BeginRead;
  try
    _FileNameNormalize(AFileInfo); // ToDo: use optimistic logic

    VHandle :=
      TFileSystem.CreateFileX(
        AFileInfo,
        GENERIC_READ,
        FILE_SHARE_READ,
        nil,
        OPEN_EXISTING,
        FILE_ATTRIBUTE_NORMAL,
        0
      );
    try
      VFileStream := THandleStream.Create(VHandle);
      try
        // read Count
        VStartPos := VFileStream.Size - SizeOf(VCount);
        VFileStream.Position := VStartPos;
        VFileStream.ReadBuffer(VCount, SizeOf(VCount));

        SetLength(AMetaData, VCount);

        for I := VCount - 1 downto 0 do begin
          // read field len
          Dec(VStartPos, SizeOf(VLen));
          VFileStream.Position := VStartPos;
          VFileStream.ReadBuffer(VLen, SizeOf(VLen));

          // prepare dest string
          SetLength(AMetaData[I], VLen);

          if VLen = 0 then begin
            Continue;
          end;

          VPtr := @(AMetaData[I][1]);
          ZeroMemory(VPtr, VLen);

          // read field value
          Dec(VStartPos, VLen);
          VFileStream.Position := VStartPos;
          VFileStream.ReadBuffer(VPtr^, VLen);
        end;
      finally
        VFileStream.Free;
      end;
    finally
      TFileSystem.Win32Check( CloseHandle(VHandle) );
    end;
  finally
    FLock.EndRead;
  end;
end;

function FileSetDate(Handle: Integer; Age: Integer): Boolean; inline;
var
  VLocalFileTime, VFileTime: TFileTime;
begin
  Result :=
    DosDateTimeToFileTime(LongRec(Age).Hi, LongRec(Age).Lo, VLocalFileTime) and
    LocalFileTimeToFileTime(VLocalFileTime, VFileTime) and
    SetFileTime(Handle, nil, nil, @VFileTime);
end;

procedure TFileSystemStorage.Write(
  const AData: Pointer;
  const ASize: Int64;
  const AFileInfo: PFileInfoRec;
  const AMetaData: TFileMetaData
);
var
  I: Integer;
  VLen: Integer;
  VPtr: PAnsiChar;
  VMetaCount: Integer;
  VAttr: DWORD;
  VHandle: THandle;
  VErrCode: Cardinal;
  VUnique: string;
  VFileDir: string;
  VFileDirTmp: string;
  VFileStream: THandleStream;
  VFileNameOrigin: string;
begin
  VUnique := '.' + IntToStr(GetCurrentThreadId) + '.tmp';

  FLock.BeginWrite;
  try
    // Prepare file name

    _FileNameNormalize(AFileInfo); // ToDo: use optimistic logic
    VFileNameOrigin := AFileInfo.FileNameS;

    // Generate unique temp file name
    AFileInfo.FileNameS := AFileInfo.FileNameS + VUnique;
    if AFileInfo.IsLongName then begin
      AFileInfo.FileNameW := AFileInfo.FileNameW + VUnique;
    end else begin
      // CreateFileX will check if the new file name is long or not
    end;

    // Create directories

    VFileDir := ExtractFileDir(VFileNameOrigin);
    if not TFileSystem.ForceDirectoriesX(VFileDir) then begin
      // Possible fail is becouse Windows doesn't allow to create a Dir if a
      // File with same name exists

      VErrCode := GetLastError;
      if VErrCode = ERROR_ALREADY_EXISTS then begin
        // search for a file and rename it
        repeat
          VFileDirTmp := VFileDir;
          if TFileSystem.FileExistsX(VFileDir) then begin
            // rename existing file
            TFileSystem.Win32Check(
              TFileSystem.RenameFileX(VFileDir, VFileDir + '#_')
            );
            // now we can safely create a dir
            TFileSystem.Win32Check(
              TFileSystem.ForceDirectoriesX(ExtractFileDir(VFileNameOrigin))
            );
            Break;
          end else begin
            VFileDir := ExtractFileDir(VFileDir);
            if (Length(VFileDir) = 3) or (VFileDir = VFileDirTmp) then begin
              raise EFileSystemStorage.Create(
                'Can''t create directory: ' + ExtractFileDir(VFileNameOrigin)
              );
            end;
          end;
        until False;
      end else begin
        RaiseLastOSError(VErrCode);
      end;
    end;

    // Create file and save data to the disk

    VMetaCount := Length(AMetaData);
    Assert(VMetaCount < MAXWORD);

    if VMetaCount > 0 then begin
      VAttr := FILE_ATTRIBUTE_SYSTEM;
    end else begin
      VAttr := FILE_ATTRIBUTE_NORMAL;
    end;

    VHandle :=
      TFileSystem.CreateFileX(
        AFileInfo,
        GENERIC_WRITE,
        0,
        nil,
        CREATE_ALWAYS,
        VAttr,
        0
      );
    try
      TFileSystem.Win32Check( FileSetDate(VHandle, DateTimeToFileDate(Now)) );

      VFileStream := THandleStream.Create(VHandle);
      try
        if (AData <> nil) and (ASize > 0) then begin
          VFileStream.Size := ASize;
          VFileStream.Position := 0;
          VFileStream.WriteBuffer(AData^, ASize);
        end;
      
        if VAttr = FILE_ATTRIBUTE_SYSTEM then begin
          for I := 0 to VMetaCount - 1 do begin
            VLen := Length(AMetaData[I]);
            Assert(VLen < MAXBYTE);
            if VLen > 0 then begin
              VPtr := @(AMetaData[I][1]);
              VFileStream.WriteBuffer(VPtr^, Byte(VLen));
            end;
            VFileStream.WriteBuffer(Byte(VLen), 1);
          end;
          VFileStream.WriteBuffer(Word(VMetaCount), 2);
        end;
      finally
        VFileStream.Free;
      end;
    finally
      TFileSystem.Win32Check( CloseHandle(VHandle) );
    end;

    // Rename file back to the original name
    TFileSystem.Win32Check(
      TFileSystem.RenameFileX(AFileInfo.FileNameS, VFileNameOrigin)
    );

    AFileInfo.FileNameS := VFileNameOrigin;
    if AFileInfo.IsLongName then begin
      AFileInfo.FileNameW := '\\?\' + AFileInfo.FileNameS;
    end;
  finally
    FLock.EndWrite;
  end;
end;

end.
