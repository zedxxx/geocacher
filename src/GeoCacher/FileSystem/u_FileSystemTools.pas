unit u_FileSystemTools;

interface

function IsRelativePath(const Path: string): Boolean; inline;
function GetFullPath(const ABasePath, ARelativePath: string): string;
function GetAppPath: string; inline;
function PathToFullPath(const APath: string): string;

procedure CreateDirIfNotExists(const APath: string);
procedure CreateFileIfNotExists(const AFileName: string);

implementation

uses
  Windows,
  SysUtils,
  ShLwApi;

function IsRelativePath(const Path: string): Boolean; inline;
var
  L: Integer;
begin
  L := Length(Path);
  Result := (L > 0) and (Path[1] <> PathDelim) and (L > 1) and (Path[2] <> ':');
end;

function GetFullPath(const ABasePath, ARelativePath: string): string;
begin
  SetLength(Result, MAX_PATH);
  PathCombine(@Result[1], PChar(ExtractFilePath(ABasePath)), PChar(ARelativePath));
  SetLength(Result, LStrLen(PChar(Result)));
  Result := IncludeTrailingPathDelimiter(Result);
end;

function GetAppPath: string; inline;
begin
  Result := ExtractFilePath(ParamStr(0));
end;

function PathToFullPath(const APath: string): string;
begin
  if IsRelativePath(APath) then begin
    Result := GetFullPath(GetAppPath, APath);
  end else begin
    Result := APath;
  end;
end;

procedure CreateDirIfNotExists(const APath: string);
var
  I: Integer;
  VPath: string;
begin
  I := LastDelimiter(PathDelim, APath);
  VPath := Copy(APath, 1, I);
  if not DirectoryExists(VPath) then begin
    if not ForceDirectories(VPath) then begin
      RaiseLastOSError;
    end;
  end;
end;

procedure CreateFileIfNotExists(const AFileName: string);
var
  I: Integer;
begin
  if not FileExists(AFileName) then begin

    CreateDirIfNotExists(ExtractFilePath(AFileName));

    I := FileCreate(AFileName);
    if I = Integer(INVALID_HANDLE_VALUE) then begin
      RaiseLastOSError;
    end;
    FileClose(I);
  end;
end;

end.
