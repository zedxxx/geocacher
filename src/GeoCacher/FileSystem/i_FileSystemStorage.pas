unit i_FileSystemStorage;

interface

uses
  Classes,
  t_FileInfo;

type
  TFileMetaData = array of AnsiString;

  IFileSystemStorage = interface
  ['{E30EE996-C235-4404-9B57-A509F521FDD7}']
    procedure GetInfo(
      const AFileName: string;
      out AFileInfo: TFileInfoRec
    );

    procedure ReadMetadata(
      const AFileInfo: PFileInfoRec;
      out AMetaData: TFileMetaData
    );

    procedure Read(
      const AStream: TMemoryStream;
      const AFileInfo: PFileInfoRec;
      out AMetaData: TFileMetaData
    );

    procedure Write(
      const AData: Pointer;
      const ASize: Int64;
      const AFileInfo: PFileInfoRec;
      const AMetaData: TFileMetaData = nil
    );
  end;

implementation

end.
