unit u_HttpHeadersTools;

interface

uses
  RegExpr;

function GetHeaderValue(const AHead: string; const ARegEx: TRegExpr; const AName: string): string;

procedure SetLastModified(var AHead: string; const ADate: TDateTime; const ARegEx: TRegExpr); overload;
procedure SetLastModified(var AHead: string; const ALastModified: string; const ARegEx: TRegExpr); overload;

function SetETag(var AHead: string; const AETag: string; const ARegEx: TRegExpr): Boolean;

procedure ClearProxyHeaders(var AHead: string; const ARegEx: TRegExpr);
procedure ClearContentLength(var AHead: string; const ARegEx: TRegExpr);
procedure ClearChunked(var AHead: string; const ARegEx: TRegExpr);

procedure PrepareClientHeaders(var AHead: string; const ARegEx: TRegExpr; const AContentLength: Int64);

implementation

uses
  IdGlobal,
  Classes,
  SysUtils;

const
  EOL: string = #13#10;

function GetHeaderValue(const AHead: string; const ARegEx: TRegExpr; const AName: string): string;
var
  I, M: Boolean;
begin
  I := ARegEx.ModifierI;
  M := ARegEx.ModifierM;

  ARegEx.ModifierI := True;
  ARegEx.ModifierM := True;

  ARegEx.Expression := '^' + AName + '\s*:\s*(.+?)\r\n';
  if ARegEx.Exec(AHead) then begin
    Result := AName + ': ' + ARegEx.Match[1];
  end else begin
    Result := '';
  end;

  ARegEx.ModifierI := I;
  ARegEx.ModifierM := M;
end;

procedure SetLastModified(var AHead: string; const ADate: TDateTime; const ARegEx: TRegExpr);
var
  VLastModified: string;
begin
  VLastModified := 'If-Modified-Since: ' + LocalDateTimeToGMT(ADate, True) + EOL;

  ARegEx.Expression := '(If-Modified-Since.*?\n)';
  if ARegEx.Exec(AHead) then begin
    AHead := ARegEx.Replace(AHead, VLastModified, False);
  end else begin
    AHead := AHead + VLastModified;
  end;
end;

procedure SetLastModified(var AHead: string; const ALastModified: string; const ARegEx: TRegExpr);
var
  VLastModified: string;
begin
  if ALastModified = '' then begin
    VLastModified := '';
  end else begin
    VLastModified := 'If-Modified-Since: ' + ALastModified + EOL;
  end;

  ARegEx.Expression := '(If-Modified-Since.*?\n)';
  if ARegEx.Exec(AHead) then begin
    AHead := ARegEx.Replace(AHead, VLastModified, False);
  end else begin
    AHead := AHead + VLastModified;
  end;
end;

function SetETag(var AHead: string; const AETag: string; const ARegEx: TRegExpr): Boolean;
var
  I: Integer;
  VETag: string;
begin
  Result := False;

  if AETag = '' then begin
    Exit;
  end;

  I := Pos(':', AETag);
  if I = 0 then begin
    Exit;
  end;

  VETag := Trim(Copy(AETag, I+1));
  if VETag <> '' then begin
    VETag := 'If-None-Match: ' + VETag + EOL;
  end else begin
    Exit;
  end;

  ARegEx.Expression := '(If-None-Match.*?\n)';
  if ARegEx.Exec(AHead) then begin
    AHead := ARegEx.Replace(AHead, VETag, False);
  end else begin
    AHead := AHead + VETag;
  end;

  Result := True;
end;

procedure ClearProxyHeaders(var AHead: string; const ARegEx: TRegExpr);
begin
  ARegEx.Expression := 'Proxy-.*?\n';
  while ARegEx.Exec(AHead) do begin
    AHead := ARegEx.Replace(AHead, '', False);
  end;
end;

procedure ClearContentLength(var AHead: string; const ARegEx: TRegExpr);
begin
  ARegEx.Expression := 'Content-Length.*?\n';
  if ARegEx.Exec(AHead) then begin
    AHead := ARegEx.Replace(AHead, '', False);
  end;
end;

procedure ClearChunked(var AHead: string; const ARegEx: TRegExpr);
const
  cChunked = 'chunked';
var
  I: Integer;
  VText: string;
  VList: TStringList;
begin
  ARegEx.Expression := 'Transfer-Encoding: (.*?)\n';
  while ARegEx.Exec(AHead) do begin
    VText := LowerCase(Trim(ARegEx.Match[1]));
    if Pos(cChunked, VText) > 0 then begin
      if VText = cChunked then begin
        // remove Transfer-Encoding
        AHead := ARegEx.Replace(AHead, '', False);
      end else begin
        VText := StringReplace(VText, cChunked, '', []);
        VList := TStringList.Create;
        try
          VList.Delimiter := ',';
          VList.StrictDelimiter := True;
          VList.DelimitedText := VText;
          VText := '';
          for I := 0 to VList.Count - 1 do begin
            if Trim(VList.Strings[I]) <> '' then begin
              if VText <> '' then begin
                VText := VText + ', ';
              end;
              VText := VText + Trim(VList.Strings[I]);
            end;
          end;
          if VText <> '' then begin
            AHead := ARegEx.Replace(AHead, 'Transfer-Encoding: ' + VText + EOL, False);
          end else begin
            AHead := ARegEx.Replace(AHead, '', False);
          end;
        finally
          VList.Free;
        end;
      end;
    end;
  end;
end;

procedure PrepareClientHeaders(
  var AHead: string;
  const ARegEx: TRegExpr;
  const AContentLength: Int64
);
var
  VContentLengthStr: string;
begin
  ARegEx.Expression := '^HTTP.*?\n';
  if ARegEx.Exec(AHead) then begin
    AHead := ARegEx.Replace(AHead, '', False);
  end;

  ARegEx.Expression := '(Content-Length.*?\n)';
  VContentLengthStr := 'Content-Length: ' + IntToStr(AContentLength) + EOL;
  if ARegEx.Exec(AHead) then begin
    AHead := ARegEx.Replace(AHead, VContentLengthStr, False);
  end else begin
    AHead := AHead + VContentLengthStr;
  end;

  StringReplace(AHead, EOL+EOL, EOL, []);
end;

end.
