unit i_AppNotifier;

interface

type
  IAppNotifier = interface
    ['{F912C7E7-A262-4E17-9FA2-850B6ED45F39}']

    procedure HttpActivityOn;
    procedure HttpActivityOff;

    procedure RefreshTrayIcon;
    procedure RefreshTrayMenu;

    procedure RunGeoGui;

    procedure CloseApp;
  end;

  IAppNotifierInternal = interface(IAppNotifier)
    ['{C8A95EAB-8D6D-4A3E-B109-0ECE08DFA405}']

    procedure StartNotify(const AHwd: THandle);
    procedure StopNotify;
  end;

implementation

end.
