unit c_AppNotifier;

interface

uses
  Messages;

const
  WM_APP_NOTIFIER_HTTP_ACTIVITY_ON  = WM_USER + 100;
  WM_APP_NOTIFIER_HTTP_ACTIVITY_OFF = WM_USER + 101;

  WM_APP_NOTIFIER_REFRESH_TRAY_ICON = WM_USER + 102;
  WM_APP_NOTIFIER_REFRESH_TRAY_MENU = WM_USER + 103;

  WM_APP_NOTIFIER_RUN_GEOGUI        = WM_USER + 104;

implementation

end.
