unit u_AppNotifier;

interface

uses
  Windows,
  Messages,
  c_AppNotifier,
  i_AppNotifier;

type
  TAppNotifier = class(TInterfacedObject, IAppNotifier, IAppNotifierInternal)
  private
    FHwd: THandle;
    FIsEnabled: Boolean;

    procedure DoPostMessage(const AMsg: Cardinal); inline;
  private
    { IAppNotifier }
    procedure HttpActivityOn;
    procedure HttpActivityOff;
    procedure RefreshTrayIcon;
    procedure RefreshTrayMenu;
    procedure RunGeoGui;
    procedure CloseApp;
  private
    { IAppNotifierInternal }
    procedure StartNotify(const AHwd: THandle);
    procedure StopNotify;
  public
    constructor Create;
  end;

implementation

{ TAppNotifier }

constructor TAppNotifier.Create;
begin
  inherited Create;
  StopNotify;
end;

procedure TAppNotifier.DoPostMessage(const AMsg: Cardinal);
begin
  if FIsEnabled then begin
    PostMessage(FHwd, AMsg, 0, 0);
  end else begin
    Assert(False);
  end;
end;

procedure TAppNotifier.HttpActivityOn;
begin
  DoPostMessage(WM_APP_NOTIFIER_HTTP_ACTIVITY_ON);
end;

procedure TAppNotifier.HttpActivityOff;
begin
  DoPostMessage(WM_APP_NOTIFIER_HTTP_ACTIVITY_OFF);
end;

procedure TAppNotifier.RefreshTrayIcon;
begin
  DoPostMessage(WM_APP_NOTIFIER_REFRESH_TRAY_ICON);
end;

procedure TAppNotifier.RefreshTrayMenu;
begin
  DoPostMessage(WM_APP_NOTIFIER_REFRESH_TRAY_MENU);
end;

procedure TAppNotifier.RunGeoGui;
begin
  DoPostMessage(WM_APP_NOTIFIER_RUN_GEOGUI);
end;

procedure TAppNotifier.CloseApp;
begin
  DoPostMessage(WM_QUIT);
end;

procedure TAppNotifier.StartNotify(const AHwd: THandle);
begin
  FHwd := AHwd;
  FIsEnabled := True;
end;

procedure TAppNotifier.StopNotify;
begin
  FHwd := 0;
  FIsEnabled := False;
end;

end.
