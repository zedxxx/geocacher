unit u_HttpDownloaderByIndy;

interface

uses
  Classes,
  SysUtils,
  IdHTTP,
  IdGlobal,
  IdComponent,
  IdTCPConnection,
  FastInterlocked,
  t_AppConfig,
  u_HttpContext,
  u_ServerActivityStat,
  u_ServerInterceptLog,
  u_HttpDownloader;

type
  THttpDownloaderByIndy = class(THttpDownloader)
  private
    FIdHTTP: TIdCustomHTTP;
    FIsChunked: Boolean;
    FStat: TServerActivityStat;
    FIntercept: TConnectionInterceptLog;
    procedure OnStatus(
      ASender: TObject;
      const AStatus: TIdStatus;
      const AStatusText: string
    );
    procedure OnChunkReceived(
      ASender: TObject;
      var AChunk: TIdBytes
    );
    procedure InitIdHTTP(
      const AIntercept: TObject;
      const AIsHttps: Boolean;
      const AProxyInfo: PProxyInfo
    );
  public
    constructor Create;
    destructor Destroy; override;
    function DoRequest(const ACtx: THttpContext): Boolean; override;
    function GetRawConnection: TIdTcpConnection; override;
  end;

  EHttpDownloaderByIndy = class(Exception);

implementation

uses
  WinSock,
  IdStack,
  IdSSLOpenSSL,
  t_CustomHTTPServer,
  u_GlobalLog,
  u_HttpRequestInfo,
  u_HttpHeadersTools,
  u_WebClientFactory;

{ THttpDownloaderByIndy }

constructor THttpDownloaderByIndy.Create;
begin
  inherited Create;
  FIdHTTP := nil;
  FStat := nil;
  FIntercept := nil;
end;

destructor THttpDownloaderByIndy.Destroy;
begin
  if Assigned(FIdHTTP) then begin
    try
      FIdHTTP.Disconnect;
    except
      // ToDo: log error
    end;
    FreeAndNil(FIdHTTP);
  end;
  FIntercept := nil;
  FStat := nil;
  inherited Destroy;
end;

procedure THttpDownloaderByIndy.InitIdHTTP(
  const AIntercept: TObject;
  const AIsHttps: Boolean;
  const AProxyInfo: PProxyInfo
);
begin
  FIdHTTP := TWebClientFactory.CreateHttpClient(AIsHttps, AProxyInfo);

  // setup OnStatus
  FIdHTTP.OnStatus := Self.OnStatus;
  if AIsHttps then begin
    FIdHTTP.IOHandler.OnStatus := Self.OnStatus;
  end;

  // setup Intercept
  if Assigned(AIntercept) then begin
    FIntercept := AIntercept as TConnectionInterceptLog;
    FIdHTTP.Intercept := FIntercept;
    if AIsHttps then begin
      FIdHTTP.IOHandler.Intercept := FIntercept;
      (FIdHTTP.IOHandler as TIdSSLIOHandlerSocketOpenSSL).OnStatusInfoEx :=
        FIntercept.OnStatusInfoEx;
    end;
  end;
end;

procedure THttpDownloaderByIndy.OnChunkReceived(
  ASender: TObject;
  var AChunk: TIdBytes
);
begin
  FIsChunked := True;
end;

procedure THttpDownloaderByIndy.OnStatus(
  ASender: TObject;
  const AStatus: TIdStatus;
  const AStatusText: string
);
begin
  case AStatus of
    hsConnected: FStat.RemoteConnected;
    hsDisconnected: FStat.RemoteDisconnected;
  end;
  if Assigned(FIntercept) then begin
    FIntercept.OnStatus(ASender, AStatus, AStatusText);
  end;
end;

function THttpDownloaderByIndy.DoRequest(const ACtx: THttpContext): Boolean;
const
  cMaxRetryCount = 2;
var
  VRetryCount: Integer;
  VRawHeaders: string;
begin
  Result := False;
  VRetryCount := 0;
  repeat
    try
      Inc(VRetryCount);

      if not Assigned(FIdHTTP) then begin
        InitIdHTTP(
          ACtx.Intercept,
          ACtx.IsHTTPS,
          @ACtx.AppConfigRec.Proxy
        );
        FStat := ACtx.HttpdStat as TServerActivityStat;
        Assert(FStat <> nil);
      end else begin
        FIdHTTP.CheckForGracefulDisconnect(False);
      end;

      VRawHeaders := ACtx.HEAD;
      ClearContentLength(VRawHeaders, ACtx.RegEx);
      FIdHTTP.Request.CustomHeaders.Text := VRawHeaders;

      FIsChunked := False;

      FIdHTTP.HTTPOptions := [
        hoNoParseMetaHTTPEquiv,
        hoNoParseXmlCharset,
        hoNoProtocolErrorException,
        hoWantProtocolErrorContent,
        hoKeepOrigProtocol
      ];

      FIdHTTP.Request.Accept := '';
      FIdHTTP.Request.AcceptEncoding := '';
      FIdHTTP.Request.UserAgent := '';
      FIdHTTP.AllowCookies := False;
      FIdHTTP.HandleRedirects := False;
      FIdHTTP.OnChunkReceived := Self.OnChunkReceived;

      FIdHTTP.ConnectTimeout := 0; // 20 sec.
      FIdHTTP.ReadTimeout := 30000;

      ACtx.BODY.Clear;

      FStat.TransferBegin;
      try
        case ACtx.ReqInfo.CommandType of
          hcGET    : FIdHTTP.Get(ACtx.URL, ACtx.BODY);
          hcPOST   : FIdHTTP.Post(ACtx.URL, ACtx.ReqInfo.PostStream, ACtx.BODY);
          hcHEAD   : FIdHTTP.Head(ACtx.URL);
          hcPUT    : FIdHTTP.Put(ACtx.URL, ACtx.ReqInfo.PostStream, ACtx.BODY);
          hcOPTION : FIdHttp.Options(ACtx.URL, ACtx.BODY);
          hcDELETE : FIdHttp.Delete(ACtx.URL, ACtx.BODY);
          hcTRACE  : FIdHttp.Trace(ACtx.URL, ACtx.BODY);
          hcPATCH  : FIdHttp.Patch(ACtx.URL, ACtx.ReqInfo.PostStream, ACtx.BODY);
        else
          raise Exception.CreateFmt(
            '[%s] Unsupported command: %s', [Self.ClassName, ACtx.ReqInfo.Command]
          );
        end;
      finally
        FStat.TransferEnd;
      end;

      ACtx.RespNo := FIdHTTP.ResponseCode;
      ACtx.HEAD := FIdHTTP.Response.RawHeaders.Text;

      ACtx.Counters.UpHead := Length(VRawHeaders) + 1;
      if Assigned(ACtx.ReqInfo.PostStream) then begin
        Inc(ACtx.Counters.UpBody, ACtx.ReqInfo.PostStream.Size);
      end;

      ACtx.Counters.DownHead := Length(ACtx.HEAD) + 1;
      ACtx.Counters.DownBody := ACtx.Body.Size;
      
      if FIsChunked then begin
        ClearChunked(ACtx.HEAD, ACtx.RegEx);
      end;
      
      Result := True;
    except
      on E: EIdSocketError do begin
        if E.LastError = WSAETIMEDOUT then begin
          // connection timed out
          if VRetryCount >= cMaxRetryCount then begin
            Result := True;
            ACtx.RespNo := 504; // Gateway Timeout
            ACtx.HEAD := '';
            ACtx.Body.Clear;
          end;
        end else begin
          raise;
        end;
      end;
    end;
  until Result;
end;

function THttpDownloaderByIndy.GetRawConnection: TIdTcpConnection;
begin
  if FIdHttp = nil then begin
    raise EHttpDownloaderByIndy.Create('Indy HTTP client not assigned!');
  end else if not FIdHttp.Connected then begin
    raise EHttpDownloaderByIndy.Create('Indy HTTP client not connected!');
  end else begin
    Result := FIdHttp as TIdTcpConnection;
    Assert(Result <> nil);
  end;
end;

end.
