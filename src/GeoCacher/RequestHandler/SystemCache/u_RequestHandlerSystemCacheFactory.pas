unit u_RequestHandlerSystemCacheFactory;

interface

uses
  SysUtils,
  t_RequestHandler,
  i_MimeTable,
  i_UrlTransformer,
  i_FileSystemStorage,
  i_RequestHandlerFactory,
  u_ObjectPool,
  u_HttpContext;

type
  TRequestHandlerSystemCacheFactory = class(TInterfacedObject, IRequestHandlerFactory)
  private
    FPool: TObjectPool;
    FMimeTable: IMimeTable;
    FStorage: IFileSystemStorage;
    FUrlTransformer: IUrlTransformer;
  private
    { IRequestHandlerFactory }
    function Build(const ACtx: THttpContext): TRequestHandlerArray;
    procedure ReturnToPool(var AHandler: TRequestHandlerArray);
  public
    constructor Create;
    destructor Destroy; override;
  end;

  ERequestHandlerSystemCacheFactory = class(Exception);

implementation

uses
  u_MimeTable,
  u_UrlTransformer,
  u_FileSystemStorage,
  u_RequestHandlerSystemCache;

{ TRequestHandlerSystemCacheFactory }

constructor TRequestHandlerSystemCacheFactory.Create;
begin
  inherited Create;
  FPool := TObjectPool.Create;
  FStorage := TFileSystemStorage.Create;
  FMimeTable := TMimeTable.Create;
  FUrlTransformer := TUrlTransformer.Create;
end;

destructor TRequestHandlerSystemCacheFactory.Destroy;
begin
  FreeAndNil(FPool);
  inherited Destroy;
end;

function TRequestHandlerSystemCacheFactory.Build(const ACtx: THttpContext): TRequestHandlerArray;
begin
  SetLength(Result, 1);
  Result[0] := FPool.Pull as TRequestHandlerSystemCache;
  if Result[0] = nil then begin
    Result[0] :=
      TRequestHandlerSystemCache.Create(
        FStorage,
        FUrlTransformer,
        FMimeTable
      );
  end;
end;

procedure TRequestHandlerSystemCacheFactory.ReturnToPool(var AHandler: TRequestHandlerArray);
var
  I: Integer;
begin
  for I := 0 to Length(AHandler) - 1 do begin
    if AHandler[I] is TRequestHandlerSystemCache then begin
      FPool.Push(AHandler[I]);
      AHandler[I] := nil;
    end else begin
      raise ERequestHandlerSystemCacheFactory.Create(
        'Unknown Handler class: ' + AHandler[I].ClassName
      );
    end;
  end;
  AHandler := nil;
end;

end.
