unit u_RequestHandlerSystemCache;

interface

uses
  RegExpr,
  t_AppConfig,
  t_RequestHandler,
  t_FileInfo,
  i_MimeTable,
  i_UrlTransformer,
  i_FileSystemStorage,
  u_UrlToFileNameConverter,
  u_UrlTransformer,
  u_HttpContext;

type
  TRequestHandlerSystemCache = class(TRequestHandler)
  private
    FStorage: IFileSystemStorage;
    FFileInfo: TFileInfoRec;

    FMimeTable: IMimeTable;
    FUrlConverter: TUrlToFileNameConverter;
    FUrlTransformer: IUrlTransformer;

    FIsFoundInCache: Boolean;
    FIsDontUpdate: Boolean;

    procedure MakeAnswerFromCache(const ACtx: THttpContext);
    procedure MakeAnswerNoContent(const ACtx: THttpContext);
  private
    procedure DoOnUrlToFileNameConvert(const ACtx: THttpContext);
    procedure DoOnBeforeRequestSend(const ACtx: THttpContext);
    procedure DoOnBeforeAnswerSend(const ACtx: THttpContext);
  public
    constructor Create(
      const AStorage: IFileSystemStorage;
      const AUrlTransformer: IUrlTransformer;
      const AMimeTable: IMimeTable
    );
    destructor Destroy; override;
  end;

implementation

uses
  SysUtils,
  flcStrings,
  t_CustomHTTPServer,
  u_GlobalLog,
  u_HttpHeadersTools;

const
  cEOL = #13#10;

{ TRequestHandlerSystemCache }

constructor TRequestHandlerSystemCache.Create(
  const AStorage: IFileSystemStorage;
  const AUrlTransformer: IUrlTransformer;
  const AMimeTable: IMimeTable
);
begin
  Assert(AMimeTable <> nil);
  Assert(AUrlTransformer <> nil);
  Assert(AStorage <> nil);

  inherited Create;

  FStorage := AStorage;
  FUrlTransformer := AUrlTransformer;
  FMimeTable := AMimeTable;

  //FIsDontUpdate := True;

  FUrlConverter := TUrlToFileNameConverter.Create;

  OnUrlToFileNameConvert := Self.DoOnUrlToFileNameConvert;
  OnBeforeRequestSend := Self.DoOnBeforeRequestSend;
end;

destructor TRequestHandlerSystemCache.Destroy;
begin
  FreeAndNil(FUrlConverter);
  inherited;
end;

procedure TRequestHandlerSystemCache.DoOnUrlToFileNameConvert(const ACtx: THttpContext);

  function _IsCacheAllowed: Boolean;
  var
    VUserAgent: string;
  begin
    // ToDo: let user decide it
    VUserAgent := LowerCase(ACtx.ReqInfo.UserAgent);
    Result := Pos('googleearth', VUserAgent) > 0;
  end;

var
  VUrl: string;
  VFileName: string;
begin
  ACtx.FileName := '';
  
  if True {_IsCacheAllowed} then begin
    // transform url
    VUrl := FUrlTransformer.DoTransform(ACtx.Url, ACtx.RegEx);
    // convert url to file name
    VFileName := string(FUrlConverter.DoConvert(AnsiString(VUrl)));

    if VFileName <> '' then begin
      ACtx.FileName := ACtx.WebCacheConfig.PathFull + VFileName;
    end;
  end;
end;

procedure TRequestHandlerSystemCache.DoOnBeforeRequestSend(const ACtx: THttpContext);

  function _SetETag: Boolean;
  var
    I: Integer;
    VStr, VStrLower: string;
    VFileMetaData: TFileMetaData;
  begin
    Result := False;
    FStorage.ReadMetadata(@FFileInfo, VFileMetaData);
    ACtx.FileName := FFileInfo.FileNameS;
    for I := 0 to Length(VFileMetaData) - 1 do begin
      VStr := string(VFileMetaData[I]);
      VStrLower := LowerCase(VStr);
      if Pos('etag', VStrLower) > 0 then begin
        Result := SetETag(ACtx.Head, VStr, ACtx.RegEx);
        Break;
      end;
    end;
  end;

begin
  OnBeforeAnswerSend := nil;
  FIsFoundInCache := False;

  if ACtx.ReqInfo.CommandType <> hcGET then begin
    Exit;
  end;

  if ACtx.AllowCacheRead and ACtx.WebCacheConfig.AllowRead and (ACtx.FileName <> '') then begin
    FStorage.GetInfo(ACtx.FileName, FFileInfo);
    ACtx.FileName := FFileInfo.FileNameS;
    FIsFoundInCache := FFileInfo.IsExists;
  end;

  if FIsFoundInCache then begin
    if FIsDontUpdate then begin
      MakeAnswerFromCache(ACtx);
      Exit;
    end else begin
      if not _SetETag then begin
        SetLastModified(ACtx.Head, FFileInfo.LastModified, ACtx.RegEx);
      end;
    end;
  end;

  if ACtx.AllowInternetRequest then begin
    OnBeforeAnswerSend := Self.DoOnBeforeAnswerSend;
  end else begin
    if FIsFoundInCache then begin
      MakeAnswerFromCache(ACtx);
    end else begin
      MakeAnswerNoContent(ACtx);
    end;
  end;
end;

procedure TRequestHandlerSystemCache.MakeAnswerNoContent(const ACtx: THttpContext);
begin
  ACtx.Body.Clear;

  ACtx.RespNo := 204;
  ACtx.Head :=
    'Server: GeoCacher' + cEOL +
    'Content-Length: 0' + cEOL +
    'Connection: Keep-Alive' + cEOL;
end;

procedure TRequestHandlerSystemCache.DoOnBeforeAnswerSend(const ACtx: THttpContext);

  procedure _AddMetaValue(const AValue: string; var AMetaData: TFileMetaData);
  var
    I: Integer;
  begin
    if AValue <> '' then begin
      I := Length(AMetaData);
      SetLength(AMetaData, I + 1);
      AMetaData[I] := AnsiString(AValue);
    end;
  end;

var
  VHead: string;
  VFileMetaData: TFileMetaData;
begin
  if ACtx.FileName = '' then begin
    Exit;
  end;

  case ACtx.RespNo of

    200: begin
      if
        ACtx.AllowCacheWrite and
        ACtx.WebCacheConfig.AllowWrite and
        (FFileInfo.FileNameS <> '')
      then begin
        VFileMetaData := nil;
        VHead := LowerCase(ACtx.Head);

        if Pos('content-type', VHead) > 0 then begin
          _AddMetaValue(
            GetHeaderValue(ACtx.Head, ACtx.RegEx, 'Content-Type'),
            VFileMetaData
          );
        end;

        if Pos('content-encoding', VHead) > 0 then begin
          _AddMetaValue(
            GetHeaderValue(ACtx.Head, ACtx.RegEx, 'Content-Encoding'),
            VFileMetaData
          );
        end;

        if Pos('etag', VHead) > 0 then begin
          _AddMetaValue(
            GetHeaderValue(ACtx.Head, ACtx.RegEx, 'Etag'),
            VFileMetaData
          );
        end;

        if Pos('access-control-allow-origin', VHead) > 0 then begin
          _AddMetaValue(
            GetHeaderValue(ACtx.Head, ACtx.RegEx, 'Access-Control-Allow-Origin'),
            VFileMetaData
          );
        end;

        if Pos('access-control-allow-credentials', VHead) > 0 then begin
          _AddMetaValue(
            GetHeaderValue(ACtx.Head, ACtx.RegEx, 'Access-Control-Allow-Credentials'),
            VFileMetaData
          );
        end;

        try
          FStorage.Write(
            ACtx.Body.Memory,
            ACtx.Body.Size,
            @FFileInfo,
            VFileMetaData
          );
          Inc(ACtx.Counters.CacheWrite, ACtx.Body.Size);
          ACtx.FileName := FFileInfo.FileNameS;
          ACtx.FileNameInfo := FFileInfo.FileNameS; // for gui
        except
          on E: Exception do begin
            GLog.Error(
              Format('Url: %s' + cEOL + 'FileName: %s', [ACtx.Url, FFileInfo.FileNameS]),
              E
            );
          end;
        end;
      end;
    end;

    304: begin
      if FIsFoundInCache then begin
        MakeAnswerFromCache(ACtx);
      end;
    end;
  end;
end;

procedure TRequestHandlerSystemCache.MakeAnswerFromCache(const ACtx: THttpContext);
var
  I: Integer;
  VValue: string;
  VValueLower: string;
  VRawValues: string;
  VContentType: string;
  VContentEncoding: string;
  VAllowOrigin, VAllowCredentials: string;
  VFileMetaData: TFileMetaData;
begin
  FStorage.Read(ACtx.Body, @FFileInfo, VFileMetaData);

  ACtx.FileName := FFileInfo.FileNameS;
  ACtx.FileNameInfo := FFileInfo.FileNameS; // for gui
  Inc(ACtx.Counters.CacheRead, ACtx.Body.Size);

  VRawValues := '';
  VContentType := '';
  VContentEncoding := '';
  VAllowOrigin := '';
  VAllowCredentials := '';

  for I := 0 to Length(VFileMetaData) - 1 do begin
    VValue := string(VFileMetaData[I]);
    VValueLower := LowerCase(VValue);
    if Pos('content-type', VValueLower) = 1 then begin
      VContentType := VValue + cEOL;
    end else
    if Pos('content-encoding', VValueLower) = 1 then begin
      VContentEncoding := VValue + cEOL;
    end else
    if Pos('access-control-allow-origin', VValueLower) = 1 then begin
      VAllowOrigin := VValue + cEOL;
    end else
    if Pos('access-control-allow-credentials', VValueLower) = 1 then begin
      VAllowCredentials := VValue + cEOL;
    end else
    begin
      VRawValues := VRawValues + VValue + cEOL;
    end;
  end;

  if VContentType = '' then begin
    VContentType := FMimeTable.GetFileMimeType(FFileInfo.FileNameS);
    if VContentType <> '' then begin
      VContentType := 'Content-Type: ' + VContentType + cEOL;
    end;
  end;

  if ACtx.RespNo = 304 then begin
    ACtx.RespNo := 200;
    ACtx.Head :=
      ACtx.Head +
      VContentType +
      VContentEncoding +
      VAllowOrigin +
      VAllowCredentials;
  end else begin
    ACtx.RespNo := 200;
    ACtx.Head :=
      'Server: GeoCacher' + cEOL +
      VRawValues +
      VContentType +
      VContentEncoding +
      VAllowOrigin +
      VAllowCredentials +
      'Connection: Keep-Alive' + cEOL;
  end;
end;

end.
