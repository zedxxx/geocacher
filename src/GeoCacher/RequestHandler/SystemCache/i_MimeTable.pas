unit i_MimeTable;

interface

type
  IMimeTable = interface
  ['{4A794877-17FC-478A-8CD4-FBDA54FB4F0C}']
    function GetFileMimeType(const AFileName: string): string;
  end;

implementation

end.
