unit u_UrlTransformer;

interface

uses
  Classes,
  RegExpr,
  i_UrlTransformer;

type
  TUrlTransformer = class(TInterfacedObject, IUrlTransformer)
  private
    FList: TList;
    procedure LoadList;
    procedure ClearList;
  private
    { IUrlTransformer }
    function DoTransform(
      const AUrl: string;
      const ARegExpr: TRegExpr
    ): string;
  public
    constructor Create;
    destructor Destroy; override;
  end;

implementation

type
  TUrlTransformerRec = record
    Enabled: Boolean;
    Expression: string;
    Replacement: string;
  end;
  PUrlTransformerRec = ^TUrlTransformerRec;

{ TUrlTransformer }

constructor TUrlTransformer.Create;
begin
  inherited Create;
  FList := TList.Create;
  LoadList;
end;

destructor TUrlTransformer.Destroy;
begin
  ClearList;
  FList.Free;
  inherited;
end;

procedure TUrlTransformer.ClearList;
var
  I: Integer;
  VItem: PUrlTransformerRec;
begin
  for I := 0 to FList.Count - 1 do begin
    VItem := PUrlTransformerRec(FList.Items[I]);
    if VItem <> nil then begin
      Dispose(VItem);
    end;
  end;
  FList.Clear;
end;

procedure TUrlTransformer.LoadList;

  procedure _AddNew(const AExpr, ARepl: string);
  var
    VItem: PUrlTransformerRec;
  begin
    New(VItem);
    VItem.Enabled := True;
    VItem.Expression := AExpr;
    VItem.Replacement := ARepl;
    FList.Add(VItem);
  end;

begin
  // ToDo: Load this list from disk

  // Google
  _AddNew(
    '^(.*?)mw(\d+)\.google\.com(.*)$',
    '$1' + 'mw\.google\.com' + '$3'
  );
  _AddNew(
    '^(.*?)lh(\d+)\.googleusercontent\.com(.*)$',
    '$1' + 'lh\.googleusercontent\.com' + '$3'
  );
  _AddNew(
    '^(.*?)lh(\d+)\.ggpht\.com(.*)$',
    '$1' + 'lh\.ggpht\.com' + '$3'
  );
  _AddNew(
    '^(.*?)cdn(\d+)\.360cities\.net(.*)$',
    '$1' + 'cdn\.360cities\.net' + '$3'
  );
  _AddNew(
    '^(.*?)clients(\d+)\.google\.com(.*)$',
    '$1' + 'clients\.google\.com' + '$3'
  );

  // Yandex
  _AddNew(
    '^(.*?)sat(\d+)\.maps\.yandex\.net(.*)$',
    '$1' + 'sat\.maps\.yandex\.net' + '$3'
  );
  _AddNew(
    '^(.*?)vec(\d+)\.maps\.yandex\.net(.*)$',
    '$1' + 'vec\.maps\.yandex\.net' + '$3'
  );
  _AddNew(
    '^(.*?)(\d\d)\.srdr\.maps\.yandex\.net(.*)$',
    '$1' + 'srdr\.maps\.yandex\.net' + '$3'
  );
end;

function TUrlTransformer.DoTransform(
  const AUrl: string;
  const ARegExpr: TRegExpr
): string;
var
  I: Integer;
  VItem: PUrlTransformerRec;
begin
  Result := AUrl;
  for I := 0 to FList.Count - 1 do begin
    VItem := PUrlTransformerRec(FList.Items[I]);
    if (VItem <> nil) and (VItem.Enabled) then begin
      ARegExpr.Expression := VItem.Expression;
      if ARegExpr.Exec(Result) then begin
        Result := ARegExpr.Replace(Result, VItem.Replacement, True);
        Break;
      end;
    end;
  end;
end;

end.
