unit u_UrlToFileNameConverter;

interface

uses
  Windows,
  flcStrings,
  flcStringBuilder;

type
  TStringBuilderEx = class(TAnsiStringBuilder)
  public
    procedure Reset;
  end;

  TUrlToFileNameConverter = class(TObject)
  private
    FBuilder: TStringBuilderEx;

    FIsRemoveWWW: Boolean;
    FIsRemovePort80: Boolean;
    FIsRemoveHttps: Boolean;

    procedure URLPrepare(
      const AUrl: AnsiString;
      out APath: AnsiString
    );

    class procedure LimPathLen(
      var APath: AnsiString;
      const ALim: Integer
    );
  public
    function DoConvert(const AUrl: AnsiString): AnsiString;
  public
    constructor Create;
    destructor Destroy; override;
  end;

implementation

uses
  SysUtils,
  libcrc32,
  u_FileSystem;

{ TUrlToFileNameConverter }

constructor TUrlToFileNameConverter.Create;
begin
  inherited Create;
  FBuilder := TStringBuilderEx.Create(256);

  FIsRemoveWWW := True;
  FIsRemovePort80 := True;
  FIsRemoveHttps := True;
end;

destructor TUrlToFileNameConverter.Destroy;
begin
  FreeAndNil(FBuilder);
  inherited;
end;

function TUrlToFileNameConverter.DoConvert(const AUrl: AnsiString): AnsiString;
const
  cMaxLen = 200;
begin
  Assert(AUrl <> '');

  URLPrepare(AUrl, Result);

  if Length(Result) > cMaxLen then begin // Mimic HC (do not change this condition!)
    LimPathLen(Result, cMaxLen);
  end;
end;

function _StrMatch(A, B: PAnsiChar): Boolean; inline;
begin
  while (B^ <> #0) and (A^ <> #0) do begin
    if A^ = B^ then begin
      Inc(A);
      Inc(B);
    end else begin
      Result := False;
      Exit;
    end;
  end;
  Result := True;
end;

procedure TUrlToFileNameConverter.URLPrepare(
  const AUrl: AnsiString;
  out APath: AnsiString
);
const
  cDoubleSlash: array [Boolean] of AnsiString = ('\~', '#%~');
  cSingleSlash: array [Boolean] of AnsiString = ('\', '#%');
var
  VPtr: PAnsiChar;
  VIsParamsPart: Boolean;
begin
  FBuilder.Reset;

  VPtr := PAnsiChar(AUrl);
  VIsParamsPart := False;

  if _StrMatch(VPtr, 'https://') then begin
    Inc(VPtr, 8);
    if not FIsRemoveHttps then begin
      FBuilder.Append('https!\');
    end;
  end else if _StrMatch(VPtr, 'http://') then begin
    Inc(VPtr, 7);
  end;

  if _StrMatch(VPtr, 'www.') then begin
    Inc(VPtr, 4);
    if not FIsRemoveWWW then begin
      FBuilder.Append('www.');
    end;
  end;

  while VPtr^ <> #0 do begin
    case VPtr^ of
      '/': begin
        if (VPtr + 1)^ = '/' then begin
          FBuilder.Append(cDoubleSlash[VIsParamsPart]);
          Inc(VPtr, 2);
          Continue;
        end else begin
          FBuilder.Append(cSingleSlash[VIsParamsPart]);
        end;
      end;

      '*': FBuilder.Append('#x');
      '\': FBuilder.Append('#~');
      '|': FBuilder.Append('#i');
      '!': FBuilder.Append('#I');
      '"': FBuilder.Append('%22');
      '<': FBuilder.Append('%3C');
      '>': FBuilder.Append('%3E');

      ':': begin
        if _StrMatch(VPtr, ':80/') then begin
          Inc(VPtr, 3);
          if not FIsRemovePort80 then begin
            FBuilder.Append('!80');
          end;
          Continue;
        end else begin
          FBuilder.Append('!');
        end;
      end;

      '?': begin
        if not VIsParamsPart then begin
          VIsParamsPart := True;
          if (VPtr + 1)^ = #0 then begin
            FBuilder.Append('^\#_');
          end else begin
            FBuilder.Append('^\');
          end;
        end else begin
          FBuilder.Append('#^');
        end;
      end;

      '#': begin
        Break;
      end
    else
      FBuilder.AppendCh(VPtr^);
    end;
    Inc(VPtr);
  end;

  FBuilder.Pack;
  APath := FBuilder.AsAnsiString;
end;

class procedure TUrlToFileNameConverter.LimPathLen(
  var APath: AnsiString;
  const ALim: Integer
);
const
  cCrcDigitsCount = 8;
var
  VPtr, VStart, VEnd: PAnsiChar;
begin
  VStart := PAnsiChar(APath);
  VEnd := VStart + Length(APath);
  VPtr := VEnd - 1;

  if VEnd - VStart > ALim then begin
    while VPtr > VStart do begin
      if (VPtr^ = '\') and (VPtr + 1 - VStart <= ALim - cCrcDigitsCount) then begin
        Inc(VPtr);
        Break;
      end else begin
        Dec(VPtr);
      end;
    end;

    if VPtr > VStart then begin
      SetLength(APath, VPtr - VStart);
      APath := APath + AnsiString(IntToHex(crc32(0, VPtr, VEnd - VPtr), cCrcDigitsCount));
    end else begin
      // name is still too long
    end;
  end;
end;

{ TStringBuilderEx }

procedure TStringBuilderEx.Reset;
begin
  FLength := 0;
end;

end.
