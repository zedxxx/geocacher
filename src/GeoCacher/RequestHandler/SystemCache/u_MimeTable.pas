unit u_MimeTable;

interface

uses
  flcDataStructs,
  i_MimeTable;

type
  TMimeTable = class(TInterfacedObject, IMimeTable)
  private
    FExtMaxLen: Integer;
    FTable: TStringDictionary;
    procedure _UpdateMaxLen(const AExt: string); inline;
    function MakeTable: TStringDictionary;
  private
    { IMimeTable }
    function GetFileMimeType(const AFileName: string): string;
  public
    constructor Create;
    destructor Destroy; override;
  end;

implementation

uses
  Classes,
  SysUtils,
  IdGlobalProtocols;

const
  cAdditionalMimeTypes: array [0..1] of record Ext, Mime: string end = (
    (Ext: '.kml'; Mime: 'application/vnd.google-earth.kml+xml'),
    (Ext: '.kmz'; Mime: 'application/vnd.google-earth.kmz')
  );

{ TMimeTable }

constructor TMimeTable.Create;
begin
  inherited Create;
  FExtMaxLen := 0;
  FTable := MakeTable;
end;

destructor TMimeTable.Destroy;
begin
  FreeAndNil(FTable);
  inherited Destroy;
end;

procedure TMimeTable._UpdateMaxLen(const AExt: string);
var
  VExtLen: Integer;
begin
  Assert(AExt <> '');
  VExtLen := Length(AExt);
  if FExtMaxLen < VExtLen then begin
    FExtMaxLen := VExtLen;
  end;
end;

function TMimeTable.MakeTable: TStringDictionary;
var
  I: Integer;
  VList: TStringList;
  VIdMimeTable: TIdMimeTable;
  VKey, VVal: TStringArray;
begin
  VIdMimeTable := nil;
  VList := nil;
  VKey := nil;
  VVal := nil;
  try
    VIdMimeTable := TIdMimeTable.Create(False);
    VIdMimeTable.LoadTypesFromOS := False;
    VIdMimeTable.BuildCache;

    VList := TStringList.Create;
    VIdMimeTable.SaveToStrings(VList);

    VKey := TStringArray.Create;
    VVal := TStringArray.Create;

    for I := 0 to VList.Count - 1 do begin
      VKey.AppendItem(VList.Names[I]);
      VVal.AppendItem(VList.ValueFromIndex[I]);
      _UpdateMaxLen(VList.Names[I]);
    end;

    Result := TStringDictionary.CreateEx(VKey, VVal, True, True, ddIgnore);

    VKey := nil;
    VVal := nil;

    for I := 0 to Length(cAdditionalMimeTypes) - 1 do begin
      Result.Add(cAdditionalMimeTypes[I].Ext, cAdditionalMimeTypes[I].Mime);
      _UpdateMaxLen(cAdditionalMimeTypes[I].Ext);
    end;
    
  finally
    VIdMimeTable.Free;
    VList.Free;
    VKey.Free;
    VVal.Free;
  end;
end;

function TMimeTable.GetFileMimeType(const AFileName: string): string;
var
  I: Integer;
  VExt: string;
  VFound: Boolean;
begin
  VFound := False;

  VExt := LowerCase(ExtractFileExt(AFileName));

  I := Length(VExt);
  if (I > 0) and (I <= FExtMaxLen) then begin
    VFound := FTable.LocateItem(VExt, Result) >= 0;
  end;

  if not VFound then begin
    Result := '';
  end;
end;

end.
