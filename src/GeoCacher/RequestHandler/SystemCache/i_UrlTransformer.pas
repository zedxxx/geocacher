unit i_UrlTransformer;

interface

uses
  RegExpr;

type
  IUrlTransformer = interface
  ['{6B252D40-42A2-409F-89A3-1B2E1E56599D}']
    function DoTransform(
      const AUrl: string;
      const ARegExpr: TRegExpr
    ): string;
  end;

implementation

end.
