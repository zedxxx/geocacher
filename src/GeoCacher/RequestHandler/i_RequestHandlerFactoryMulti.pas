unit i_RequestHandlerFactoryMulti;

interface

uses
  u_RequestHandlerMulti,
  u_HttpContext;

type
  IRequestHandlerFactoryMulti = interface
    ['{7D43B1E5-C9DD-4BD0-9662-75DAAFE806FB}']
    function Build(const ACtx: THttpContext): TRequestHandlerMulti;
    procedure ReturnToPool(var AHandler: TRequestHandlerMulti);
  end;

implementation

end.
