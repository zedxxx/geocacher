unit t_RequestHandler;

interface

uses
  u_ObjectPool,
  u_HttpContext;

type
  TRequestHandlerEventType = (
    rhetOnUrlToFileNameConvert,
    rhetOnBeforeRequestSend,
    rhetOnBeforeAnswerSend
  );

  TRequestHandlerEventProc = procedure(const ACtx: THttpContext) of object;

  TRequestHandlerEvents = array[TRequestHandlerEventType] of TRequestHandlerEventProc;

  TRequestHandler = class abstract (TPooledObject)
  protected
    FEvents: TRequestHandlerEvents;

    function GetEventProc(
      const AEventType: TRequestHandlerEventType
    ): TRequestHandlerEventProc; inline;

    procedure SetEventProc(
      const AEventType: TRequestHandlerEventType;
      const AProc: TRequestHandlerEventProc
    ); inline;
  protected
    property OnUrlToFileNameConvert: TRequestHandlerEventProc
      index rhetOnUrlToFileNameConvert read GetEventProc write SetEventProc;

    property OnBeforeRequestSend: TRequestHandlerEventProc
      index rhetOnBeforeRequestSend read GetEventProc write SetEventProc;

    property OnBeforeAnswerSend: TRequestHandlerEventProc
      index rhetOnBeforeAnswerSend read GetEventProc write SetEventProc;
  public
    constructor Create;
    property Events: TRequestHandlerEvents read FEvents;
  end;

  TRequestHandlerArray = array of TRequestHandler;

implementation

{ TRequestHandler }

constructor TRequestHandler.Create;
var
  I: TRequestHandlerEventType;
begin
  inherited Create;
  for I := Low(TRequestHandlerEventType) to High(TRequestHandlerEventType) do begin
    FEvents[I] := nil;
  end;
end;

function TRequestHandler.GetEventProc(
  const AEventType: TRequestHandlerEventType
): TRequestHandlerEventProc;
begin
  Result := FEvents[AEventType];
end;

procedure TRequestHandler.SetEventProc(
  const AEventType: TRequestHandlerEventType;
  const AProc: TRequestHandlerEventProc
);
begin
  FEvents[AEventType] := AProc;
end;


{
  Init
      ��������� ��� �������� ���������� �� ����� ������ HandyCache � ���
      ������� ������ "���������� ����������" �� ������� ���������/����������.
      ��� �������� ���������� ����� ����� ���������� ����������� � ������.
      �������, ���� ���� ���������� ���������, ����� �������� ���������� �
      ������ � ������ ������ ���������� ����������.

  Options
      ��������� ��� ������� ������ ��������� ���������� �� ������� ���������/����������.

  Timer1s
      ��������� ��� � �������.

  Timer1m
      ��������� ��� � ������.

  URLToFileNameConverting
      ���������, ����� ��������� ��������� �������������� URL � ��� ����� � ����.

  BeforeViewInMonitor
      ��������� ����� ��������� ����� ������ � ��������.

  BeforeAuthorization
      ���������, ����� ������� ������ �� ������� � ����������� �������
      ������������ ������ ��� ���������.

  RequestHeaderReceived
      ���������, ����� ������� ��������� ������� �� �������.

  BeforeRequestHeaderSend
      ���������, ����� ��������� ������� ����� � �������� �� ������.

  BeforeRequestBodySend
      ���������, ����� �������� ��������� ������ ������ ��� �������� �� ������
      ������� POST.

  AnswerHeaderReceived
      ���������, ����� ������� ��������� ������ �� �������.

  BeforeAnswerHeaderSend
      ���������, ����� ��������� ������ ����� � �������� �������.
      ��������� ����� ���� ������� �� ������� ��� ����������� ����� HandyCache.

  BeforeAnswerBodySend
      ���������, ����� �������� ��������� ������ ������ ��� �������� �������.
      ������ ����� ���� �������� �� ������� ��� ����� �� ����. ���� ������
      ���������, �� ��� ����� ��������� ����������� ���������������.

  Destroy
      ��������� ��� ���������� ������ ���������� (��� �������� ���������� ��
      ������ ������� ����������, ��� ������������ ���������� �� ������
      ���������� ����������, ��� �������� HandyCache)
}

end.
