unit u_RequestHandlerFactoryMulti;

interface

uses
  t_RequestHandler,
  i_RequestHandlerFactory,
  i_RequestHandlerFactoryMulti,
  u_RequestHandlerMulti,
  u_ObjectPool,
  u_HttpContext;

type
  TRequestHandlerFactoryMulti = class(TInterfacedObject, IRequestHandlerFactoryMulti)
  private
    FCount: Integer;
    FFactory: array of IRequestHandlerFactory;
    FPool: TOBjectPool;
    procedure Add(const AFactory: IRequestHandlerFactory);
  public
    constructor Create;
    destructor Destroy; override;
  public
    function Build(const ACtx: THttpContext): TRequestHandlerMulti;
    procedure ReturnToPool(var AHandler: TRequestHandlerMulti);
  end;

implementation

uses
  SysUtils,
  u_RequestHandlerGeFactory,
  u_RequestHandlerSystemCacheFactory;

{ TRequestHandlerFactoryMulti }

constructor TRequestHandlerFactoryMulti.Create;
begin
  inherited Create;
  FCount := 0;
  FFactory := nil;

  // SystemCache handler
  Add(TRequestHandlerSystemCacheFactory.Create as IRequestHandlerFactory);

  // Embedded handlers
  Add(TRequestHandlerGeFactory.Create as IRequestHandlerFactory);

  // ToDo: External handlers

  // ToDo: SystemDecompress handler to decode gzip, br, deflate content
  // Add(TRequestHandlerSystemDecompressFactory.Create as IRequestHandlerFactory);

  FPool := TObjectPool.Create;
end;

destructor TRequestHandlerFactoryMulti.Destroy;
begin
  FreeAndNil(FPool);
  inherited Destroy;
end;

procedure TRequestHandlerFactoryMulti.Add(const AFactory: IRequestHandlerFactory);
begin
  SetLength(FFactory, FCount + 1);
  FFactory[FCount] := AFactory;
  Inc(FCount);
end;

function TRequestHandlerFactoryMulti.Build(const ACtx: THttpContext): TRequestHandlerMulti;
var
  I: Integer;
  VTmp: TRequestHandlerArray;
begin
  Result := FPool.Pull as TRequestHandlerMulti;

  if Result = nil then begin
    Result := TRequestHandlerMulti.Create;
  end;

  for I := 0 to FCount - 1 do begin
    VTmp := FFactory[I].Build(ACtx);
    if Length(VTmp) > 0 then begin
      Result.Push(I, VTmp);
    end;
  end;
end;

procedure TRequestHandlerFactoryMulti.ReturnToPool(var AHandler: TRequestHandlerMulti);
var
  I: Integer;
  VTmp: TRequestHandlerArray;
begin
  if not Assigned(AHandler) then begin
    Exit;
  end;
  
  while AHandler.Pop(I, VTmp) do begin
    if (I < FCount) and Assigned(FFactory[I]) then begin
      FFactory[I].ReturnToPool(VTmp);
    end else begin
      Assert(False);
    end;
  end;
  
  FPool.Push(AHandler);
  AHandler := nil;
end;

end.
