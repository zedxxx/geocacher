unit i_RequestHandlerFactory;

interface

uses
  t_RequestHandler,
  u_HttpContext;

type
  IRequestHandlerFactory = interface
    ['{6AC5D18B-2398-4290-B8C2-3634B71D19FF}']
    function Build(const ACtx: THttpContext): TRequestHandlerArray;
    procedure ReturnToPool(var AHandler: TRequestHandlerArray);
  end;

implementation

end.
