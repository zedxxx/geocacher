unit u_RequestHandlerMulti;

interface

uses
  t_RequestHandler,
  u_ObjectPool,
  u_HttpContext;

type
  TRequestHandlerMulti = class(TPooledObject)
  private
    type
      TItemRec = record
        FID: Integer;
        FHandler: TRequestHandlerArray;
      end;
  private
    FItems: array of TItemRec;
    FCount: Integer;

    function CallEventsASC(
      const AEventType: TRequestHandlerEventType;
      const ACtx: THttpContext
    ): Integer;

    function CallEventsDESC(
      const AEventType: TRequestHandlerEventType;
      const ACtx: THttpContext
    ): Integer;

    procedure Clear;
  public
    function OnUrlToFileNameConvert(const ACtx: THttpContext): Integer; inline;
    function OnBeforeRequestSend(const ACtx: THttpContext): Integer; inline;
    function OnBeforeAnswerSend(const ACtx: THttpContext): Integer; inline;

    procedure Push(const AFactoryID: Integer; const AHandler: TRequestHandlerArray);
    function Pop(out AFactoryID: Integer; out AHandler: TRequestHandlerArray): Boolean;

    constructor Create;
    destructor Destroy; override;
  end;

implementation

uses
  SysUtils;

{ TRequestHandlerMulti }

constructor TRequestHandlerMulti.Create;
begin
  inherited Create;
  FCount := 0;
  FItems := nil;
end;

destructor TRequestHandlerMulti.Destroy;
begin
  Clear;
  inherited Destroy;
end;

procedure TRequestHandlerMulti.Clear;
var
  I, J: Integer;
begin
  for I := 0 to FCount - 1 do begin
    for J := 0 to Length(FItems[I].FHandler) - 1 do begin
      FreeAndNil(FItems[I].FHandler[J]);
    end;
  end;
end;

function TRequestHandlerMulti.CallEventsASC(
  const AEventType: TRequestHandlerEventType;
  const ACtx: THttpContext
): Integer;
var
  I, J: Integer;
  VArr: TRequestHandlerArray;
  VEvent: TRequestHandlerEventProc;
begin
  Result := 0;
  for I := 0 to FCount - 1 do begin
    VArr := FItems[I].FHandler;
    for J := 0 to Length(VArr) - 1 do begin
      VEvent := VArr[J].Events[AEventType];
      if Assigned(VEvent) then begin
        VEvent(ACtx);
        Inc(Result);
      end;
    end;
  end;
end;

function TRequestHandlerMulti.CallEventsDESC(
  const AEventType: TRequestHandlerEventType;
  const ACtx: THttpContext
): Integer;
var
  I, J: Integer;
  VArr: TRequestHandlerArray;
  VEvent: TRequestHandlerEventProc;
begin
  Result := 0;
  for I := FCount - 1 downto 0 do begin
    VArr := FItems[I].FHandler;
    for J := Length(VArr) - 1 downto 0 do begin
      VEvent := VArr[J].Events[AEventType];
      if Assigned(VEvent) then begin
        VEvent(ACtx);
        Inc(Result);
      end;
    end;
  end;
end;

function TRequestHandlerMulti.OnUrlToFileNameConvert(const ACtx: THttpContext): Integer;
begin
  Result := CallEventsASC(rhetOnUrlToFileNameConvert, ACtx);
end;

function TRequestHandlerMulti.OnBeforeRequestSend(const ACtx: THttpContext): Integer;
begin
  Result := CallEventsASC(rhetOnBeforeRequestSend, ACtx);
end;

function TRequestHandlerMulti.OnBeforeAnswerSend(const ACtx: THttpContext): Integer;
begin
  Result := CallEventsDESC(rhetOnBeforeAnswerSend, ACtx);
end;

procedure TRequestHandlerMulti.Push(
  const AFactoryID: Integer;
  const AHandler: TRequestHandlerArray
);
begin
  if Length(FItems) <= FCount then begin
    SetLength(FItems, FCount + 1);
  end;
  FItems[FCount].FID := AFactoryID;
  FItems[FCount].FHandler := AHandler;
  Inc(FCount);
end;

function TRequestHandlerMulti.Pop(
  out AFactoryID: Integer;
  out AHandler: TRequestHandlerArray
): Boolean;
begin
  Result := FCount > 0;
  if Result then begin
    Dec(FCount);
    AFactoryID := FItems[FCount].FID;
    AHandler := FItems[FCount].FHandler;
    FItems[FCount].FHandler := nil;
  end;
end;

end.
