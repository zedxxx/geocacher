unit flatfile_regexpr;

interface

uses
  RegExpr;

function UrlToFileName(
  const AUrl:string;
  const ARegEx: TRegExpr
): string;

var
  GVerFold: array of string;

implementation

uses
  Types,
  Classes,
  SysUtils,
  StrUtils,
  IniFiles,
  u_FileSystemTools;

type
  TRegEx = record
    Exp    : array of string;
    Repl   : array of string;
    Active : array of Boolean;
    Count  : Integer;
  end;

var
  GUrlToPathRE: TRegEx;

function GeNameStringToXY(const AName: string): TPoint;
var
  I: Integer;
begin
  Result.X := 0;
  Result.Y := 0;

  for I := 2 to Length(AName) do begin
    Result.X := Result.X * 2;
    Result.Y := Result.Y * 2;

    case AName[I] of
      '0': begin
        Inc(Result.Y);
      end;

      '1': begin
        Inc(Result.X);
        Inc(Result.Y);
      end;

      '2': begin
        Inc(Result.X);
      end;

      '3': begin
        // do nothing
      end;
    else
      Assert(False, AName);
    end;
  end;
end;

function UrlToFileName(
  const AUrl:string;
  const ARegEx: TRegExpr
): string;
var
  I: Integer;
  VPoint: TPoint;
begin
  Result := AUrl;

  for I := 0 to GUrlToPathRE.count - 1 do begin
    if GUrlToPathRE.Active[I] then begin
      ARegEx.Expression := GUrlToPathRE.Exp[I];
      if ARegEx.Exec(Result) then begin
        Result := ARegEx.Replace(Result, GUrlToPathRE.Repl[I], True);
      end;
    end;
  end;

  if Result = '' then begin
    Result := AUrl;
  end;

  ARegEx.Expression := '-(\d+)-';
  if ARegEx.Exec(AUrl) then begin
    VPoint := GENameStringToXY(ARegEx.Match[1]);
    Result := StringReplace(Result, '<Z>',  IntToStr(Length(ARegEx.Match[1])), [rfReplaceAll,rfIgnoreCase]);
    Result := StringReplace(Result, '<X>',  IntToStr(VPoint.X),                [rfReplaceAll,rfIgnoreCase]);
    Result := StringReplace(Result, '<Y>',  IntToStr(VPoint.Y),                [rfReplaceAll,rfIgnoreCase]);
    Result := StringReplace(Result, '<Xi>', IntToStr(VPoint.X div 1024),       [rfReplaceAll,rfIgnoreCase]);
    Result := StringReplace(Result, '<Yi>', IntToStr(VPoint.Y div 1024),       [rfReplaceAll,rfIgnoreCase]);
  end;

  Result := StringReplace(Result, 'http://',  '', [rfIgnoreCase]);
  Result := StringReplace(Result, 'https://', '', [rfIgnoreCase]);
  Result := StringReplace(Result, '/', '\', [rfReplaceAll]);
  Result := StringReplace(Result, '?', '\', [rfReplaceAll]);
end;

procedure LoadExpressions;
var
  I, VCount: Integer;
  VIni: TMemIniFile;
  VIniFileName: string;
  VSectionName: string;
  VList: TStringList;
begin
  GVerFold := nil;
  GUrlToPathRE.Count := 0;

  VIniFileName := GetAppPath + 'options\TileCacheRE.ini';

  if not FileExists(VIniFileName) then begin
    Exit;
  end;

  VIni:=TMemIniFile.Create(VIniFileName);
  try
    VCount := VIni.ReadInteger('Count','RegExprMAX',100);
    SetLength(GUrlToPathRE.Exp,VCount);
    SetLength(GUrlToPathRE.Repl,VCount);
    SetLength(GUrlToPathRE.Active,VCount);

    for I := 0 to VCount - 1 do begin
      VSectionName := 'RegExpr#' + IntToStr(I+1);
      if VIni.SectionExists(VSectionName) then begin
        GUrlToPathRE.Exp[GUrlToPathRE.Count] := VIni.ReadString(VSectionName,'Expr','');
        GUrlToPathRE.Repl[GUrlToPathRE.Count] := VIni.ReadString(VSectionName,'Replace','');
        GUrlToPathRE.Active[GUrlToPathRE.Count] := VIni.ReadBool(VSectionName,'Active',False);
        Inc(GUrlToPathRE.Count);
      end;
    end;

    VList := TStringList.Create;
    try
      VIni.ReadSectionValues('folders', VList);
      VCount := VList.Count;
      SetLength(GVerFold, VCount);
      for I := 0 to VCount - 1 do begin
        GVerFold[I] := MidStr(VList[I],Pos('=',VList[I])+1,255);
        if GVerFold[I] <> '' then begin
          GVerFold[I] := PathToFullPath(GVerFold[I]);
        end;
      end;
    finally
      VList.Free;
    end;
  finally
   VIni.Free;
  end;
end;

initialization
  LoadExpressions;

end.
