unit u_GeFlatFileRequestValidator;

interface

uses
  u_HttpContext;

type
  TGeFlatFileRequestValidator = class
  private
    FErrorMsg: string;
    procedure ShowErrorSync;
    procedure SwitchToOfflineMode(const ACtx: THttpContext);
  public
    procedure CheckUrl(const AUrl: string; const ACtx: THttpContext);
    procedure CheckCookie(const ACtx: THttpContext);
  end;

implementation

uses
  Classes,
  SysUtils,
  StrUtils,
  c_GeAuth,
  t_AppConfig,
  u_GlobalLog;

resourcestring
  rsRequestBadVersionDetected = 'Online mode disabled: Detected flatfile request with bad tile version!';
  rsRequestBadCookieDetected = 'Online mode disabled: Detected flatfile request with bad cookie value!';

{ TGeFlatFileRequestValidator }

procedure TGeFlatFileRequestValidator.CheckUrl(const AUrl: string; const ACtx: THttpContext);

  function _IsFakeVersion(const AUrl: string): Boolean;
  const
    CFakeVersion = $2020;
  var
    I: Integer;
    VVersion: string;
  begin
    I := LastDelimiter('.', AUrl);
    if I > 0 then begin
      VVersion := Copy(AUrl, I+1);
    end;

    // get history image version
    I := Pos('-', VVersion);
    if I > 0 then begin
      VVersion := Copy(VVersion, 1, I-1);
    end;

    if TryStrToInt(VVersion, I) then begin
      Result := I = CFakeVersion;
    end else begin
      raise Exception.CreateFmt('Failed to detect tile version: %s', [AUrl]);
    end;
  end;

begin
  if _IsFakeVersion(AUrl) then begin
    SwitchToOfflineMode(ACtx);

    FErrorMsg := rsRequestBadVersionDetected;
    TThread.Synchronize(nil, ShowErrorSync);

    raise Exception.Create(FErrorMsg);
  end;
end;

procedure TGeFlatFileRequestValidator.CheckCookie(const ACtx: THttpContext);
const
  CSessionId = 'sessionid=';
var
  I, J: Integer;
  VCookie: string;
  VIsValid: Boolean;
begin
  if not ACtx.IsSessionIdRequired then begin
    Exit;
  end;

  VIsValid := False;

  VCookie := ACtx.ReqInfo.RawHeaders.Values['Cookie'];

  I := Pos(CSessionId, LowerCase(VCookie));
  if I > 0 then begin
    Inc(I, Length(CSessionId));

    J := PosEx(';', VCookie, I);
    if J > 0 then begin
      VCookie := Copy(VCookie, I, J-I);
    end else begin
      VCookie := Copy(VCookie, I);
    end;

    VIsValid := (VCookie <> '') and (VCookie <> CFakeSessionId);
  end;

  if not VIsValid then begin
    SwitchToOfflineMode(ACtx);

    FErrorMsg := rsRequestBadCookieDetected;
    TThread.Synchronize(nil, ShowErrorSync);

    raise Exception.Create(FErrorMsg);
  end;
end;

procedure TGeFlatFileRequestValidator.SwitchToOfflineMode(const ACtx: THttpContext);
begin
  ACtx.AppConfigRec.WorkMode := wmCacheOnly;
  ACtx.AppConfig.SetWorkMode(wmCacheOnly);
end;

procedure TGeFlatFileRequestValidator.ShowErrorSync;
begin
  GLog.ShowErrorMessage(FErrorMsg);
end;

end.
