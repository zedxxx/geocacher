unit u_RequestHandlerGeFlatFile;

interface

uses
  Classes,
  SysUtils,
  flcStdTypes,
  flcStrings,
  t_RequestHandler,
  u_GeFlatFilePacketValidator,
  u_GeFlatFileRequestValidator,
  u_HttpContext;

type
  TRequestHandlerGeFlatFile = class(TRequestHandler)
  private
    type
      TItemsRec = record
        Data: Pointer;
        Size: Integer;
        Name: string;
      end;
      TItemsRecArray = array of TItemsRec;
  private
    FItems: TItemsRecArray;
    FMissingItemsIdx: array of Integer;
    FUrlOrigin: string;
    FUrlBasePart: string;
    FIsSingleTileRequest: Boolean;
    FPacketValidator: TGePacketValidator;
    FRequestValidator: TGeFlatFileRequestValidator;
    class procedure UpdateFileNameInfo(var AInfo: string; const AFileName: string); inline;
    procedure LoadMissingItemsFromStream(const AStream: TMemoryStream);
    procedure WriteItemsToStream(const AStream: TMemoryStream);
    procedure MakeResponse(const ACtx: THttpContext);
    class function ParseFlatFile(const AStr: string; out ADB: string): StringArray;
    class function CanUsePlaceholder(const ACtx: THttpContext; const ATileUrl: string): Boolean;
    procedure ReadTileFromCache(const ACtx: THttpContext);
    procedure ReadItemsFromCache(const ACtx: THttpContext);
    procedure WriteMissingItemsToCache(const ACtx: THttpContext);

    function GetFileName(
      const AUrl: string;
      const ACtx: THttpContext
    ): string;
    function GetFileNameRead(
      const AUrl: string;
      const ACtx: THttpContext;
      out AFileName: string
    ): Boolean;
    function GetFileNameWrite(
      const AUrl: string;
      const ACtx: THttpContext
    ): string; inline;
  private
    procedure DoOnUrlToFileNameConvert(const ACtx: THttpContext);
    procedure DoOnBeforeRequestSend(const ACtx: THttpContext);
    procedure DoOnBeforeAnswerSend(const ACtx: THttpContext);
  public
    constructor Create;
    destructor Destroy; override;
  end;

  ERequestHandlerGeFlatFile = class(Exception);

implementation

uses
  t_AppConfig,
  u_GlobalLog,
  u_HttpRequestInfo,
  flatfile_regexpr,
  flatfile_tile_cache,
  flatfile_any_version;

const
  cFlatFileMagic: Byte = $01;

{ TRequestHandlerGeFlatFile }

constructor TRequestHandlerGeFlatFile.Create;
begin
  inherited Create;

  FPacketValidator := TGePacketValidator.Create;
  FRequestValidator := TGeFlatFileRequestValidator.Create;

  OnUrlToFileNameConvert := Self.DoOnUrlToFileNameConvert;
  OnBeforeRequestSend := Self.DoOnBeforeRequestSend;
end;

destructor TRequestHandlerGeFlatFile.Destroy;
begin
  FreeAndNil(FPacketValidator);
  inherited;
end;

procedure TRequestHandlerGeFlatFile.DoOnUrlToFileNameConvert(const ACtx: THttpContext);
begin
  ACtx.FileName := '';
end;

class function TRequestHandlerGeFlatFile.CanUsePlaceholder(
  const ACtx: THttpContext;
  const ATileUrl: string
): Boolean;
begin
  if ACtx.FlatFileConfig.JpegPlaceholderEnabled then begin
    ACtx.RegEx.Expression := 'f1-\d+-i\.';
    Result := ACtx.RegEx.Exec(ATileUrl);
  end else begin
    Result := False;
  end;
end;

function TRequestHandlerGeFlatFile.GetFileName(
  const AUrl: string;
  const ACtx: THttpContext
): string;
const
  CGoogleHost: array [Boolean] of string = ('kh.google.com/', 'khmdb.google.com/');
var
  VUrl: string;
  VIsDbRequest: Boolean;
  VEarthEnterprise: PGoogleEarthEnterpriseConfigRec;
begin
  VUrl := AUrl;

  VEarthEnterprise := @ACtx.AppConfigRec.EarthEnterprise;
  if VEarthEnterprise.Enabled then begin
    if VEarthEnterprise.MixFlatFileCache then begin
      VIsDbRequest := Pos('flatfile?db=', AUrl) > 0;
      VUrl := StringReplace(AUrl, VEarthEnterprise.Address, CGoogleHost[VIsDbRequest], []);
    end else begin
      VUrl := StringReplace(AUrl, '://', '://TilesEnt/', []);
    end;
  end;

  Result := ACtx.FlatFileConfig.PathFull + UrlToFileName(VUrl, ACtx.RegEx);
end;

function TRequestHandlerGeFlatFile.GetFileNameRead(
  const AUrl: string;
  const ACtx: THttpContext;
  out AFileName: string
): Boolean;
begin
  AFileName := GetFileName(AUrl, ACtx);

  if FileExists(AFileName) then begin
    Result := True;
    Exit;
  end;

  if ACtx.FlatFileConfig.AllowReadAnyVersion then begin
    AFileName := GetAnyVersion(
      AUrl,
      AFileName,
      ACtx.AppConfigRec.WorkMode = wmCacheOnly
    );
    Result := AFileName <> '';
  end else begin
    Result := False;
  end;
end;

function TRequestHandlerGeFlatFile.GetFileNameWrite(
  const AUrl: string;
  const ACtx: THttpContext
): string;
begin
  Result := GetFileName(AUrl, ACtx);
end;

class function TRequestHandlerGeFlatFile.ParseFlatFile(
  const AStr: string;
  out ADB: string
): StringArray;
const
  cSep: ByteCharSet = ['&', '+'];
var
  I: Integer;
  VCount: Integer;
  VFrom, VResultCount: Integer;
  VStrArr: StringArray;
begin
  ADB := '';
  VStrArr := StrSplitCharSet(AStr, cSep);
  VCount := Length(VStrArr);
  if VCount = 1 then begin
    Result := VStrArr;
  end else if VCount > 1 then begin
    VFrom := 0;
    VResultCount := VCount;
    I := Pos('db=', VStrArr[0]);
    if I > 0 then begin
      ADB := Copy(VStrArr[0], I + 3);
      Inc(VFrom);
      Dec(VResultCount);
    end;
    if Pos('v=', VStrArr[VCount - 1]) > 0 then begin
      Dec(VResultCount);
    end;
    if VResultCount > 0 then begin
      if VFrom = 0 then begin
        SetLength(VStrArr, VResultCount);
        Result := VStrArr;
      end else begin
        Result := Copy(VStrArr, VFrom, VResultCount);
      end;
    end else begin
      raise ERequestHandlerGeFlatFile.Create('VResultCount = 0');
    end;
  end else begin // VCount <= 0
    raise ERequestHandlerGeFlatFile.Create('VCount = 0');
  end;
end;

procedure TRequestHandlerGeFlatFile.ReadTileFromCache(const ACtx: THttpContext);

  procedure _MakeAnswer;
  begin
    ACtx.Url := '';
    ACtx.RespNo := 200;
    ACtx.Head :=
      'Server: GeoCacher (FromCache)' + #13#10 +
      'Content-Length: ' + IntToStr(ACtx.Body.Size) + #13#10 +
      'Content-Type: application/octet-stream' + #13#10 +
      'Connection: Keep-Alive' + #13#10;
  end;

  procedure _TryMakeAnswerWithPlaceholder;
  begin
    if CanUsePlaceholder(ACtx, ACtx.Url) then begin
      ACtx.Body.WriteBuffer(
        ACtx.FlatFileConfig.JpegPlaceholderData[0],
        Length(ACtx.FlatFileConfig.JpegPlaceholderData)
      );
      _MakeAnswer;
    end;
  end;

var
  VSize: Integer;
  VIsValidPacket: Boolean;
  VFileName: string;
begin
  if not GetFileNameRead(ACtx.Url, ACtx, VFileName) then begin
    _TryMakeAnswerWithPlaceholder;
    Exit; // not found
  end;

  VSize := TileCacheReadEx(VFileName, ACtx.Body);
  if VSize <= 0 then begin
    _TryMakeAnswerWithPlaceholder;
    Exit; // error readnig
  end;

  VIsValidPacket :=
    FPacketValidator.IsValidPacket(
      ACtx.Body.Memory,
      VSize,
      ACtx.Url,
      ACtx.RegEx,
      ACtx.FlatFileConfig
    );

  if not VIsValidPacket then begin
    ACtx.Body.Clear; // invalid content
    _TryMakeAnswerWithPlaceholder;
    Exit;
  end;

  _MakeAnswer;

  Inc(ACtx.Counters.CacheRead, VSize);
  UpdateFileNameInfo(ACtx.FileNameInfo, VFileName);
end;

procedure TRequestHandlerGeFlatFile.ReadItemsFromCache(const ACtx: THttpContext);
var
  I: Integer;
  VTailStr: string;
  VFileName: string;
  VRequestStr: string;
  VFound: Boolean;
  VMissingCount: Integer;
begin
  VMissingCount := 0;
  VRequestStr := '';

  for I := 0 to Length(FItems) - 1 do begin
    VFound := GetFileNameRead(FUrlBasePart + FItems[I].Name, ACtx, VFileName);

    if VFound then begin
      VFound :=
        TileCacheRead(
          VFileName,
          ACtx.Body,
          FItems[I].Data,
          FItems[I].Size
        );
    end;

    if VFound then begin
      VFound :=
        FPacketValidator.IsValidPacket(
          FItems[I].Data,
          FItems[I].Size,
          FItems[I].Name,
          ACtx.RegEx,
          ACtx.FlatFileConfig
        );
      if not VFound then begin
        FreeMem(FItems[I].Data);
        FItems[I].Data := nil;
        FItems[I].Size := 0;
      end;
    end;

    if VFound then begin
      Inc(ACtx.Counters.CacheRead, FItems[I].Size);
      UpdateFileNameInfo(ACtx.FileNameInfo, VFileName);
    end else
    if CanUsePlaceholder(ACtx, FItems[I].Name) then begin
      FItems[I].Size := Length(ACtx.FlatFileConfig.JpegPlaceholderData);
      GetMem(FItems[I].Data, FItems[I].Size);
      Move(ACtx.FlatFileConfig.JpegPlaceholderData[0], FItems[I].Data^, FItems[I].Size);
    end else begin
      FMissingItemsIdx[VMissingCount] := I;
      Inc(VMissingCount);

      if VRequestStr = '' then begin
        VRequestStr := FItems[I].Name;
      end else begin
        VRequestStr := VRequestStr + '+' + FItems[I].Name;
      end;
    end;
  end;

  SetLength(FMissingItemsIdx, VMissingCount);

  if VMissingCount > 1 then begin
    VTailStr := '&v=1'
  end else begin
    VTailStr := '';
  end;

  if VRequestStr <> '' then begin
    ACtx.Url := FUrlBasePart + VRequestStr + VTailStr;
  end else begin
    ACtx.Url := '';
  end;
end;

procedure TRequestHandlerGeFlatFile.DoOnBeforeRequestSend(const ACtx: THttpContext);
var
  I: Integer;
  VItems: StringArray;
  VDataBase: string;
  VInfo: THTTPRequestInfo;
  VAllowInternet: Boolean;
  VAllowCacheRead: Boolean;
begin
  VAllowInternet := ACtx.AllowInternetRequest;
  VAllowCacheRead := ACtx.AllowCacheRead and ACtx.FlatFileConfig.AllowRead;

  FUrlOrigin := ACtx.Url;

  if not VAllowInternet then begin
    ACtx.RespNo := 204;
    ACtx.Head := '';
  end;

  VInfo := ACtx.ReqInfo as THTTPRequestInfo;

  FIsSingleTileRequest := Pos('+', VInfo.QueryParams) = 0;

  if FIsSingleTileRequest then begin
    // https://khmdb.google.com/flatfile?db=tm&qp-0303-q.229
    if VAllowInternet then begin
      FRequestValidator.CheckUrl(FUrlOrigin, ACtx);
    end;
    if VAllowCacheRead then begin
      ReadTileFromCache(ACtx);
    end;
  end else begin
    // https://kh.google.com/flatfile?f1-002-i.280+f1-012-i.212+q2-0-q.281&v=1
    // https://khmdb.google.com/flatfile?db=sky&q2-0023-q.10+q2-0200-q.10+q2-0301-q.10+q2-0310-q.10+q2-0311-q.10&v=1

    VItems := ParseFlatFile(VInfo.QueryParams, VDataBase);
    if VDataBase <> '' then begin
      VDataBase := 'db=' + VDataBase + '&';
    end;
    I := Pos('?', ACtx.Url);
    Assert(I > 0);
    FUrlBasePart := Copy(ACtx.Url, 1, I) + VDataBase;

    SetLength(FItems, Length(VItems));
    SetLength(FMissingItemsIdx, Length(FItems));
    for I := 0 to Length(VItems) - 1 do begin
      FItems[I].Data := nil;
      FItems[I].Size := 0;
      FItems[I].Name := VItems[I];
      FMissingItemsIdx[I] := I;

      if VAllowInternet then begin
        FRequestValidator.CheckUrl(VItems[I], ACtx);
      end;
    end;

    if VAllowCacheRead then begin
      ReadItemsFromCache(ACtx);
    end;
  end;

  if VAllowInternet and (ACtx.Url <> '') then begin
    FRequestValidator.CheckCookie(ACtx);
    OnBeforeAnswerSend := DoOnBeforeAnswerSend;
  end else begin
    OnBeforeAnswerSend := nil;
    MakeResponse(ACtx);
  end;
end;

procedure TRequestHandlerGeFlatFile.DoOnBeforeAnswerSend(const ACtx: THttpContext);
var
  VFileName: string;
  VAllowCacheWrite: Boolean;
begin
  if ACtx.RespNo = 200 then begin
    ACtx.Body.Position := 0;
    VAllowCacheWrite := ACtx.AllowCacheWrite and ACtx.FlatFileConfig.AllowWrite;
    if FIsSingleTileRequest then begin
      if VAllowCacheWrite then begin
        VFileName := GetFileNameWrite(ACtx.Url, ACtx);
        TileCacheWriteEx(VFileName, ACtx.Body);
        Inc(ACtx.Counters.CacheWrite, ACtx.Body.Size);
        UpdateFileNameInfo(ACtx.FileNameInfo, VFileName);
      end;
    end else begin
      LoadMissingItemsFromStream(ACtx.Body);
      if VAllowCacheWrite then begin
        WriteMissingItemsToCache(ACtx);
      end;
    end;
  end;
  MakeResponse(ACtx);
end;

procedure TRequestHandlerGeFlatFile.MakeResponse(const ACtx: THttpContext);
begin
  if not FIsSingleTileRequest then begin
    WriteItemsToStream(ACtx.Body);
  end;
  if (ACtx.Body.Size > 0) and (ACtx.RespNo <> 200) then begin
    ACtx.RespNo := 200;
    ACtx.Head :=
      'Server: GeoCacher (FromCache)' + #13#10 +
      'Content-Type: application/octet-stream' + #13#10 +
      'Connection: Keep-Alive' + #13#10;
  end;
  ACtx.Url := FUrlOrigin;
end;

procedure TRequestHandlerGeFlatFile.WriteItemsToStream(const AStream: TMemoryStream);
var
  I: Integer;
  VCount: Integer;
  VTilesCount: Byte;
  VTileOffset: Integer;
begin
  AStream.Clear;
  VCount := Length(FItems);
  try
    if VCount > 1 then begin
      VTilesCount := VCount;
      VTileOffset := 2 + VCount * 4; // first tile offset = header size
      // write header
      AStream.WriteBuffer(cFlatFileMagic, 1);
      AStream.WriteBuffer(VTilesCount, 1);
      for I := 0 to VCount - 1 do begin
        AStream.WriteBuffer(VTileOffset, 4);
        Inc(VTileOffset, FItems[I].Size);
      end;
    end;
    // write tile(s)
    for I := 0 to VCount - 1 do begin
      if (FItems[I].Data <> nil) and (FItems[I].Size > 0) then begin
        AStream.WriteBuffer(FItems[I].Data^, FItems[I].Size);
      end;
    end;
    AStream.Position := 0;
  finally
    for I := 0 to VCount - 1 do begin
      if FItems[I].Data <> nil then begin
        FreeMem(FItems[I].Data);
      end;
    end;
  end;
end;

procedure TRequestHandlerGeFlatFile.LoadMissingItemsFromStream(
  const AStream: TMemoryStream
);
var
  I, J: Integer;
  VSize: Integer;
  VCount: Integer;
  VTilesCount: Integer;
  VTileOffset: Integer;
  VMemory, VTail: PByte;
begin
  VSize := AStream.Size;

  if VSize <= 0 then begin
    Exit;
  end;

  VCount := Length(FMissingItemsIdx);
  if VCount = 1 then begin
    I := FMissingItemsIdx[0];
    GetMem(FItems[I].Data, VSize);
    AStream.ReadBuffer(FItems[I].Data^, VSize);
    FItems[I].Size := VSize;
  end else begin
    VMemory := AStream.Memory;
    VTail := VMemory;
    Inc(VTail, VSize);
    if Byte(VMemory^) = cFlatFileMagic then begin
      Inc(VMemory);
      VTilesCount := Byte(VMemory^);
      if VTilesCount = VCount then begin
        Inc(VMemory, 1 + (VTilesCount - 1) * 4);
        for I := VTilesCount - 1 downto 0 do begin
          VTileOffset := PInteger(VMemory)^;
          Dec(VMemory, 4);
          J := FMissingItemsIdx[I];
          Assert(J <> -1);
          FItems[J].Size := VSize - VTileOffset;
          if FItems[J].Size > 0 then begin
            GetMem(FItems[J].Data, FItems[J].Size);
            Dec(VTail, FItems[J].Size);
            Move(VTail^, FItems[J].Data^, FItems[J].Size);
          end;
          Dec(VSize, FItems[J].Size);
        end;
      end else begin
        raise ERequestHandlerGeFlatFile.CreateFmt(
          'Count mismatch: %d <> %d', [VTilesCount, VCount]
        );
      end;
    end else begin
      raise ERequestHandlerGeFlatFile.CreateFmt(
        'Incorrect magic value: %d', [Byte(VMemory^)]
      );
    end;
  end;
end;

procedure TRequestHandlerGeFlatFile.WriteMissingItemsToCache(const ACtx: THttpContext);
var
  I, J: Integer;
  VData: array of Pointer;
  VSize: array of Integer;
  VUrl: array of string;
  VFileName: array of string;
begin
  I := Length(FMissingItemsIdx);

  SetLength(VData, I);
  SetLength(VSize, I);
  SetLength(VUrl, I);
  SetLength(VFileName, I);

  for I := 0 to Length(FMissingItemsIdx) - 1 do begin
    J := FMissingItemsIdx[I];

    VData[I] := FItems[J].Data;
    VSize[I] := FItems[J].Size;

    VUrl[I] := FUrlBasePart + FItems[J].Name;
    VFileName[I] := GetFileNameWrite(VUrl[I], ACtx);
  end;

  TileCacheWriteArr(VFileName, VData, VSize, ACtx.Body);

  for I := 0 to Length(FMissingItemsIdx) - 1 do begin
    UpdateFileNameInfo(ACtx.FileNameInfo, VFileName[I]);
    Inc(ACtx.Counters.CacheWrite, VSize[I]);
  end;
end;

class procedure TRequestHandlerGeFlatFile.UpdateFileNameInfo(
  var AInfo: string;
  const AFileName: string
);
begin
  if AInfo <> '' then begin
    AInfo := AInfo + #13#10 + AFileName;
  end else begin
    AInfo := AFileName;
  end;
end;

end.
