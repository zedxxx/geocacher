unit flatfile_tile_cache;

interface

uses
  Classes;

function TileCacheRead(
  const AFileName: string;
  const ATempStream: TMemoryStream;
  out AData: Pointer;
  out ASize: Integer
): Boolean;

function TileCacheReadEx(
  const AFileName: string;
  const ABody: TMemoryStream
): Integer;

procedure TileCacheWriteArr(
  const AFileName: array of string;
  const AData: array of Pointer;
  const ASize: array of Integer;
  const ATempStream: TMemoryStream
);

procedure TileCacheWriteEx(
  const AFileName: string;
  const ABody: TMemoryStream
);

implementation

uses
  SysUtils,
  u_GlobalLog,
  u_Synchronizer,
  u_FileSystemTools;

var
  GTileCacheLock: IReadWriteSync = nil;

function TileCacheRead(
  const AFileName: string;
  const ATempStream: TMemoryStream;
  out AData: Pointer;
  out ASize: Integer
): Boolean;
begin
  Result := False;

  GTileCacheLock.BeginRead;
  try
    try
      ATempStream.Clear;
      ATempStream.LoadFromFile(AFileName);
      ASize := ATempStream.Size;
      if ASize > 0 then begin
        GetMem(AData, ASize);
        ATempStream.Position := 0;
        ATempStream.ReadBuffer(AData^, ASize);
        Result := True;
      end;
    except
      on E: Exception do begin
        GLog.Error('TileCacheRead: ' + AFileName, E);
      end;
    end;
  finally
    GTileCacheLock.EndRead;
  end;
end;

function TileCacheReadEx(
  const AFileName: string;
  const ABody: TMemoryStream
): Integer;
begin
  GTileCacheLock.BeginRead;
  try
    try
      ABody.Clear;
      ABody.LoadFromFile(AFileName);
      Result := ABody.Size;
      ABody.Position := 0;
    except
      on E: Exception do begin
        Result := 0;
        GLog.Error('TileCacheReadEx: ' + AFileName, E);
      end;
    end;
  finally
    GTileCacheLock.EndRead;
  end;
end;

procedure TileCacheWriteArr(
  const AFileName: array of string;
  const AData: array of Pointer;
  const ASize: array of Integer;
  const ATempStream: TMemoryStream
);
var
  I: Integer;
begin
  GTileCacheLock.BeginWrite;
  try
    for I := 0 to Length(AData) - 1 do begin
      try
        if (AData[I] <> nil) and (ASize[I] > 0) and (AFileName[I] <> '' ) then begin
          CreateDirIfNotExists(ExtractFilePath(AFileName[I]));
          ATempStream.Clear;
          ATempStream.WriteBuffer(AData[I]^, ASize[I]);
          ATempStream.SaveToFile(AFileName[I]);
        end;
      except
        on E: Exception do begin
          GLog.Error('TileCacheWriteArr: ' + AFileName[I], E);
        end;
      end;
    end;
  finally
    GTileCacheLock.EndWrite;
  end;
end;

procedure TileCacheWriteEx(
  const AFileName: string;
  const ABody: TMemoryStream
);
begin
  GTileCacheLock.BeginWrite;
  try
    try
      if (ABody <> nil) and (ABody.Size > 0) and (AFileName <> '' ) then begin
        CreateDirIfNotExists(ExtractFilePath(AFileName));
        ABody.SaveToFile(AFileName);
        ABody.Position := 0;
      end;
    except
      on E: Exception do begin
        GLog.Error('TileCacheWriteEx: ' + AFileName, E);
      end;
    end;
  finally
    GTileCacheLock.EndWrite;
  end;
end;

initialization
  GTileCacheLock := GSync.SyncStd.Make;

end.
