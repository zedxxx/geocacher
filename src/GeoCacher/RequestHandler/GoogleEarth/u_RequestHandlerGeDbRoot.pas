unit u_RequestHandlerGeDbRoot;

interface

uses
  Classes,
  t_AppConfig,
  t_RequestHandler,
  i_GeDbRootStorage,
  u_HttpContext;

type
  TRequestHandlerGeDbRoot = class(TRequestHandler)
  private
    FStream: TMemoryStream;
    FStorage: IGeDbRootStorage;
    FIsFoundInCache: Boolean;
    FContentType: string;
    FFileName: string;
    procedure DoPatch(const ACtx: THttpContext);
    procedure MakeAnswerFromCache(const ACtx: THttpContext);
  private
    procedure DoOnUrlToFileNameConvert(const ACtx: THttpContext);
    procedure DoOnBeforeRequestSend(const ACtx: THttpContext);
    procedure DoOnBeforeAnswerSend(const ACtx: THttpContext);
  public
    constructor Create(const AStorage: IGeDbRootStorage);
    destructor Destroy; override;
  end;

implementation

uses
  SysUtils,
  libdbroot,
  u_GlobalLog,
  u_HttpHeadersTools;

{ TRequestHandlerGeDbRoot }

constructor TRequestHandlerGeDbRoot.Create(const AStorage: IGeDbRootStorage);
begin
  inherited Create;

  FStorage := AStorage;

  OnUrlToFileNameConvert := Self.DoOnUrlToFileNameConvert;
  OnBeforeRequestSend := Self.DoOnBeforeRequestSend;

  FStream := TMemoryStream.Create;
end;

destructor TRequestHandlerGeDbRoot.Destroy;
begin
  FreeAndNil(FStream);
  inherited Destroy;
end;

procedure TRequestHandlerGeDbRoot.DoOnUrlToFileNameConvert(const ACtx: THttpContext);
begin
  ACtx.FileName := '';
end;

procedure TRequestHandlerGeDbRoot.DoOnBeforeRequestSend(const ACtx: THttpContext);
var
  VLastModified: string;
begin
  FFileName := '';
  FContentType := '';
  FIsFoundInCache := False;

  if ACtx.AllowCacheRead and ACtx.DbRootConfig.AllowRead then begin
    FIsFoundInCache :=
      FStorage.Read(
        ACtx.Url,
        FStream,
        ACtx.DbRootConfig,
        FContentType,
        VLastModified,
        FFileName
      );
  end;

  if ACtx.AllowInternetRequest then begin
    if not FIsFoundInCache then begin
      VLastModified := '';
    end;
    SetLastModified(ACtx.Head, VLastModified, ACtx.RegEx);
    OnBeforeAnswerSend := DoOnBeforeAnswerSend;
  end else begin
    if FIsFoundInCache then begin
      MakeAnswerFromCache(ACtx);
    end else begin
      ACtx.RespNo := 404;
      ACtx.Head := '';
    end;
    OnBeforeAnswerSend := nil;
  end;
end;

procedure TRequestHandlerGeDbRoot.DoOnBeforeAnswerSend(const ACtx: THttpContext);
begin
  case ACtx.RespNo of
    200: begin
      if ACtx.AllowCacheWrite and ACtx.DbRootConfig.AllowWrite then begin
        FStorage.Write(
          ACtx.Url,
          ACtx.Head,
          ACtx.Body,
          ACtx.DbRootConfig,
          FFileName
        );
        ACtx.FileNameInfo := FFileName; // for gui
        ACtx.Counters.CacheWrite := ACtx.Body.Size;
      end;

      DoPatch(ACtx);
    end;

    304: begin
      MakeAnswerFromCache(ACtx);
    end;
  end;
end;

procedure TRequestHandlerGeDbRoot.MakeAnswerFromCache(const ACtx: THttpContext);
const
  cEOL = #13#10;
begin
  ACtx.FileNameInfo := FFileName; // for gui
  ACtx.Body.LoadFromStream(FStream);
  ACtx.Counters.CacheRead := ACtx.Body.Size;

  DoPatch(ACtx);

  if FContentType = '' then begin
    FContentType := 'Content-Type: application/octet-stream' + cEOL;
  end else begin
    FContentType := 'Content-Type: ' + FContentType + cEOL;
  end;

  if ACtx.RespNo = 304 then begin
    ACtx.RespNo := 200;
    ACtx.Head := ACtx.Head + FContentType;
  end else begin
    ACtx.RespNo := 200;
    ACtx.Head :=
      'Server: GeoCacher' + cEOL +
      FContentType +
      'Content-Length: ' + IntToStr(ACtx.Body.Size) + cEOL +
      'Connection: Keep-Alive' + cEOL;
  end;
end;

procedure TRequestHandlerGeDbRoot.DoPatch(const ACtx: THttpContext);

  procedure _LogError(const AError: PAnsiChar);
  begin
    if AError <> nil then begin
      GLog.Error(Self.ClassName + ': ' + string(AError));
    end else begin
      GLog.Error(Self.ClassName + ': ' + 'Unexpected empty error');
    end;
  end;

var
  VStr: string_t;
  VRoot: dbroot_t;
  VCount: uint32_t;
  VDoRepack: Boolean;
  VStream: TMemoryStream;
  VConfig: PGoogleEarthDbRootConfigRec;
  VIsEarth: Boolean;
begin
  VConfig := ACtx.DbRootConfig;
  VStream := ACtx.Body;

  if not VConfig.AllowPatch then begin
    Exit;
  end;

  VDoRepack := False;
  VIsEarth := Pos('db=', ACtx.Url) = 0;

  if not dbroot_open(VStream.Memory, VStream.Size, VRoot) then begin
    _LogError(VRoot.error);
    Exit;
  end;

  try
    if VConfig.RemoveCopyrights then begin
      if dbroot_clear_copyright_string(VCount, VRoot) then begin
        VDoRepack := True;
      end else begin
        _LogError(VRoot.error);
      end;
    end;

    if VIsEarth and VConfig.DisableGeLogo then begin
      if dbroot_set_use_ge_logo(False, VRoot) then begin
        VDoRepack := True;
      end else begin
        _LogError(VRoot.error);
      end;
    end;

    if VIsEarth and VConfig.DisableDiskCache then begin
      if dbroot_disable_disk_cache(True, VRoot) then begin
        VDoRepack := True;
      end else begin
        _LogError(VRoot.error);
      end;
    end;

    if not ACtx.AllowInternetRequest and VConfig.DisableAuthentication then begin
      if dbroot_disable_authentication(True, VRoot) then begin
        VDoRepack := True;
      end else begin
        _LogError(VRoot.error);
      end;
    end;

    if VIsEarth and VConfig.FixPlanetaryDataBase then begin
      if dbroot_fix_planetary_database(VRoot) then begin
        VDoRepack := True;
      end else begin
        _LogError(VRoot.error);
      end;
    end;

    if VConfig.MaxRequestsPerQuery > 0 then begin
      if dbroot_set_max_requests_per_query(VConfig.MaxRequestsPerQuery, VRoot) then begin
        VDoRepack := True;
      end else begin
        _LogError(VRoot.error);
      end;
    end;

    if VIsEarth and (VConfig.DiscoverabilityAltitude > 0) then begin
      if dbroot_set_discoverability_altitude_meters(VConfig.DiscoverabilityAltitude, VRoot) then begin
        VDoRepack := True;
      end else begin
        _LogError(VRoot.error);
      end;
    end;

    if VDoRepack then begin
      if dbroot_pack(VStr, VRoot) then begin
        VStream.Clear;
        VStream.WriteBuffer(VStr.data^, VStr.size);
        VStream.Position := 0;
      end else begin
        _LogError(VRoot.error);
      end;
    end;
  finally
    if not dbroot_close(VRoot) then begin
      _LogError(VRoot.error);
    end;
  end;
end;

end.
