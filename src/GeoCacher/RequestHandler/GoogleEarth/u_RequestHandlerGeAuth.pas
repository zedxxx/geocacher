unit u_RequestHandlerGeAuth;

interface

uses
  t_RequestHandler,
  u_HttpContext;

type
  TRequestHandlerGeAuth = class(TRequestHandler)
  private
    procedure DoOnUrlToFileNameConvert(const ACtx: THttpContext);
    procedure DoOnBeforeRequestSend(const ACtx: THttpContext);
    procedure DoOnBeforeAnswerSend(const ACtx: THttpContext);
  public
    constructor Create;
  end;

implementation

uses
  SysUtils,
  t_AppConfig,
  c_GeAuth;

{ TRequestHandlerGeAuth }

constructor TRequestHandlerGeAuth.Create;
begin
  inherited Create;
  OnUrlToFileNameConvert := Self.DoOnUrlToFileNameConvert;
  OnBeforeRequestSend := Self.DoOnBeforeRequestSend;
end;

procedure TRequestHandlerGeAuth.DoOnUrlToFileNameConvert(const ACtx: THttpContext);
begin
  ACtx.FileName := '';
end;

procedure TRequestHandlerGeAuth.DoOnBeforeRequestSend(const ACtx: THttpContext);
var
  VSize: Integer;
  VIsPost49: Boolean;
begin
  VIsPost49 := ACtx.ReqInfo.PostStream.Size = 49;

  if ACtx.AllowInternetRequest then begin
    if VIsPost49 then begin
      OnBeforeAnswerSend := DoOnBeforeAnswerSend;
    end else begin
      OnBeforeAnswerSend := nil;
    end;
  end else begin
    if VIsPost49 then begin
      VSize := Length(CFakeGeAuth);
      ACtx.RespNo := 200;
      ACtx.Head :=
        'Server: GeoCacher (FromCache)' + #13#10 +
        'Content-Type: application/octet-stream' + #13#10 +
        'Content-Length: ' + IntToStr(VSize) + #13#10 +
        'Connection: Keep-Alive' + #13#10;

      ACtx.Body.WriteBuffer(CFakeGeAuth, VSize);
      Inc(ACtx.Counters.CacheRead, ACtx.Body.Size);
    end else begin
      ACtx.RespNo := 403;
      ACtx.Head := '';
    end;
    OnBeforeAnswerSend := nil;
  end;
end;

procedure TRequestHandlerGeAuth.DoOnBeforeAnswerSend(const ACtx: THttpContext);
const
  CGeAuthCheckSummOffset = 8;
var
  P: PByte;
begin
  if ACtx.RespNo = 200 then begin
    P := ACtx.Body.Memory;
    Inc(P, ACtx.Body.Size - CGeAuthCheckSummOffset);
    P^ := P^ + 1;
  end;
end;

end.
