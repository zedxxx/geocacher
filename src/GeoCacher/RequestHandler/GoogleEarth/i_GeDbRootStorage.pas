unit i_GeDbRootStorage;

interface

uses
  Classes,
  t_AppConfig;

type
  IGeDbRootStorage = interface
  ['{123A1B07-5A45-4C71-8BEC-AC24530EC97E}']
    procedure Write(
      const AUrl: string;
      const AHead: string;
      const AStream: TMemoryStream;
      const AConfig: PGoogleEarthDbRootConfigRec;
      out AFileName: string
    );

    function Read(
      const AUrl: string;
      const AStream: TMemoryStream;
      const AConfig: PGoogleEarthDbRootConfigRec;
      out AContentType: string;
      out ALastModified: string;
      out AFileName: string
    ): Boolean;
  end;

implementation

end.
