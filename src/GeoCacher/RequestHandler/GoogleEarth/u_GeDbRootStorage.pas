unit u_GeDbRootStorage;

interface

uses
  Windows,
  Classes,
  SysUtils,
  t_AppConfig,
  i_GeDbRootStorage;

type
  TGeDbRootStorage = class(TInterfacedObject, IGeDbRootStorage)
  private
    FLock: IReadWriteSync;
    function GetFileName(const AUrl: string; const ACachePath: string): string;
    function GetContentType(const AHead: string): string;
    function GetLastModified(const AHead: string): string;
  private
    { IGeDbRootStorage }
    procedure Write(
      const AUrl: string;
      const AHead: string;
      const AStream: TMemoryStream;
      const AConfig: PGoogleEarthDbRootConfigRec;
      out AFileName: string
    );
    function Read(
      const AUrl: string;
      const AStream: TMemoryStream;
      const AConfig: PGoogleEarthDbRootConfigRec;
      out AContentType: string;
      out ALastModified: string;
      out AFileName: string
    ): Boolean;
  public
    constructor Create;
  end;

implementation

uses
  IniFiles,
  RegExpr,
  libcrc32,
  u_Synchronizer,
  u_DateTimeTools,
  u_FileSystem,
  u_FileSystemTools;

{ TGeDbRootStorage }

constructor TGeDbRootStorage.Create;
begin
  inherited Create;

  FLock := GSync.SyncStd.Make;
end;

function TGeDbRootStorage.GetFileName(
  const AUrl: string;
  const ACachePath: string
): string;
var
  VRegExpr: TRegExpr;
begin
  Result := '';

  VRegExpr := TRegExpr.Create;
  try
    VRegExpr.ModifierI := True;

    // Earth
    // https://kh.google.com/dbRoot.v5?hl=ru&gl=ru&output=proto&cv=7.3.2.5481&ct=pro
    VRegExpr.Expression := '://kh\.google\.com/dbRoot\.v5\?(.*)';
    if VRegExpr.Exec(AUrl) then begin
      Result := ACachePath + 'earth\' + VRegExpr.Match[1];
      Exit;
    end;

    // History/Mars/Moon/Sky
    // https://khmdb.google.com/dbRoot.v5?db=tm&hl=ru&gl=ru&output=proto&cv=7.3.2.5481&ct=pro
    VRegExpr.Expression := '://khmdb\.google\.com/dbRoot\.v5\?db=(.+?)&(.*)';
    if VRegExpr.Exec(AUrl) then begin
      Result := ACachePath + VRegExpr.Match[1] + '\' + VRegExpr.Match[2];
    end;
  finally
    VRegExpr.Free;
  end;
end;

function TGeDbRootStorage.Read(
  const AUrl: string;
  const AStream: TMemoryStream;
  const AConfig: PGoogleEarthDbRootConfigRec;
  out AContentType: string;
  out ALastModified: string;
  out AFileName: string
): Boolean;
var
  VCRC32: Cardinal;
  VNewCRC32: Cardinal;
  VIniFileName: string;
  VIniFile: TMemIniFile;
begin
  Result := False;
  AStream.Clear;

  if not AConfig.AllowRead then begin
    Exit;
  end;

  AFileName := GetFileName(AUrl, AConfig.PathFull);
  if AFileName = '' then begin
    Assert(
      False,
      Format('%s: Build FileName for url "%s" failed!', [Self.ClassName, AUrl])
    );
    Exit;
  end;
  VIniFileName := AFileName + '.ini';

  FLock.BeginRead;
  try
    if not FileExists(VIniFileName) then begin
      Exit;
    end;

    VIniFile := TMemIniFile.Create(VIniFileName);
    try
      VCRC32 := VIniFile.ReadInteger('Main', 'CRC32', 0);
      AContentType := VIniFile.ReadString('Main', 'ContentType', '');
      ALastModified := VIniFile.ReadString('Main', 'LastModified', '');
    finally
      VIniFile.Free;
    end;

    AStream.LoadFromFile(AFileName);
  finally
    FLock.EndRead;
  end;

  if AStream.Size > 0 then begin
    VNewCRC32 := crc32(0, AStream.Memory, AStream.Size);
    Result := VCRC32 = VNewCRC32;
  end;
end;

function TGeDbRootStorage.GetContentType(const AHead: string): string;
var
  VRegExpr: TRegExpr;
begin
  VRegExpr := TRegExpr.Create;
  try
    VRegExpr.ModifierI := True;
    VRegExpr.ModifierM := True;

    VRegExpr.Expression := '^Content-Type\s*:\s*(.+?)\r\n';
    if VRegExpr.Exec(AHead) then begin
      Result := VRegExpr.Match[1];
    end else begin
      Result := '';
    end;
  finally
    VRegExpr.Free;
  end;
end;

function TGeDbRootStorage.GetLastModified(const AHead: string): string;
var
  VRegExpr: TRegExpr;
begin
  VRegExpr := TRegExpr.Create;
  try
    VRegExpr.ModifierI := True;
    VRegExpr.ModifierM := True;

    VRegExpr.Expression := '^Last-Modified\s*:\s*(.+?)\r\n';
    if VRegExpr.Exec(AHead) then begin
      Result := VRegExpr.Match[1];
    end else begin
      Result := DateTimeToRFC1123(LocalTimeToUTC(Now));
    end;
  finally
    VRegExpr.Free;
  end;
end;

procedure TGeDbRootStorage.Write(
  const AUrl: string;
  const AHead: string;
  const AStream: TMemoryStream;
  const AConfig: PGoogleEarthDbRootConfigRec;
  out AFileName: string
);
var
  VCRC32: Cardinal;
  VLastModified: string;
  VContentType: string;
  VUnique: string;
  VIniFileName: string;
  VIniFile: TMemIniFile;
begin
  if not AConfig.AllowWrite then begin
    AFileName := '';
    Exit;
  end;

  VUnique := '.' + IntToStr(GetCurrentThreadId) + '.tmp';

  AFileName := GetFileName(AUrl, AConfig.PathFull);
  if AFileName = '' then begin
    Assert(
      False,
      Format('%s: UrlToFileName for url "%s" failed!', [Self.ClassName, AUrl])
    );
    Exit;
  end;
  VIniFileName := AFileName + '.ini';

  VCRC32 := crc32(0, AStream.Memory, AStream.Size);
  VContentType := GetContentType(AHead);
  VLastModified := GetLastModified(AHead);

  FLock.BeginWrite;
  try
    CreateFileIfNotExists(VIniFileName + VUnique);

    VIniFile := TMemIniFile.Create(VIniFileName + VUnique);
    try
      VIniFile.WriteString('Main', 'CRC32', '$' + IntToHex(VCRC32, 8));
      VIniFile.WriteString('Main', 'ContentType', VContentType);
      VIniFile.WriteString('Main', 'LastModified', VLastModified);
      VIniFile.UpdateFile;
    finally
      VIniFile.Free;
    end;

    AStream.SaveToFile(AFileName + VUnique);

    if not TFileSystem.RenameFileX(AFileName + VUnique, AFileName) then begin
      RaiseLastOSError;
    end;

    if not TFileSystem.RenameFileX(VIniFileName + VUnique, VIniFileName) then begin
      RaiseLastOSError;
    end;
  finally
    FLock.EndWrite;
  end;
end;

end.
