unit u_GeFlatFilePacketValidator;

interface

uses
  RegExpr,
  t_AppConfig;

type
  TGePacketId = (
    pktQ2,
    pktQp,
    pktJpeg,
    pktJpegTm,
    pktTerrain,
    pktLayer,
    pktTexture,
    pktUnk
  );

const
  CGePacketRegExp: array [pktQ2..pktTexture] of string = (
    'q2-\d+-q\.\d+',
    'qp-\d+-q\.\d+',
    'f1-\d+-i\.\d+',
    'f1-\d+-i\.\d+-\w+',
    'f1c-\d+-t\.\d+',
    'f1c-\d+-d\.\d+\.\d+',
    'f1-\d+-d\.\d+\.\d+'
  );

type
  TGePacketValidator = class
  private
    FTmpBuff: Pointer;
    FTmpBuffSize: Integer;
    FRawBuff: Pointer;
    FRawBuffSize: Integer;
  private
    function GetPacketId(const AUrl: string; const ARegEx: TRegExpr): TGePacketId;

    function IsValidJpeg(const AData: Pointer; const ASize: Integer): Boolean;    
    function TryDecodeJpeg(const AData: Pointer; const ASize: Integer): Boolean;

    function IsValidTexture(AData: PByte; const ASize: Integer): Boolean;
  public
    function IsValidPacket(
      const AData: Pointer;
      const ASize: integer;
      const AUrl: string;
      const ARegEx: TRegExpr;
      const AConfig: PFlatFileTileCacheConfigRec
    ): Boolean;
    constructor Create;
    destructor Destroy; override;
  end;

implementation

uses
  Windows,
  SysUtils,
  LibJpeg62,
  libgepacket,
  u_GlobalLog;

{ TGePacketValidator }

constructor TGePacketValidator.Create;
begin
  inherited Create;
  FTmpBuff := nil;
  FTmpBuffSize := 0;
  FRawBuff := nil;
  FRawBuffSize := 0;
end;

destructor TGePacketValidator.Destroy;
begin
  if FTmpBuff <> nil then begin
    FreeMem(FTmpBuff);
  end;
  if FRawBuff <> nil then begin
    FreeMem(FRawBuff);
  end;
  inherited Destroy;
end;

function TGePacketValidator.GetPacketId(
  const AUrl: string;
  const ARegEx: TRegExpr
): TGePacketId;
var
  I: TGePacketId;
begin
  Result := pktUnk;
  for I := Low(CGePacketRegExp) to High(CGePacketRegExp) do begin
    ARegEx.Expression := CGePacketRegExp[I];
    if ARegEx.Exec(AUrl) then begin
      Result := I;
      Break;
    end;
  end;
end;

function TGePacketValidator.IsValidPacket(
  const AData: Pointer;
  const ASize: integer;
  const AUrl: string;
  const ARegEx: TRegExpr;
  const AConfig: PFlatFileTileCacheConfigRec
): Boolean;
var
  VPacketId: TGePacketId;
  VDecompressedSize: Cardinal;
begin
  Result := False;

  if (AData = nil) or (ASize = 0) or (AUrl = '') then begin
    Assert(False);
    Exit;
  end;

  if not AConfig.TestContentOnRead then begin
    Result := True;
    Exit;
  end;

  try
    VPacketId := GetPacketId(AUrl, ARegEx);
    if VPacketId = pktUnk then begin
      Result := True;
      Exit;
    end;

    if FTmpBuffSize < ASize then begin
      ReallocMem(FTmpBuff, ASize);
      FTmpBuffSize := ASize;
    end;

    Move(AData^, FTmpBuff^, ASize);

    gepacket_encode(FTmpBuff, ASize);

    case VPacketId of
      pktJpeg, pktJpegTm: begin
        Result := IsValidJpeg(FTmpBuff, ASize);
        if Result and AConfig.DeepTestJpegOnRead then begin
          Result := TryDecodeJpeg(FTmpBuff, ASize);
        end;
      end;

      pktTexture: begin
        Result := IsValidTexture(FTmpBuff, ASize);
      end;

      pktQ2, pktQp, pktTerrain, pktLayer: begin
        if gepacket_is_compressed(FTmpBuff, ASize) then begin
          Result := gepacket_get_decompressed_size(FTmpBuff, ASize, VDecompressedSize);
          if Result then begin
            if FRawBuffSize < Integer(VDecompressedSize) then begin
              ReallocMem(FRawBuff, VDecompressedSize);
              FRawBuffSize := VDecompressedSize;
            end;
            Result := gepacket_decompress(FTmpBuff, ASize, FRawBuff, VDecompressedSize);
          end;
        end else begin
          if VPacketId = pktLayer then begin
            Result := True; // layer can be uncompressed
          end else begin
            Result := False; // q2, qp and terrain are always compressed
          end;
        end;
      end;
    else
      Assert(False, 'Unexpected Packet ID: ' + IntToStr(Integer(VPacketId)));
    end;

    if not Result then begin
      GLog.Error(Format('BAD_TILE: size=%d, url=%s', [ASize, AUrl]));
    end;
  except
    on E: Exception do begin
      GLog.Error(Self.ClassName + ': ' + AUrl, E);
    end;
  end;
end;

function TGePacketValidator.IsValidTexture(AData: PByte; const ASize: Integer): Boolean;
const
  BOF_3D_MODEL = $0183; // 83 01
  EOF_3D_MODEL = $0184; // 84 01
begin
  Assert(AData <> nil);

  Result := False;

  if ASize < 4 then begin
    Exit;
  end;

  if PWord(AData)^ <> BOF_3D_MODEL then begin
    Exit;
  end;

  Inc(AData, ASize - 2);
  if PWord(AData)^ <> EOF_3D_MODEL then begin
    Exit;
  end;

  Result := True;
end;

function TGePacketValidator.IsValidJpeg(const AData: Pointer; const ASize: Integer): Boolean;
const
  BOF_JPEG = $D8FF; // $FFD8;
  EOF_JPEG = $D9FF; // $FFD9;
var
  VData: PByte;
begin
  Assert(AData <> nil);

  Result := False;

  if ASize < 4 then begin
    Exit;
  end;

  if PWord(AData)^ <> BOF_JPEG then begin
    Exit;
  end;

  VData := PByte(AData);
  Inc(VData, ASize - 2);
  if PWord(VData)^ <> EOF_JPEG then begin
    Exit;
  end;

  Result := True;
end;

procedure _libjpeg_error_exit(cinfo: j_common_ptr); cdecl;
var
  VMsg: AnsiString;
begin
  SetLength(VMsg, 256);
  cinfo.err.format_message(cinfo, PAnsiChar(VMsg));
  cinfo.global_state := 0;
  jpeg_abort(cinfo);
  GLog.Error(
    'LIBJPEG ERROR [' + IntToStr(cinfo.err.msg_code) + '] ' + string(PAnsiChar(VMsg))
  );
end;

procedure _libjpeg_output_message(cinfo: j_common_ptr); cdecl;
var
  VMsg: AnsiString;
begin
  SetLength(VMsg, 256);
  cinfo.err.format_message(cinfo, PAnsiChar(VMsg));
  cinfo.global_state := 0;
  GLog.Error(
    'LIBJPEG OUTPUT [' + IntToStr(cinfo.err.msg_code) + '] ' + string(PAnsiChar(VMsg))
  );
end;

function TGePacketValidator.TryDecodeJpeg(const AData: Pointer; const ASize: Integer): Boolean;
var
  cinfo: jpeg_decompress_struct;
  cerr: jpeg_error_mgr;
  VBuff: PByte;
  VLineSize: Cardinal;
  VCount: Cardinal;
  VOutputSize: Integer;
begin
  Result := False;

  FillChar(cinfo, SizeOf(jpeg_decompress_struct), 0);
  FillChar(cerr, SizeOf(jpeg_error_mgr), 0);

  cinfo.err := jpeg_std_error(@cerr);
  cerr.error_exit := _libjpeg_error_exit;
  cerr.output_message := _libjpeg_output_message;

  jpeg_create_decompress(@cinfo);
  try
    jpeg_mem_src(@cinfo, AData, ASize);

    cinfo.global_state := DSTATE_START;

    if jpeg_read_header(@cinfo, True) <> 1 then begin
      Exit;
    end;

    jpeg_start_decompress(@cinfo);
    try
      VLineSize := cinfo.output_width * Cardinal(cinfo.output_components);
      VOutputSize := VLineSize * cinfo.output_height;
      
      if FRawBuffSize < VOutputSize then begin
        ReallocMem(FRawBuff, VOutputSize);
        FRawBuffSize := VOutputSize;
      end;
      VBuff := FRawBuff;

      while cinfo.output_scanline < cinfo.output_height do begin
        if cinfo.global_state = 0 then begin
          Break; // abort by error
        end;
        VCount := jpeg_read_scanlines(@cinfo, @VBuff, cinfo.rec_outbuf_height);
        if VCount > 0 then begin
          Inc(VBuff, VCount * VLineSize);
        end;
      end;

      Result := (cinfo.global_state <> 0) and (cerr.num_warnings = 0);
    finally
      if cinfo.global_state <> 0 then begin
        jpeg_finish_decompress(@cinfo);
      end;
    end;
  finally
    jpeg_destroy_decompress(@cinfo);
  end;
end;

end.
