unit flatfile_any_version;

interface

uses
  Windows,
  StrUtils,
  SysUtils;

function GetAnyVersion(
  const AUrl: string;
  const AFileName: string;
  const AIsCacheOnly: Boolean
): string;

implementation

uses
  flatfile_regexpr;

function CreateFileName(const AFileName, APath, AVer, ATimeTail: string): string; inline;
var
  VStr: string;
begin
  VStr := MidStr(AFileName, Length(APath) + 1, 255);
  Result :=
    ReplaceStr(APath + AVer + '\' + MidStr(VStr, Pos('\', VStr) + 1, 255), '.*', '.' + AVer) +
    ATimeTail;
end;

function GetAnyVersion(
  const AUrl: string;
  const AFileName: string;
  const AIsCacheOnly: Boolean
): string;

var
  A: array [0..1023] of Integer;

  procedure _QuickSort(const p, q: Integer);
  var
    I, j, r, t: Integer;
  begin
    if p<q then begin
      r:=A[p];
      I:=p-1;
      j:=q+1;
      while I<j do begin
        repeat
          Inc(I);
        until A[I]>=r;
        repeat
          Dec(j);
        until A[j]<=r;
        if I<j then begin
          t:=A[I];
          A[I]:=A[j];
          A[j]:=t;
        end;
      end;
      _QuickSort(p,j);
      _QuickSort(j+1,q);
    end;
  end;

var
  I, VFound, VCount: Integer;
  VIsHist, VIsDirVer: Boolean;
  VExt, VVer, VMaskFileName, VFileName, VDirName, VTimeTail: string;
  VSearchRec: TSearchRec;
begin
  VFileName := AFileName;

  // ���� ���� Qtree � �� ������� "������ �� ����" - ������� ��� ����� ���
  if ContainsStr(AUrl, '-q.') and not AIsCacheOnly then begin
    Result := '';
    Exit;
  end;

  // ������ - � ��� URL ��������� �����, ���������� ������
  VVer := MidStr(ExtractFileExt(AUrl),2,20);
  VTimeTail:='';

  // ��� ������������ ������?
  VIsHist := ContainsStr(AUrl, 'flatfile?db=tm');
  if VIsHist then begin
    VTimeTail:=MidStr(VVer, Pos('-', VVer)+1, 20);
    VVer := LeftStr(VVer, Pos('-', VVer)-1);
  end;

  // ��� ����� �������� � ���� ����� ������?
  VIsDirVer:=false;
  VDirName:='';
  VCount:=Length(GVerFold);
  for I:=0 to VCount - 1 do begin
     VIsDirVer:=LeftStr(VFileName,Length(GVerFold[I])) = GVerFold[I];
     if VIsDirVer then begin
       VDirName:=GVerFold[I];
       Break;
     end;
  end;

  // ������������ ����� ������ ������ � �����
  VMaskFileName := ReplaceStr(VFileName, '.'+VVer, '.*');
  // ��� ������������� ����� ������� � ����� ������ ����� �������,
  // ����� ������ ��� ������ ������� ������
  if VIsHist then begin
    VMaskFileName := ChangeFileExt(VMaskFileName, '.*');
  end;

  if VIsDirVer then begin
    // ��� �����, � ������� ������ - � ���� ����� - � ����� ����� ������ ������
    // ����� ��������� ������
    // ����� ��� ����� ������, ���������� ����������� ���������,
    // ������������ ��� �����. ���� �� ���� � ���� ����� - �������. ���� ��� - �������.
    // (�������������, ��� �����, ��������������� ������ ������ � ���� �����
    // ����������� ������ ���� ���)
    VCount := 0;
    VFound := SysUtils.FindFirst(VDirName + '*', faDirectory, VSearchRec);
    while VFound = 0 do begin
       if (VSearchRec.Name<>'.') and (VSearchRec.Name<>'..') then begin
         VExt := VSearchRec.Name;
         A[VCount] := StrToInt(VExt);
         Inc(VCount);
       end;
       VFound := FindNext(VSearchRec);
    end;
    SysUtils.FindClose(VSearchRec);
    _QuickSort(0,VCount-1);
    // ��������� ������� ������, ������� � ��������� ������
    for I:=VCount-1 downto 0 do begin
      VVer := IntToStr(A[I]);
      VFileName := CreateFileName(VMaskFileName, VDirName, VVer, VTimeTail);
      if FileExists(VFileName) then begin
        Result:=VFileName;
        Exit;
      end;
    end;
    Result:='';
    Exit;
  end;

  VCount := 0;
  FillMemory(@A, SizeOf(A), 0);
  // ����� ���� � ��������� ��������� �������
  try
    VFound := SysUtils.FindFirst(VMaskFileName, faAnyFile and not faDirectory, VSearchRec);
    while VFound = 0 do begin
      VExt := MidStr(ExtractFileExt(VSearchRec.Name),2,20);
      // � ������������ ������ � �������� ������ ����� ������ - ����� ������ -
      // ������� ���, �������� ������ ������
      if VIsHist then begin
        VExt := LeftStr(VExt, Pos('-', VExt)-1);
      end;
      A[VCount] := StrToInt(VExt);
      Inc(VCount);
      VFound := FindNext(VSearchRec);
    end;
    // ���� �� ����� ������ - �� ������
    if VCount=0 then begin
      Result := '';
      Exit;
    end;

    if VIsHist then
      VTimeTail := '-' + VTimeTail;  // 1.4.3 fix

    // ���� ���� ���� - �� ��� � �����
    if VCount=1 then begin
      VVer := IntToStr(A[0]);
      Result := ReplaceStr(VMaskFileName, '.*', '.'+VVer) + VTimeTail;
      Exit;
    end;
    // ���� ������� ��� ����� - ��������� ��� ������� ����������
    if VCount=2 then begin
      if A[0]>A[1] then
        VVer := IntToStr(A[0])
      else
        VVer := IntToStr(A[1]);
      Result := ReplaceStr(VMaskFileName, '.*', '.'+VVer) + VTimeTail;
      Exit;
    end;
    // ����� - ������� ��������� ������ - ����������� ������
    _QuickSort(0,VCount-1);
    // ����� ����� ������� ������ �� ���������
    VVer := IntToStr(A[VCount-1]);
    Result := ReplaceStr(VMaskFileName, '.*', '.'+VVer) + VTimeTail;
  finally
    SysUtils.FindClose(VSearchRec);
  end;
end;

end.
