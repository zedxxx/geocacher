unit u_RequestHandlerGeFactory;

interface

uses
  SysUtils,
  i_GeDbRootStorage,
  i_RequestHandlerFactory,
  t_RequestHandler,
  u_RequestHandlerGeDbRoot,
  u_RequestHandlerGeFlatFile,
  u_ObjectPool,
  u_HttpContext;

type
  TRequestHandlerGeFactory = class(TInterfacedObject, IRequestHandlerFactory)
  private
    FPool: TObjectPool;
    FGeDbRootStorage: IGeDbRootStorage;
  private
    { IRequestHandlerFactory }
    function Build(const ACtx: THttpContext): TRequestHandlerArray;
    procedure ReturnToPool(var AHandler: TRequestHandlerArray);
  public
    constructor Create;
    destructor Destroy; override;
  end;

  ERequestHandlerGeFactory = class(Exception);

implementation

uses
  RegExpr,
  t_AppConfig,
  u_GeDbRootStorage,
  u_RequestHandlerGeAuth;

{ TRequestHandlerGeFactory }

constructor TRequestHandlerGeFactory.Create;
begin
  inherited Create;

  FPool := TObjectPool.Create;
  FGeDbRootStorage := TGeDbRootStorage.Create;
end;

destructor TRequestHandlerGeFactory.Destroy;
begin
  FreeAndNil(FPool);
  inherited Destroy;
end;

function TRequestHandlerGeFactory.Build(const ACtx: THttpContext): TRequestHandlerArray;
var
  VHostRegEx: string;
  VEarthEnterprise: PGoogleEarthEnterpriseConfigRec;
begin
  Result := nil;

  VEarthEnterprise := @ACtx.AppConfigRec.EarthEnterprise;

  if
    VEarthEnterprise.Enabled and
    ACtx.Url.StartsWith(VEarthEnterprise.Address)
  then begin
    VHostRegEx := '^' + RegExpr.QuoteRegExprMetaChars(VEarthEnterprise.Address);
  end else begin
    VHostRegEx := '^(http|https)://(kh|khmdb)\.google\.com/';
  end;

  ACtx.RegEx.Expression := VHostRegEx + 'flatfile\?(.*?)lf-0-icons/';
  if ACtx.RegEx.Exec(ACtx.URL) then begin
    Exit;
  end;

  ACtx.RegEx.Expression := VHostRegEx + 'flatfile\?';
  if ACtx.RegEx.Exec(ACtx.URL) then begin
    SetLength(Result, 1);
    Result[0] := FPool.Pull as TRequestHandlerGeFlatFile;
    if Result[0] = nil then begin
      Result[0] := TRequestHandlerGeFlatFile.Create;
    end;
    Exit;
  end;

  if ACtx.IsHoogle then begin
    ACtx.RegEx.Expression := VHostRegEx + 'geauth';
    if ACtx.RegEx.Exec(ACtx.URL) then begin
      SetLength(Result, 1);
      Result[0] := TRequestHandlerGeAuth.Create;
      Exit;
    end;
  end;

  ACtx.RegEx.Expression := VHostRegEx + 'dbRoot\.v5';
  if ACtx.RegEx.Exec(ACtx.URL) then begin
    SetLength(Result, 1);
    Result[0] := TRequestHandlerGeDbRoot.Create(FGeDbRootStorage);
    Exit;
  end;
end;

procedure TRequestHandlerGeFactory.ReturnToPool(var AHandler: TRequestHandlerArray);
var
  I: Integer;
begin
  for I := 0 to Length(AHandler) - 1 do begin
    if AHandler[I] is TRequestHandlerGeFlatFile then begin
      FPool.Push(AHandler[I]);
      AHandler[I] := nil;
    end else if
      (AHandler[I] is TRequestHandlerGeAuth) or
      (AHandler[I] is TRequestHandlerGeDbRoot)
    then begin
      FreeAndNil(AHandler[I]);
    end else begin
      raise ERequestHandlerGeFactory.Create(
        'Unknown Handler class: ' + AHandler[I].ClassName
      );
    end;
  end;
  AHandler := nil;
end;

end.
