unit u_HttpRequestInfo;

interface

uses
  Classes,
  IdCookie,
  IdHTTPHeaderInfo,
  t_CustomHTTPServer;

type
  THttpRequestInfo = class(TIdRequestHeaderInfo)
  public
    FAuthExists: Boolean;
    FPostStream: TStream;
    FRawHTTPCommand: string;
    FRemoteIP: string;
    FDocument: string;
    FURI: string;
    FCommand: string;
    FVersion: string;
    FVersionMajor: Integer;
    FVersionMinor: Integer;
    FAuthUsername: string;
    FAuthPassword: string;
    FQueryParams: string;
    FCommandType: THTTPCommandType;
  public
    constructor Create(AOwner: TPersistent); override;
    destructor Destroy; override;
    //
    procedure ProcessHeaders; override;
    function IsVersionAtLeast(const AMajor, AMinor: Integer): Boolean; inline;
    //
    property AuthExists: Boolean read FAuthExists;
    property AuthPassword: string read FAuthPassword;
    property AuthUsername: string read FAuthUsername;
    property Command: string read FCommand;
    property CommandType: THTTPCommandType read FCommandType;
    property Document: string read FDocument;
    property URI: string read FURI;
    property PostStream: TStream read FPostStream;
    property RawHTTPCommand: string read FRawHTTPCommand;
    property RemoteIP: String read FRemoteIP;
    property QueryParams: string read FQueryParams;
    property Version: string read FVersion;
    property VersionMajor: Integer read FVersionMajor;
    property VersionMinor: Integer read FVersionMinor;
  end;

implementation

uses
  SysUtils,
  IdURI,
  IdGlobal,
  IdGlobalProtocols;

{ THttpRequestInfo }

constructor THttpRequestInfo.Create(AOwner: TPersistent);
begin
  inherited Create(AOwner);
  FCommandType := hcUnknown;
  ContentLength := -1;
end;

destructor THttpRequestInfo.Destroy;
begin
  FreeAndNil(FPostStream);
  inherited Destroy;
end;

function THttpRequestInfo.IsVersionAtLeast(const AMajor, AMinor: Integer): Boolean;
begin
  Result := (FVersionMajor > AMajor) or
            ((FVersionMajor = AMajor) and (FVersionMinor >= AMinor));
end;

procedure THttpRequestInfo.ProcessHeaders;
begin
  inherited ProcessHeaders;
  
  if FProxyConnection = '' then begin
    FProxyConnection := FRawHeaders.Values['Proxy-Connection'];
  end;
end;

end.
