unit u_HttpServer;

interface

uses
  FastInterlocked,
  i_AppConfig,
  i_GuiEventDispatcher,
  t_ServerActivityStat;

procedure HttpServerStart(
  const AAppConfig: IAppConfig;
  const AGuiEventDispatcher: IGuiEventDispatcher
);
procedure HttpServerStop;

function GetHttpServerStat(out AStat: TServerActivityStatRec): Boolean;

implementation

uses
  Windows,
  SysUtils,
  RegExpr,
  IdStack,
  IdStackConsts,
  IdException,
  IdGlobal,
  IdContext,
  IdTCPConnection,
  IdSSLOpenSSL,
  IdSSLOpenSSLHeaders,
  u_HttpContext,
  i_Listener,
  i_RequestHandlerFactoryMulti,
  t_AppConfig,
  t_RequestHandler,
  t_CustomHTTPServer,
  t_HttpRequestCounters,
  u_Synchronizer,
  u_ListenerByEvent,
  u_GlobalLog,
  u_HostForSslChecker,
  u_ServerActivityStat,
  u_HttpHeadersTools,
  u_SSLCertificateProvider,
  u_SSLIOHandlerHelper,
  u_SSLOpenSSLHeadersExtended,
  u_ServerInterceptLog,
  u_TcpTunnel,
  u_HttpDownloader,
  u_HttpDownloaderByIndy,
  u_HttpRequestInfo,
  u_HttpResponseInfo,
  u_CustomHTTPServer,
  u_GuiStatistic,
  u_GuiTcpServer,
  u_GuiEventUrlData,
  u_RequestHandlerMulti,
  u_RequestHandlerFactoryMulti;

type
  THttpServer = class(TObject)
  private
    FAppConfig: IAppConfig;
    FAppConfigLock: IReadWriteSync;
    FAppConfigStatic: IAppConfigStatic;
    FAppConfigChangeCounter: Integer;
    FAppConfigChangeListener: IListener;

    FServer: TCustomHTTPServer;
    FSSLHelper: TSSLIOHandlerHelper;
    FSSLCertProvider: TSSLCertificateProvider;
    FInterceptLog: TServerInterceptLog;
    FActivityStat: TServerActivityStat;
    FHostChecker: THostForSslChecker;
    FRequestHandlerFactory: IRequestHandlerFactoryMulti;
    FGuiEventDispatcher: IGuiEventDispatcher;

    class procedure SendConnectionEstablished(
      const AContext: TIdContext;
      const AResponseInfo: THttpResponseInfo
    );
    class procedure SendServiceUnavailable(
      const AContext: TIdContext;
      const AResponseInfo: THttpResponseInfo
    );
    class procedure SendWSAError(
      const AContext: TIdContext;
      const AResponseInfo: THttpResponseInfo;
      const AWSAError: Integer
    );
  private
    procedure OnConnect(AContext: TIdContext);
    procedure OnDisconnect(AContext: TIdContext);

    function GetConnectionContext(
      AContext: TIdContext
    ): THttpContext; inline;
    procedure OnQuerySSLPort(
      APort: TIdPort;
      var VUseSSL: Boolean
    );
    procedure OnHeadersAvailable(
      const AContext: TIdContext;
      const ARequestInfo: THTTPRequestInfo;
      const AResponseInfo: THttpResponseInfo;
      var VContinueProcessing: Boolean
    );
    procedure OnHttpCommand(
      const AContext: TIdContext;
      const ARequestInfo: THTTPRequestInfo;
      const AResponseInfo: THttpResponseInfo
    );
    procedure OnCommandOther(
      const ACtx: THttpContext;
      var ADoDisconnect: Boolean;
      var ADoRequestEnd: Boolean
    );
    procedure OnCommandConnect(
      const ACtx: THttpContext;
      var ADoDisconnect: Boolean;
      var ADoRequestEnd: Boolean
    );
    procedure OnAppConfigChange(
      const AStatic: IInterface
    );
    procedure SetupAppConfig(const ACtx: THttpContext);
  public
    constructor Create(
      const AAppConfig: IAppConfig;
      const AGuiEventDispatcher: IGuiEventDispatcher
    );
    destructor Destroy; override;

    property ActivityStat: TServerActivityStat read FActivityStat;
  end;

var
  GHttpServer: THttpServer = nil;
  GRequestId: Integer = 0;

procedure HttpServerStart(
  const AAppConfig: IAppConfig;
  const AGuiEventDispatcher: IGuiEventDispatcher
);
begin
  GHttpServer := THttpServer.Create(AAppConfig, AGuiEventDispatcher);
end;

procedure HttpServerStop;
begin
  FreeAndNil(GHttpServer);
end;

function GetHttpServerStat(out AStat: TServerActivityStatRec): Boolean;
begin
  Result := False;
  if Assigned(GHttpServer) then begin
    GHttpServer.ActivityStat.GetStatRec(AStat);
    Result := True;
  end;
end;

{ THttpServer }

constructor THttpServer.Create(
  const AAppConfig: IAppConfig;
  const AGuiEventDispatcher: IGuiEventDispatcher
);

  procedure _InitOpenSsl(const AConfig: PSslInterceptConfigRec);
  begin
    IdOpenSSLSetLibPath(AConfig.OpenSslPath);
    if not IdSSLOpenSSL.LoadOpenSSLLibrary then begin
      raise Exception.Create('OpenSSL library load failed!');
    end;
    LoadOpenSSLLibraryExtended(AConfig.OpenSslPath);
  end;

  function _NewInterceptLog(const AConfig: PDebugLogConfigRec): TServerInterceptLog;
  begin
    if AConfig.HttpLogEnabled then begin
      Result :=
        TServerInterceptLog.Create(
          AAppConfig.AppPath + 'HttpServer.log',
          AConfig.HttpLogWithContent
        );
    end else begin
      Result := nil;
    end;
  end;

var
  VConfig: PAppConfigRec;
begin
  inherited Create;

  FGuiEventDispatcher := AGuiEventDispatcher;

  FAppConfig := AAppConfig;
  FAppConfigLock := GSync.SyncVariable.Make;
  FAppConfigStatic := FAppConfig.GetStatic;

  VConfig := FAppConfigStatic.RecPtr;

  _InitOpenSsl(@VConfig.SslIntercept);

  FServer := TCustomHTTPServer.Create(nil);

  FServer.ServerSoftware := 'GeoCacher';
  FServer.KeepAlive := True;
  FServer.OnConnect := Self.OnConnect;
  FServer.OnDisconnect := Self.OnDisconnect;
  FServer.OnHeadersAvailable := Self.OnHeadersAvailable;
  FServer.OnHttpCommand := Self.OnHttpCommand;
  FServer.OnQuerySSLPort := Self.OnQuerySSLPort;
  FServer.MaxConnections := 255;
  FServer.ListenQueue := 255;

  with FServer.Bindings.Add do begin
    IP := '0.0.0.0';
    Port := VConfig.ListeningPortHttp;
  end;

  FInterceptLog := _NewInterceptLog(@VConfig.Debug);
  if Assigned(FInterceptLog) then begin
    FServer.Intercept := FInterceptLog;
  end;

  FSSLHelper := TSSLIOHandlerHelper.Create;
  FServer.IOHandler := FSSLHelper.MakeServerIOHandler(FServer);
  FSSLCertProvider := TSSLCertificateProvider.Create;
  FHostChecker := THostForSslChecker.Create(@VConfig.SslIntercept);

  FActivityStat := TServerActivityStat.Create;

  FRequestHandlerFactory := TRequestHandlerFactoryMulti.Create;

  FAppConfigChangeCounter := 0;
  FAppConfigChangeListener := TNotifyEventListener.Create(Self.OnAppConfigChange);
  FAppConfig.ChangeNotifier.Add(FAppConfigChangeListener);

  FServer.Active := True;
end;

destructor THttpServer.Destroy;
begin
  if Assigned(FAppConfig) and Assigned(FAppConfigChangeListener) then begin
    FAppConfig.ChangeNotifier.Remove(FAppConfigChangeListener);
    FAppConfigChangeListener := nil;
  end;

  FServer.Active := False;

  FreeAndNil(FServer);
  FreeAndNil(FInterceptLog);
  FreeAndNil(FSSLHelper);
  FreeAndNil(FSSLCertProvider);
  FreeAndNil(FHostChecker);
  FreeAndNil(FActivityStat);

  inherited Destroy;
end;

procedure THttpServer.OnAppConfigChange(const AStatic: IInterface);
begin
  FAppConfigLock.BeginWrite;
  try
    FAppConfigStatic := AStatic as IAppConfigStatic;
    Assert(FAppConfigStatic <> nil);
    Inc(FAppConfigChangeCounter);
  finally
    FAppConfigLock.EndWrite;
  end;

  // ToDo: restart server on ListeningPortHttp or SslIntercept changed
end;

procedure THttpServer.OnQuerySSLPort(APort: TIdPort; var VUseSSL: Boolean);
begin
  VUseSSL := False;
end;

procedure THttpServer.OnConnect(AContext: TIdContext);
var
  VCtx: THttpContext;
begin
  FActivityStat.LocalConnected;

  VCtx := THttpContext.Create(FAppConfig, FActivityStat);

  if Assigned(FInterceptLog) then begin
    VCtx.Intercept :=
      FInterceptLog.MakeClientConnectionIntercept(AContext.Connection.Tag);
  end;

  AContext.Data := VCtx;
end;

procedure THttpServer.OnDisconnect(AContext: TIdContext);
var
  VCtx: THttpContext;
begin
  if Assigned(AContext.Data) then begin
    VCtx := AContext.Data as THttpContext;
    AContext.Data := nil;
    VCtx.Free;
  end;

  FActivityStat.LocalDisconnected;
end;

function THttpServer.GetConnectionContext(AContext: TIdContext): THttpContext;
begin
  Assert(AContext.Data <> nil);
  Result := AContext.Data as THttpContext;
  Assert(Result <> nil);
end;

procedure THttpServer.SetupAppConfig(const ACtx: THttpContext);
var
  VStatic: IAppConfigStatic;
  VCounter: Integer;
begin
  if ACtx.AppConfigCounter <> FAppConfigChangeCounter then begin
    FAppConfigLock.BeginRead;
    try
      VStatic := FAppConfigStatic;
      VCounter := FAppConfigChangeCounter;
    finally
      FAppConfigLock.EndRead;
    end;
    ACtx.SetConfig(VStatic, VCounter);
  end;
end;

procedure THttpServer.OnHeadersAvailable(
  const AContext: TIdContext;
  const ARequestInfo: THTTPRequestInfo;
  const AResponseInfo: THttpResponseInfo;
  var VContinueProcessing: Boolean
);

  function _UrlFromRequestInfo(const ACtx: THttpContext): string;
  var
    I: Integer;
    VProtocol, VParams: string;
  begin
    if ARequestInfo.CommandType = hcConnect then begin
      Result := '';
      Exit;
    end;

    if ACtx.IsHttps then begin
      VProtocol := 'https://';
    end else begin
      I := Pos('://', ARequestInfo.URI);
      if I > 0 then begin
        VProtocol := LowerCase(Copy(ARequestInfo.URI, 1, I+2));
        ACtx.IsHttps := VProtocol = 'https://';
      end else begin
        VProtocol := 'http://';
      end;
    end;

    VParams := Trim(ARequestInfo.QueryParams);
    if VParams <> '' then begin
      VParams := '?' + VParams;
    end;

    Result := VProtocol + ARequestInfo.Host + ARequestInfo.Document + VParams;
  end;

  procedure _FixGoogleEarthRequest(const ACtx: THttpContext);
  const
    CHoogle = '.hoogle.';
    CGoogle = '.google.';
  var
    VHost: string;
  begin
    VHost := ARequestInfo.Host;

    if Pos(CHoogle, VHost) > 0 then begin
      ACtx.IsHttps := True;
      ACtx.IsHoogle := True;
      ACtx.IsSessionIdRequired := True;
      ARequestInfo.FURI := StringReplace(ARequestInfo.URI, CHoogle, CGoogle, [rfIgnoreCase]);
      ARequestInfo.FRawHTTPCommand := StringReplace(ARequestInfo.RawHTTPCommand, CHoogle, CGoogle, [rfIgnoreCase]);
      ARequestInfo.Host := StringReplace(VHost, CHoogle, CGoogle, [rfIgnoreCase]);
      ARequestInfo.RawHeaders.Values['Host'] := ARequestInfo.Host;
    end else
    if (Pos('kh.google.com', VHost) > 0) or
       (Pos('khmdb.google.com', VHost) > 0)
    then begin
      ACtx.IsHttps := True;
      ACtx.IsSessionIdRequired := True;
    end;
  end;

  procedure _FixGoogleEarthEnterpriseRequest(const ACtx: THttpContext);
  const
    CGee = 'ec.geocacher';
  var
    VHost: string;
  begin
    VHost := ARequestInfo.Host;

    if Pos(CGee, VHost) > 0 then begin

      if Pos('flatfile?db=', ARequestInfo.Document) > 0 then begin
        VHost := 'khmdb.google.com';
      end else begin
        VHost := 'kh.google.com';
      end;

      ARequestInfo.FURI := StringReplace(ARequestInfo.URI, CGee, VHost, [rfIgnoreCase]);
      ARequestInfo.FRawHTTPCommand := StringReplace(ARequestInfo.RawHTTPCommand, CGee, VHost, [rfIgnoreCase]);

      ARequestInfo.Host := VHost;
      ARequestInfo.RawHeaders.Values['Host'] := ARequestInfo.Host;

      ACtx.IsHttps := True;
      ACtx.IsGee := True;
      ACtx.IsSessionIdRequired := True;
    end;
  end;

var
  VCtx: THttpContext;
begin
  VContinueProcessing := True;

  VCtx := GetConnectionContext(AContext);
  VCtx.Reset;
  SetupAppConfig(VCtx);

  ARequestInfo.Host := LowerCase(ARequestInfo.Host);
  ARequestInfo.RawHeaders.Values['Host'] := ARequestInfo.Host;

  _FixGoogleEarthRequest(VCtx);
  _FixGoogleEarthEnterpriseRequest(VCtx);

  VCtx.Url := _UrlFromRequestInfo(VCtx);
  VCtx.Head := ARequestInfo.RawHeaders.Text;
  VCtx.IdCtx := AContext;
  VCtx.ReqInfo := ARequestInfo;
  VCtx.RespInfo := AResponseInfo;

  VCtx.RegEx.Expression := 'Proxy-.*?\n';
  while VCtx.RegEx.Exec(VCtx.Head) do begin
    VCtx.Head := VCtx.RegEx.Replace(VCtx.Head, '', False);
  end;
end;

procedure THttpServer.OnHttpCommand(
  const AContext: TIdContext;
  const ARequestInfo: THTTPRequestInfo;
  const AResponseInfo: THttpResponseInfo
);
var
  VInfoStr: string;
  VRequestId: Integer;
  VUrlDataEvent: Pointer;
  VDoDisconnect: Boolean;
  VDoRequestEnd: Boolean;
  VIsConnectRequest: Boolean;
  VCtx: THttpContext;
begin
  VCtx := nil;
  VDoDisconnect := False;
  VDoRequestEnd := True;

  VRequestId := InterlockedIncrement(GRequestId);

  FActivityStat.RequestBegin;
  try
    VIsConnectRequest := ARequestInfo.CommandType = hcCONNECT;

    VCtx := GetConnectionContext(AContext);
    try
      if VIsConnectRequest then begin
        VInfoStr := 'CONNECT: ' + ARequestInfo.Host;
      end else begin
        VInfoStr := VCtx.Url;
      end;

      if (FGuiEventDispatcher <> nil) and (FGuiEventDispatcher.UrlDataListenersCount > 0) then begin
        VUrlDataEvent := VCtx.UrlData.Pack(VInfoStr, VRequestId);
        FGuiEventDispatcher.ProcessUrlDataEvent(VUrlDataEvent);
      end;

      if VIsConnectRequest then begin
        OnCommandConnect(VCtx, VDoDisconnect, VDoRequestEnd);
      end else begin
        OnCommandOther(VCtx, VDoDisconnect, VDoRequestEnd);
      end;
    except
      on E: EIdSocketError do begin
        VDoRequestEnd := True;
        case E.LastError of
          Id_WSAECONNABORTED,
          Id_WSAECONNREFUSED: begin
            VDoDisconnect := True;
            VCtx.WSAError := E.LastError;
            SendWSAError(AContext, AResponseInfo, VCtx.WSAError);
            Exit;
          end;
        else
          raise;
        end;
      end;
      on E: Exception do begin
        VDoRequestEnd := True;

        GLog.Error(VInfoStr, E);

        VCtx.RespNo := 500;
        AResponseInfo.ResponseNo := 500;
        AResponseInfo.ContentText := E.Message;
        AResponseInfo.CloseConnection := True;
      end;
    end;
  finally
    if VCtx <> nil then
    try
      GuiStatisticUpdate(VCtx.Counters);
      if (FGuiEventDispatcher <> nil) and (FGuiEventDispatcher.UrlDataListenersCount > 0) then begin
        if (VCtx.WSAError = 0) and (VCtx.RespNo = 0) then begin
          VCtx.RespNo := AResponseInfo.ResponseNo;
        end;
        VUrlDataEvent :=
          VCtx.UrlData.Pack(
            VCtx.RespNo,
            VCtx.WSAError,
            AResponseInfo.ResponseText,
            VCtx.FileNameInfo,
            VCtx.Counters
          );
        FGuiEventDispatcher.ProcessUrlDataEvent(VUrlDataEvent);
      end;
    except
      //
    end;
    if VDoRequestEnd then begin
      FActivityStat.RequestEnd;
    end;
    if VDoDisconnect then begin
      AContext.Connection.Disconnect;
    end;
  end;
end;

procedure THttpServer.OnCommandOther(
  const ACtx: THttpContext;
  var ADoDisconnect: Boolean;
  var ADoRequestEnd: Boolean
);

  procedure _WriteHeaders(
    const AConnection: TIdTCPConnection;
    const AResponseNo: Integer;
    const AResponseText: string;
    const ARawHeaders: string
  );
  var
    VBufferingStarted: Boolean;
  begin
    VBufferingStarted := not AConnection.IOHandler.WriteBufferingActive;
    if VBufferingStarted then begin
      AConnection.IOHandler.WriteBufferOpen;
    end;
    try
      AConnection.IOHandler.Write(
        'HTTP/1.1 ' + IntToStr(AResponseNo) + ' ' + AResponseText + EOL +
        ARawHeaders + EOL
      );
      if VBufferingStarted then begin
        AConnection.IOHandler.WriteBufferClose;
      end;
    except
      if VBufferingStarted then begin
        AConnection.IOHandler.WriteBufferCancel;
      end;
      raise;
    end;
  end;

  function _GetUpgradeProtocol: string;
  begin
    ACtx.RegEx.Expression := '(U|u)pgrade:(.*?)\n';
    if ACtx.RegEx.Exec(ACtx.Head) then begin
      Result := ' (' + Trim(ACtx.RegEx.Match[2]) + ')';
    end else begin
      Result := '';
    end;
  end;

var
  VProtocol: string;
  VHitCount: Integer;
  VHandler: TRequestHandlerMulti;
  VDownloader: THttpDownloader;
  VTcpTunnel: TTcpTunnel;
begin
  VDownloader := nil;

  VHandler := FRequestHandlerFactory.Build(ACtx);
  try
    VHandler.OnUrlToFileNameConvert(ACtx);

    VHandler.OnBeforeRequestSend(ACtx);

    if (ACtx.RespNo = 0) and ACtx.AllowInternetRequest then begin
      if Assigned(ACtx.HttpLoader) then begin
        VDownloader := ACtx.HttpLoader as THttpDownloader;
      end else begin
        VDownloader := THttpDownloaderByIndy.Create;
        ACtx.HttpLoader := VDownloader;
      end;
      VDownloader.DoRequest(ACtx);
    end;

    VHitCount := VHandler.OnBeforeAnswerSend(ACtx);
    if (ACtx.RespNo = 0) and (VHitCount = 0) and not ACtx.AllowInternetRequest then begin
      ACtx.RespNo := 503;
      ACtx.Head := '';
    end;
  finally
    FRequestHandlerFactory.ReturnToPool(VHandler);
  end;

  ACtx.RespInfo.ContentText := '';
  ACtx.RespInfo.CloseConnection := False;

  if ACtx.RespNo <> 0 then begin
    ACtx.RespInfo.ResponseNo := ACtx.RespNo;
    if ACtx.RespNo = 101 then begin
      VProtocol := _GetUpgradeProtocol;
      if VProtocol <> '' then begin
        ACtx.RespInfo.ResponseText := ACtx.RespInfo.ResponseText + VProtocol;
      end;
    end;
  end else begin
    raise Exception.Create('Undefined "RespNo"');
  end;

  PrepareClientHeaders(
    ACtx.Head,
    ACtx.RegEx,
    ACtx.Body.Size
  );

  _WriteHeaders(
    ACtx.IdCtx.Connection,
    ACtx.RespInfo.ResponseNo,
    ACtx.RespInfo.ResponseText,
    ACtx.Head
  );
  ACtx.RespInfo.HeaderHasBeenWritten := True;

  ACtx.RespInfo.ContentStream := ACtx.Body;
  ACtx.RespInfo.ContentLength := ACtx.Body.Size;
  ACtx.RespInfo.FreeContentStream := False;

  ACtx.RespInfo.WriteContent;

  if ACtx.RespNo = 101 then begin
    Assert(VDownloader <> nil);

    VTcpTunnel :=
      TTcpTunnel.Create(
        FActivityStat,
        ACtx.IdCtx.Connection,
        VDownloader.GetRawConnection
      );
    try
      ADoRequestEnd := False;
      FActivityStat.RequestEnd;

      VTcpTunnel.Run(ACtx);
    finally
      ADoDisconnect := True;
      VTcpTunnel.Free;
    end;
  end;
end;

procedure THttpServer.OnCommandConnect(
  const ACtx: THttpContext;
  var ADoDisconnect: Boolean;
  var ADoRequestEnd: Boolean
);

  function GetHostAndPort(
    out AHost: string;
    out APort: Integer;
    const ARegEx: TRegExpr
  ): Boolean;
  begin
    Result := False;
    ARegEx.Expression := '^CONNECT (.*?):(\d+)\sHTTP/1\.\d\s*$';
    if ARegEx.Exec(ACtx.ReqInfo.RawHTTPCommand) then begin
      AHost := Trim(ARegEx.Match[1]);
      APort := StrToInt(ARegEx.Match[2]);
      Result := True;
    end else begin
      ARegEx.Expression := '(.*?):(\d+)';
      if ARegEx.Exec(ACtx.ReqInfo.Host) then begin
        AHost := ARegEx.Match[1];
        APort := StrToInt(ARegEx.Match[2]);
        Result := True;
      end;
    end;
  end;

var
  VHost: string;
  VPort: Integer;
  VTcpTunnel: TTcpTunnel;
  VSSLIOHandler: TIdSSLIOHandlerSocketOpenSSL;
  VSSLContext: TIdSSLContextExtended;
  VIntercept: TConnectionInterceptLog;
begin
  if not GetHostAndPort(VHost, VPort, ACtx.RegEx) then begin
    raise Exception.Create(
      'Error parsing host and port: ' + ACtx.ReqInfo.RawHTTPCommand
    );
  end;
  if ACtx.IsHoogle or FHostChecker.IsEnabled(VHost, VPort) then begin
    // ssl intercept for this host is enabled
    SendConnectionEstablished(ACtx.IdCtx, ACtx.RespInfo);

    VSSLIOHandler := ACtx.IdCtx.Connection.Socket as TIdSSLIOHandlerSocketOpenSSL;
    VSSLContext := VSSLIOHandler.SSLContext as TIdSSLContextExtended;

    VIntercept := VSSLIOHandler.Intercept as TConnectionInterceptLog;
    if Assigned(VIntercept) then begin
      VSSLContext.OnStatusInfoEx := VIntercept.OnStatusInfoEx;
    end;
    if ACtx.IsHoogle then begin
      VHost := StringReplace(VHost, '.google.', '.hoogle.', []);
    end;
    // set fake (self-generated) certificate and key
    VSSLContext.UseCertificate( FSSLCertProvider.GetDummyCert(VHost) );
    // begin ssl handshake with peer
    VSSLIOHandler.PassThrough := False; // can raise exceptions on ssl errors
    // and here we can handle any peer requests - MITM attak is seccessfull
    ACtx.IsHttps := True;
  end else begin
    if ACtx.AllowInternetRequest then begin
      VTcpTunnel := TTcpTunnel.Create(FActivityStat, ACtx.IdCtx.Connection);
      try
        VTcpTunnel.Connect(VHost, VPort, ACtx.ProxyConfig);
        SendConnectionEstablished(ACtx.IdCtx, ACtx.RespInfo);

        ADoRequestEnd := False;
        FActivityStat.RequestEnd;

        VTcpTunnel.Run(ACtx);
      finally
        ADoDisconnect := True;
        VTcpTunnel.Free;
      end;
    end else begin
      SendServiceUnavailable(ACtx.IdCtx, ACtx.RespInfo); // 503
    end;
  end;
end;

class procedure THttpServer.SendConnectionEstablished(
  const AContext: TIdContext;
  const AResponseInfo: THttpResponseInfo
);
begin
  AResponseInfo.ResponseNo := 200;
  AResponseInfo.ResponseText := 'Connection established (GC)';

  AContext.Connection.IOHandler.Write(
    'HTTP/1.1 200 ' + AResponseInfo.ResponseText + EOL + EOL
  );
  AContext.Connection.IOHandler.WriteBufferFlush;
  AResponseInfo.HeaderHasBeenWritten := True;
end;

class procedure THttpServer.SendServiceUnavailable(
  const AContext: TIdContext;
  const AResponseInfo: THttpResponseInfo
);
begin
  AResponseInfo.ResponseNo := 503;
  AContext.Connection.IOHandler.Write(
    'HTTP/1.1 503 ' + AResponseInfo.ResponseText + ' (GC)' + EOL + EOL
  );
  AContext.Connection.IOHandler.WriteBufferFlush;
  AResponseInfo.HeaderHasBeenWritten := True;
end;

class procedure THttpServer.SendWSAError(
  const AContext: TIdContext;
  const AResponseInfo: THttpResponseInfo;
  const AWSAError: Integer
);
begin
  case AWSAError of
    Id_WSAECONNREFUSED: AResponseInfo.ResponseNo := 502;
  else
    AResponseInfo.ResponseNo := 500;
  end;

  AContext.Connection.IOHandler.Write(
    Format(
      'HTTP/1.1 %d %s (GC)' + EOL + EOL,
      [AResponseInfo.ResponseNo, AResponseInfo.ResponseText]
    )
  );

  AContext.Connection.IOHandler.WriteBufferFlush;
  AResponseInfo.HeaderHasBeenWritten := True;
end;

end.
