unit u_ServerInterceptLog;

interface

uses
  Windows,
  Classes,
  SysUtils,
  IdCTypes,
  IdLogBase,
  IdIntercept,
  IdComponent,
  IdSSLOpenSSL,
  IdSSLOpenSSLHeaders,
  IdServerInterceptLogBase;

type
  TServerInterceptLog = class(TIdServerInterceptLogBase)
  private
    FFileStream: TFileStream;
    FWriteRawData: Boolean;
    FConnectionsGlobalIDCount: Integer;
  public
    constructor Create(const AFileName: string; const AWriteRawData: Boolean);
    destructor Destroy; override;

    procedure LogException(const E: Exception; const AConnection: TComponent = nil);

    procedure DoLogWriteString(const AText: string); override;
    function Accept(AConnection: TComponent): TIdConnectionIntercept; override;

    function MakeClientConnectionIntercept(
      const AConnectionGlobalID: Integer
    ): TIdConnectionIntercept;

    property WriteRawData: Boolean read FWriteRawData write FWriteRawData;
  end;

  TConnectionInterceptLog = class(TIdLogBase)
  private
    FHttpServerLog: TServerInterceptLog;
    FConnectionGlobalID: Integer;
    function GetLogData(const AData: string): string;
  public
    procedure LogReceivedData(const AText, AData: string); override;
    procedure LogSentData(const AText, AData: string); override;
    procedure LogStatus(const AText: string); override;
  public
    procedure OnStatus(
      ASender: TObject;
      const AStatus: TIdStatus;
      const AStatusText: string
    );
    procedure OnStatusInfoEx(
      ASender : TObject;
      const AsslSocket: PSSL;
      const AWhere, Aret: TIdC_INT;
      const AType, AMsg: string
    );
  public
    constructor Create(
      const AHttpServerLog: TServerInterceptLog;
      const AConnectionGlobalID: Integer
    );
  end;

implementation

uses
  IdGlobal,
  IdIOHandlerSocket,
  IdResourceStringsCore,
  IdTCPConnection;

function GetConnectionInfo(const AConnection: TComponent): string;
var
  LSocket: TIdIOHandlerSocket;
begin
  if Assigned(AConnection) and (AConnection is TIdTCPConnection) then begin
    LSocket := TIdTCPConnection(AConnection).Socket;
    if (LSocket <> nil) and (LSocket.Binding <> nil) then begin
      with LSocket.Binding do begin
        Result := IP + ':' + IntToStr(Port) + '/' + PeerIP + ':' + IntToStr(PeerPort);
      end;
      Exit;
    end;
  end;
  Result := '';
end;

function GetConnectionID(const AConnectionGlobalID: Integer): string; inline;
var
  VThreadID: string;
  VConnectionID: string;
begin
  VThreadID := IntToStr(GetCurrentThreadID);
  while Length(VThreadID) < 5 do begin
    VThreadID := '0' + VThreadID;
  end;
  if AConnectionGlobalID > 0 then begin
    VConnectionID := IntToHex(AConnectionGlobalID, 8);
  end else begin
    VConnectionID := '';
  end;
  Result := Format('[%s:%s]', [VConnectionID, VThreadID]);
end;

function GetTimeStamp: string; inline;
begin
  Result := FormatDateTime('yyyy-mm-dd hh:nn:ss.zzz', Now);
end;

function IsHeader(const AData: string): Boolean;
begin
  Result :=
    (Pos('GET',     AData) = 1) or
    (Pos('POST',    AData) = 1) or
    (Pos('HEAD',    AData) = 1) or
    (Pos('PUT',     AData) = 1) or
    (Pos('DELETE',  AData) = 1) or
    (Pos('TRACE',   AData) = 1) or
    (Pos('OPTIONS', AData) = 1) or
    (Pos('CONNECT', AData) = 1);

  if not Result then begin
    Result := (Pos('HTTP', AData) = 1);
  end;
end;

{ TConnectionInterceptLog }

constructor TConnectionInterceptLog.Create(
  const AHttpServerLog: TServerInterceptLog;
  const AConnectionGlobalID: Integer
);
begin
  inherited Create(nil);
  FHttpServerLog := AHttpServerLog;
  FConnectionGlobalID := AConnectionGlobalID;
  LogTime := False;
  ReplaceCRLF := False;
  Active := True;
end;

function TConnectionInterceptLog.GetLogData(const AData: string): string;
var
  I: Integer;
  VData, VEOL: string;
begin
  if FHttpServerLog.WriteRawData then begin
    Result := AData;
  end else begin
    Result := '';
    VData := AData;
    if IsHeader(AData) then begin
      I := Pos(EOL+EOL, AData);
      if I > 0 then begin
        Result := EOL + Copy(AData, 1, I + 3);
        VData := Copy(AData, I + 4, Length(AData) - (I + 3));
      end;
    end;
    if VData <> '' then begin
      VEOL := EOL;
      if Result <> '' then begin
        VEOL := VEOL + EOL;
      end;
      VData := '< ' + IntToStr(Length(VData)) + ' bytes >' + VEOL;
    end;
    Result := Result + VData;
  end;
end;

procedure TConnectionInterceptLog.LogReceivedData(const AText, AData: string);
begin
  FHttpServerLog.LogWriteString(
    GetConnectionID(FConnectionGlobalID) + ' ' + GetTimeStamp + ' ' +
    Trim(RSLogRecv) + ': ' + GetLogData(AData)
  );
end;

procedure TConnectionInterceptLog.LogSentData(const AText, AData: string);
begin
  FHttpServerLog.LogWriteString(
    GetConnectionID(FConnectionGlobalID) + ' ' + GetTimeStamp + ' ' +
    Trim(RSLogSent) + ': ' + GetLogData(AData)
  );
end;

procedure TConnectionInterceptLog.LogStatus(const AText: string);
begin
  FHttpServerLog.LogWriteString(
    GetConnectionID(FConnectionGlobalID) + ' ' + GetTimeStamp + ' ' +
    Trim(RSLogStat) + ': ' +
    AText + ' ' + GetConnectionInfo(FConnection) + EOL
  );
end;

procedure TConnectionInterceptLog.OnStatus(ASender: TObject; const AStatus: TIdStatus;
  const AStatusText: string);
begin
  LogStatus(ASender.ClassName + ' ' + AStatusText);
end;

procedure TConnectionInterceptLog.OnStatusInfoEx(
  ASender : TObject;
  const AsslSocket: PSSL;
  const AWhere, Aret: TIdC_INT;
  const AType, AMsg: string
);

  function GetProtocolVersion(const AValue: Integer): string;
  begin
    case AValue of
      SSL2_VERSION: Result := 'SSL 2.0';
      SSL3_VERSION: Result := 'SSL 3.0';
      TLS1_VERSION: Result := 'TLS 1.0';
      TLS1_1_VERSION: Result := 'TLS 1.1';
      TLS1_2_VERSION: Result := 'TLS 1.2';
    else
      Result := 'Unknown SSL/TLS version: 0x' + IntToHex(AValue, 4);
    end;
  end;

  function BeautifyStr(const AStr: string): string;
  var
    VTmp: string;
  begin
    VTmp := AStr;
    repeat
      Result := StringReplace(VTmp, '  ', ' ', [rfReplaceAll]);
      if Result = VTmp then begin
        Break;
      end else begin
        VTmp := Result;
      end;
    until False;
    Result := StringReplace(Result, #13#10, '', [rfReplaceAll]);
    Result := StringReplace(Result, #13, '', [rfReplaceAll]);
    Result := StringReplace(Result, #10, '', [rfReplaceAll]);
  end;

var
  VCipher: TIdSSLCipher;
  VSSLSocket: TIdSSLSocket;
  VInfoStr, VLogStr, VStatusStr: string;
begin
  VInfoStr :=
    GetConnectionID(FConnectionGlobalID) + ' ' + GetTimeStamp + ' ' +
    Trim(RSLogStat) + ': ' + ASender.ClassName + ' ';

  VLogStr := VInfoStr + AType + ' "' + AMsg + '"' + EOL;
  try
    if (AWhere and SSL_CB_HANDSHAKE_DONE) > 0 then begin
      if AsslSocket = nil then begin
        Exit;
      end;

      VSSLSocket := TIdSSLSocket(SSL_get_app_data(AsslSocket));
      if not Assigned(VSSLSocket) then begin
        Exit;
      end;

      VCipher := VSSLSocket.Cipher;
      if not Assigned(VCipher) then begin
        Exit;
      end;

      VStatusStr := 'Protocol: ' + GetProtocolVersion(AsslSocket.version);
      VLogStr := VLogStr + VInfoStr + VStatusStr + EOL;

      VStatusStr :=
        'Cipher: name = ' + VCipher.Name + '; ' +
        'desc = ' + VCipher.Description + '; ' +
        'bits = ' + IntToStr(VCipher.Bits) + '; ' +
        'version = ' + VCipher.Version;
      VLogStr := VLogStr + VInfoStr + BeautifyStr(VStatusStr) + EOL;
    end;
  finally
    FHttpServerLog.LogWriteString(VLogStr);
  end;
end;

{ TServerInterceptLog }

constructor TServerInterceptLog.Create(
  const AFileName: string;
  const AWriteRawData: Boolean
);
begin
  inherited Create;
  FConnectionsGlobalIDCount := 0;
  FFileStream := TFileStream.Create(AFileName, fmCreate);
  FreeAndNil(FFileStream);
  FFileStream := TFileStream.Create(AFileName, fmOpenWrite or fmShareDenyNone);
  FWriteRawData := AWriteRawData;
  Self.ReplaceCRLF := False;
end;

destructor TServerInterceptLog.Destroy;
begin
  FreeAndNil(FFileStream);
  inherited Destroy;
end;

procedure TServerInterceptLog.DoLogWriteString(const AText: string);
begin
  if AText <> '' then begin
    FFileStream.WriteBuffer(AText[1], Length(AText)*SizeOf(Char));
  end;
end;

procedure TServerInterceptLog.LogException(
  const E: Exception;
  const AConnection: TComponent
);
var
  VID: Integer;
begin
  if Assigned(AConnection) and (AConnection is TIdTCPConnection) then begin
    VID := TIdTCPConnection(AConnection).Tag;
  end else begin
    VID := -1;
  end;
  LogWriteString(
    GetConnectionID(VID) + ' ' + GetTimeStamp +
    '  Err: ' + GetConnectionInfo(AConnection) + ' ' +
    StringReplace(E.ClassName + ': ' + E.Message, #13#10, ' ', [rfReplaceAll]) +
    EOL
  );
end;

function TServerInterceptLog.Accept(AConnection: TComponent): TIdConnectionIntercept;
var
  VID: Integer;
  VConnection: TIdTCPConnection;
  VSSLIOHandler: TIdSSLIOHandlerSocketOpenSSL;
  VConnectionInterceptLog: TConnectionInterceptLog;
begin
  if Assigned(AConnection) then begin
    VID := InterlockedIncrement(FConnectionsGlobalIDCount);
    AConnection.Tag := VID;
  end else begin
    VID := -1;
  end;

  VConnectionInterceptLog := TConnectionInterceptLog.Create(Self, VID);

  if AConnection is TIdTCPConnection then begin
    VConnection := AConnection as TIdTCPConnection;
    VConnection.OnStatus := VConnectionInterceptLog.OnStatus;
    if VConnection.Socket is TIdSSLIOHandlerSocketOpenSSL then begin
      VSSLIOHandler := VConnection.Socket as TIdSSLIOHandlerSocketOpenSSL;
      VSSLIOHandler.OnStatusInfoEx := VConnectionInterceptLog.OnStatusInfoEx;
    end;
  end;

  Result := VConnectionInterceptLog;
end;

function TServerInterceptLog.MakeClientConnectionIntercept(
  const AConnectionGlobalID: Integer
): TIdConnectionIntercept;
begin
  Result := TConnectionInterceptLog.Create(Self, AConnectionGlobalID);
end;

end.
