unit u_WebClientFactory;

interface

uses
  IdHTTP,
  IdTCPClient,
  IdIOHandlerSocket,
  IdHTTPHeaderInfo,
  t_AppConfig;

type
  TWebClientFactory = record
  private
    class procedure SetupTransparentProxy(
      const AIOHandler: TIdIOHandlerSocket;
      const AProxyInfo: PProxyInfo
    ); static;
    class procedure SetupHttpTransparentProxy(
      const AIOHandler: TIdIOHandlerSocket;
      const AProxyInfo: PProxyInfo
    ); static;
    class procedure SetupSocksTransparentProxy(
      const AIOHandler: TIdIOHandlerSocket;
      const AProxyInfo: PProxyInfo
    ); static;
    class procedure SetupHttpProxy(
      const AProxyParams: TIdProxyConnectionInfo;
      const AProxyInfo: PProxyInfo
    ); static;
  public
    class function CreateTcpClient(
      const AHost: string;
      const APort: Integer;
      const AProxyInfo: PProxyInfo
    ): TIdTCPClient; static;

    class function CreateHttpClient(
      const AWithSSLHandler: Boolean;
      const AProxyInfo: PProxyInfo
    ): TIdCustomHTTP; static;
  end;

implementation

uses
  SysUtils,
  IdGlobal,
  IdSocks,
  IdSSLOpenSSL,
  IdIOHandlerStack,
  IdConnectThroughHttpProxy;

procedure _CheckHttpProxyType(const AProxyType: TProxyType); inline;
begin
  if AProxyType <> ptHttp then begin
    raise Exception.Create(
      'Not a HTTP proxy type: ' + IntToStr(Integer(AProxyType))
    );
  end;
end;

function _GetSSLVersions: TIdSSLVersions; inline;
var
  I: TIdSSLVersion;
begin
  Result := [];
  for I := Low(TIdSSLVersion) to High(TIdSSLVersion) do begin
    Include(Result, I);
  end;
end;

{ TWebClientFactory }

class function TWebClientFactory.CreateHttpClient(
  const AWithSSLHandler: Boolean;
  const AProxyInfo: PProxyInfo
): TIdCustomHTTP;
var
  VClient: TIdCustomHTTP;
  VIOHandler: TIdSSLIOHandlerSocketOpenSSL;
begin
  VClient := TIdCustomHTTP.Create(nil);
  try
    // setup ssl handler
    if AWithSSLHandler then begin
      VIOHandler := TIdSSLIOHandlerSocketOpenSSL.Create(VClient);
      VIOHandler.SSLOptions.SSLVersions := _GetSSLVersions;
      VIOHandler.SSLOptions.CipherList := 'ALL';
      VClient.IOHandler := VIOHandler;
    end;

    // setup proxy
    if AProxyInfo.Enabled then begin
      case AProxyInfo.PType of
        ptNone: begin
          // nothing to do
        end;

        ptHttp: begin
          SetupHttpProxy(VClient.ProxyParams, AProxyInfo);
        end;

        ptSocks4, ptSocks4A, ptSocks5: begin
          if not AWithSSLHandler then begin
            VClient.IOHandler := TIdIOHandlerStack.Create(VClient);
          end;
          SetupSocksTransparentProxy(
            VClient.IOHandler as TIdIOHandlerSocket,
            AProxyInfo
          );
        end;
      else
        Assert(False);
      end;
    end;

    Result := VClient;
    VClient := nil;
  finally
    VClient.Free;
  end;
end;

class function TWebClientFactory.CreateTcpClient(
  const AHost: string;
  const APort: Integer;
  const AProxyInfo: PProxyInfo
): TIdTCPClient;
var
  VClient: TIdTCPClient;
begin
  Assert(AHost <> '');
  Assert(APort > 0);

  VClient := TIdTCPClient.Create(nil);
  try
    VClient.Host := AHost;
    VClient.Port := APort;
    VClient.ConnectTimeout := IdTimeoutDefault;

    if AProxyInfo.Enabled then begin
      VClient.IOHandler := TIdIOHandlerStack.Create(VClient);
      SetupTransparentProxy(VClient.IOHandler as TIdIOHandlerSocket, AProxyInfo);
    end;

    Result := VClient;
    VClient := nil;
  finally
    VClient.Free;
  end;
end;

class procedure TWebClientFactory.SetupTransparentProxy(
  const AIOHandler: TIdIOHandlerSocket;
  const AProxyInfo: PProxyInfo
);
begin
  if not AProxyInfo.Enabled or (AProxyInfo.PType = ptNone) then begin
    Exit;
  end;
  case AProxyInfo.PType of
    ptHttp: begin
      SetupHttpTransparentProxy(AIOHandler, AProxyInfo);
    end;
    ptSocks4, ptSocks4A, ptSocks5: begin
      SetupSocksTransparentProxy(AIOHandler, AProxyInfo);
    end;
  else
    Assert(False);
  end;
end;

class procedure TWebClientFactory.SetupHttpTransparentProxy(
  const AIOHandler: TIdIOHandlerSocket;
  const AProxyInfo: PProxyInfo
);
var
  VHttpProxy: TIdConnectThroughHttpProxy;
begin
  _CheckHttpProxyType(AProxyInfo.PType);

  VHttpProxy := TIdConnectThroughHttpProxy.Create(AIOHandler);

  VHttpProxy.Host := AProxyInfo.Host;
  VHttpProxy.Port := AProxyInfo.Port;
  VHttpProxy.Username := AProxyInfo.Username;
  VHttpProxy.Password := AProxyInfo.Password;

  AIOHandler.TransparentProxy := VHttpProxy;

  // Only now set to Enabled!
  // It's reseted when TransparentProxy is assigned
  AIOHandler.TransparentProxy.Enabled := True;
end;

class procedure TWebClientFactory.SetupSocksTransparentProxy(
  const AIOHandler: TIdIOHandlerSocket;
  const AProxyInfo: PProxyInfo
);
var
  VSocksProxy: TIdSocksInfo;
begin
  VSocksProxy := TIdSocksInfo.Create(AIOHandler);
  try
    case AProxyInfo.PType of
      ptSocks4:  VSocksProxy.Version := svSocks4;
      ptSocks4A: VSocksProxy.Version := svSocks4A;
      ptSocks5:  VSocksProxy.Version := svSocks5;
    else
      raise Exception.Create(
        'Unknown SOCKS proxy type: ' + IntToStr(Integer(AProxyInfo.PType))
      );
    end;

    VSocksProxy.Host := AProxyInfo.Host;
    VSocksProxy.Port := AProxyInfo.Port;

    if AProxyInfo.Username <> '' then begin
      VSocksProxy.Authentication := saUsernamePassword;
      VSocksProxy.Username := AProxyInfo.Username;
      VSocksProxy.Password := AProxyInfo.Password;
    end else begin
      VSocksProxy.Authentication := saNoAuthentication;
    end;

    AIOHandler.TransparentProxy := VSocksProxy;
    VSocksProxy := nil;

    // Only now set to Enabled!
    // It's reseted when TransparentProxy is assigned
    AIOHandler.TransparentProxy.Enabled := True;
  finally
    VSocksProxy.Free;
  end;
end;

class procedure TWebClientFactory.SetupHttpProxy(
  const AProxyParams: TIdProxyConnectionInfo;
  const AProxyInfo: PProxyInfo
);
begin
  _CheckHttpProxyType(AProxyInfo.PType);
  
  AProxyParams.ProxyServer := AProxyInfo.Host;
  AProxyParams.ProxyPort := AProxyInfo.Port;
  AProxyParams.ProxyUsername := AProxyInfo.Username;
  AProxyParams.ProxyPassword := AProxyInfo.Password;
end;

end.
