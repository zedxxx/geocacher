unit t_CustomHTTPServer;

interface

type
  THTTPCommandType = (
    hcUnknown,
    hcHEAD,
    hcGET,
    hcPOST,
    hcDELETE,
    hcPUT,
    hcTRACE,
    hcOPTION,
    hcCONNECT,
    hcPATCH
  );

implementation

end.
