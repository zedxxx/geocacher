unit u_GuiEvent;

interface

{.$DEFINE EVENT_WITH_ID}

uses
  {$IFDEF EVENT_WITH_ID}
  Windows,
  {$ENDIF}
  SysUtils;

const
  EM_MAGIC = $454D3033; // 'EM03'

const
  EM_OK               = 01;

  EM_GET_STAT         = 02;
  EM_STAT             = 03;

  EM_START_MONITOR    = 04;
  EM_MONITOR_STARTED  = 05;

  EM_STOP_MONITOR     = 06;
  EM_MONITOR_STOPPED  = 07;

  EM_GET_VERSION      = 08;
  EM_VERSION          = 09;

  EM_GET_OPTIONS      = 10;
  EM_SET_OPTIONS      = 11;
  EM_OPTIONS          = 12;

  EM_URL_DATA_1       = 13;
  EM_URL_DATA_2       = 14;
  EM_URL_DATA_3       = 15;

  EM_PING             = 16;
  EM_PONG             = 17;

type
  TEventMessage = record
  private
    type
      TEventMessageHead = packed record
        Magic    : Integer;
        Command  : Integer;
        ID       : Integer;
        DataSize : Integer;
      end;
      PEventMessageHead = ^TEventMessageHead;
  public
    const HeadSize = SizeOf(TEventMessageHead);
  public
    class function Validate(const AEvent: Pointer): Boolean; overload; inline; static;

    class function Alloc(const ACommand: Integer; const ADataSize: Integer): Pointer; static;
    class procedure Free(const AEvent: Pointer); inline; static;

    class function Copy(const AEvent: Pointer): Pointer; inline; static;

    class function Magic(const AEvent: Pointer): Integer; inline; static;
    class function Command(const AEvent: Pointer): Integer; inline; static;
    class function ID(const AEvent: Pointer): Integer; inline; static;
    class function DataSize(const AEvent: Pointer): Integer; inline; static;
    class function RawSize(const AEvent: Pointer): Integer; inline; static;
    class function Data(const AEvent: Pointer): PByte; inline; static;

    class function CommandToText(const ACommand: Integer): string; static;
  end;

  EEventMessage = class(Exception);

implementation

{$IFDEF EVENT_WITH_ID}
var
  GEventID: Integer = 0;
{$ENDIF}

procedure _CheckSize(const ASize: Integer); inline;
begin
  if ASize < 0 then begin
    raise EEventMessage.Create('Invalid "Size" value!');
  end;
end;

class function TEventMessage.Validate(const AEvent: Pointer): Boolean;
begin
  Result := (AEvent <> nil) and (PInteger(AEvent)^ = EM_MAGIC);
end;

class function TEventMessage.Alloc(const ACommand: Integer; const ADataSize: Integer): Pointer;
var
  P: PEventMessageHead;
begin
  _CheckSize(ADataSize);
  GetMem(Result, HeadSize + ADataSize);
  P := PEventMessageHead(Result);
  P.Magic := EM_MAGIC;
  P.Command := ACommand;
  P.ID := {$IFDEF EVENT_WITH_ID} InterlockedIncrement(GEventID) {$ELSE} 0 {$ENDIF};
  P.DataSize := ADataSize;
end;

class procedure TEventMessage.Free(const AEvent: Pointer);
begin
  FreeMem(AEvent);
end;

class function TEventMessage.Copy(const AEvent: Pointer): Pointer;
var
  VRawSize: Integer;
begin
  VRawSize := RawSize(AEvent);
  _CheckSize(VRawSize);
  GetMem(Result, VRawSize);
  Move(AEvent^, Result^, VRawSize);
end;

class function TEventMessage.Magic(const AEvent: Pointer): Integer;
begin
  Result := PEventMessageHead(AEvent).Magic;
end;

class function TEventMessage.Command(const AEvent: Pointer): Integer;
begin
  Result := PEventMessageHead(AEvent).Command;
end;

class function TEventMessage.ID(const AEvent: Pointer): Integer;
begin
  Result := PEventMessageHead(AEvent).ID;
end;

class function TEventMessage.DataSize(const AEvent: Pointer): Integer;
begin
  Result := PEventMessageHead(AEvent).DataSize;
  _CheckSize(Result);
end;

class function TEventMessage.RawSize(const AEvent: Pointer): Integer;
begin
  Result := HeadSize + PEventMessageHead(AEvent).DataSize;
   _CheckSize(Result);
end;

class function TEventMessage.Data(const AEvent: Pointer): PByte;
begin
  if PEventMessageHead(AEvent).DataSize > 0 then begin
    Result := AEvent;
    Inc(Result, HeadSize);
  end else begin
    Result := nil;
  end;
end;

class function TEventMessage.CommandToText(const ACommand: Integer): string;
begin
  case ACommand of
    EM_OK              : Result := 'OK';

    EM_GET_STAT        : Result := 'GET_STAT';
    EM_STAT            : Result := 'STAT';

    EM_START_MONITOR   : Result := 'START_MONITOR';
    EM_MONITOR_STARTED : Result := 'MONITOR_STARTED';

    EM_STOP_MONITOR    : Result := 'STOP_MONITOR';
    EM_MONITOR_STOPPED : Result := 'MONITOR_STOPPED';

    EM_GET_VERSION     : Result := 'GET_VERSION';
    EM_VERSION         : Result := 'VERSION';

    EM_GET_OPTIONS     : Result := 'GET_OPTIONS';
    EM_SET_OPTIONS     : Result := 'SET_OPTIONS';
    EM_OPTIONS         : Result := 'OPTIONS';

    EM_URL_DATA_1      : Result := 'URL_DATA_1';
    EM_URL_DATA_2      : Result := 'URL_DATA_2';
    EM_URL_DATA_3      : Result := 'URL_DATA_3';

    EM_PING            : Result := 'PING';
    EM_PONG            : Result := 'PONG'; 
  else
    Result := '0x' + IntToHex(ACommand, 8);
  end;
end;

end.
