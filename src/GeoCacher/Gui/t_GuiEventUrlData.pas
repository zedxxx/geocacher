unit t_GuiEventUrlData;

interface

uses
  t_HttpRequestCounters;

type
  TUrlDataEventType = (
    etNew,
    etUpdate,
    etDone
  );

  TGuiEventUrlDataRec = record
    EventType : TUrlDataEventType;

    RequestId : Integer;

    RespNo    : Cardinal;
    WSAError  : Cardinal;

    Url       : string;
    RespText  : string;
    Hint      : string;

    Counters  : THttpRequestCounters;
  end;
  PGuiEventUrlDataRec = ^TGuiEventUrlDataRec;

implementation

end.
