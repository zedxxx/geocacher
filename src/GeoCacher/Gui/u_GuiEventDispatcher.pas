unit u_GuiEventDispatcher;

interface

uses
  Windows,
  Classes,
  SysUtils,
  i_AppConfig,
  i_GuiEventDispatcher,
  u_HttpContext;

type
  TOnConfigChangeEvent =
    procedure(
      const ASenderID: THandle;
      const AOptName: AnsiString
    ) of object;

  TGuiEventDispatcher = class(TInterfacedObject, IGuiEventDispatcher)
  private
    type
      TListenerRec = record
        ID: THandle;
        Opt: TOnSendToClientEvent;
        UrlData: TOnSendToClientEvent;
      end;
      TConnCountStat = record
        InCount: Integer;
        OutCount: Integer;
      end;
  private
    FAppConfig: IAppConfig;

    FLock: IReadWriteSync;
    FListener: array of TListenerRec;

    FUrlDataListeners: Integer;
    FOptChangeListeners: Integer;

    FConnCountStat: TConnCountStat;

    procedure AddListener(
      const AListenerID: THandle;
      const AOnOptChangeEvent: TOnSendToClientEvent;
      const AOnUrlDataEvent: TOnSendToClientEvent
    );

    procedure DeleteListener(const AIndex: Integer);

    function OptionsToEventMessage: Pointer;

  private
    { IGuiEventDispatcher }
    procedure AddUrlDataListener(
      const AListenerID: THandle;
      const AOnSendToClientEvent: TOnSendToClientEvent
    );

    procedure RemoveUrlDataListener(
      const AListenerID: THandle
    );

    function GetUrlDataListenersCount: Integer;

    procedure AddOptionsChangeListener(
      const AListenerID: THandle;
      const AOnSendToClientEvent: TOnSendToClientEvent
    );

    procedure RemoveOptionsChangeListener(
      const AListenerID: THandle
    );

    procedure RemoveAllListeners(
      const AListenerID: THandle
    );

    procedure ProcessUrlDataEvent(
      const AEvent: Pointer
    );

    procedure ProcessOptionsChange(
      const ASenderID: THandle
    );

    function Process(
      const ASenderID: THandle;
      const AEvent: Pointer;
      out AResponse: Pointer
    ): Boolean;

    procedure SetConnCountStat(
      const AInConnCount: Integer;
      const AOutConnCount: Integer
    );
  public
    constructor Create(
      const AAppConfig: IAppConfig
    );
  end;

implementation

uses
  u_AppVersionInfo,
  u_Synchronizer,
  u_GuiEvent,
  u_GuiStatistic;

{ TGuiEventDispatcher }

constructor TGuiEventDispatcher.Create(
  const AAppConfig: IAppConfig
);
begin
  inherited Create;

  FAppConfig := AAppConfig;

  FLock := GSync.SyncVariable.Make;
  FListener := nil;
  FUrlDataListeners := 0;
  FOptChangeListeners := 0;
  FillChar(FConnCountStat, SizeOf(TConnCountStat), 0);
end;

procedure TGuiEventDispatcher.AddListener(
  const AListenerID: THandle;
  const AOnOptChangeEvent: TOnSendToClientEvent;
  const AOnUrlDataEvent: TOnSendToClientEvent
);
var
  I: Integer;
  VFound: Boolean;
begin
  FLock.BeginWrite;
  try
    VFound := False;
    for I := Low(FListener) to High(FListener) do begin
      if FListener[I].ID = AListenerID then begin
        if Assigned(AOnOptChangeEvent) then begin
          FListener[I].Opt := AOnOptChangeEvent;
          Inc(FOptChangeListeners);
        end;
        if Assigned(AOnUrlDataEvent) then begin
          FListener[I].UrlData := AOnUrlDataEvent;
          Inc(FUrlDataListeners);
        end;
        VFound := True;
        Break;
      end;
    end;
    if not VFound then begin
      I := Length(FListener);
      SetLength(FListener, I + 1);
      FListener[I].ID := AListenerID;
      FListener[I].Opt := AOnOptChangeEvent;
      FListener[I].UrlData := AOnUrlDataEvent;
    end;
  finally
    FLock.EndWrite;
  end;
end;

procedure TGuiEventDispatcher.AddOptionsChangeListener(
  const AListenerID: THandle;
  const AOnSendToClientEvent: TOnSendToClientEvent
);
begin
  Assert(Assigned(AOnSendToClientEvent));
  AddListener(AListenerID, AOnSendToClientEvent, nil);
end;

procedure TGuiEventDispatcher.AddUrlDataListener(
  const AListenerID: THandle;
  const AOnSendToClientEvent: TOnSendToClientEvent
);
begin
  Assert(Assigned(AOnSendToClientEvent));
  AddListener(AListenerID, nil, AOnSendToClientEvent);
end;

procedure TGuiEventDispatcher.DeleteListener(const AIndex: Integer);
var
  I: Integer;
  VCount: Integer;
begin
  VCount := Length(FListener);

  Assert(VCount > 0);
  Assert(AIndex < VCount);

  I := VCount - AIndex;
  if I > 0 then begin
    Move(FListener[AIndex + 1], FListener[AIndex], SizeOf(TListenerRec) * I);
  end;

  SetLength(FListener, VCount - 1);
end;

procedure TGuiEventDispatcher.RemoveAllListeners(const AListenerID: THandle);
var
  I: Integer;
begin
  FLock.BeginWrite;
  try
    for I := Low(FListener) to High(FListener) do begin
      if FListener[I].ID = AListenerID then begin
        if Assigned(FListener[I].Opt) then begin
          Dec(FOptChangeListeners);
        end;
        if Assigned(FListener[I].UrlData) then begin
          Dec(FUrlDataListeners);
        end;
        DeleteListener(I);
        Break;
      end;
    end;
  finally
    FLock.EndWrite;
  end;
end;

procedure TGuiEventDispatcher.RemoveOptionsChangeListener(const AListenerID: THandle);
var
  I: Integer;
begin
  FLock.BeginWrite;
  try
    for I := Low(FListener) to High(FListener) do begin
      if FListener[I].ID = AListenerID then begin
        if not Assigned(FListener[I].UrlData) then begin
          DeleteListener(I);
        end else begin
          FListener[I].Opt := nil;
          Dec(FOptChangeListeners);
        end;
        Break;
      end;
    end;
  finally
    FLock.EndWrite;
  end;
end;

procedure TGuiEventDispatcher.RemoveUrlDataListener(const AListenerID: THandle);
var
  I: Integer;
begin
  FLock.BeginWrite;
  try
    for I := Low(FListener) to High(FListener) do begin
      if FListener[I].ID = AListenerID then begin
        if not Assigned(FListener[I].Opt) then begin
          DeleteListener(I);
        end else begin
          FListener[I].UrlData := nil;
          Dec(FUrlDataListeners);
        end;
        Break;
      end;
    end;
  finally
    FLock.EndWrite;
  end;
end;

function TGuiEventDispatcher.GetUrlDataListenersCount: Integer;
begin
  Result := FUrlDataListeners;
end;

procedure TGuiEventDispatcher.ProcessOptionsChange(const ASenderID: THandle);
var
  I: Integer;
  VEvent: Pointer;
begin
  if FOptChangeListeners = 0 then begin
    Exit;
  end;

  VEvent := nil;

  FLock.BeginRead;
  try
    for I := Low(FListener) to High(FListener) do begin
      if (FListener[I].ID <> ASenderID) and Assigned(FListener[I].Opt) then begin
        if VEvent = nil then begin
          VEvent := OptionsToEventMessage;
        end else begin
          VEvent := TEventMessage.Copy(VEvent);
        end;
        FListener[I].Opt(VEvent);
      end;
    end;
  finally
    FLock.EndRead;
  end;
end;

procedure TGuiEventDispatcher.ProcessUrlDataEvent(const AEvent: Pointer);
var
  I: Integer;
  VEvent: Pointer;
begin
  if FUrlDataListeners = 0 then begin
    TEventMessage.Free(AEvent);
    Exit;
  end;

  FLock.BeginRead;
  try
    VEvent := nil;
    for I := Low(FListener) to High(FListener) do begin
      if Assigned(FListener[I].UrlData) then begin
        if VEvent = nil then begin
          VEvent := AEvent;
        end else begin
          VEvent := TEventMessage.Copy(VEvent);
        end;
        FListener[I].UrlData(VEvent);
      end;
    end;
  finally
    FLock.EndRead;
  end;
end;

function TGuiEventDispatcher.Process(
  const ASenderID: THandle;
  const AEvent: Pointer;
  out AResponse: Pointer
): Boolean;
var
  I: Integer;
  P: PByte;
  VDataSize: Cardinal;
  VStream: TMemoryStream;
  VAppVersion: AnsiString;
begin
  Result := False;
  AResponse := nil;

  if not TEventMessage.Validate(AEvent) then begin
    Exit;
  end;

  case TEventMessage.Command(AEvent) of

    EM_GET_STAT: begin
      // ������ �� ������� ���������� ������ GeoCacher-�
      VDataSize := SizeOf(GStatistic) + SizeOf(FConnCountStat.InCount) * 2;
      AResponse := TEventMessage.Alloc(EM_STAT, VDataSize);
      P := TEventMessage.Data(AResponse);
      I := SizeOf(GStatistic);
      Move(GStatistic, P^, I);
      Inc(P, I);
      I := SizeOf(FConnCountStat.InCount);
      Move(FConnCountStat.InCount, P^, I);
      Inc(P, I);
      Move(FConnCountStat.OutCount, P^, I);
    end;

    EM_START_MONITOR: begin
      // ������ �� ������� �� ������ ��������� ������ ����������� �������
      AResponse := TEventMessage.Alloc(EM_MONITOR_STARTED, 0);
    end;

    EM_STOP_MONITOR: begin
      // ������ �� ������� �� ����� ��������� ������ ����������� �������
      AResponse := TEventMessage.Alloc(EM_MONITOR_STOPPED, 0);
    end;

    EM_GET_OPTIONS: begin
      // ������ �� ������� �� ��������� ������������
      AResponse := OptionsToEventMessage;
    end;

    EM_GET_VERSION: begin
      // ������ �� ������� ������� ������ GeoCaher-a
      VAppVersion := AnsiString(GetAppVersionText);
      VDataSize := Length(VAppVersion) + 1;
      AResponse := TEventMessage.Alloc(EM_VERSION, VDataSize * 2);
      P := TEventMessage.Data(AResponse);
      Move(PAnsiChar(VAppVersion)^, P^, VDataSize); // current version
      Inc(P, VDataSize);
      Move(PAnsiChar(VAppVersion)^, P^, VDataSize); // available version
    end;

    EM_SET_OPTIONS: begin
      // � GUI �������� ������
      P := TEventMessage.Data(AEvent);
      I := TEventMessage.DataSize(AEvent);
      if (P <> nil) and (I > 0) then begin
        VStream := TMemoryStream.Create;
        try
          VStream.WriteBuffer(P^, I);
          VStream.Position := 0;
          FAppConfig.LoadFromStream(VStream);
        finally
          VStream.Free;
        end;
      end;
    end;
  end;

  Result := AResponse <> nil;
end;

function TGuiEventDispatcher.OptionsToEventMessage: Pointer;
var
  P: PByte;
  VDataSize: Integer;
  VStream: TMemoryStream;
begin
  VStream := TMemoryStream.Create;
  try
    FAppConfig.SaveToStream(VStream);

    VDataSize := VStream.Size;
    Result := TEventMessage.Alloc(EM_OPTIONS, VDataSize);

    P := TEventMessage.Data(Result);
    Move(VStream.Memory^, P^, VDataSize);
  finally
    VStream.Free;
  end;
end;

procedure TGuiEventDispatcher.SetConnCountStat(
  const AInConnCount: Integer;
  const AOutConnCount: Integer
);
begin
  FConnCountStat.InCount := AInConnCount;
  FConnCountStat.OutCount := AOutConnCount;
end;

end.
