unit i_GuiEventDispatcher;

interface

uses
  u_HttpContext;

type
  TOnSendToClientEvent = procedure(const AEvent: Pointer) of object;

  IGuiEventDispatcher = interface
    ['{482694DF-6DB2-4F95-9004-AABEC127D768}']
    procedure AddUrlDataListener(
      const AListenerID: THandle;
      const AOnSendToClientEvent: TOnSendToClientEvent
    );

    procedure RemoveUrlDataListener(
      const AListenerID: THandle
    );

    function GetUrlDataListenersCount: Integer;
    property UrlDataListenersCount: Integer read GetUrlDataListenersCount;

    procedure AddOptionsChangeListener(
      const AListenerID: THandle;
      const AOnSendToClientEvent: TOnSendToClientEvent
    );

    procedure RemoveOptionsChangeListener(
      const AListenerID: THandle
    );

    procedure RemoveAllListeners(
      const AListenerID: THandle
    );

    procedure ProcessUrlDataEvent(
      const AEvent: Pointer
    );

    procedure ProcessOptionsChange(
      const ASenderID: THandle
    );

    function Process(
      const ASenderID: THandle;
      const AEvent: Pointer;
      out AResponse: Pointer
    ): Boolean;

    procedure SetConnCountStat(
      const AInConnCount: Integer;
      const AOutConnCount: Integer
    );
  end;

implementation

end.
