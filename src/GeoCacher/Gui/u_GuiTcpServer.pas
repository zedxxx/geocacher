unit u_GuiTcpServer;

interface

uses
  i_AppConfig,
  i_GuiEventDispatcher;

procedure GuiServerStart(
  const AAppConfig: IAppConfig;
  const AEventDispatcher: IGuiEventDispatcher
);

procedure GuiServerStop;

implementation

uses
  ScktComp,
  SysUtils,
  i_Listener,
  u_ListenerByEvent,
  u_GlobalLog,
  u_GuiClientThread;

type
  TGuiTcpServer = class
  private
    FAppConfig: IAppConfig;
    FAppConfigChangeListener: IListener;

    FServer: TServerSocket;
    FEventDispatcher: IGuiEventDispatcher;
  private
    procedure OnConfigChange(
      const AStatic: IInterface
    );
    procedure OnGetThread(
      Sender: TObject;
      ClientSocket: TServerClientWinSocket;
      var SocketThread: TServerClientThread
    );
    procedure OnClientError(
      Sender: TObject;
      Socket: TCustomWinSocket;
      ErrorEvent: TErrorEvent;
      var ErrorCode: Integer
    );
  public
    constructor Create(
      const AAppConfig: IAppConfig;
      const AEventDispatcher: IGuiEventDispatcher
    );
    destructor Destroy; override;
  end;

var
  GTcpServer: TGuiTcpServer = nil;

procedure GuiServerStart(
  const AAppConfig: IAppConfig;
  const AEventDispatcher: IGuiEventDispatcher
);
begin
  try
    GTcpServer := TGuiTcpServer.Create(AAppConfig, AEventDispatcher);
  except
    on E: Exception do begin
      GLog.ShowExceptionMessage(E, 'Failed to start GuiServer!');
    end;
  end;
end;

procedure GuiServerStop;
begin
  FreeAndNil(GTcpServer);
end;

{ TGuiTcpServer }

constructor TGuiTcpServer.Create(
  const AAppConfig: IAppConfig;
  const AEventDispatcher: IGuiEventDispatcher
);
begin
  Assert(AEventDispatcher <> nil);
  inherited Create;

  FAppConfig := AAppConfig;
  FEventDispatcher := AEventDispatcher;

  FAppConfigChangeListener := TNotifyEventListener.Create(Self.OnConfigChange);
  FAppConfig.ChangeNotifier.Add(FAppConfigChangeListener);

  FServer := TServerSocket.Create(nil);

  FServer.Port := FAppConfig.GetStatic.RecPtr.ListeningPortTcp;
  FServer.ServerType := stThreadBlocking;
  FServer.ThreadCacheSize := 0;

  FServer.OnGetThread := Self.OnGetThread;
  FServer.OnClientError := Self.OnClientError;

  FServer.Open;
end;

destructor TGuiTcpServer.Destroy;
begin
  try
    if Assigned(FAppConfig) and Assigned(FAppConfigChangeListener) then begin
      FAppConfig.ChangeNotifier.Remove(FAppConfigChangeListener);
      FAppConfigChangeListener := nil;
    end;
    FServer.Close;
    FreeAndNil(FServer);
  except
    on E: Exception do begin
      GLog.Error(Self.ClassName, E);
    end;
  end;

  inherited Destroy;
end;

procedure TGuiTcpServer.OnConfigChange(const AStatic: IInterface);
var
  VStatic: IAppConfigStatic;
  VPortNew: Integer;
begin
  VStatic := AStatic as IAppConfigStatic;
  Assert(VStatic <> nil);

  VPortNew := VStatic.RecPtr.ListeningPortTcp;
  if VPortNew <> FServer.Port then begin
    // restart server
    FServer.Close;
    FServer.Port := VPortNew;
    FServer.Open;
  end;
end;

procedure TGuiTcpServer.OnClientError(
  Sender: TObject;
  Socket: TCustomWinSocket;
  ErrorEvent: TErrorEvent;
  var ErrorCode: Integer
);
begin
  ErrorCode := 0;
end;

procedure TGuiTcpServer.OnGetThread(
  Sender: TObject;
  ClientSocket: TServerClientWinSocket;
  var SocketThread: TServerClientThread
);
begin
  SocketThread :=
    TGuiClientThread.Create(
      ClientSocket,
      FAppConfig,
      FEventDispatcher
    );

  {$IFDEF UNICODE}
  SocketThread.Start;
  {$ELSE}
  SocketThread.Resume;
  {$ENDIF}
end;

end.
