unit u_GuiSocketStream;

interface

uses
  Windows,
  SyncObjs,
  ScktComp,
  WinSock,
  SysUtils,
  Classes;

type
  TGuiSocketStreamIoResult = (
    ssrOk,
    ssrTimeout,
    ssrSocketError,
    ssrDisconnected,
    ssrUndef
  );

  TGuiSocketStream = class
  private
    FSocket: TCustomWinSocket;
    FTimeout: Longint;
    FEvent: TSimpleEvent;
    FIoResult: TGuiSocketStreamIoResult;
  public
    constructor Create(const ASocket: TCustomWinSocket; const ATimeOut: Longint);
    destructor Destroy; override;

    function WaitForData(const ATimeout: Longint): Boolean;

    function Read(var Buffer; const ACount: Longint): Cardinal;
    function Write(const Buffer; const ACount: Longint): Cardinal;
    
    property IoResult: TGuiSocketStreamIoResult read FIoResult;
  end;

implementation

uses
  RTLConsts;

{ TGuiSocketStream }

constructor TGuiSocketStream.Create(
  const ASocket: TCustomWinSocket;
  const ATimeOut: Longint
);
begin
  inherited Create;
  if ASocket.ASyncStyles <> [] then begin
    raise ESocketError.CreateRes(@sSocketMustBeBlocking);
  end;
  FSocket := ASocket;
  FTimeOut := ATimeOut;
  FEvent := TSimpleEvent.Create;
  FIoResult := ssrUndef;
end;

destructor TGuiSocketStream.Destroy;
begin
  FreeAndNil(FEvent);
  inherited Destroy;
end;

function TGuiSocketStream.WaitForData(const ATimeout: Longint): Boolean;
var
  FDSet: TFDSet;
  VTimeVal: TTimeVal;
begin
  VTimeVal.tv_sec := ATimeout div 1000;
  VTimeVal.tv_usec := (ATimeout mod 1000) * 1000;
  FD_ZERO(FDSet);
  FD_SET(FSocket.SocketHandle, FDSet);
  Result := select(0, @FDSet, nil, nil, @VTimeVal) > 0;
end;

function TGuiSocketStream.Read(var Buffer; const ACount: Longint): Cardinal;
var
  VOverlapped: TOverlapped;
  VErrorCode: Cardinal;
begin
  Result := 0;
  FIoResult := ssrUndef;

  FSocket.Lock;
  try
    if not FSocket.Connected then begin
      Result := 0;
      FIoResult := ssrDisconnected;
      Exit;
    end;
    FillChar(VOverlapped, SizeOf(VOverlapped), 0);
    VOverlapped.hEvent := FEvent.Handle;
    if not ReadFile(FSocket.SocketHandle, Buffer, ACount, Result, @VOverlapped) then begin
      VErrorCode := GetLastError;
      if VErrorCode = ERROR_IO_PENDING then begin
        // ok
      end else
      if VErrorCode = ERROR_NETNAME_DELETED then begin
        Result := 0;
        FIoResult := ssrSocketError;
        Exit;
      end else begin
        raise ESocketError.CreateResFmt(
          @sSocketIOError, [sSocketRead, VErrorCode, SysErrorMessage(VErrorCode)]
        );
      end;
    end;
    if FEvent.WaitFor(FTimeOut) = wrSignaled then begin
      if GetOverlappedResult(FSocket.SocketHandle, VOverlapped, Result, False) then begin
        FIoResult := ssrOk;
      end else begin
        Result := 0;
        FIoResult := ssrSocketError;
      end;
    end else begin
      Result := 0;
      FIoResult := ssrTimeout;
    end;
  finally
    FSocket.Unlock;
  end;
end;

function TGuiSocketStream.Write(const Buffer; const ACount: Longint): Cardinal;
var
  VOverlapped: TOverlapped;
  VErrorCode: Integer;
begin
  Result := 0;
  FIoResult := ssrUndef;

  FSocket.Lock;
  try
    if not FSocket.Connected then begin
      Result := 0;
      FIoResult := ssrDisconnected;
      Exit;
    end;
    FillChar(VOverlapped, SizeOf(VOverlapped), 0);
    VOverlapped.hEvent := FEvent.Handle;
    if not WriteFile(FSocket.SocketHandle, Buffer, ACount, Result, @VOverlapped) then begin
      VErrorCode := GetLastError;
      if VErrorCode = ERROR_IO_PENDING then begin
        // ok
      end else
      if VErrorCode = ERROR_NETNAME_DELETED then begin
        Result := 0;
        FIoResult := ssrSocketError;
        Exit;
      end else begin
        raise ESocketError.CreateResFmt(@sSocketIOError, [sSocketWrite, VErrorCode,
          SysErrorMessage(VErrorCode)]);
      end;
    end;
    if FEvent.WaitFor(FTimeOut) = wrSignaled then begin
      if GetOverlappedResult(FSocket.SocketHandle, VOverlapped, Result, False) then begin
        FIoResult := ssrOk;
      end else begin
        Result := 0;
        FIoResult := ssrSocketError;
      end;
    end else begin
      Result := 0;
      FIoResult := ssrTimeout;
      // ToDo: cancel operation
    end;
  finally
    FSocket.Unlock;
  end;
end;

end.
