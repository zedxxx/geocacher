unit u_SendQueueSimple;

interface

uses
  Windows,
  SysUtils;

type
  TQueueArray = array of Pointer;
  PQueueArray = ^TQueueArray;

  TSendQueueSimple = class
  private
    type
      TQueueItemRec = record
        Arr: TQueueArray;
        Count: Integer;
        Capacity: Integer;
        IsEmpty: Boolean;
      end;
      PQueueItemRec = ^TQueueItemRec;
  private
    FLock: IReadWriteSync;
    FQueue: array [Boolean] of TQueueItemRec;
    FIndex: Boolean;
  public
    procedure Enqueue(const AEvent: Pointer); inline;
    function TryEnqueue(const AEvent: Pointer): Boolean; inline;
    procedure Dequeue(out AEvents: PQueueArray; out ACount: Integer); inline;
    function IsEmpty: Boolean; inline;
  public
    constructor Create;
  end;

implementation

uses
  u_Synchronizer;

const
  CInitialCapacity = 4*1024;

{ TSendQueueSimple }

constructor TSendQueueSimple.Create;
var
  I: Boolean;
begin
  inherited Create;

  FLock := GSync.SyncStd.Make;

  for I := Low(FQueue) to High(FQueue) do begin
    FQueue[I].IsEmpty := True;
    FQueue[I].Count := 0;
    FQueue[I].Capacity := CInitialCapacity;
    SetLength(FQueue[I].Arr, FQueue[I].Capacity);
  end;

  FIndex := False;
end;

function TSendQueueSimple.TryEnqueue(const AEvent: Pointer): Boolean;
var
  I: Integer;
  VQueue: PQueueItemRec;
begin
  Result := False;

  FLock.BeginRead;
  try
    VQueue := @FQueue[FIndex];
    I := InterlockedIncrement(VQueue.Count);
    if I <= VQueue.Capacity then begin
      VQueue.Arr[I-1] := AEvent;
      VQueue.IsEmpty := False;
      Result := True;
    end;
  finally
    FLock.EndRead;
  end;
end;

procedure TSendQueueSimple.Enqueue(const AEvent: Pointer);
const
  CGrowCount = 1024;
var
  VQueue: PQueueItemRec;
begin
  FLock.BeginWrite;
  try
    VQueue := @FQueue[FIndex];
    Inc(VQueue.Count);
    if VQueue.Count > VQueue.Capacity then begin
      VQueue.Count := VQueue.Capacity + 1;
      SetLength(VQueue.Arr, VQueue.Capacity + CGrowCount);
      Inc(VQueue.Capacity, CGrowCount);
      //OutputDebugString('TSendQueueSimple -> Grow');
    end;
    VQueue.Arr[VQueue.Count-1] := AEvent;
    VQueue.IsEmpty := False;
  finally
    FLock.EndWrite;
  end;
end;

procedure TSendQueueSimple.Dequeue(out AEvents: PQueueArray; out ACount: Integer);
var
  VQueue: PQueueItemRec;
begin
  AEvents := nil;
  ACount := 0;

  VQueue := @FQueue[FIndex];

  if VQueue.IsEmpty then begin
    Exit;
  end;

  FLock.BeginWrite;
  try
    FIndex := not FIndex;
  finally
    FLock.EndWrite;
  end;

  AEvents := @VQueue.Arr;
  ACount := VQueue.Count;
  if ACount > VQueue.Capacity then begin
    ACount := VQueue.Capacity;
  end;

  VQueue.Count := 0;
  VQueue.IsEmpty := True;
end;

function TSendQueueSimple.IsEmpty: Boolean;
begin
  FLock.BeginWrite;
  try
    Result := FQueue[False].IsEmpty and FQueue[True].IsEmpty;
  finally
    FLock.EndWrite;
  end;
end;

end.
