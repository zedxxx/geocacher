unit u_GuiStatistic;

interface

uses
  t_HttpRequestCounters;

type
  TGuiStatistic = packed record
    InetHead   : UInt64;
    InetData   : UInt64;
    SentHead   : UInt64;
    SentData   : UInt64;
    CacheRead  : UInt64;
    CacheWrite : UInt64;
  end;

procedure GuiStatisticUpdate(const ACounters: PHttpRequestCounters);

var
  GStatistic: TGuiStatistic;

implementation

uses
  SysUtils,
  u_Synchronizer;

var
  GLock: IReadWriteSync = nil;

procedure GuiStatisticUpdate(const ACounters: PHttpRequestCounters);
begin
  GLock.BeginWrite;
  try
    Inc(GStatistic.InetHead, ACounters.DownHead);
    Inc(GStatistic.InetData, ACounters.DownBody);
    Inc(GStatistic.SentHead, ACounters.UpHead);
    Inc(GStatistic.SentData, ACounters.UpBody);
    Inc(GStatistic.CacheRead, ACounters.CacheRead);
    Inc(GStatistic.CacheWrite, ACounters.CacheWrite);
  finally
    GLock.EndWrite;
  end;
end;

initialization
  GLock := GSync.SyncVariable.Make;
  FillChar(GStatistic, SizeOf(GStatistic), 0);

finalization
  GLock := nil;

end.
