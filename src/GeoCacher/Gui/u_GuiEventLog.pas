unit u_GuiEventLog;

interface

uses
  Windows,
  Classes,
  SysUtils,
  SyncObjs;

type
  IUniqIdGenerator = interface
    ['{AE4B3110-B517-4BE4-A2AD-0C7EAF98988D}']
    function MakeNewId: string;
  end;

  TUniqIdGeneratorByDate = class(TInterfacedObject, IUniqIdGenerator)
  private
    { IUniqIdGenerator }
    function MakeNewId: string;
  end;

  TUniqIdGeneratorByCounter = class(TInterfacedObject, IUniqIdGenerator)
  private
    class var FCounter: Integer;
    class procedure ResetCounter;
  private
    { IUniqIdGenerator }
    function MakeNewId: string;
  end;

  IGuiEventLog = interface
    ['{400E71F7-231D-4EDD-B151-3378FA576DFA}']
    procedure Info(const AMsg: string);
    procedure InfoFmt(const AMsg: string; const Args: array of const);

    procedure Error(const AMsg: string);
    procedure ErrorFmt(const AMsg: string; const Args: array of const);

    procedure Debug(const AMsg: string);
    procedure DebugFmt(const AMsg: string; const Args: array of const);

    procedure EventSend(const AEvent: Pointer);
    procedure EventReceive(const AEvent: Pointer);
  end;

  TGuiEventLog = class(TInterfacedObject, IGuiEventLog)
  private
    type
      TLogMsgType = (mtInfo, mtError, mtDebug);
      TLogEventType = (etSend, etReceive);
    const
      CLogMsgType: array [TLogMsgType] of string = ('{INF}', '{ERR}', '{DBG}');
      CLogEventType: array [TLogEventType] of string = ('{EMS}', '{EMR}');
  private
    FLock: TCriticalSection;
    FStream: TFileStream;
    FPrepareFailed: Boolean;
    FAppPath: string;
    FFileNamePrefix: string;
    FUniqIdGenerator: IUniqIdGenerator;
    function MakeFileName: string;
    function PrepareStream: Boolean;
    procedure WriteText(const AText: string);
    procedure WriteMsg(const AMsg: string; const AMsgType: TLogMsgType); inline;
    procedure WriteEvent(const AEvent: Pointer; const AEventType: TLogEventType);
  private
    { IGuiEventLog }
    procedure Info(const AMsg: string);
    procedure InfoFmt(const AMsg: string; const Args: array of const);

    procedure Error(const AMsg: string);
    procedure ErrorFmt(const AMsg: string; const Args: array of const);

    procedure Debug(const AMsg: string);
    procedure DebugFmt(const AMsg: string; const Args: array of const);

    procedure EventSend(const AEvent: Pointer);
    procedure EventReceive(const AEvent: Pointer);
  public
    constructor Create(
      const AAppPath: string;
      const AFileNamePrefix: string;
      const AUniqIdGenerator: IUniqIdGenerator
    );
    destructor Destroy; override;
  end;

implementation

uses
  u_GuiEvent,
  u_FileSystemTools;

{ TGuiEventLog }

constructor TGuiEventLog.Create(
  const AAppPath: string;
  const AFileNamePrefix: string;
  const AUniqIdGenerator: IUniqIdGenerator
);
begin
  Assert(AFileNamePrefix <> '');

  inherited Create;

  FAppPath := AAppPath;
  FFileNamePrefix := AFileNamePrefix;
  FUniqIdGenerator := AUniqIdGenerator;

  FLock := TCriticalSection.Create;
  FStream := nil;
  FPrepareFailed := False;
end;

destructor TGuiEventLog.Destroy;
begin
  FreeAndNil(FStream);
  FreeAndNil(FLock);
  inherited Destroy;
end;

procedure TGuiEventLog.Info(const AMsg: string);
begin
  WriteMsg(AMsg, mtInfo);
end;

procedure TGuiEventLog.InfoFmt(const AMsg: string; const Args: array of const);
begin
  WriteMsg(Format(AMsg, Args), mtInfo);
end;

procedure TGuiEventLog.Error(const AMsg: string);
begin
  WriteMsg(AMsg, mtError);
end;

procedure TGuiEventLog.ErrorFmt(const AMsg: string; const Args: array of const);
begin
  WriteMsg(Format(AMsg, Args), mtError);
end;

procedure TGuiEventLog.Debug(const AMsg: string);
begin
  WriteMsg(AMsg, mtDebug);
end;

procedure TGuiEventLog.DebugFmt(const AMsg: string; const Args: array of const);
begin
  WriteMsg(Format(AMsg, Args), mtDebug);
end;

procedure TGuiEventLog.EventSend(const AEvent: Pointer);
begin
  WriteEvent(AEvent, etSend);
end;

procedure TGuiEventLog.EventReceive(const AEvent: Pointer);
begin
  WriteEvent(AEvent, etReceive);
end;

function TGuiEventLog.MakeFileName: string;
var
  VUniqId: string;
begin
  Result := FAppPath + FFileNamePrefix;

  if Assigned(FUniqIdGenerator) then begin
    VUniqId := FUniqIdGenerator.MakeNewId;

    if VUniqId <> '' then begin
      Result := Result + '.' + VUniqId;
    end;
  end;

  Result := Result + '.log';
end;

function TGuiEventLog.PrepareStream: Boolean;
var
  VFileName: string;
begin
  Result := False;

  if FPrepareFailed then begin
    Exit;
  end;

  Result := (FStream <> nil);
  if Result then begin
    Exit;
  end;

  try
    VFileName := MakeFileName;    
    FStream := TFileStream.Create(VFileName, fmCreate);
    FreeAndNil(FStream);
    FStream := TFileStream.Create(VFileName, fmOpenWrite or fmShareDenyNone);
    Result := True;
  except
    FPrepareFailed := True;
    raise
  end;
end;

procedure TGuiEventLog.WriteEvent(const AEvent: Pointer; const AEventType: TLogEventType);
var
  VCmd: Integer;
  VData: PByte;
  VStr: string;
begin
  VCmd := TEventMessage.Command(AEvent);
  
  VStr :=
    Format(
      '%-17s %.8x; size: %d byte',
      [TEventMessage.CommandToText(VCmd),
       TEventMessage.ID(AEvent),
       TEventMessage.RawSize(AEvent)]
    );

  case VCmd of
    EM_OK: begin
      VData := TEventMessage.Data(AEvent);
      Assert(VData <> nil);
      VStr := VStr + ' id = ' + IntToHex(PInteger(VData)^, 8);
    end;

    EM_URL_DATA_1: begin
      VData := TEventMessage.Data(AEvent);
      Assert(VData <> nil);
      VStr := VStr + ' req = ' + IntToStr(PInteger(VData)^) + ';';
      Inc(VData, 4);
      VStr := VStr + ' url = ' + string(PAnsiChar(VData));
    end;

    EM_URL_DATA_2: begin
      VData := TEventMessage.Data(AEvent);
      Assert(VData <> nil);
      VStr := VStr + ' req = ' + IntToStr(PInteger(VData)^) + ';';
      Inc(VData, 4);
      VStr := VStr + ' resp = ' + IntToStr(PWord(VData)^);
    end;
  end;
  
  WriteText(CLogEventType[AEventType] + ': ' + VStr + #13#10);
end;

procedure TGuiEventLog.WriteMsg(const AMsg: string; const AMsgType: TLogMsgType);
begin
  WriteText(CLogMsgType[AMsgType] + ': ' + AMsg + #13#10);
end;

procedure TGuiEventLog.WriteText(const AText: string);
var
  VText: UTF8String;
begin
  VText := UTF8Encode(FormatDateTime('yyyy-mm-dd hh:nn:ss.zzz', Now) + ' ' + AText);

  FLock.Acquire;
  try
    if (FStream <> nil) or PrepareStream then begin
      FStream.WriteBuffer(VText[1], Length(VText)*SizeOf(VText[1]));
    end;
  finally
    FLock.Release;
  end;
end;

{ TUniqIdGeneratorByDate }

function TUniqIdGeneratorByDate.MakeNewId: string;
begin
  Result := FormatDateTime('yyyymmdd_hhnnsszzz', Now);
end;

{ TUniqIdGeneratorByCounter }

function TUniqIdGeneratorByCounter.MakeNewId: string;
begin
  Result := IntToStr(InterlockedIncrement(FCounter) - 1);
end;

class procedure TUniqIdGeneratorByCounter.ResetCounter;
begin
  FCounter := 0;
end;

initialization
  TUniqIdGeneratorByCounter.ResetCounter;

end.
