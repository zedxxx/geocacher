unit u_GuiEventUrlData;

interface

uses
  SysUtils,
  t_HttpRequestCounters,
  t_GuiEventUrlData,
  u_GuiEvent;

type
  TGuiEventUrlData = class
  private
    FUrl: UTF8String;
    FRequestId: Integer;
  public
    procedure Reset;

    function Pack(
      const AUrl: string;
      const ARequestId: Integer
    ): Pointer; overload;

    function Pack(
      const ARespNo: Cardinal;
      const AWSAError: Cardinal;
      const ARespText: string;
      const AHint: string;
      const ACounters: PHttpRequestCounters
    ): Pointer; overload;

    class procedure Unpack(
      const AEvent: Pointer;
      const AEventCommand: Integer;
      const AUrlData: PGuiEventUrlDataRec
    );
  end;

implementation

const
  CRequestIdEmpty = -1;

procedure _WriteStr(var P: PByte; const AStr: UTF8String); inline;
var
  I: Integer;
begin
  I := Length(AStr);
  Move(PAnsiChar(AStr)^, P^, I);
  Inc(P, I);

  P^ := 0;
  Inc(P);
end;

function _ReadStr(var P: PByte): string; inline;
var
  VStr: UTF8String;
begin
  VStr := PAnsiChar(P);
  Inc(P, Length(VStr) + 1);

  Result := {$IFDEF UNICODE}UTF8ToString{$ELSE}UTF8Decode{$ENDIF}(VStr);
end;

{ TGuiEventUrlData }

procedure TGuiEventUrlData.Reset;
begin
  FUrl := '';
  FRequestId := CRequestIdEmpty;
end;

function TGuiEventUrlData.Pack(
  const AUrl: string;
  const ARequestId: Integer
): Pointer;
var
  I: Integer;
  P: PByte;
  VDataSize: Cardinal;
begin
  FUrl := UTF8Encode(AUrl);
  FRequestId := ARequestId;

  VDataSize :=
    4 +
    Length(FUrl) + 1 +
    1;

  Result := TEventMessage.Alloc(EM_URL_DATA_1, VDataSize);

  P := TEventMessage.Data(Result);

  I := 4;
  PInteger(P)^ := FRequestId;
  Inc(P, I);

  _WriteStr(P, FUrl);

  P^ := 0;
end;

function TGuiEventUrlData.Pack(
  const ARespNo: Cardinal;
  const AWSAError: Cardinal;
  const ARespText: string;
  const AHint: string;
  const ACounters: PHttpRequestCounters
): Pointer;
var
  I: Integer;
  P: PByte;
  VHint: UTF8String;
  VRespText: UTF8String;
  VDataSize: Integer;
begin
  Assert(FRequestId <> CRequestIdEmpty);

  VHint := UTF8Encode(AHint);
  VRespText := UTF8Encode(ARespText);

  VDataSize :=
    4 +
    SizeOf(ARespNo) +
    SizeOf(AWSAError) +
    Length(FUrl) + 1 +
    SizeOf(THttpRequestCounters) +
    Length(VRespText) + 1 +
    Length(VHint) + 1 +
    1;

  Result := TEventMessage.Alloc(EM_URL_DATA_3, VDataSize);

  P := TEventMessage.Data(Result);

  I := 4;
  PInteger(P)^ := FRequestId;
  Inc(P, I);

  I := SizeOf(ARespNo);
  Move(ARespNo, P^, I);
  Inc(P, I);

  I := SizeOf(AWSAError);
  Move(AWSAError, P^, I);
  Inc(P, I);

  _WriteStr(P, FUrl);

  I := SizeOf(THttpRequestCounters);
  Move(ACounters^, P^, I);
  Inc(P, I);

  _WriteStr(P, VRespText);

  _WriteStr(P, VHint);

  P^ := 0;
end;

class procedure TGuiEventUrlData.Unpack(
  const AEvent: Pointer;
  const AEventCommand: Integer;
  const AUrlData: PGuiEventUrlDataRec
);
var
  P: PByte;
begin
  case AEventCommand of

    EM_URL_DATA_1: begin
      with AUrlData^ do begin
        EventType := etNew;

        P := TEventMessage.Data(AEvent);

        RequestId := PInteger(P)^;
        Inc(P, 4);

        Url := _ReadStr(P);
      end;
    end;

//    EM_URL_DATA_2: begin
//      EventType := etUpdate;
//    end;

    EM_URL_DATA_3: begin
      with AUrlData^ do begin
        EventType := etDone;

        P := TEventMessage.Data(AEvent);

        RequestId := PInteger(P)^;
        Inc(P, 4);

        RespNo := PCardinal(P)^;
        Inc(P, SizeOf(RespNo));

        WSAError := PCardinal(P)^;
        Inc(P, SizeOf(WSAError));

        Url := _ReadStr(P);

        Counters := PHttpRequestCounters(P)^;
        Inc(P, SizeOf(Counters));

        RespText := _ReadStr(P);

        Hint := _ReadStr(P);
      end;
    end;
  else
    raise Exception.Create(
      'Unexpected event: ' + TEventMessage.CommandToText(AEventCommand)
    );
  end;
end;

end.
