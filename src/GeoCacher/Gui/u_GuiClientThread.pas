unit u_GuiClientThread;

interface

uses
  Windows,
  ScktComp,
  i_AppConfig,
  i_GuiEventDispatcher,
  u_SendQueueSimple,
  u_GuiEvent,
  u_GuiEventLog,
  u_GuiSocketStream;

type
  TGuiClientThread = class(TServerClientThread)
  private
    FLog: IGuiEventLog;
    FEventDispatcher: IGuiEventDispatcher;
    FReceiveBuffer: PByte;
    FReceiveBufferSize: Integer;
    FLastTick: Cardinal;
    FSendQueue: TSendQueueSimple;

    FOkEvent: Pointer;
    FPingEvent: Pointer;

    function DoCheckIoResult(
      const AIoResult: TGuiSocketStreamIoResult
    ): Boolean; inline;

    function DoSendData(
      const ASocketStream: TGuiSocketStream;
      const AData: Pointer;
      const ASize: Integer
    ): Boolean; inline;

    function DoSendEvent(
      const ASocketStream: TGuiSocketStream;
      const AEvent: Pointer
    ): Boolean; inline;

    function DoReceiveData(
      const ASocketStream: TGuiSocketStream;
      const ABuffer: Pointer;
      const ABufferSize: Integer;
      out AReceivedSize: Integer
    ): Boolean; inline;

    function DoReceivePartialEvent(
      const ASocketStream: TGuiSocketStream;
      var AReceivedSize: Integer;
      var AEvent: PByte
    ): Boolean;

    procedure ReadDataFromClient(
      const ASocketStream: TGUISocketStream
    );

    procedure ClearQueue;
    procedure AddToQueue(const AEvent: Pointer); inline;
  public
    constructor Create(
      const ASocket: TServerClientWinSocket;
      const AAppConfig: IAppConfig;
      const AEventDispatcher: IGuiEventDispatcher
    );
    destructor Destroy; override;
  public
    procedure ClientExecute; override;
  end;

implementation

uses
  SysUtils,
  u_GlobalLog,
  u_ReadableThreadNames;

{ TGuiClientThread }

constructor TGuiClientThread.Create(
  const ASocket: TServerClientWinSocket;
  const AAppConfig: IAppConfig;
  const AEventDispatcher: IGuiEventDispatcher
);
var
  VStatic: IAppConfigStatic;
begin
  Assert(AEventDispatcher <> nil);

  inherited Create(True, ASocket);

  FEventDispatcher := AEventDispatcher;

  FReceiveBufferSize := 64*1024;
  GetMem(FReceiveBuffer, FReceiveBufferSize);

  FLastTick := 0;

  FSendQueue := TSendQueueSimple.Create;

  VStatic := AAppConfig.GetStatic;

  FLog := nil;
  if VStatic.RecPtr.Debug.GuiThreadLogEnabled then begin
    FLog :=
      TGuiEventLog.Create(
        AAppConfig.AppPath,
        'GuiTcpServer',
        TUniqIdGeneratorByCounter.Create
      );
  end;

  FOkEvent := nil;
  if VStatic.RecPtr.Debug.GuiThreadSendOk then begin
    FOkEvent := TEventMessage.Alloc(EM_OK, 4);
  end;

  FPingEvent := TEventMessage.Alloc(EM_PING, 0);
end;

destructor TGuiClientThread.Destroy;
begin
  ClearQueue;
  FreeAndNil(FSendQueue);

  FreeMem(FReceiveBuffer);

  TEventMessage.Free(FOkEvent);
  TEventMessage.Free(FPingEvent);

  inherited Destroy;
end;

function TGuiClientThread.DoCheckIoResult(
  const AIoResult: TGuiSocketStreamIoResult
): Boolean;
begin
  Result := False;

  case AIoResult of

    ssrOk: begin
      Result := True;
    end;

    ssrTimeout: begin
      if Assigned(FLog) then begin
        FLog.Error('Client timed out');
      end;
      ClientSocket.Close;
    end;

    ssrSocketError: begin
      if Assigned(FLog) then begin
        FLog.Error('Lost connection');
      end;
      ClientSocket.Close;
    end;

    ssrDisconnected: begin
      if Assigned(FLog) then begin
        FLog.Error('Client is not connected');
      end;
    end;
  else
    raise Exception.CreateFmt('Unexpected IoResult: %d', [Integer(AIoResult)]);
  end;
end;

function TGuiClientThread.DoSendData(
  const ASocketStream: TGuiSocketStream;
  const AData: Pointer;
  const ASize: Integer
): Boolean;
var
  VBytesCount: Integer;
begin
  Assert(AData <> nil);
  Assert(ASize > 0);
  VBytesCount := ASocketStream.Write(AData^, ASize);
  Result := DoCheckIoResult(ASocketStream.IoResult);
  if Result and (VBytesCount <> ASize) then begin
    Assert(False);
  end;
end;

function TGuiClientThread.DoSendEvent(
  const ASocketStream: TGuiSocketStream;
  const AEvent: Pointer
): Boolean;
begin
  Assert(TEventMessage.Validate(AEvent));
  if Assigned(FLog) then begin
    FLog.EventSend(AEvent);
  end;
  Result := DoSendData(ASocketStream, AEvent, TEventMessage.RawSize(AEvent));
end;

function TGuiClientThread.DoReceiveData(
  const ASocketStream: TGuiSocketStream;
  const ABuffer: Pointer;
  const ABufferSize: Integer;
  out AReceivedSize: Integer
): Boolean;
begin
  AReceivedSize := ASocketStream.Read(ABuffer^, ABufferSize);
  Result := DoCheckIoResult(ASocketStream.IoResult) and (AReceivedSize > 0);
  if Assigned(FLog) then begin
    FLog.DebugFmt('Received: %d byte', [AReceivedSize]);
  end;
end;

procedure TGuiClientThread.ClientExecute;
var
  I: Integer;
  VTick: Cardinal;
  VItems: PQueueArray;
  VItemsCount: Integer;
  VSocketStream: TGuiSocketStream;
begin
  SetCurrentThreadName(Self.ClassName);

  if Assigned(FLog) then begin
    FLog.InfoFmt('Client: %s:%d, ThreadID: %d',
      [ClientSocket.RemoteAddress, ClientSocket.RemotePort, GetCurrentThreadId]);
  end;

  ClearQueue;

  FEventDispatcher.AddOptionsChangeListener(Self.Handle, Self.AddToQueue);

  VSocketStream := TGuiSocketStream.Create(ClientSocket, 30000);
  try
    while not Terminated and ClientSocket.Connected do begin
      try
        // send events, if any
        FSendQueue.Dequeue(VItems, VItemsCount);
        try
          for I := 0 to VItemsCount - 1 do begin
            if not DoSendEvent(VSocketStream, VItems^[I]) then begin
              Exit;
            end;
            if I mod 128 = 0 then begin
              if VSocketStream.WaitForData(50) then begin
                ReadDataFromClient(VSocketStream);
              end;
            end;
          end;
        finally
          for I := 0 to VItemsCount - 1 do begin
            TEventMessage.Free(VItems^[I]);
          end;
        end;

        if VSocketStream.WaitForData(250) then begin
          ReadDataFromClient(VSocketStream);
        end;

        // send "ping"
        VTick := GetTickCount;
        if (VTick < FLastTick) or (VTick - FLastTick > 5000) then begin
          if not DoSendEvent(VSocketStream, FPingEvent) then begin
            Exit;
          end;
        end;

        Sleep(20);
      except
        on E: Exception do begin
          if Assigned(FLog) then begin
            FLog.Error(E.ClassName + ': ' + E.Message);
          end;
          GLog.Error(Self.ClassName, E);
          ClientSocket.Close;
        end;
      end;
    end;
  finally
    FEventDispatcher.RemoveAllListeners(Self.Handle);
    VSocketStream.Free;
    ClearQueue;
  end;
end;

function TGuiClientThread.DoReceivePartialEvent(
  const ASocketStream: TGuiSocketStream;
  var AReceivedSize: Integer;
  var AEvent: PByte
): Boolean;
var
  P: PByte;
  VSize: Integer;
  VHeadOffset: Int64;
  VAvailableSize: Integer;
  VMissingSize: Integer;
begin
  if Assigned(FLog) then begin
    FLog.Debug('Start partial event receive');
  end;

  Result := False;

  VHeadOffset := Int64(AEvent) - Int64(FReceiveBuffer);
  Assert(VHeadOffset >= 0);

  while not Result do begin
    VAvailableSize := AReceivedSize - VHeadOffset;
    Assert(VAvailableSize >= 0);

    if VAvailableSize >= TEventMessage.HeadSize then begin

      if TEventMessage.Magic(AEvent) <> EM_MAGIC then begin
        Assert(False);
        Exit;
      end;

      VMissingSize := TEventMessage.RawSize(AEvent) - VAvailableSize;

      if Assigned(FLog) then begin
        FLog.DebugFmt('Missing size: %d byte', [VMissingSize]);
      end;
    end else begin
      VMissingSize := TEventMessage.HeadSize - VAvailableSize;

      if Assigned(FLog) then begin
        FLog.DebugFmt('Missing head size: %d byte', [VMissingSize]);
      end;
    end;

    // ����������� ������ ������
    if AReceivedSize + VMissingSize > FReceiveBufferSize then begin
      Inc(FReceiveBufferSize, VMissingSize);
      ReallocMem(FReceiveBuffer, FReceiveBufferSize);

      AEvent := FReceiveBuffer;
      Inc(AEvent, VHeadOffset);

      if Assigned(FLog) then begin
        FLog.DebugFmt('ReceiveBuffer increased from %d to %d bytes',
          [AReceivedSize, FReceiveBufferSize]);
      end;
    end;

    P := FReceiveBuffer;
    Inc(P, AReceivedSize);

    // ��������� � ����� ������
    if not DoReceiveData(ASocketStream, P, VMissingSize, VSize) then begin
      Exit;
    end;

    Inc(AReceivedSize, VSize);

    if VSize >= VMissingSize then begin
      Result := True;
      if Assigned(FLog) then begin
        FLog.Debug('Finish partial event receive');
      end;
    end;
  end;
end;

procedure TGuiClientThread.ReadDataFromClient(const ASocketStream: TGUISocketStream);
var
  P: PByte;
  VSize: Integer;
  VCount: Integer;
  VEvent: Pointer;
  VCommand: Integer;
  VResponse: Pointer;
  VEventSize: Integer;
begin
  P := FReceiveBuffer;

  if not DoReceiveData(ASocketStream, P, 32*1024, VSize) then begin
    Exit;
  end;

  FLastTick := GetTickCount;

  VCount := 0;
  while VCount < VSize do begin
    while (P^ = 0) and (VCount < VSize) do begin
      Inc(P);
      Inc(VCount);
    end;

    if VCount = VSize then begin
      Exit;
    end else if TEventMessage.HeadSize > VSize - VCount then begin
      // ��������� �� ��������� � �����
      if not DoReceivePartialEvent(ASocketStream, VSize, P) then begin
        Assert(False);
        Exit;
      end;
    end;

    VEvent := P;
    if TEventMessage.Magic(VEvent) <> EM_MAGIC then begin
      Assert(False);
      Exit;
    end;

    VCommand := TEventMessage.Command(VEvent);
    VEventSize := TEventMessage.RawSize(VEvent);

    if (VCommand = EM_OK) or (VCommand = EM_PONG) then begin
      if Assigned(FLog) then begin
        FLog.EventReceive(VEvent);
      end;
      Inc(P, VEventSize);
      Inc(VCount, VEventSize);
      // ������ ��������� �������
      Continue;
    end;

    if VEventSize > VSize - VCount then begin
      // ������ �� ���������� � �����
      if not DoReceivePartialEvent(ASocketStream, VSize, P) then begin
        Assert(False);
        Exit;
      end;
      // ������ ������� ������� ������
      Continue;
    end;

    if Assigned(FLog) then begin
      FLog.EventReceive(VEvent);
    end;
    
    // �������� ��������� �� ����� ������
    Inc(P, VEventSize);
    Inc(VCount, VEventSize);

    // ������ ���������� �� ������� ������
    if FEventDispatcher.Process(Self.Handle, VEvent, VResponse) then begin
      // ���� ���� ������ ��� ���������� � ������� - ���������
      AddToQueue(VResponse);
    end;

    case VCommand of
      EM_START_MONITOR: begin
        FEventDispatcher.AddUrlDataListener(Self.Handle, Self.AddToQueue);
      end;
      EM_STOP_MONITOR: begin
        FEventDispatcher.RemoveUrlDataListener(Self.Handle);
      end;
    end;

    if FOkEvent <> nil then begin
      PInteger(TEventMessage.Data(FOkEvent))^ := TEventMessage.ID(VEvent);
      if not DoSendEvent(ASocketStream, FOkEvent) then begin
        Exit;
      end;
    end;
  end;
end;

procedure TGuiClientThread.AddToQueue(const AEvent: Pointer);
begin
  if not FSendQueue.TryEnqueue(AEvent) then begin
    FSendQueue.Enqueue(AEvent);
  end;
end;

procedure TGuiClientThread.ClearQueue;
var
  I: Integer;
  VEvent: Pointer;
  VItems: PQueueArray;
  VCount: Integer;
begin
  while not FSendQueue.IsEmpty do begin
    FSendQueue.Dequeue(VItems, VCount);
    for I := 0 to VCount - 1 do begin
      VEvent := VItems^[I];
      Assert(TEventMessage.Validate(VEvent));
      TEventMessage.Free(VEvent);
    end;
  end;
end;

end.
