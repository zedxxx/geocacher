unit GeoCacher.Modules;

// This unit must be FIRST unit in dpr file

interface

{$IFDEF WIN32}

{$IFDEF RELEASE}
  {$DEFINE USE_FAST_MM}
  {$IFNDEF UNICODE}
    {$DEFINE USE_FAST_CODE}
    {$DEFINE USE_FAST_MOVE}
  {$ENDIF}
{$ENDIF}

uses
{$IFDEF USE_FAST_MM}
  {$IF CompilerVersion > 23}
  FastMM5,
  {$ELSE}
  FastMM4,
  {$IFEND}
{$ENDIF}
  {$IFDEF USE_FAST_MOVE}
  FastMove,
  {$ENDIF}
  {$IFDEF USE_FAST_CODE}
  FastCode,
  {$ENDIF}
  XPMan;

{$ENDIF}

implementation

end.
