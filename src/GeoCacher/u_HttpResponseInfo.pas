unit u_HttpResponseInfo;

interface

uses
  Classes,
  IdCookie,
  IdException,
  IdTCPConnection,
  IdHTTPHeaderInfo,
  u_HttpRequestInfo;

type
  THttpResponseInfo = class(TIdResponseHeaderInfo)
  protected
    FAuthRealm: string;
    FConnection: TIdTCPConnection;
    FResponseNo: Integer;
    FCookies: TIdCookies;
    FContentStream: TStream;
    FContentText: string;
    FCloseConnection: Boolean;
    FFreeContentStream: Boolean;
    FHeaderHasBeenWritten: Boolean;
    FResponseText: string;
    FRequestInfo: THTTPRequestInfo;
    //
    procedure ReleaseContentStream;
    procedure SetCookies(const AValue: TIdCookies);
    procedure SetHeaders; override;
    procedure SetResponseNo(const AValue: Integer);
    procedure SetCloseConnection(const Value: Boolean);
  public
    function GetServer: string;
    procedure SetServer(const Value: string);
  public
    constructor Create(
      const AOwner: TPersistent;
      const ARequestInfo: THTTPRequestInfo;
      const AConnection: TIdTCPConnection
    ); reintroduce;
    destructor Destroy; override;
    procedure Redirect(const AURL: string);
    procedure WriteHeader;
    procedure WriteContent;
    //
    property AuthRealm: string read FAuthRealm write FAuthRealm;
    property CloseConnection: Boolean read FCloseConnection write SetCloseConnection;
    property ContentStream: TStream read FContentStream write FContentStream;
    property ContentText: string read FContentText write FContentText;
    property Cookies: TIdCookies read FCookies write SetCookies;
    property FreeContentStream: Boolean read FFreeContentStream write FFreeContentStream;
    // writable for isapi compatibility. Use with care
    property HeaderHasBeenWritten: Boolean read FHeaderHasBeenWritten write FHeaderHasBeenWritten;
    property ResponseNo: Integer read FResponseNo write SetResponseNo;
    property ResponseText: String read FResponseText write FResponseText;
    property ServerSoftware: string read GetServer write SetServer;
  end;

  EIdHTTPHeaderAlreadyWritten = class(EIdException);

implementation

uses
  SysUtils,
  IdGlobal,
  IdGlobalProtocols,
  IdResourceStringsProtocols,
  t_CustomHTTPServer;

const
  GResponseNo = 200;
  GFContentLength = -1;
  
{ THttpResponseInfo }

constructor THttpResponseInfo.Create(
  const AOwner: TPersistent;
  const ARequestInfo: THTTPRequestInfo;
  const AConnection: TIdTCPConnection
);
begin
  inherited Create(AOwner);

  FRequestInfo := ARequestInfo;
  FConnection := AConnection;

  FFreeContentStream := True;

  ResponseNo := GResponseNo;
  ContentType := '';
  ContentLength := GFContentLength;

  {Some clients may not support folded lines}
  RawHeaders.FoldLines := False;
  FCookies := TIdCookies.Create(Self);

  {TODO Specify version - add a class method dummy that calls version}
  ServerSoftware := 'Indy';
end;

destructor THttpResponseInfo.Destroy;
begin
  FreeAndNil(FCookies);
  ReleaseContentStream;
  inherited Destroy;
end;

procedure THttpResponseInfo.Redirect(const AURL: string);
begin
  ResponseNo := 302;
  Location := AURL;
end;

procedure THttpResponseInfo.ReleaseContentStream;
begin
  if FreeContentStream then begin
    IdDisposeAndNil(FContentStream);
  end else begin
    FContentStream := nil;
  end;
end;

procedure THttpResponseInfo.SetCloseConnection(const Value: Boolean);
begin
  Connection := iif(Value, 'close', 'keep-alive');    {Do not Localize}
  // TODO: include a 'Keep-Alive' header to specify a timeout value
  FCloseConnection := Value;
end;

procedure THttpResponseInfo.SetCookies(const AValue: TIdCookies);
begin
  FCookies.Assign(AValue);
end;

procedure THttpResponseInfo.SetHeaders;
var
  I: Integer;
begin
  inherited SetHeaders;
  if Server <> '' then begin
    FRawHeaders.Values['Server'] := Server;    {Do not Localize}
  end;
  if Location <> '' then begin
    FRawHeaders.Values['Location'] := Location;    {Do not Localize}
  end;
  if FLastModified > 0 then begin
    FRawHeaders.Values['Last-Modified'] := LocalDateTimeToHttpStr(FLastModified); {do not localize}
  end;
  if FWWWAuthenticate.Count > 0 then begin
    FRawHeaders.Values['WWW-Authenticate'] := ''; {Do not Localize}
    for I := 0 to FWWWAuthenticate.Count-1 do begin
      FRawHeaders.AddValue('WWW-Authenticate', FWWWAuthenticate[I]);    {Do not Localize}
    end;
  end
  else if AuthRealm <> '' then begin
    FRawHeaders.Values['WWW-Authenticate'] := 'Basic realm="' + AuthRealm + '"';    {Do not Localize}
  end;
  if FProxyAuthenticate.Count > 0 then begin
    FRawHeaders.Values['Proxy-Authenticate'] := ''; {Do not Localize}
    for I := 0 to FProxyAuthenticate.Count-1 do begin
      FRawHeaders.AddValue('Proxy-Authenticate', FProxyAuthenticate[I]);    {Do not Localize}
    end;
  end
end;

procedure THttpResponseInfo.SetResponseNo(const AValue: Integer);
begin
  FResponseNo := AValue;
  case FResponseNo of
    100: ResponseText := RSHTTPContinue;
    101: ResponseText := RSHTTPSwitchingProtocols;
    // 2XX: Success
    200: ResponseText := RSHTTPOK;
    201: ResponseText := RSHTTPCreated;
    202: ResponseText := RSHTTPAccepted;
    203: ResponseText := RSHTTPNonAuthoritativeInformation;
    204: ResponseText := RSHTTPNoContent;
    205: ResponseText := RSHTTPResetContent;
    206: ResponseText := RSHTTPPartialContent;
    // 3XX: Redirections
    301: ResponseText := RSHTTPMovedPermanently;
    302: ResponseText := RSHTTPMovedTemporarily;
    303: ResponseText := RSHTTPSeeOther;
    304: ResponseText := RSHTTPNotModified;
    305: ResponseText := RSHTTPUseProxy;
    // 4XX Client Errors
    400: ResponseText := RSHTTPBadRequest;
    401: ResponseText := RSHTTPUnauthorized;
    403: ResponseText := RSHTTPForbidden;
    404: ResponseText := RSHTTPNotFound;
    405: ResponseText := RSHTTPMethodNotAllowed;
    406: ResponseText := RSHTTPNotAcceptable;
    407: ResponseText := RSHTTPProxyAuthenticationRequired;
    408: ResponseText := RSHTTPRequestTimeout;
    409: ResponseText := RSHTTPConflict;
    410: ResponseText := RSHTTPGone;
    411: ResponseText := RSHTTPLengthRequired;
    412: ResponseText := RSHTTPPreconditionFailed;
    413: ResponseText := RSHTTPRequestEntityTooLong;
    414: ResponseText := RSHTTPRequestURITooLong;
    415: ResponseText := RSHTTPUnsupportedMediaType;
    417: ResponseText := RSHTTPExpectationFailed;
    428: ResponseText := RSHTTPPreconditionRequired;
    429: ResponseText := RSHTTPTooManyRequests;
    431: ResponseText := RSHTTPRequestHeaderFieldsTooLarge;
    // 5XX Server errors
    500: ResponseText := RSHTTPInternalServerError;
    501: ResponseText := RSHTTPNotImplemented;
    502: ResponseText := RSHTTPBadGateway;
    503: ResponseText := RSHTTPServiceUnavailable;
    504: ResponseText := RSHTTPGatewayTimeout;
    505: ResponseText := RSHTTPHTTPVersionNotSupported;
    511: ResponseText := RSHTTPNetworkAuthenticationRequired;
  else
    ResponseText := RSHTTPUnknownResponseCode;
  end;
end;

procedure THttpResponseInfo.WriteContent;
begin
  if not HeaderHasBeenWritten then begin
    WriteHeader;
  end;

  // RLebeau 11/23/2014: Per RFC 2616 Section 4.3:
  //
  // For response messages, whether or not a message-body is included with
  // a message is dependent on both the request method and the response
  // status code (section 6.1.1). All responses to the HEAD request method
  // MUST NOT include a message-body, even though the presence of entity-
  // header fields might lead one to believe they do. All 1xx
  // (informational), 204 (no content), and 304 (not modified) responses
  // MUST NOT include a message-body. All other responses do include a
  // message-body, although it MAY be of zero length.

  if not (
    (FRequestInfo.CommandType = hcHEAD) or
    ((ResponseNo div 100) = 1) or   // informational
    (ResponseNo = 204) or           // no content
    (ResponseNo = 304)              // not modified
    ) then
  begin
    // Always check ContentText first
    if ContentText <> '' then begin
      FConnection.IOHandler.Write(ContentText, CharsetToEncoding(CharSet));
    end
    else if Assigned(ContentStream) then begin
      // If ContentLength has been assigned then do not send the entire file,
      // in case it grew after WriteHeader() generated the 'Content-Length'
      // header.  We cannot exceed the byte count that we told the client
      // we will be sending...

      // TODO: apply this rule to ContentText as well...

      // TODO: stop resetting Position to 0, send from the current Position...

      if HasContentLength then begin
        if ContentLength > 0 then begin
          ContentStream.Position := 0;
          FConnection.IOHandler.Write(ContentStream, ContentLength, False);
        end;
      end else begin
        ContentStream.Position := 0;
        FConnection.IOHandler.Write(ContentStream);
      end;
    end
    else begin
      FConnection.IOHandler.Write('<HTML><BODY><B>' + IntToStr(ResponseNo) + ' ' + ResponseText    {Do not Localize}
       + '</B></BODY></HTML>', CharsetToEncoding(CharSet));    {Do not Localize}
    end;
  end;

  // Clear All - This signifies that WriteConent has been called.
  ContentText := '';    {Do not Localize}
  ReleaseContentStream;
end;

procedure THttpResponseInfo.WriteHeader;
var
  i: Integer;
  LBufferingStarted: Boolean;
begin
  if HeaderHasBeenWritten then begin
    raise EIdHTTPHeaderAlreadyWritten.Create(RSHTTPHeaderAlreadyWritten);
  end;
  FHeaderHasBeenWritten := True;

  if AuthRealm <> '' then
  begin
    ResponseNo := 401;
    if (Length(ContentText) = 0) and (not Assigned(ContentStream)) then
    begin
      ContentType := 'text/html; charset=utf-8';    {Do not Localize}
      ContentText := '<HTML><BODY><B>' + IntToStr(ResponseNo) + ' ' + ResponseText + '</B></BODY></HTML>';    {Do not Localize}
      ContentLength := -1; // calculated below
    end;
  end;

  // RLebeau 5/15/2012: for backwards compatibility. We really should
  // make the user set this every time instead...
  if ContentType = '' then begin
    if (ContentText <> '') or (Assigned(ContentStream)) then begin
      ContentType := 'text/html; charset=ISO-8859-1'; {Do not Localize}
    end;
  end;

  // RLebeau: according to RFC 2616 Section 4.4:
  //
  // If a Content-Length header field (section 14.13) is present, its
  // decimal value in OCTETs represents both the entity-length and the
  // transfer-length. The Content-Length header field MUST NOT be sent
  // if these two lengths are different (i.e., if a Transfer-Encoding
  // header field is present). If a message is received with both a
  // Transfer-Encoding header field and a Content-Length header field,
  // the latter MUST be ignored.
  // ...
  // Messages MUST NOT include both a Content-Length header field and a
  // non-identity transfer-coding. If the message does include a non-
  // identity transfer-coding, the Content-Length MUST be ignored.

  if (ContentLength = -1) and
    ((TransferEncoding = '') or TextIsSame(TransferEncoding, 'identity')) then {do not localize}
  begin
    if not (
      (FRequestInfo.CommandType = hcHEAD) or
      ((ResponseNo div 100) = 1) or   // informational
      (ResponseNo = 204) or           // no content
      (ResponseNo = 304)              // not modified
      ) then
    begin
      // Always check ContentText first
      if ContentText <> '' then begin
        ContentLength := CharsetToEncoding(CharSet).GetByteCount(ContentText);
      end
      else if Assigned(ContentStream) then begin
        ContentLength := ContentStream.Size;
      end else begin
        ContentType := 'text/html; charset=utf-8';    {Do not Localize}
        ContentText := '<HTML><BODY><B>' + IntToStr(ResponseNo) + ' ' + ResponseText + '</B></BODY></HTML>';    {Do not Localize}
        ContentLength := CharsetToEncoding(CharSet).GetByteCount(ContentText);
      end;
    end else begin
      ContentLength := 0;
    end;
  end;

  if Date <= 0 then begin
    Date := Now;
  end;

  SetHeaders;

  LBufferingStarted := not FConnection.IOHandler.WriteBufferingActive;
  if LBufferingStarted then begin
    FConnection.IOHandler.WriteBufferOpen;
  end;
  try
    // Write HTTP status response
    // TODO: if the client sent an HTTP/1.0 request, send an HTTP/1.0 response?
    FConnection.IOHandler.WriteLn('HTTP/1.1 ' + IntToStr(ResponseNo) + ' ' + ResponseText);    {Do not Localize}
    // Write headers
    FConnection.IOHandler.Write(RawHeaders);
    // Write cookies
    for i := 0 to Cookies.Count - 1 do begin
      FConnection.IOHandler.WriteLn('Set-Cookie: ' + Cookies[i].ServerCookie);    {Do not Localize}
    end;
    // HTTP headers end with a double CR+LF
    FConnection.IOHandler.WriteLn;
    if LBufferingStarted then begin
      FConnection.IOHandler.WriteBufferClose;
    end;
  except
    if LBufferingStarted then begin
      FConnection.IOHandler.WriteBufferCancel;
    end;
    raise;
  end;
end;

function THttpResponseInfo.GetServer: string;
begin
  Result := Server;
end;

procedure THttpResponseInfo.SetServer(const Value: string);
begin
  Server := Value;
end;


end.
