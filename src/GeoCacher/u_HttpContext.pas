unit u_HttpContext;

interface

uses
  Classes,
  RegExpr,
  IdContext,
  i_AppConfig,
  t_AppConfig,
  t_CustomHTTPServer,
  t_HttpRequestCounters,
  u_HttpRequestInfo,
  u_HttpResponseInfo,
  u_GuiEventUrlData,
  u_ServerActivityStat;

type
  THttpContext = class(TObject)
  private
    FCounters: THttpRequestCounters;
  public
    AppConfig: IAppConfig;
    AppConfigStatic: IAppConfigStatic;
    AppConfigCounter: Integer;
    AppConfigRec: PAppConfigRec;

    ProxyConfig: PProxyInfo;
    DbRootConfig: PGoogleEarthDbRootConfigRec;
    FlatFileConfig: PFlatFileTileCacheConfigRec;
    WebCacheConfig: PFileSystemCacheConfigRec;

    AllowCacheRead: Boolean;
    AllowCacheWrite: Boolean;
    AllowInternetRequest: Boolean;

    ReqID        : Integer;
    RespNo       : Word;
    WSAError     : Cardinal;
    Url          : string;
    Head         : string;
    Body         : TMemoryStream;
    FileName     : string;
    FileNameInfo : string;

    IsHttps      : Boolean;
    IsHoogle     : Boolean;
    IsGee        : Boolean;
    IsSessionIdRequired: Boolean;
    RegEx        : TRegExpr;
    HttpLoader   : TObject;
    HttpdStat    : TServerActivityStat;
    Intercept    : TObject;  // TConnectionInterceptLog(TIdConnectionIntercept)
    IdCtx        : TIdContext;
    ReqInfo      : THTTPRequestInfo;
    RespInfo     : THttpResponseInfo;
    Counters     : PHttpRequestCounters;
    UrlData      : TGuiEventUrlData;
  public
    procedure Reset;

    procedure SetConfig(
      const AAppConfigStatic: IAppConfigStatic;
      const AAppConfigCounter: Integer
    );
  public
    constructor Create(
      const AAppConfig: IAppConfig;
      const AActivityStat: TServerActivityStat
    );
    destructor Destroy; override;
  end;

implementation

uses
  SysUtils;

{ THttpContext }

constructor THttpContext.Create(
  const AAppConfig: IAppConfig;
  const AActivityStat: TServerActivityStat
);
begin
  inherited Create;

  AppConfig := AAppConfig;
  HttpdStat := AActivityStat;

  ReqID := 0;
  Body := TMemoryStream.Create;
  RegEx := TRegExpr.Create;

  HttpLoader := nil;
  Intercept := nil;

  IsHttps := False;
  IsHoogle := False;
  IsGee := False;
  IsSessionIdRequired := False;

  Counters := @FCounters;
  UrlData := TGuiEventUrlData.Create;

  Reset;

  SetConfig(AppConfig.GetStatic, 0);
end;

destructor THttpContext.Destroy;
begin
  FreeAndNil(HttpLoader);
  FreeAndNil(Body);
  FreeAndNil(RegEx);
  FreeAndNil(UrlData);

  Intercept := nil;
  HttpdStat := nil;
  IdCtx := nil;
  ReqInfo := nil;
  RespInfo := nil;

  inherited Destroy;
end;

procedure THttpContext.Reset;
begin
  RespNo := 0;
  WSAError := 0;
  Url := '';
  Head := '';

  Body.Clear;

  FileName := '';
  FileNameInfo := '';

  IdCtx := nil;
  ReqInfo := nil;
  RespInfo := nil;

  FillChar(FCounters, SizeOf(THttpRequestCounters), 0);

  UrlData.Reset;
end;

procedure THttpContext.SetConfig(
  const AAppConfigStatic: IAppConfigStatic;
  const AAppConfigCounter: Integer
);
begin
  AppConfigStatic := AAppConfigStatic;
  AppConfigCounter := AAppConfigCounter;

  AppConfigRec := AppConfigStatic.RecPtr;

  ProxyConfig := @AppConfigRec.Proxy;
  DbRootConfig := @AppConfigRec.DbRoot;
  FlatFileConfig := @AppConfigRec.FlatFileCache;
  WebCacheConfig := @AppConfigRec.WebCache;

  AllowCacheRead := AppConfigRec.WorkMode in [wmInetAndCacheRW, wmInetAndCacheRO, wmCacheOnly];
  AllowCacheWrite := AppConfigRec.WorkMode in [wmInetAndCacheRW, wmInetAndCacheWO];
  AllowInternetRequest := AppConfigRec.WorkMode <> wmCacheOnly;
end;

end.
