unit u_GeoCacher;

interface

uses
  Classes,
  Messages,
  i_AppConfig,
  i_AppNotifier,
  u_AppTray,
  u_ServerActivityStatNotifier;

type
  TGeoCacher = class
  private
    FHwd: THandle;
    FAppTray: TAppTray;

    FAppConfig: IAppConfig;
    FAppNotifier: IAppNotifierInternal;

    FConfigFileStream: TFileStream;
    FServerActivityStatNotifier: TServerActivityStatNotifier;

    procedure WindowProc(var AMsg: TMessage);
    procedure OnBeforeMessageLoop;
    procedure OnAfterMessageLoop;

    procedure RunGeoGui;
  public
    procedure Run;
  public
    constructor Create;
    destructor Destroy; override;
  end;

implementation

uses
  Windows,
  SysUtils,
  ShellApi,
  c_AppNotifier,
  i_GuiEventDispatcher,
  u_AppConfig,
  u_AppNotifier,
  u_GlobalLog,
  u_HttpServer,
  u_GuiTcpServer,
  u_GuiEventDispatcher,
  u_FileSystemTools,
  u_ReadableThreadNames;

{ TGeoCacher }

constructor TGeoCacher.Create;
var
  VConfigFileName: string;
begin
  SetCurrentThreadName('MainThread');

  inherited Create;

  FAppTray := nil;
  FAppConfig := TAppConfig.Create;
  FAppNotifier := TAppNotifier.Create;

  VConfigFileName := FAppConfig.AppPath + 'options\GeoCacher.ini';
  CreateFileIfNotExists(VConfigFileName);

  FConfigFileStream := TFileStream.Create(VConfigFileName, fmOpenReadWrite or fmShareDenyWrite);
end;

destructor TGeoCacher.Destroy;
begin
  FreeAndNil(FConfigFileStream);
  inherited Destroy;
end;

procedure TGeoCacher.WindowProc(var AMsg: TMessage);
begin
  AMsg.Result := 0;

  case AMsg.Msg of

    WM_APP_NOTIFIER_HTTP_ACTIVITY_ON: begin
      if FAppTray <> nil then FAppTray.SetBusy;
    end;

    WM_APP_NOTIFIER_HTTP_ACTIVITY_OFF: begin
      if FAppTray <> nil then FAppTray.SetIdle;
    end;

    WM_APP_NOTIFIER_REFRESH_TRAY_ICON: begin
      if FAppTray <> nil then FAppTray.RefreshIcon;
    end;

    WM_APP_NOTIFIER_REFRESH_TRAY_MENU: begin
      if FAppTray <> nil then FAppTray.RefreshMenu;
    end;

    WM_APP_NOTIFIER_RUN_GEOGUI: begin
      Self.RunGeoGui;
    end;

  else
    AMsg.Result := DefWindowProc(FHwd, AMsg.Msg, AMsg.WParam, AMsg.LParam);
  end;
end;

procedure TGeoCacher.OnBeforeMessageLoop;
var
  VGuiEventDispatcher: IGuiEventDispatcher;
begin
  FAppNotifier.StartNotify(FHwd);

  FAppConfig.LoadFromStream(FConfigFileStream);

  if FAppConfig.GetStatic.RecPtr.ShowTrayIcon then begin
    FAppTray := TAppTray.Create(FAppConfig, FAppNotifier);
  end;

  VGuiEventDispatcher :=
    TGuiEventDispatcher.Create(
      FAppConfig
    );

  FServerActivityStatNotifier :=
    TServerActivityStatNotifier.Create(
      FAppNotifier,
      VGuiEventDispatcher
    );

  HttpServerStart(FAppConfig, VGuiEventDispatcher);
  GuiServerStart(FAppConfig, VGuiEventDispatcher);
end;

procedure TGeoCacher.OnAfterMessageLoop;
begin
  FAppNotifier.StopNotify;

  FAppConfig.SaveToStream(FConfigFileStream);

  HttpServerStop;
  GuiServerStop;

  FreeAndNil(FServerActivityStatNotifier);
  FreeAndNil(FAppTray);
end;

procedure TGeoCacher.Run;
var
  VMsg: TMsg;
begin
  FHwd := AllocateHWnd(Self.WindowProc);
  if FHwd = 0 then begin
    RaiseLastOSError;
  end;
  try
    OnBeforeMessageLoop;
    try
      while GetMessage(VMsg, 0, 0, 0) do begin
        TranslateMessage(VMsg);
        DispatchMessage(VMsg);
      end;
    finally
      OnAfterMessageLoop;
    end;
  finally
    DeallocateHWnd(FHwd);
  end;
end;

{$IFNDEF UNICODE}
function GetProcessId(Process: THandle): DWORD; stdcall; external 'kernel32.dll';
{$ENDIF}

function RunApp(
  const AExecutableFile: string;
  const AParameters: string;
  const AShowOption: Integer = SW_SHOWNORMAL
): Integer;
var
  VInfo: TShellExecuteInfo;
begin
  Result := 0;

  if not FileExists(AExecutableFile) then begin
    Exit;
  end;

  FillChar(VInfo, SizeOf(VInfo), 0);
  VInfo.cbSize := SizeOf(TShellExecuteInfo);
  VInfo.fMask := SEE_MASK_NOCLOSEPROCESS;
  VInfo.lpFile := PChar(AExecutableFile);
  VInfo.lpParameters := PChar(AParameters);
  VInfo.nShow := AShowOption;

  if ShellExecuteEx(@VInfo) then begin
    WaitForInputIdle(VInfo.hProcess, 3000);
    Result := GetProcessID(VInfo.hProcess);
  end;
end;

procedure TGeoCacher.RunGeoGui;
var
  VParams: string;
begin
  if not FAppConfig.IsGeoGuiAvailable then begin
    GLog.ShowErrorMessage('GeoGui is not available!');
    Exit;
  end;

  VParams := IntToStr(FAppConfig.GetStatic.RecPtr.ListeningPortTcp);

  if RunApp(FAppConfig.GeoGuiFileName, VParams) = 0 then begin
    GLog.ShowErrorMessage('Failed to start GeoGui!');
  end;
end;

end.

