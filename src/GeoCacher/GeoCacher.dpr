program GeoCacher;

uses
  GeoCacher.Modules,
  Windows,
  SysUtils,
  i_AppConfig in 'AppConfig\i_AppConfig.pas',
  t_AppConfig in 'AppConfig\t_AppConfig.pas',
  u_AppConfig in 'AppConfig\u_AppConfig.pas',
  u_AppConfigStatic in 'AppConfig\u_AppConfigStatic.pas',
  c_AppNotifier in 'AppNotifier\c_AppNotifier.pas',
  i_AppNotifier in 'AppNotifier\i_AppNotifier.pas',
  u_AppNotifier in 'AppNotifier\u_AppNotifier.pas',
  u_AppTray in 'AppTray\u_AppTray.pas',
  u_AppTrayMenu in 'AppTray\u_AppTrayMenu.pas',
  i_FileSystemStorage in 'FileSystem\i_FileSystemStorage.pas',
  t_FileInfo in 'FileSystem\t_FileInfo.pas',
  u_FileSystem in 'FileSystem\u_FileSystem.pas',
  u_FileSystemStorage in 'FileSystem\u_FileSystemStorage.pas',
  u_FileSystemTools in 'FileSystem\u_FileSystemTools.pas',
  u_SendQueueSimple in 'Gui\Queue\u_SendQueueSimple.pas',
  i_GuiEventDispatcher in 'Gui\i_GuiEventDispatcher.pas',
  t_GuiEventUrlData in 'Gui\t_GuiEventUrlData.pas',
  u_GuiClientThread in 'Gui\u_GuiClientThread.pas',
  u_GuiEvent in 'Gui\u_GuiEvent.pas',
  u_GuiEventDispatcher in 'Gui\u_GuiEventDispatcher.pas',
  u_GuiEventLog in 'Gui\u_GuiEventLog.pas',
  u_GuiEventUrlData in 'Gui\u_GuiEventUrlData.pas',
  u_GuiSocketStream in 'Gui\u_GuiSocketStream.pas',
  u_GuiStatistic in 'Gui\u_GuiStatistic.pas',
  u_GuiTcpServer in 'Gui\u_GuiTcpServer.pas',
  c_GeAuth in 'RequestHandler\GoogleEarth\c_GeAuth.pas',
  flatfile_any_version in 'RequestHandler\GoogleEarth\flatfile_any_version.pas',
  flatfile_regexpr in 'RequestHandler\GoogleEarth\flatfile_regexpr.pas',
  flatfile_tile_cache in 'RequestHandler\GoogleEarth\flatfile_tile_cache.pas',
  i_GeDbRootStorage in 'RequestHandler\GoogleEarth\i_GeDbRootStorage.pas',
  u_GeDbRootStorage in 'RequestHandler\GoogleEarth\u_GeDbRootStorage.pas',
  u_GeFlatFilePacketValidator in 'RequestHandler\GoogleEarth\u_GeFlatFilePacketValidator.pas',
  u_GeFlatFileRequestValidator in 'RequestHandler\GoogleEarth\u_GeFlatFileRequestValidator.pas',
  u_RequestHandlerGeAuth in 'RequestHandler\GoogleEarth\u_RequestHandlerGeAuth.pas',
  u_RequestHandlerGeDbRoot in 'RequestHandler\GoogleEarth\u_RequestHandlerGeDbRoot.pas',
  u_RequestHandlerGeFactory in 'RequestHandler\GoogleEarth\u_RequestHandlerGeFactory.pas',
  u_RequestHandlerGeFlatFile in 'RequestHandler\GoogleEarth\u_RequestHandlerGeFlatFile.pas',
  i_MimeTable in 'RequestHandler\SystemCache\i_MimeTable.pas',
  i_UrlTransformer in 'RequestHandler\SystemCache\i_UrlTransformer.pas',
  u_MimeTable in 'RequestHandler\SystemCache\u_MimeTable.pas',
  u_RequestHandlerSystemCache in 'RequestHandler\SystemCache\u_RequestHandlerSystemCache.pas',
  u_RequestHandlerSystemCacheFactory in 'RequestHandler\SystemCache\u_RequestHandlerSystemCacheFactory.pas',
  u_UrlToFileNameConverter in 'RequestHandler\SystemCache\u_UrlToFileNameConverter.pas',
  u_UrlTransformer in 'RequestHandler\SystemCache\u_UrlTransformer.pas',
  i_RequestHandlerFactory in 'RequestHandler\i_RequestHandlerFactory.pas',
  i_RequestHandlerFactoryMulti in 'RequestHandler\i_RequestHandlerFactoryMulti.pas',
  t_RequestHandler in 'RequestHandler\t_RequestHandler.pas',
  u_RequestHandlerFactoryMulti in 'RequestHandler\u_RequestHandlerFactoryMulti.pas',
  u_RequestHandlerMulti in 'RequestHandler\u_RequestHandlerMulti.pas',
  i_SSLCertificate in 'SSLSertificate\i_SSLCertificate.pas',
  u_SSLCertificate in 'SSLSertificate\u_SSLCertificate.pas',
  u_SSLCertificateCache in 'SSLSertificate\u_SSLCertificateCache.pas',
  u_SSLCertificateFactory in 'SSLSertificate\u_SSLCertificateFactory.pas',
  u_SSLCertificateGenerator in 'SSLSertificate\u_SSLCertificateGenerator.pas',
  u_SSLCertificatePemUtils in 'SSLSertificate\u_SSLCertificatePemUtils.pas',
  u_SSLCertificateProvider in 'SSLSertificate\u_SSLCertificateProvider.pas',
  u_SSLOpenSSLHeadersExtended in 'SSLSertificate\u_SSLOpenSSLHeadersExtended.pas',
  t_ServerActivityStat in 'ServerActivityStat\t_ServerActivityStat.pas',
  u_ServerActivityStat in 'ServerActivityStat\u_ServerActivityStat.pas',
  u_ServerActivityStatNotifier in 'ServerActivityStat\u_ServerActivityStatNotifier.pas',
  t_CustomHTTPServer in 't_CustomHTTPServer.pas',
  t_HttpRequestCounters in 't_HttpRequestCounters.pas',
  u_AppVersionInfo in 'u_AppVersionInfo.pas',
  u_CustomHTTPServer in 'u_CustomHTTPServer.pas',
  u_DateTimeTools in 'u_DateTimeTools.pas',
  u_GeoCacher in 'u_GeoCacher.pas',
  u_GlobalLog in 'u_GlobalLog.pas',
  u_HostForSslChecker in 'u_HostForSslChecker.pas',
  u_HttpContext in 'u_HttpContext.pas',
  u_HttpDownloader in 'u_HttpDownloader.pas',
  u_HttpDownloaderByIndy in 'u_HttpDownloaderByIndy.pas',
  u_HttpHeadersTools in 'u_HttpHeadersTools.pas',
  u_HttpRequestInfo in 'u_HttpRequestInfo.pas',
  u_HttpResponseInfo in 'u_HttpResponseInfo.pas',
  u_HttpServer in 'u_HttpServer.pas',
  u_ObjectPool in 'u_ObjectPool.pas',
  u_SSLIOHandlerHelper in 'u_SSLIOHandlerHelper.pas',
  u_ServerInterceptLog in 'u_ServerInterceptLog.pas',
  u_TcpTunnel in 'u_TcpTunnel.pas',
  u_WebClientFactory in 'u_WebClientFactory.pas';

{$R *.res}
{$R MainIcon\icon.res}

const
  IMAGE_DLLCHARACTERISTICS_NX_COMPAT = $0100;
  IMAGE_DLLCHARACTERISTICS_DYNAMIC_BASE = $0040;

{$SetPEOptFlags IMAGE_DLLCHARACTERISTICS_NX_COMPAT} // enables DEP
{$SetPEOptFlags IMAGE_DLLCHARACTERISTICS_DYNAMIC_BASE} // enables ASLR

{$SetPEFlags IMAGE_FILE_EXECUTABLE_IMAGE}

{$IFNDEF DEBUG}
  {$SetPEFlags IMAGE_FILE_DEBUG_STRIPPED}
  {$SetPEFlags IMAGE_FILE_LINE_NUMS_STRIPPED}
  {$SetPEFlags IMAGE_FILE_LOCAL_SYMS_STRIPPED}
{$ENDIF}

resourcestring
  rsClosedOnFatalError = 'GeoCacher terminated due to fatal error!';

var
  VGeoCacher: TGeoCacher;
begin
  {$IFDEF DEBUG}
  //ReportMemoryLeaksOnShutdown := True;
  {$ENDIF}
  
  GLog := TGlobalLog.Create;
  try
    try
      VGeoCacher := TGeoCacher.Create;
      try
        VGeoCacher.Run;
      finally
        VGeoCacher.Free;
      end;
    except
      on E: Exception do begin
        GLog.ShowExceptionMessage(E, rsClosedOnFatalError);
      end;
    end;
  finally
    FreeAndNil(GLog);
  end;
end.
