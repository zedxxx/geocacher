unit u_AppVersionInfo;

interface

function GetAppVersionText: string;

implementation

uses
  SysUtils,
  ExeInfo;

function GetAppVersionText: string;
var
  VMajor, VMinor, VRelease, VBuild: Word;
begin
  if ExeInfo.GetBuildVersionInfo(VMajor, VMinor, VRelease, VBuild) then begin
    Result := Format('%d.%d', [VMajor, VMinor]);
  end else begin
    Result := '';
  end;
end;

end.
