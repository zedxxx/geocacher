unit u_HttpDownloader;

interface

uses
  IdTCPConnection,
  u_HttpContext;

type
  THttpDownloader = class(TObject)
  public
    function DoRequest(const ACtx: THttpContext): Boolean; virtual; abstract;
    function GetRawConnection: TIdTcpConnection; virtual; abstract;
  end;

implementation

end.
