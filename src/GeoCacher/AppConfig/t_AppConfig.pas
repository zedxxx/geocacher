unit t_AppConfig;

interface

uses
  SysUtils,
  StrUtils,
  IniFiles;

type
  TArrayOfByte = array of Byte;
  TArrayOfString = array of string;

  TProxyType = (
    ptNone,
    ptHttp,
    ptSocks4,
    ptSocks4A,
    ptSocks5
  );

  TProxyInfo = record
    Enabled  : Boolean;

    PType    : TProxyType;
    Host     : string;
    Port     : Word;
    Username : string;
    Password : string;

    procedure Init;
    procedure Read(const AIni: TCustomIniFile);
    procedure Write(const AIni: TCustomIniFile);
  end;
  PProxyInfo = ^TProxyInfo;

  TFlatFileTileCacheConfigRec = record
    Path: string;
    PathFull: string;

    AllowRead: Boolean;
    AllowWrite: Boolean;

    AllowReadAnyVersion: Boolean;

    TestContentOnRead: Boolean;
    DeepTestJpegOnRead: Boolean;

    JpegPlaceholderEnabled: Boolean;
    JpegPlaceholderFileName: string;
    JpegPlaceholderData: TArrayOfByte;

    procedure Init;
    procedure Read(const AIni: TCustomIniFile);
    procedure Write(const AIni: TCustomIniFile);
  end;
  PFlatFileTileCacheConfigRec = ^TFlatFileTileCacheConfigRec;

  TFileSystemCacheConfigRec = record
    Path: string;
    PathFull: string;

    AllowRead: Boolean;
    AllowWrite: Boolean;

    // ToDo:
    //
    // White List
    // Black List
    // SaveToCache List
    //

    procedure Init;
    procedure Read(const AIni: TCustomIniFile);
    procedure Write(const AIni: TCustomIniFile);
  end;
  PFileSystemCacheConfigRec = ^TFileSystemCacheConfigRec;

  TGoogleEarthDbRootConfigRec = record
    Path: string;
    PathFull: string;

    AllowRead: Boolean;
    AllowWrite: Boolean;

    AllowPatch: Boolean;

    DisableGeLogo: Boolean;
    RemoveCopyrights: Boolean;
    DisableDiskCache: Boolean;
    DisableAuthentication: Boolean;
    FixPlanetaryDataBase: Boolean;

    DiscoverabilityAltitude: Integer;
    MaxRequestsPerQuery: Integer;

    procedure Init;
    procedure Read(const AIni: TCustomIniFile);
    procedure Write(const AIni: TCustomIniFile);
  end;
  PGoogleEarthDbRootConfigRec = ^TGoogleEarthDbRootConfigRec;

  TSslInterceptConfigRec = record
    Enabled: Boolean;

    HostsMask: TArrayOfString;
    HostsExcludeMask: TArrayOfString;

    OpenSslPath: string;

    procedure Init;
    procedure Read(const AIni: TCustomIniFile);
    procedure Write(const AIni: TCustomIniFile);
  end;
  PSslInterceptConfigRec = ^TSslInterceptConfigRec;

  TDebugLogConfigRec = record
    HttpLogEnabled: Boolean;
    HttpLogWithContent: Boolean;

    GuiThreadLogEnabled: Boolean;
    GuiThreadSendOk: Boolean;

    procedure Init;
    procedure Read(const AIni: TCustomIniFile);
    procedure Write(const AIni: TCustomIniFile);
  end;
  PDebugLogConfigRec = ^TDebugLogConfigRec;

  TGoogleEarthEnterpriseConfigRec = record
    Enabled: Boolean;

    Address: string;
    MixFlatFileCache: Boolean;

    procedure Init;
    procedure Read(const AIni: TCustomIniFile);
    procedure Write(const AIni: TCustomIniFile);
  end;
  PGoogleEarthEnterpriseConfigRec = ^TGoogleEarthEnterpriseConfigRec;

  TInetAndCacheMode = (
    wmInetOnly,
    wmInetAndCacheRW,
    wmInetAndCacheRO,
    wmInetAndCacheWO,
    wmCacheOnly
  );

  TAppConfigRec = record
    ListeningPortHttp: Word;
    ListeningPortTcp: Word;

    WorkMode: TInetAndCacheMode;
    LanguageCode: string;
    ShowTrayIcon: Boolean;

    Proxy: TProxyInfo;
    DbRoot: TGoogleEarthDbRootConfigRec;
    WebCache: TFileSystemCacheConfigRec;
    FlatFileCache: TFlatFileTileCacheConfigRec;
    SslIntercept: TSslInterceptConfigRec;

    Debug: TDebugLogConfigRec;

    EarthEnterprise: TGoogleEarthEnterpriseConfigRec;

    procedure Init;
    procedure Read(const AIni: TCustomIniFile);
    procedure Write(const AIni: TCustomIniFile);
  end;
  PAppConfigRec = ^TAppConfigRec;

implementation

uses
  u_FileSystemTools;

function _GetFullPath(const APath: string): string; inline;
begin
  Result := PathToFullPath(APath);
end;

function SplitStr(const AStr: string; const ASep: Char): TArrayOfString;
var
  I, J, K: Integer;
begin
  if AStr = '' then begin
    Result := nil;
    Exit;
  end;

  SetLength(Result, 32);

  J := 1;
  K := 0;
  while J < Length(AStr) do begin
    if K >= Length(Result) then begin
      SetLength(Result, K + 32);
    end;
    I := PosEx(ASep, AStr, J);
    if I = 0 then begin
      Result[K] := Trim(Copy(AStr, J));
      if Result[K] <> '' then begin
        Inc(K);
      end;
      Break;
    end;
    Result[K] := Trim(Copy(AStr, J, I-J));
    if Result[K] <> '' then begin
      Inc(K);
    end;
    J := I + 1;
  end;

  SetLength(Result, K);
end;

function JoinStr(const AArray: TArrayOfString; const ASep: Char): string;
var
  I: Integer;
begin
  if Length(AArray) = 0 then begin
    Result := '';
    Exit;
  end;

  Result := AArray[0];

  for I := 1 to Length(AArray) - 1 do begin
    Result := Result + ASep + AArray[I];
  end;
end;

{ TProxyInfo }

const
  CProxyIniSection = 'Proxy';

procedure TProxyInfo.Init;
begin
  Enabled := False;
  PType := ptNone;
  Host := '';
  Port := 0;
  Username := '';
  Password := '';
end;

procedure TProxyInfo.Read(const AIni: TCustomIniFile);
begin
  Enabled := AIni.ReadBool(CProxyIniSection, 'Enabled', Enabled);
  PType := TProxyType(AIni.ReadInteger(CProxyIniSection, 'Type', Integer(PType)));
  Host := AIni.ReadString(CProxyIniSection, 'Host', Host);
  Port := AIni.ReadInteger(CProxyIniSection, 'Port', Port);
  Username := AIni.ReadString(CProxyIniSection, 'Username', Username);
  Password := AIni.ReadString(CProxyIniSection, 'Password', Password);
end;

procedure TProxyInfo.Write(const AIni: TCustomIniFile);
begin
  AIni.WriteBool(CProxyIniSection, 'Enabled', Enabled);
  AIni.WriteInteger(CProxyIniSection, 'Type', Integer(PType));
  AIni.WriteString(CProxyIniSection, 'Host', Host);
  AIni.WriteInteger(CProxyIniSection, 'Port', Port);
  AIni.WriteString(CProxyIniSection, 'Username', Username);
  AIni.WriteString(CProxyIniSection, 'Password', Password);
end;

{ TFlatFileTileCacheConfigRec }

const
  CFlatFileIniSection = 'FlatFileCache';

procedure TFlatFileTileCacheConfigRec.Init;
begin
  Path := '.\cache\';
  PathFull :=  _GetFullPath(Path);
  AllowRead := True;
  AllowWrite := True;
  AllowReadAnyVersion := False;
  TestContentOnRead := False;
  DeepTestJpegOnRead := False;
  JpegPlaceholderEnabled := False;
  JpegPlaceholderFileName := '';
  JpegPlaceholderData := nil;
end;

procedure TFlatFileTileCacheConfigRec.Read(const AIni: TCustomIniFile);
begin
  Path := AIni.ReadString(CFlatFileIniSection, 'Path', Path);
  PathFull :=  _GetFullPath(Path);

  AllowRead := AIni.ReadBool(CFlatFileIniSection, 'AllowRead', AllowRead);
  AllowWrite := AIni.ReadBool(CFlatFileIniSection, 'AllowWrite', AllowWrite);
  AllowReadAnyVersion := AIni.ReadBool(CFlatFileIniSection, 'AllowReadAnyVersion', AllowReadAnyVersion);

  TestContentOnRead := AIni.ReadBool(CFlatFileIniSection, 'TestContentOnRead', TestContentOnRead);
  DeepTestJpegOnRead := AIni.ReadBool(CFlatFileIniSection, 'DeepTestJpegOnRead', DeepTestJpegOnRead);

  JpegPlaceholderEnabled := AIni.ReadBool(CFlatFileIniSection, 'JpegPlaceholderEnabled', JpegPlaceholderEnabled);
  JpegPlaceholderFileName := AIni.ReadString(CFlatFileIniSection, 'JpegPlaceholderFileName', JpegPlaceholderFileName);
end;

procedure TFlatFileTileCacheConfigRec.Write(const AIni: TCustomIniFile);
begin
  AIni.WriteString(CFlatFileIniSection, 'Path', Path);

  AIni.WriteBool(CFlatFileIniSection, 'AllowRead', AllowRead);
  AIni.WriteBool(CFlatFileIniSection, 'AllowWrite', AllowWrite);
  AIni.WriteBool(CFlatFileIniSection, 'AllowReadAnyVersion', AllowReadAnyVersion);

  AIni.WriteBool(CFlatFileIniSection, 'TestContentOnRead', TestContentOnRead);
  AIni.WriteBool(CFlatFileIniSection, 'DeepTestJpegOnRead', DeepTestJpegOnRead);

  AIni.WriteBool(CFlatFileIniSection, 'JpegPlaceholderEnabled', JpegPlaceholderEnabled);
  AIni.WriteString(CFlatFileIniSection, 'JpegPlaceholderFileName', JpegPlaceholderFileName);
end;

{ TFileSystemCacheConfigRec }

const
  CFileSystemCacheIniSection = 'WebCache';

procedure TFileSystemCacheConfigRec.Init;
begin
  Path := '.\cache\WebData\';
  PathFull := _GetFullPath(Path);

  AllowRead := True;
  AllowWrite := True;
end;

procedure TFileSystemCacheConfigRec.Read(const AIni: TCustomIniFile);
begin
  Path := AIni.ReadString(CFileSystemCacheIniSection, 'Path', Path);
  PathFull :=  _GetFullPath(Path);

  AllowRead := AIni.ReadBool(CFileSystemCacheIniSection, 'AllowRead', AllowRead);
  AllowWrite := AIni.ReadBool(CFileSystemCacheIniSection, 'AllowWrite', AllowWrite);
end;

procedure TFileSystemCacheConfigRec.Write(const AIni: TCustomIniFile);
begin
  AIni.WriteString(CFileSystemCacheIniSection, 'Path', Path);

  AIni.WriteBool(CFileSystemCacheIniSection, 'AllowRead', AllowRead);
  AIni.WriteBool(CFileSystemCacheIniSection, 'AllowWrite', AllowWrite);
end;

{ TGoogleEarthDbRootConfigRec }

const
  CDbRootCacheIniSection = 'dbRootCache';
  CDbRootPatchIniSection = 'dbRootPatch';

procedure TGoogleEarthDbRootConfigRec.Init;
begin
  Path := '.\cache\dbRoot\';
  PathFull := _GetFullPath(Path);

  AllowRead := True;
  AllowWrite := True;

  AllowPatch := True;

  DisableGeLogo := True;
  RemoveCopyrights := True;
  DisableDiskCache := True;
  DisableAuthentication := True;
  FixPlanetaryDataBase := True;

  DiscoverabilityAltitude := 0;
  MaxRequestsPerQuery := 0;
end;

procedure TGoogleEarthDbRootConfigRec.Read(const AIni: TCustomIniFile);
begin
  Path := AIni.ReadString(CDbRootCacheIniSection, 'Path', Path);
  PathFull :=  _GetFullPath(Path);

  AllowRead := AIni.ReadBool(CDbRootCacheIniSection, 'AllowRead', AllowRead);
  AllowWrite := AIni.ReadBool(CDbRootCacheIniSection, 'AllowWrite', AllowWrite);

  AllowPatch := AIni.ReadBool(CDbRootPatchIniSection, 'Enable', AllowPatch);

  DisableGeLogo := AIni.ReadBool(CDbRootPatchIniSection, 'DisableGeLogo', DisableGeLogo);
  RemoveCopyrights := AIni.ReadBool(CDbRootPatchIniSection, 'RemoveCopyrights', RemoveCopyrights);
  DisableDiskCache := AIni.ReadBool(CDbRootPatchIniSection, 'DisableDiskCache', DisableDiskCache);
  DisableAuthentication := AIni.ReadBool(CDbRootPatchIniSection, 'DisableAuthentication', DisableAuthentication);
  FixPlanetaryDataBase := AIni.ReadBool(CDbRootPatchIniSection, 'FixPlanetaryDataBase', FixPlanetaryDataBase);

  DiscoverabilityAltitude := AIni.ReadInteger(CDbRootPatchIniSection, 'DiscoverabilityAltitude', DiscoverabilityAltitude);
  MaxRequestsPerQuery := AIni.ReadInteger(CDbRootPatchIniSection, 'MaxRequestsPerQuery', MaxRequestsPerQuery);
end;

procedure TGoogleEarthDbRootConfigRec.Write(const AIni: TCustomIniFile);
begin
  AIni.WriteString(CDbRootCacheIniSection, 'Path', Path);

  AIni.WriteBool(CDbRootCacheIniSection, 'AllowRead', AllowRead);
  AIni.WriteBool(CDbRootCacheIniSection, 'AllowWrite', AllowWrite);

  AIni.WriteBool(CDbRootPatchIniSection, 'Enable', AllowPatch);

  AIni.WriteBool(CDbRootPatchIniSection, 'DisableGeLogo', DisableGeLogo);
  AIni.WriteBool(CDbRootPatchIniSection, 'RemoveCopyrights', RemoveCopyrights);
  AIni.WriteBool(CDbRootPatchIniSection, 'DisableDiskCache', DisableDiskCache);
  AIni.WriteBool(CDbRootPatchIniSection, 'DisableAuthentication', DisableAuthentication);
  AIni.WriteBool(CDbRootPatchIniSection, 'FixPlanetaryDataBase', FixPlanetaryDataBase);

  AIni.WriteInteger(CDbRootPatchIniSection, 'DiscoverabilityAltitude', DiscoverabilityAltitude);
  AIni.WriteInteger(CDbRootPatchIniSection, 'MaxRequestsPerQuery', MaxRequestsPerQuery);
end;

{ TSslInterceptConfigRec }

const
  CSslIniSection = 'Ssl';

  CSslHostsDelim = ';';
  CSslHostsDefault =
    '*\.google\.com'            + CSslHostsDelim +
    '*\.googleapis\.com'        + CSslHostsDelim +
    '*\.gstatic\.com'           + CSslHostsDelim +
    '*\.ggpht\.com'             + CSslHostsDelim +
    '*\.googleusercontent\.com' + CSslHostsDelim +
    '*\.360cities\.net'         + CSslHostsDelim +
    '*\.panoramio\.com'         + CSslHostsDelim +
    '*\.wikimedia\.org'         + CSslHostsDelim;

procedure TSslInterceptConfigRec.Init;
begin
  Enabled := True;
  HostsMask := nil;
  HostsExcludeMask := nil;
  OpenSslPath := _GetFullPath('.\openssl\');
end;

procedure TSslInterceptConfigRec.Read(const AIni: TCustomIniFile);
var
  VHosts: string;
begin
  Enabled := AIni.ReadBool(CSslIniSection, 'Enabled', Enabled);

  VHosts := AIni.ReadString(CSslIniSection, 'Hosts', CSslHostsDefault);
  HostsMask := SplitStr(VHosts, CSslHostsDelim);

  VHosts := AIni.ReadString(CSslIniSection, 'HostsExclude', '');
  HostsExcludeMask := SplitStr(VHosts, CSslHostsDelim);
end;

procedure TSslInterceptConfigRec.Write(const AIni: TCustomIniFile);
begin
  AIni.WriteBool(CSslIniSection, 'Enabled', Enabled);
  AIni.WriteString(CSslIniSection, 'Hosts', JoinStr(HostsMask, CSslHostsDelim));
  AIni.WriteString(CSslIniSection, 'HostsExclude', JoinStr(HostsExcludeMask, CSslHostsDelim));
end;

{ TAppConfigRec }

const
  CAppConfigIniSection = 'Main';

procedure TAppConfigRec.Init;
begin
  ListeningPortHttp := 8081;
  ListeningPortTcp := 8082;

  WorkMode := wmInetAndCacheRW;
  LanguageCode := 'en';
  ShowTrayIcon := True;

  Proxy.Init;
  DbRoot.Init;
  WebCache.Init;
  FlatFileCache.Init;
  SslIntercept.Init;
  Debug.Init;
  EarthEnterprise.Init;
end;

procedure TAppConfigRec.Read(const AIni: TCustomIniFile);
begin
  ListeningPortHttp := AIni.ReadInteger(CAppConfigIniSection, 'ListeningPortHttp', ListeningPortHttp);
  ListeningPortTcp := AIni.ReadInteger(CAppConfigIniSection, 'ListeningPortTcp', ListeningPortTcp);
  WorkMode := TInetAndCacheMode(AIni.ReadInteger(CAppConfigIniSection, 'InetAndCacheMode', Integer(WorkMode)));
  LanguageCode := AIni.ReadString(CAppConfigIniSection, 'LanguageCode', LanguageCode);
  ShowTrayIcon := AIni.ReadBool(CAppConfigIniSection, 'ShowTrayIcon', ShowTrayIcon);

  Proxy.Read(AIni);
  DbRoot.Read(AIni);
  WebCache.Read(AIni);
  FlatFileCache.Read(AIni);
  SslIntercept.Read(AIni);
  Debug.Read(AIni);
  EarthEnterprise.Read(AIni);
end;

procedure TAppConfigRec.Write(const AIni: TCustomIniFile);
begin
  AIni.WriteInteger(CAppConfigIniSection, 'ListeningPortHttp', ListeningPortHttp);
  AIni.WriteInteger(CAppConfigIniSection, 'ListeningPortTcp', ListeningPortTcp);
  AIni.WriteInteger(CAppConfigIniSection, 'InetAndCacheMode', Integer(WorkMode));
  AIni.WriteString(CAppConfigIniSection, 'LanguageCode', LanguageCode);
  AIni.WriteBool(CAppConfigIniSection, 'ShowTrayIcon', ShowTrayIcon);

  Proxy.Write(AIni);
  DbRoot.Write(AIni);
  WebCache.Write(AIni);
  FlatFileCache.Write(AIni);
  SslIntercept.Write(AIni);
  Debug.Write(AIni);
  EarthEnterprise.Write(AIni);
end;

{ TDebugLogConfigRec }

const
  CDebugIniSection = 'Debug';

procedure TDebugLogConfigRec.Init;
begin
  HttpLogEnabled := False;
  HttpLogWithContent := False;

  GuiThreadLogEnabled := False;
  GuiThreadSendOk := False;
end;

procedure TDebugLogConfigRec.Read(const AIni: TCustomIniFile);
begin
  HttpLogEnabled := AIni.ReadBool(CDebugIniSection, 'HttpLogEnabled', HttpLogEnabled);
  HttpLogWithContent := AIni.ReadBool(CDebugIniSection, 'HttpLogWithContent', HttpLogWithContent);

  GuiThreadLogEnabled := AIni.ReadBool(CDebugIniSection, 'GuiThreadLogEnabled', GuiThreadLogEnabled);
  GuiThreadSendOk := AIni.ReadBool(CDebugIniSection, 'GuiThreadSendOk', GuiThreadSendOk);
end;

procedure TDebugLogConfigRec.Write(const AIni: TCustomIniFile);
begin
  AIni.WriteBool(CDebugIniSection, 'HttpLogEnabled', HttpLogEnabled);
  AIni.WriteBool(CDebugIniSection, 'HttpLogWithContent', HttpLogWithContent);

  AIni.WriteBool(CDebugIniSection, 'GuiThreadLogEnabled', GuiThreadLogEnabled);
  AIni.WriteBool(CDebugIniSection, 'GuiThreadSendOk', GuiThreadSendOk);
end;

{ TGoogleEarthEnterpriseConfigRec }

const
  CEarthEnterpriseIniSection = 'EarthEnterprise';

procedure TGoogleEarthEnterpriseConfigRec.Init;
begin
  Enabled := False;
  Address := '';
  MixFlatFileCache := False;
end;

procedure TGoogleEarthEnterpriseConfigRec.Read(const AIni: TCustomIniFile);
begin
  Enabled := AIni.ReadBool(CEarthEnterpriseIniSection, 'Enabled', Enabled);
  Address := AIni.ReadString(CEarthEnterpriseIniSection, 'Address', Address);
  MixFlatFileCache := AIni.ReadBool(CEarthEnterpriseIniSection, 'MixFlatFileCache', MixFlatFileCache);

  Enabled := Enabled and (Address <> '');

  if Enabled and (Address[Length(Address)] <> '/') then begin
    Address := Address + '/';
  end;
end;

procedure TGoogleEarthEnterpriseConfigRec.Write(const AIni: TCustomIniFile);
begin
  AIni.WriteBool(CEarthEnterpriseIniSection, 'Enabled', Enabled);
  AIni.WriteString(CEarthEnterpriseIniSection, 'Address', Address);
  AIni.WriteBool(CEarthEnterpriseIniSection, 'MixFlatFileCache', MixFlatFileCache);
end;

end.
