unit u_AppConfig;

interface

uses
  Classes,
  SysUtils,
  t_AppConfig,
  i_AppConfig,
  i_Notifier,
  i_Listener,
  i_LanguageManager;

type
  TAppConfig = class(TInterfacedObject, IAppConfig)
  private
    FRec: TAppConfigRec;
    FAppPathFull: string;
    FLanguageManager: ILanguageManager;
    FGeoGuiFileName: string;
    FIsGeoGuiAvailable: Boolean;

    FLock: IReadWriteSync;
    FStatic: IAppConfigStatic;
    FNotifierInternal: INotifierInternal;

    FIgnoreLanguageChange: Boolean;
    FLanguageChangeListener: IListener;

    procedure OnLanguageChange;

    function MakeStatic: IAppConfigStatic; inline;
    procedure UpdateLanguage;
    procedure UpdateJpegPlaceholder;
    procedure DoChanged;
  private
    { IAppConfig }
    procedure SetWorkMode(const AValue: TInetAndCacheMode);
    procedure SetUseDbRootPatch(const AValue: Boolean);
    procedure SetFlatFileAnyVersion(const AValue: Boolean);

    procedure LoadFromStream(const AStream: TStream);
    procedure SaveToStream(const AStream: TStream);

    function GetChangeNotifier: INotifier;

    function GetLanguageManager: ILanguageManager;
    function GetAppPath: string;
    function GetIsGeoGuiAvailable: Boolean;
    function GetGeoGuiFileName: string;

    function GetStatic: IAppConfigStatic;
  public
    constructor Create;
    destructor Destroy; override;
  end;

implementation

uses
  {$IFDEF UNICODE}
  IniFiles,
  {$ELSE}
  Compatibility,
  CompatibilityIniFiles,
  {$ENDIF}
  libgepacket,
  u_Notifier,
  u_ListenerByEvent,
  u_Synchronizer,
  u_LanguageManager,
  u_FileSystemTools,
  u_AppConfigStatic;

{ TAppConfig }

constructor TAppConfig.Create;
begin
  inherited Create;

  FLock := GSync.SyncStd.Make;
  FNotifierInternal := TNotifierBase.Create;

  FAppPathFull := ExtractFilePath(ParamStr(0));

  FGeoGuiFileName := FAppPathFull + 'GeoGui.exe';
  FIsGeoGuiAvailable := FileExists(FGeoGuiFileName);

  FRec.Init;

  FIgnoreLanguageChange := False;
  FLanguageManager := TLanguageManager.Create(FAppPathFull + 'lang');
  UpdateLanguage;

  FLanguageChangeListener := TNotifyNoMmgEventListener.Create(Self.OnLanguageChange);
  FLanguageManager.ChangeNotifier.Add(FLanguageChangeListener);

  UpdateJpegPlaceholder;

  FStatic := MakeStatic;
end;

destructor TAppConfig.Destroy;
begin
  if Assigned(FLanguageManager) and Assigned(FLanguageChangeListener) then begin
    FLanguageManager.ChangeNotifier.Remove(FLanguageChangeListener);
    FLanguageChangeListener := nil;
  end;
  inherited Destroy;
end;

procedure TAppConfig.OnLanguageChange;
var
  VCode: string;
begin
  if FIgnoreLanguageChange then begin
    Exit;
  end;

  FLock.BeginWrite;
  try
    VCode := FLanguageManager.CurrentLanguageCode;
    if FRec.LanguageCode <> VCode then begin
      FRec.LanguageCode := VCode;
      DoChanged;
    end;
  finally
    FLock.EndWrite;
  end;
end;

procedure TAppConfig.UpdateLanguage;
var
  I: Integer;
begin
  if FLanguageManager.LanguageList.FindCode(FRec.LanguageCode, I) then begin
    FIgnoreLanguageChange := True;
    try
      FLanguageManager.CurrentLanguageCode := FRec.LanguageCode;
    finally
      FIgnoreLanguageChange := False;
    end;
  end else begin
    FRec.LanguageCode := FLanguageManager.CurrentLanguageCode;
  end;
end;

procedure TAppConfig.UpdateJpegPlaceholder;
var
  VDoReset: Boolean;
  VFileName: string;
  VStream: TMemoryStream;
begin
  VDoReset := True;

  if FRec.FlatFileCache.JpegPlaceholderEnabled then begin
    VFileName := FRec.FlatFileCache.JpegPlaceholderFileName;
    VFileName := PathToFullPath(ExtractFilePath(VFileName)) + ExtractFileName(VFileName);
    if FileExists(VFileName) then begin
      VStream := TMemoryStream.Create;
      try
        VStream.LoadFromFile(VFileName);
        libgepacket.gepacket_encode(VStream.Memory, VStream.Size);
        SetLength(FRec.FlatFileCache.JpegPlaceholderData, VStream.Size);
        VStream.ReadBuffer(
          FRec.FlatFileCache.JpegPlaceholderData[0],
          VStream.Size
        );
        VDoReset := False;
      finally
        VStream.Free;
      end;
    end;
  end;

  if VDoReset then begin
    FRec.FlatFileCache.JpegPlaceholderData := nil;
    FRec.FlatFileCache.JpegPlaceholderEnabled := False;
  end;
end;

function TAppConfig.MakeStatic: IAppConfigStatic;
begin
  Result := TAppConfigStatic.Create(@FRec);
end;

procedure TAppConfig.DoChanged;
begin
  FStatic := MakeStatic;
  FNotifierInternal.Notify(FStatic);
end;

function TAppConfig.GetAppPath: string;
begin
  Result := FAppPathFull;
end;

function TAppConfig.GetChangeNotifier: INotifier;
begin
  Result := FNotifierInternal as INotifier;
end;

function TAppConfig.GetGeoGuiFileName: string;
begin
  Result := FGeoGuiFileName;
end;

function TAppConfig.GetIsGeoGuiAvailable: Boolean;
begin
  Result := FIsGeoGuiAvailable;
end;

function TAppConfig.GetLanguageManager: ILanguageManager;
begin
  Result := FLanguageManager;
end;

procedure TAppConfig.LoadFromStream(const AStream: TStream);
var
  VIni: TMemIniFile;
begin
  FLock.BeginWrite;
  try
    VIni := TMemIniFile.Create(AStream, TEncoding.UTF8);
    try
      FRec.Read(VIni);
    finally
      VIni.Free;
    end;
    UpdateLanguage;
    UpdateJpegPlaceholder;
    DoChanged;
  finally
    FLock.EndWrite;
  end;
end;

procedure TAppConfig.SaveToStream(const AStream: TStream);
var
  VIni: TMemIniFile;
begin
  VIni := TMemIniFile.Create(AStream, TEncoding.UTF8);
  try
    FLock.BeginRead;
    try
      FRec.Write(VIni);
    finally
      FLock.EndRead;
    end;

    VIni.UpdateFile;
  finally
    VIni.Free;
  end;
end;

procedure TAppConfig.SetWorkMode(const AValue: TInetAndCacheMode);
begin
  FLock.BeginWrite;
  try
    if FRec.WorkMode <> AValue then begin
      FRec.WorkMode := AValue;
      DoChanged;
    end;
  finally
    FLock.EndWrite;
  end;
end;

procedure TAppConfig.SetFlatFileAnyVersion(const AValue: Boolean);
begin
  FLock.BeginWrite;
  try
    with FRec.FlatFileCache do begin
      if AllowReadAnyVersion <> AValue then begin
        AllowReadAnyVersion := AValue;
        DoChanged;
      end;
    end;
  finally
    FLock.EndWrite;
  end;
end;

procedure TAppConfig.SetUseDbRootPatch(const AValue: Boolean);
begin
  FLock.BeginWrite;
  try
    with FRec.DbRoot do begin
      if AllowPatch <> AValue then begin
        AllowPatch := AValue;
        DoChanged;
      end;
    end;
  finally
    FLock.EndWrite;
  end;
end;

function TAppConfig.GetStatic: IAppConfigStatic;
begin
  FLock.BeginRead;
  try
    Result := FStatic;
  finally
    FLock.EndRead;
  end;
end;

end.
