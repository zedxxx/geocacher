unit i_AppConfig;

interface

uses
  Classes,
  t_AppConfig,
  i_Notifier,
  i_LanguageManager;

type
  IAppConfigStatic = interface
    ['{194372C0-9D5C-4FA3-8F0A-E1A46A1575E3}']

    function GetRecPtr: PAppConfigRec;
    property RecPtr: PAppConfigRec read GetRecPtr;
  end;

  IAppConfig = interface
    ['{E00AC3A1-35F9-423E-ABF9-262295872B92}']

    procedure SetWorkMode(const AValue: TInetAndCacheMode);
    procedure SetUseDbRootPatch(const AValue: Boolean);
    procedure SetFlatFileAnyVersion(const AValue: Boolean);

    procedure LoadFromStream(const AStream: TStream);
    procedure SaveToStream(const AStream: TStream);

    function GetChangeNotifier: INotifier;
    property ChangeNotifier: INotifier read GetChangeNotifier;

    function GetLanguageManager: ILanguageManager;
    property LanguageManager: ILanguageManager read GetLanguageManager;

    function GetAppPath: string;
    property AppPath: string read GetAppPath;

    function GetIsGeoGuiAvailable: Boolean;
    property IsGeoGuiAvailable: Boolean read GetIsGeoGuiAvailable;

    function GetGeoGuiFileName: string;
    property GeoGuiFileName: string read GetGeoGuiFileName;

    function GetStatic: IAppConfigStatic;
  end;

implementation

end.
