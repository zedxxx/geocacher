unit u_AppConfigStatic;

interface

uses
  t_AppConfig,
  i_AppConfig;

type
  TAppConfigStatic = class(TInterfacedObject, IAppConfigStatic)
  private
    FRec: TAppConfigRec;
  private
    { IAppConfigStatic }
    function GetRecPtr: PAppConfigRec;
  public
    constructor Create(const ARec: PAppConfigRec);
  end;

implementation

{ TAppConfigStatic }

constructor TAppConfigStatic.Create(const ARec: PAppConfigRec);
begin
  inherited Create;

  FRec := ARec^; // make a copy
  FRec.FlatFileCache.JpegPlaceholderData := Copy(ARec.FlatFileCache.JpegPlaceholderData)
end;

function TAppConfigStatic.GetRecPtr: PAppConfigRec;
begin
  Result := @FRec;
end;

end.
