unit u_CustomHTTPServer;

interface

uses
  Classes,
  SysUtils,
  IdAssignedNumbers,
  IdContext,
  IdException,
  IdGlobal,
  IdStack,
  IdExceptionCore,
  IdGlobalProtocols,
  IdHeaderList,
  IdCustomTCPServer,
  IdTCPConnection,
  IdStackConsts,
  IdBaseComponent,
  t_CustomHTTPServer,
  u_GlobalLog,
  u_HttpRequestInfo,
  u_HttpResponseInfo;

const
  Id_TId_HTTPServer_KeepAlive = True;
  Id_TId_HTTPMaximumHeaderLineCount = 1024;

  HTTPRequestStrings: array[0..Ord(High(THTTPCommandType))] of string =
    ('UNKNOWN', 'HEAD','GET','POST','DELETE','PUT','TRACE', 'OPTIONS', 'CONNECT', 'PATCH');

type
  //events
  TIdHTTPCreatePostStream = procedure(AContext: TIdContext; AHeaders: TIdHeaderList; var VPostStream: TStream) of object;
  TIdHTTPDoneWithPostStream = procedure(AContext: TIdContext; ARequestInfo: THTTPRequestInfo; var VCanFree: Boolean) of object;
  TIdHTTPParseAuthenticationEvent = procedure(AContext: TIdContext; const AAuthType, AAuthData: String; var VUsername, VPassword: String; var VHandled: Boolean) of object;

  TIdHTTPCommandEvent = procedure(
    const AContext: TIdContext;
    const ARequestInfo: THTTPRequestInfo;
    const AResponseInfo: THttpResponseInfo
  ) of object;

  TIdHTTPCommandError = procedure(AContext: TIdContext;
    ARequestInfo: THTTPRequestInfo; AResponseInfo: THttpResponseInfo;
    AException: Exception) of object;

  TIdHTTPHeadersAvailableEvent = procedure(
    const AContext: TIdContext;
    const ARequestInfo: THTTPRequestInfo;
    const AResponseInfo: THttpResponseInfo;
    var VContinueProcessing: Boolean
  ) of object;


  TIdHTTPHeadersBlockedEvent = procedure(AContext: TIdContext; AHeaders: TIdHeaderList; var VResponseNo: Integer; var VResponseText, VContentText: String) of object;
  TIdHTTPHeaderExpectationsEvent = procedure(AContext: TIdContext; const AExpectations: String; var VContinueProcessing: Boolean) of object;
  TIdHTTPQuerySSLPortEvent = procedure(APort: TIdPort; var VUseSSL: Boolean) of object;

  //objects
  EIdHTTPServerError = class(EIdException);
  EIdHTTPErrorParsingCommand = class(EIdHTTPServerError);
  EIdHTTPUnsupportedAuthorisationScheme = class(EIdHTTPServerError);

  TCustomHTTPServer = class(TIdCustomTCPServer)
  protected
    FKeepAlive: Boolean;
    FParseParams: Boolean;
    FServerSoftware: string;

    FOnCreatePostStream: TIdHTTPCreatePostStream;
    FOnDoneWithPostStream: TIdHTTPDoneWithPostStream;
    FOnParseAuthentication: TIdHTTPParseAuthenticationEvent;
    FOnHttpCommand: TIdHTTPCommandEvent;
    FOnCommandConnect: TIdHTTPCommandEvent;
    FOnCommandError: TIdHTTPCommandError;
    FOnHeadersAvailable: TIdHTTPHeadersAvailableEvent;
    FOnHeadersBlocked: TIdHTTPHeadersBlockedEvent;
    FOnHeaderExpectations: TIdHTTPHeaderExpectationsEvent;
    FOnQuerySSLPort: TIdHTTPQuerySSLPortEvent;

    FMaximumHeaderLineCount: Integer;

    procedure CreatePostStream(ASender: TIdContext; AHeaders: TIdHeaderList; var VPostStream: TStream); virtual;
    procedure DoneWithPostStream(ASender: TIdContext; ARequestInfo: THTTPRequestInfo); virtual;

    procedure DoConnect(AContext: TIdContext); override;
    procedure DoHeadersBlocked(ASender: TIdContext; AHeaders: TIdHeaderList; var VResponseNo: Integer; var VResponseText, VContentText: String); virtual;
    function DoHeaderExpectations(ASender: TIdContext; const AExpectations: String): Boolean; virtual;
    function DoQuerySSLPort(APort: TIdPort): Boolean; virtual;
    //
    function DoExecute(AContext:TIdContext): Boolean; override;
    //
    procedure InitComponent; override;
  public
    destructor Destroy; override;
  public
    property DefaultPort default IdPORT_HTTP;
    property KeepAlive: Boolean read FKeepAlive write FKeepAlive default Id_TId_HTTPServer_KeepAlive;
    property MaximumHeaderLineCount: Integer read FMaximumHeaderLineCount write FMaximumHeaderLineCount default Id_TId_HTTPMaximumHeaderLineCount;
    property ServerSoftware: string read FServerSoftware write FServerSoftware;
    //
    property OnCreatePostStream: TIdHTTPCreatePostStream read FOnCreatePostStream write FOnCreatePostStream;
    property OnDoneWithPostStream: TIdHTTPDoneWithPostStream read FOnDoneWithPostStream write FOnDoneWithPostStream;

    property OnHttpCommand: TIdHTTPCommandEvent read FOnHttpCommand write FOnHttpCommand;
    property OnCommandError: TIdHTTPCommandError read FOnCommandError write FOnCommandError;

    property OnHeadersAvailable: TIdHTTPHeadersAvailableEvent read FOnHeadersAvailable write FOnHeadersAvailable;
    property OnHeadersBlocked: TIdHTTPHeadersBlockedEvent read FOnHeadersBlocked write FOnHeadersBlocked;
    property OnHeaderExpectations: TIdHTTPHeaderExpectationsEvent read FOnHeaderExpectations write FOnHeaderExpectations;

    property OnParseAuthentication: TIdHTTPParseAuthenticationEvent read FOnParseAuthentication write FOnParseAuthentication;
    property OnQuerySSLPort: TIdHTTPQuerySSLPortEvent read FOnQuerySSLPort write FOnQuerySSLPort;
  end;

implementation

uses
  {$IFDEF VCL_XE3_OR_ABOVE}
  System.SyncObjs,
  {$ENDIF}
  {$IFDEF VCL_2010_OR_ABOVE}
    {$IFDEF WINDOWS}
  Windows,
    {$ENDIF}
  {$ENDIF}
  IdURI,
  IdIOHandler,
  IdIOHandlerSocket,
  IdSSL,
  IdResourceStringsCore,
  IdResourceStringsProtocols,
  IdHTTPHeaderInfo;

type
  // "protected hack" for call TIdRequestHeaderInfo.ProcessHeaders
  TIdRequestHeaderInfo = class(IdHttpHeaderInfo.TIdRequestHeaderInfo);
  
function DecodeHTTPCommand(const ACmd: string): THTTPCommandType;
var
  I: Integer;
begin
  Result := hcUnknown;
  for I := Low(HTTPRequestStrings) to High(HTTPRequestStrings) do begin
    if TextIsSame(ACmd, HTTPRequestStrings[I]) then begin
      Result := THTTPCommandType(I);
      Exit;
    end;
  end;
end;

function InternalReadLn(AIOHandler: TIdIOHandler): String; inline;
begin
  Result := AIOHandler.ReadLn;
  if AIOHandler.ReadLnTimedout then begin
    raise EIdReadTimeout.Create(RSReadTimeout);
  end;
end;

{ TIdCustomHTTPServer }

procedure TCustomHTTPServer.InitComponent;
begin
  inherited InitComponent;
  DefaultPort := IdPORT_HTTP;
  FKeepAlive := Id_TId_HTTPServer_KeepAlive;
  FMaximumHeaderLineCount := Id_TId_HTTPMaximumHeaderLineCount;
end;

destructor TCustomHTTPServer.Destroy;
begin
  Active := False; // Set Active to false in order to close all active sessions.
  inherited Destroy;
end;

procedure TCustomHTTPServer.DoConnect(AContext: TIdContext);
begin
  // RLebeau 6/17/08: let the user decide whether to enable SSL in their
  // own event handler.  Indy should not be making any assumptions about
  // whether to implicitally force SSL on any given connection.  This
  // prevents a single server from handling both SSL and non-SSL connections
  // together.  The whole point of the PassThrough property is to allow
  // per-connection SSL handling.
  //
  // TODO: move this new logic into TIdCustomTCPServer directly somehow
  
  if AContext.Connection.IOHandler is TIdSSLIOHandlerSocketBase then begin
    TIdSSLIOHandlerSocketBase(AContext.Connection.IOHandler).PassThrough :=
      not DoQuerySSLPort(AContext.Connection.Socket.Binding.Port);
  end;
  inherited DoConnect(AContext);
end;

function TCustomHTTPServer.DoQuerySSLPort(APort: TIdPort): Boolean;
begin
  Result := not Assigned(FOnQuerySSLPort);
  if not Result then begin
    FOnQuerySSLPort(APort, Result);
  end;
end;

procedure TCustomHTTPServer.DoHeadersBlocked(ASender: TIdContext; AHeaders: TIdHeaderList;
  var VResponseNo: Integer; var VResponseText, VContentText: String);
begin
  VResponseNo := 403;
  VResponseText := '';
  VContentText := ''; 
  if Assigned(OnHeadersBlocked) then begin
    OnHeadersBlocked(ASender, AHeaders, VResponseNo, VResponseText, VContentText);
  end;
end;

function TCustomHTTPServer.DoHeaderExpectations(ASender: TIdContext; const AExpectations: String): Boolean;
begin
  Result := TextIsSame(AExpectations, '100-continue');  {Do not Localize}
  if Assigned(OnHeaderExpectations) then begin
    OnHeaderExpectations(ASender, AExpectations, Result);
  end;
end;

function TCustomHTTPServer.DoExecute(AContext:TIdContext): boolean;
var
  LRequestInfo: THTTPRequestInfo;
  LResponseInfo: THttpResponseInfo;

  function GetRemoteIP(ASocket: TIdIOHandlerSocket): String;
  begin
    Result := '';
    if ASocket <> nil then begin
      if ASocket.Binding <> nil then begin
        Result := ASocket.Binding.PeerIP;
      end;
    end;
  end;

  function HeadersCanContinue: Boolean;
  var
    LResponseNo: Integer;
    LResponseText, LContentText, S: String;
  begin
    // let the user decide if the request headers are acceptable
    Result := True;
    if Assigned(OnHeadersAvailable) then begin
      OnHeadersAvailable(AContext, LRequestInfo, LResponseInfo, Result);
    end;
    if not Result then begin
      DoHeadersBlocked(AContext, LRequestInfo.RawHeaders, LResponseNo, LResponseText, LContentText);
      LResponseInfo.ResponseNo := LResponseNo;
      if Length(LResponseText) > 0 then begin
        LResponseInfo.ResponseText := LResponseText;
      end; 
      LResponseInfo.ContentText := LContentText;
      LResponseInfo.CloseConnection := True;
      LResponseInfo.WriteHeader;
      if Length(LContentText) > 0 then begin
        LResponseInfo.WriteContent;
      end;
      Exit;
    end;

    // check for HTTP v1.1 'Host' and 'Expect' headers...

    if not LRequestInfo.IsVersionAtLeast(1, 1) then begin
      Exit;
    end;

    // MUST report a 400 (Bad Request) error if an HTTP/1.1
    // request does not include a 'Host' header
    S := LRequestInfo.RawHeaders.Values['Host'];
    if Length(S) = 0 then begin
      LResponseInfo.ResponseNo := 400;
      LResponseInfo.CloseConnection := True;
      LResponseInfo.WriteHeader;
      Exit;
    end;
    
    // if the client has already sent some or all of the request
    // body then don't bother checking for a v1.1 'Expect' header
    if not AContext.Connection.IOHandler.InputBufferIsEmpty then begin
      Exit;
    end;

    S := LRequestInfo.RawHeaders.Values['Expect'];
    if Length(S) = 0 then begin
      Exit;
    end;

    // check if the client expectations can be satisfied...
    Result := DoHeaderExpectations(AContext, S);
    if not Result then begin
      LResponseInfo.ResponseNo := 417;
      LResponseInfo.CloseConnection := True;
      LResponseInfo.WriteHeader;
      Exit;
    end;

    if Pos('100-continue', LowerCase(S)) > 0 then begin  {Do not Localize}
      // the client requested a '100-continue' expectation so send
      // a '100 Continue' reply now before the request body can be read
      AContext.Connection.IOHandler.WriteLn(LRequestInfo.Version + ' 100 ' + RSHTTPContinue + EOL);    {Do not Localize}
    end;
  end;

  function PreparePostStream: Boolean;
  var
    I, Size: Integer;
    S: String;
    LIOHandler: TIdIOHandler;
  begin
    Result := False;
    LIOHandler := AContext.Connection.IOHandler;

    // RLebeau 1/6/2009: don't create the PostStream unless there is
    // actually something to read. This should make it easier for the
    // request handler to know when to use the PostStream and when to
    // use the (Unparsed)Params instead...

    if (LRequestInfo.TransferEncoding <> '') and
      (not TextIsSame(LRequestInfo.TransferEncoding, 'identity')) then {do not localize}
    begin
      if IndyPos('chunked', LowerCase(LRequestInfo.TransferEncoding)) = 0 then begin {do not localize}
        LResponseInfo.ResponseNo := 400; // bad request
        LResponseInfo.CloseConnection := True;
        LResponseInfo.WriteHeader;
        Exit;
      end;
      CreatePostStream(AContext, LRequestInfo.RawHeaders, LRequestInfo.FPostStream);
      if LRequestInfo.FPostStream = nil then begin
        LRequestInfo.FPostStream := TMemoryStream.Create;
      end;
      LRequestInfo.PostStream.Position := 0;
      repeat
        S := InternalReadLn(LIOHandler);
        I := IndyPos(';', S); {do not localize}
        if I > 0 then begin
          S := Copy(S, 1, I - 1);
        end;
        Size := IndyStrToInt('$' + Trim(S), 0);      {do not localize}
        if Size = 0 then begin
          Break;
        end;
        LIOHandler.ReadStream(LRequestInfo.PostStream, Size);
        InternalReadLn(LIOHandler); // CRLF at end of chunk data
      until False;
      // skip trailer headers
      repeat until InternalReadLn(LIOHandler) = '';
      LRequestInfo.PostStream.Position := 0;
    end
    else if LRequestInfo.HasContentLength then
    begin
      CreatePostStream(AContext, LRequestInfo.RawHeaders, LRequestInfo.FPostStream);
      if LRequestInfo.FPostStream = nil then begin
        LRequestInfo.FPostStream := TMemoryStream.Create;
      end;
      LRequestInfo.PostStream.Position := 0;
      if LRequestInfo.ContentLength > 0 then begin
        LIOHandler.ReadStream(LRequestInfo.PostStream, LRequestInfo.ContentLength);
        LRequestInfo.PostStream.Position := 0;
      end;
    end
    // If HTTP Pipelining is used by the client, bytes may exist that belong to
    // the NEXT request!  We need to look at the CURRENT request and only check
    // for misreported body data if a body is actually expected.  GET and HEAD
    // requests do not have bodies...
    else if LRequestInfo.CommandType in [hcPOST, hcPUT] then
    begin
      // TODO: need to handle the case where the ContentType is 'multipart/...',
      // which is self-terminating and does not strictly require the above headers...
      if LIOHandler.InputBufferIsEmpty then begin
        LIOHandler.CheckForDataOnSource(1);
      end;
      if not LIOHandler.InputBufferIsEmpty then begin
        LResponseInfo.ResponseNo := 411; // length required
        LResponseInfo.CloseConnection := True;
        LResponseInfo.WriteHeader;
        Exit;
      end;
    end;
    Result := True;
  end;

var
  i: integer;
  s, LInputLine, LRawHTTPCommand, LCmd, LContentType: String;
  LURI: TIdURI;
  LContinueProcessing, LCloseConnection: Boolean;
  LConn: TIdTCPConnection;
begin
  LContinueProcessing := True;
  Result := False;
  LCloseConnection := not KeepAlive;
  try
    try
      LConn := AContext.Connection;
      repeat
        LInputLine := InternalReadLn(LConn.IOHandler);
        i := RPos(' ', LInputLine, -1);    {Do not Localize}
        if i = 0 then begin
          raise EIdHTTPErrorParsingCommand.Create(RSHTTPErrorParsingCommand);
        end;
        LRequestInfo := THTTPRequestInfo.Create(Self);
        try
          LResponseInfo := THttpResponseInfo.Create(Self, LRequestInfo, LConn);
          try
            // SG 05.07.99
            // Set the ServerSoftware string to what it's supposed to be.    {Do not Localize}
            LResponseInfo.ServerSoftware := Trim(ServerSoftware);

            // S.G. 6/4/2004: Set the maximum number of lines that will be catured
            // S.G. 6/4/2004: to prevent a remote resource starvation DOS
            LConn.IOHandler.MaxCapturedLines := MaximumHeaderLineCount;

            // Retrieve the HTTP version
            LRawHTTPCommand := LInputLine;
            LRequestInfo.FVersion := Copy(LInputLine, i + 1, MaxInt);

            s := LRequestInfo.Version;
            Fetch(s, '/');  {Do not localize}
            LRequestInfo.FVersionMajor := IndyStrToInt(Fetch(s, '.'), -1);  {Do not Localize}
            LRequestInfo.FVersionMinor := IndyStrToInt(S, -1);

            SetLength(LInputLine, i - 1);

            // Retrieve the HTTP header
            LRequestInfo.RawHeaders.Clear;
            LConn.IOHandler.Capture(LRequestInfo.RawHeaders, '', False);    {Do not Localize}
            // TODO: call HeadersCanContinue() here before the headers are parsed,
            // in case the user needs to overwrite any values...
            LRequestInfo.ProcessHeaders;
            
            // HTTP 1.1 connections are keep-alive by default
            if not FKeepAlive then begin
              LResponseInfo.CloseConnection := True;
            end
            else if LRequestInfo.IsVersionAtLeast(1, 1) then begin
              LResponseInfo.CloseConnection :=
                TextIsSame(LRequestInfo.ProxyConnection, 'close') or
                TextIsSame(LRequestInfo.Connection, 'close');
            end else begin
              LResponseInfo.CloseConnection :=
                not TextIsSame(LRequestInfo.ProxyConnection, 'keep-alive') or
                not TextIsSame(LRequestInfo.Connection, 'keep-alive');
            end;

            {TODO Check for 1.0 only at this point}
            LCmd := UpperCase(Fetch(LInputLine, ' '));    {Do not Localize}

            // check for overrides when LCmd is 'POST'...
            if TextIsSame(LCmd, 'POST') then begin
              s := LRequestInfo.MethodOverride; // Google/GData
              if s = '' then begin
                // TODO: make RequestInfo properties for these
                s := LRequestInfo.RawHeaders.Values['X-HTTP-Method']; // Microsoft      {do not localize}
                if s = '' then begin
                  s := LRequestInfo.RawHeaders.Values['X-METHOD-OVERRIDE']; // IBM      {do not localize}
                end;
              end;
              if s <> '' then begin
                LCmd := UpperCase(s);
              end;
            end;

            LRequestInfo.FRawHTTPCommand := LRawHTTPCommand;
            LRequestInfo.FRemoteIP := GetRemoteIP(LConn.Socket);
            LRequestInfo.FCommand := LCmd;
            LRequestInfo.FCommandType := DecodeHTTPCommand(LCmd);

            // GET data - may exist with POSTs also
            LRequestInfo.FQueryParams := LInputLine;
            LInputLine := Fetch(LRequestInfo.FQueryParams, '?');    {Do not Localize}

            LRequestInfo.FURI := LInputLine;

            // Parse the document input line
            if LInputLine = '*' then begin    {Do not Localize}
              LRequestInfo.FDocument := '*';    {Do not Localize}
            end else begin
              LURI := TIdURI.Create(LInputLine);
              try
                // SG 29/11/01: Per request of Doychin
                // Try to fill the "host" parameter
                LRequestInfo.FDocument := LURI.Path + LURI.Document;
                if (Length(LURI.Host) > 0) and (Length(LRequestInfo.Host) = 0) then begin
                  LRequestInfo.Host := LURI.Host;
                end;
              finally
                FreeAndNil(LURI);
              end;
            end;

            // RLebeau 12/14/2005: provide the user with the headers and let the
            // user decide whether the response processing should continue...
            if not HeadersCanContinue then begin
              Break;
            end;

            // retreive the base ContentType with attributes omitted
            LContentType := ExtractHeaderItem(LRequestInfo.ContentType);

            // Grab Params so we can parse them
            // POSTed data - may exist with GETs also. With GETs, the action
            // params from the form element will be posted
            // TODO: Rune this is the area that needs fixed. Ive hacked it for now
            // Get data can exists with POSTs, but can POST data exist with GETs?
            // If only the first, the solution is easy. If both - need more
            // investigation.

            if not PreparePostStream then begin
              Break;
            end;

            try
              try
                if LContinueProcessing then begin
                  if Assigned(FOnHttpCommand) then begin
                    FOnHttpCommand(AContext, LRequestInfo, LResponseInfo);
                  end;
                end;
              except
                on E: EIdSocketError do begin // don't stop socket exceptions
                  raise;
                end;
                on E: Exception do begin
                  LResponseInfo.ResponseNo := 500;
                  LResponseInfo.ContentText := E.Message;
                  if Assigned(FOnCommandError) then begin
                    FOnCommandError(AContext, LRequestInfo, LResponseInfo, E);
                  end;
                end;
              end;

              // Write even though WriteContent will, may be a redirect or other
              if not LResponseInfo.HeaderHasBeenWritten then begin
                LResponseInfo.WriteHeader;
              end;
              // Always check ContentText first
              if (Length(LResponseInfo.ContentText) > 0)
               or Assigned(LResponseInfo.ContentStream) then begin
                LResponseInfo.WriteContent;
              end;
            finally
              if LRequestInfo.PostStream <> nil then begin
                DoneWithPostStream(AContext, LRequestInfo); // don't need the PostStream anymore
              end;
            end;
          finally
            LCloseConnection := LResponseInfo.CloseConnection;
            FreeAndNil(LResponseInfo);
          end;
        finally
          FreeAndNil(LRequestInfo);
        end;
      until LCloseConnection;
    except
      on E: EIdSocketError do begin
        if
          (E.LastError = Id_WSAESHUTDOWN) or
          (E.LastError = Id_WSAECONNABORTED) or
          (E.LastError = Id_WSAECONNRESET)
        then begin
          //
        end else begin
          GLog.Error('', E);
          raise;
        end;
      end;
      on E: EIdClosedSocket do begin
        AContext.Connection.Disconnect;
      end;
      on E: EIdConnClosedGracefully do begin
        //
      end;
      on E: Exception do begin
        GLog.Error('', E);
      end;
    end;
  finally
    AContext.Connection.Disconnect(False);
  end;
end;

procedure TCustomHTTPServer.CreatePostStream(ASender: TIdContext;
  AHeaders: TIdHeaderList; var VPostStream: TStream);
begin
  if Assigned(OnCreatePostStream) then begin
    OnCreatePostStream(ASender, AHeaders, VPostStream);
  end;
end;

procedure TCustomHTTPServer.DoneWithPostStream(ASender: TIdContext;
  ARequestInfo: THTTPRequestInfo);
var
  LCanFree: Boolean;
  LStream: TStream;
begin
  LCanFree := True;
  if Assigned(FOnDoneWithPostStream) then begin
    FOnDoneWithPostStream(ASender, ARequestInfo, LCanFree);
  end;
  if LCanFree then begin
    LStream := ARequestInfo.PostStream;
    ARequestInfo.FPostStream := nil;
    IdDisposeAndNil(LStream);
  end else begin
    ARequestInfo.FPostStream := nil;
  end;
end;

end.
