unit t_HttpRequestCounters;

interface

type
  THttpRequestCounters = packed record
    UpHead     : Cardinal;
    DownHead   : Cardinal;
    UpBody     : UInt64;
    DownBody   : UInt64;
    CacheRead  : UInt64;
    CacheWrite : UInt64;
  end;
  PHttpRequestCounters = ^THttpRequestCounters;

implementation

end.
