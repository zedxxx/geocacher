unit u_SSLIOHandlerHelper;

interface

uses
  Classes,
  IdCTypes,
  IdSSLOpenSSL,
  IdSSLOpenSSLHeaders,
  i_SSLCertificate;

type
  TIdSSLContextExtended = class(TIdSSLContext)
  private
    fOnStatusInfoEx: TCallbackExEvent;
  public
    function UseCertificate(const ACert: IX509Certificate): Boolean;
    class function CreateClone(const ASSLContext: TIdSSLContext): TIdSSLContext;
    procedure InitContext(CtxMode: TIdSSLCtxMode);
    property Context: PSSL_CTX read fContext;
    property OnStatusInfoEx: TCallbackExEvent read fOnStatusInfoEx write fOnStatusInfoEx;
  end;

  TSSLIOHandlerHelper = class(TObject)
  private
    procedure OnGetPassword(var Password: string);
    function OnVerifyPeer(Certificate: TIdX509; AOk: Boolean;
      ADepth, AError: Integer): Boolean;
    procedure OnStatusInfoEx(ASender : TObject; const AsslSocket: PSSL;
      const AWhere, Aret: TIdC_INT; const AType, AMsg : String);
  public
    function MakeServerIOHandler(const AOwner: TComponent): TIdServerIOHandlerSSLOpenSSL;
  end;

implementation

uses
  SysUtils,
  IdYarn,
  IdThread,
  IdIOHandler,
  IdSocketHandle;

type
  TIdSSLSocketExtended = class(TIdSSLSocket)
  public
    property SSLContext: TIdSSLContext read fSSLContext;
  end;

  TIdSSLIOHandlerSocketOpenSSLExtended = class(TIdSSLIOHandlerSocketOpenSSL)
  public
    destructor Destroy; override;
  end;

  TIdServerIOHandlerSSLOpenSSLExtended = class(TIdServerIOHandlerSSLOpenSSL)
  public
    function Accept(ASocket: TIdSocketHandle;
      AListenerThread: TIdThread; AYarn: TIdYarn): TIdIOHandler; override;
  end;

{ TIdServerIOHandlerSSLOpenSSLExtended }

function TIdServerIOHandlerSSLOpenSSLExtended.Accept(ASocket: TIdSocketHandle;
  AListenerThread: TIdThread; AYarn: TIdYarn): TIdIOHandler;
var
  LIO: TIdSSLIOHandlerSocketOpenSSL;
begin
  Assert(ASocket<>nil);
  Assert(fSSLContext<>nil);
  LIO := TIdSSLIOHandlerSocketOpenSSLExtended.Create(nil);                      // 1
  try
    LIO.PassThrough := True;
    LIO.Open;
    if LIO.Binding.Accept(ASocket.Handle) then begin
      //we need to pass the SSLOptions for the socket from the server
      LIO.SSLOptions.Free;
      LIO.SSLOptions := nil;

      LIO.IsPeer := True;
      LIO.SSLOptions := fxSSLOptions;

      LIO.SSLSocket := TIdSSLSocketExtended.Create(Self);                       // 2
      LIO.SSLContext := TIdSSLContextExtended.CreateClone(SSLContext);          // 3
    end else begin
      FreeAndNil(LIO);
    end;
  except
    LIO.Free;
    raise;
  end;
  Result := LIO;
end;

{ TIdSSLContextExtended }

function _SSL_get_servername(const ssl: PSSL; const _type: Integer): PAnsiChar;
{$IFNDEF DEBUG} inline; {$ENDIF}
begin
  if _type <> TLSEXT_NAMETYPE_host_name then begin
    Result := nil;
  end else begin
    if (ssl.session <> nil) and (ssl.tlsext_hostname = nil) then begin
      Result := ssl.session.tlsext_hostname;
    end else begin
      Result := ssl.tlsext_hostname;
    end;
  end;
end;

{
function ServerNameCallback(ssl: PSSL; ad: TIdC_INT; arg: Pointer): TIdC_INT; cdecl;
var
  VHostName: AnsiString;
  VSSLContext: TIdSSLContextExtended;
begin
  Result := SSL_TLSEXT_ERR_NOACK;

  if ssl = nil then begin
    Exit;
  end;

  VHostName := _SSL_get_servername(ssl, TLSEXT_NAMETYPE_host_name);

  if VHostName = '' then begin
    Exit;
  end;

  VSSLContext := TIdSSLContextExtended(arg);

  //ToDo
end;
}

class function TIdSSLContextExtended.CreateClone(
  const ASSLContext: TIdSSLContext): TIdSSLContext;
var
  VSSLContext: TIdSSLContextExtended;
begin
  VSSLContext := TIdSSLContextExtended.Create;

  VSSLContext.StatusInfoOn := ASSLContext.StatusInfoOn;
  VSSLContext.VerifyOn := ASSLContext.VerifyOn;
  VSSLContext.Method := ASSLContext.Method;
  VSSLContext.SSLVersions := ASSLContext.SSLVersions;
  VSSLContext.Mode := ASSLContext.Mode;
  VSSLContext.RootCertFile := ASSLContext.RootCertFile;
  VSSLContext.CertFile := ASSLContext.CertFile;
  VSSLContext.KeyFile := ASSLContext.KeyFile;
  VSSLContext.VerifyMode := ASSLContext.VerifyMode;
  VSSLContext.VerifyDepth := ASSLContext.VerifyDepth;

  VSSLContext.InitContext(sslCtxServer);

  { ToDo: SNI detection
  if VSSLContext.Context <> nil then begin
    VSSLContext.Context.tlsext_servername_callback := ServerNameCallback;
    VSSLContext.Context.tlsext_servername_arg := VSSLContext;
  end;
  }
  Result := VSSLContext;
end;

procedure TIdSSLContextExtended.InitContext(CtxMode: TIdSSLCtxMode);
begin
  inherited InitContext(CtxMode);
end;

function TIdSSLContextExtended.UseCertificate(const ACert: IX509Certificate): Boolean;
begin
  Result := SSL_CTX_use_certificate(fContext, ACert.Certificate) > 0;
  if not Result then begin
    Exit;
  end;
  Result := SSL_CTX_use_PrivateKey(fContext, ACert.PrivateKey) > 0;
  if Result then begin
    Result := SSL_CTX_check_private_key(fContext) > 0;
  end;
end;

{ TIdSSLIOHandlerSocketOpenSSLExtended }

destructor TIdSSLIOHandlerSocketOpenSSLExtended.Destroy;
begin
  FreeAndNil(fSSLContext);
  inherited Destroy;
end;

{ TSSLIOHandlerHelper }

function TSSLIOHandlerHelper.MakeServerIOHandler(
  const AOwner: TComponent): TIdServerIOHandlerSSLOpenSSL;
var
  VSSLHandler: TIdServerIOHandlerSSLOpenSSL;
begin
  VSSLHandler := TIdServerIOHandlerSSLOpenSSLExtended.Create(AOwner);

  VSSLHandler.SSLOptions.CertFile := '';
  VSSLHandler.SSLOptions.KeyFile := '';
  VSSLHandler.SSLOptions.RootCertFile := '';

  VSSLHandler.SSLOptions.SSLVersions :=
    [sslvSSLv2, sslvSSLv23, sslvSSLv3, sslvTLSv1, sslvTLSv1_1, sslvTLSv1_2];

  VSSLHandler.SSLOptions.Mode := sslmServer;
  VSSLHandler.SSLOptions.VerifyDepth := 0;
  VSSLHandler.SSLOptions.VerifyMode := [];
  VSSLHandler.SSLOptions.CipherList := 'ALL';

  VSSLHandler.OnGetPassword := Self.OnGetPassword;
  VSSLHandler.OnVerifyPeer := Self.OnVerifyPeer;

  VSSLHandler.OnStatusInfoEx := Self.OnStatusInfoEx;

  Result := VSSLHandler;
end;

procedure TSSLIOHandlerHelper.OnGetPassword(var Password: string);
begin
  Password := '';
end;

function TSSLIOHandlerHelper.OnVerifyPeer(Certificate: TIdX509; AOk: Boolean;
  ADepth, AError: Integer): Boolean;
begin
  Result := True;
end;

procedure TSSLIOHandlerHelper.OnStatusInfoEx(ASender: TObject; const AsslSocket: PSSL;
  const AWhere, Aret: TIdC_INT; const AType, AMsg: string);
var
  VSSLSocket: TIdSSLSocketExtended;
  VSSLContext: TIdSSLContextExtended;
begin
  VSSLSocket := TIdSSLSocketExtended(SSL_get_app_data(AsslSocket));
  if VSSLSocket <> nil then begin
    VSSLContext := VSSLSocket.SSLContext as TIdSSLContextExtended;
    if (VSSLContext <> nil) and Assigned(VSSLContext.OnStatusInfoEx) then begin
      VSSLContext.OnStatusInfoEx(ASender, AsslSocket, AWhere, Aret, AType, AMsg);
    end;
  end;
end;

end.
