unit u_SSLCertificate;

interface

uses
  Classes,
  IdSSLOpenSSL,
  IdSSLOpenSSLHeaders,
  i_SSLCertificate;

type
  TX509Certificate = class(TInterfacedObject, IX509Certificate)
  private
    FCert: PX509;
    FPrivateKey: PEVP_PKEY;
    FStartDate: TDateTime;
    FEndDate: TDateTime;
    FIsDatesValid: Boolean;
    procedure LoadDates;
    procedure Clear;
    procedure RaiseIfCertNotLoaded; inline;
  private
    { IX509Certificate }
    function GetCertificate: PX509;
    function GetPrivateKey: PEVP_PKEY;

    procedure SaveToStr(const AKeyPass: AnsiString; out ACert, AKey: AnsiString);
    procedure SaveToFile(const ACertFileName, AKeyFileName: string; const AKeyPass: AnsiString);

    procedure LoadFromMem(const ACert, AKey: TMemoryStream; const AKeyPass: AnsiString = '');
    procedure LoadFromFile(const ACertFileName, AKeyFileName: string; const AKeyPass: AnsiString = '');

    function GetSerialNumber: string;
    function GetFingerprint: string;

    function GetStartDate: TDateTime;
    function GetEndDate: TDateTime;
  public
    constructor Create;
    constructor CreateWithOwn(
      const ACert: PX509;
      const APrivateKey: PEVP_PKEY
    );
    destructor Destroy; override;
  end;

implementation

uses
  SysUtils,
  u_SSLCertificatePemUtils;

{ TX509Certificate }

constructor TX509Certificate.Create;
begin
  inherited Create;
  FCert := nil;
  FPrivateKey := nil;
  FIsDatesValid := False;
end;

constructor TX509Certificate.CreateWithOwn(
  const ACert: PX509;
  const APrivateKey: PEVP_PKEY
);
begin
  Assert(ACert <> nil);
  Assert(APrivateKey <> nil);

  inherited Create;

  FCert := ACert;
  FPrivateKey := APrivateKey;

  FIsDatesValid := False;
end;

destructor TX509Certificate.Destroy;
begin
  Clear;
  inherited Destroy;
end;

function TX509Certificate.GetCertificate: PX509;
begin
  Result := FCert;
end;

function TX509Certificate.GetPrivateKey: PEVP_PKEY;
begin
  Result := FPrivateKey;
end;

procedure TX509Certificate.RaiseIfCertNotLoaded;
begin
  if FCert = nil then begin
    raise Exception.Create(Self.ClassName + ': X509 certificate not loaded');
  end;
end;

function BytesToHex(const ABytes: Pointer; const ALen: Integer): string;
const
  cHexDigits: array [0..15] of Char = (
    '0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'
  );
var
  I: Integer;
  VData: PByte;
begin
  if ALen > 0 then begin
    Assert(ABytes <> nil);
    VData := ABytes;
    SetLength(Result, ALen*2);
    for I := 0 to ALen - 1 do begin
      Result[I*2+1] := cHexDigits[(VData^ and $F0) shr 4];
      Result[I*2+2] := cHexDigits[VData^ and $F];
      Inc(VData);
    end;
  end else begin
    Result := '';
  end;
end;

function TX509Certificate.GetFingerprint: string;
var
  VLen: Cardinal;
  VBytes: array [0..EVP_MAX_MD_SIZE-1] of Byte;
begin
  RaiseIfCertNotLoaded;

  VLen := 0;
  if X509_digest(FCert, EVP_md5, PByte(@VBytes), VLen) <> 1 then begin
    raise Exception.Create(Self.ClassName + ': X509_digest failed');
  end;
  Result := BytesToHex(@VBytes, VLen);
end;

function TX509Certificate.GetSerialNumber: string;
var
  VSerialNumber: PASN1_INTEGER;
begin
  RaiseIfCertNotLoaded;

  VSerialNumber := X509_get_serialNumber(FCert);
  Result := BytesToHex(VSerialNumber.data, VSerialNumber.length);
end;

procedure TX509Certificate.LoadDates;
var
  VIdX509: TIdX509;
begin
  if FIsDatesValid then begin
    Exit;
  end;

  RaiseIfCertNotLoaded;

  VIdX509 := TIdX509.Create(FCert, False);
  try
    FStartDate := VIdX509.notBefore;
    FEndDate := VIdX509.notAfter;
    FIsDatesValid := True;
  finally
    VIdX509.Free;
  end;
end;

function TX509Certificate.GetStartDate: TDateTime;
begin
  LoadDates;
  Result := FStartDate;
end;

function TX509Certificate.GetEndDate: TDateTime;
begin
  LoadDates;
  Result := FEndDate;
end;

procedure TX509Certificate.Clear;
begin
  FIsDatesValid := False;
  if FCert <> nil then begin
    X509_free(FCert);
    FCert := nil;
  end;
  if FPrivateKey <> nil then begin
    EVP_PKEY_free(FPrivateKey);
    FPrivateKey := nil;
  end;
end;

procedure TX509Certificate.LoadFromMem(
  const ACert, AKey: TMemoryStream;
  const AKeyPass: AnsiString
);
begin
  Clear;
  FCert := CertFromPem(ACert);
  FPrivateKey := KeyFromPem(AKey, AKeyPass);
end;

procedure TX509Certificate.LoadFromFile(
  const ACertFileName: string;
  const AKeyFileName: string;
  const AKeyPass: AnsiString
);
var
  VPemKey: TMemoryStream;
  VPemCert: TMemoryStream;
begin
  if not FileExists(ACertFileName) then begin
    raise Exception.Create(Self.ClassName + ': Cert file not found: ' + ACertFileName);
  end;

  if not FileExists(AKeyFileName) then begin
    raise Exception.Create(Self.ClassName + ': Key file not found: ' + AKeyFileName);
  end;

  VPemCert := nil;
  VPemKey := nil;
  try
    VPemCert := TMemoryStream.Create;

    VPemCert.LoadFromFile(ACertFileName);
    if VPemCert.Size <= 0 then begin
      raise Exception.Create(Self.ClassName + ': Cert file is empty: ' + ACertFileName);
    end;

    VPemKey := TMemoryStream.Create;
    VPemKey.LoadFromFile(AKeyFileName);
    if VPemKey.Size <= 0 then begin
      raise Exception.Create(Self.ClassName + ': Key file is empty: ' + AKeyFileName);
    end;

    LoadFromMem(VPemCert, VPemKey, AKeyPass);
  finally
    VPemCert.Free;
    VPemKey.Free;
  end;
end;

procedure TX509Certificate.SaveToStr(const AKeyPass: AnsiString; out ACert, AKey: AnsiString);
begin
  ACert := CertToPem(FCert);
  AKey := KeyToPem(FPrivateKey, AKeyPass);
end;

procedure TX509Certificate.SaveToFile(
  const ACertFileName: string;
  const AKeyFileName: string;
  const AKeyPass: AnsiString
);
var
  VPemCert, VPemKey: AnsiString;
  VStream: TMemoryStream;
begin
  VStream := TMemoryStream.Create;
  try
    SaveToStr(AKeyPass, VPemCert, VPemKey);

    if VPemCert = '' then begin
      raise Exception.Create(Self.ClassName + ': Nothing to save, PemCert is empty');
    end;

    VStream.WriteBuffer(VPemCert[1], Length(VPemCert));
    VStream.SaveToFile(ACertFileName);

    if AKeyFileName = '' then begin
      Exit;
    end;

    VStream.Clear;

    if VPemKey = '' then begin
      raise Exception.Create(Self.ClassName + ': Nothing to save, PemKey is empty');
    end;

    VStream.WriteBuffer(VPemKey[1], Length(VPemKey));
    VStream.SaveToFile(AKeyFileName);
  finally
    VStream.Free;
  end;
end;

end.
