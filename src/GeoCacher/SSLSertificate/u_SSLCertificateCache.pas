unit u_SSLCertificateCache;

interface

uses
  SysUtils,
  flcDataStructs,
  i_SSLCertificate;

type
  TSSLCertificateCache = class(TObject)
  private
    type
      TNamesRec = record
        X509CertFileName: string;
        PrivateKeyFileName: string;
      end;
  private
    FStoragePath: string;
    FRamCacheMaxCount: Integer;
    FPrivateKeyFileName: string;
    FPrivateKeyPass: AnsiString;

    FLock: IReadWriteSync;
    FRamCache: TInterfaceDictionaryA;

    procedure AddToRam(
      const ACommonName: AnsiString;
      const ACertificate: IX509Certificate
    ); inline;

    function CommonNameToFileName(
      const ACommonName: AnsiString
    ): string; inline;

    function GetNames(
      const ACommonName: AnsiString
    ): TNamesRec; inline;

    function UseRamCache: Boolean; inline;
  public
    procedure Write(
      const ACommonName: AnsiString;
      const ACertificate: IX509Certificate;
      const AOverwrite: Boolean = False
    );
    function Read(const ACommonName: AnsiString): IX509Certificate;
  public
    constructor Create(
      const AStoragePath: string;
      const APrivateKeyFileName: string = '';
      const APrivateKeyPass: AnsiString = '';
      const ARamCacheMaxCount: Integer = -1 // 0 - disabled; -1 - unlimited
    );
    destructor Destroy; override;
  end;

implementation

uses
  flcUtils,
  flcStrings,
  u_Synchronizer,
  u_FileSystemTools,
  u_SSLCertificate;

{ TSSLCertificateCache }

constructor TSSLCertificateCache.Create(
  const AStoragePath: string;
  const APrivateKeyFileName: string;
  const APrivateKeyPass: AnsiString;
  const ARamCacheMaxCount: Integer
);
begin
  Assert(AStoragePath <> '');

  inherited Create;

  FStoragePath := AStoragePath;
  FRamCacheMaxCount := ARamCacheMaxCount;
  FPrivateKeyFileName := APrivateKeyFileName;
  FPrivateKeyPass := APrivateKeyPass;

  CreateDirIfNotExists(FStoragePath);

  FLock := GSync.SyncStd.Make;
  FRamCache := TInterfaceDictionaryA.Create;
end;

destructor TSSLCertificateCache.Destroy;
begin
  FreeAndNil(FRamCache);
  FLock := nil;
  inherited Destroy;
end;

function TSSLCertificateCache.UseRamCache: Boolean;
begin
  Result := FRamCacheMaxCount <> 0;
end;

function TSSLCertificateCache.Read(const ACommonName: AnsiString): IX509Certificate;
var
  VIntf: IInterface;
  VNames: TNamesRec;
  VNeedAddToRam: Boolean;
begin
  Result := nil;
  VNeedAddToRam := False;

  FLock.BeginRead;
  try
    if UseRamCache and (FRamCache.LocateItem(ACommonName, VIntf) >= 0) then begin
      Result := VIntf as IX509Certificate;
    end else begin
      VNames := GetNames(ACommonName);

      if VNames.PrivateKeyFileName = '' then begin
        VNames.PrivateKeyFileName := FPrivateKeyFileName;
      end;

      if FileExists(VNames.X509CertFileName) and FileExists(VNames.PrivateKeyFileName) then begin
        Result := TX509Certificate.Create;

        Result.LoadFromFile(
          VNames.X509CertFileName,
          VNames.PrivateKeyFileName,
          FPrivateKeyPass
        );

        VNeedAddToRam := UseRamCache;
      end;
    end;
  finally
    FLock.EndRead;
  end;

  if VNeedAddToRam then begin
    FLock.BeginWrite;
    try
      AddToRam(ACommonName, Result);
    finally
      FLock.EndWrite;
    end;
  end;
end;

procedure TSSLCertificateCache.Write(
  const ACommonName: AnsiString;
  const ACertificate: IX509Certificate;
  const AOverwrite: Boolean
);
var
  VNames: TNamesRec;
begin
  VNames := GetNames(ACommonName);

  if FPrivateKeyFileName <> '' then begin
    Assert(VNames.PrivateKeyFileName = '');
  end;

  FLock.BeginWrite;
  try
    // save to ram
    if UseRamCache then begin
      if not FRamCache.HasKey(ACommonName) then begin
        AddToRam(ACommonName, ACertificate);
      end else if AOverwrite then begin
        FRamCache.Item[ACommonName] := ACertificate;
      end;
    end;

    // save to disk
    if
      AOverwrite or
      not FileExists(VNames.X509CertFileName) or
      ((VNames.PrivateKeyFileName <> '') and not FileExists(VNames.PrivateKeyFileName))
    then begin

      CreateDirIfNotExists(ExtractFilePath(VNames.X509CertFileName));
      if VNames.PrivateKeyFileName <> '' then begin
        CreateDirIfNotExists(ExtractFilePath(VNames.PrivateKeyFileName));
      end;

      ACertificate.SaveToFile(
        VNames.X509CertFileName,
        VNames.PrivateKeyFileName,
        FPrivateKeyPass
      );
    end;
  finally
    FLock.EndWrite;
  end;
end;

procedure TSSLCertificateCache.AddToRam(
  const ACommonName: AnsiString;
  const ACertificate: IX509Certificate
);
begin
  Assert(ACommonName <> '');
  Assert(ACertificate <> nil);

  if FRamCacheMaxCount > 0 then begin
    if FRamCache.Count > FRamCacheMaxCount then begin
      FRamCache.Clear;
    end;
  end;
  FRamCache.Add(ACommonName, ACertificate);
end;

function TSSLCertificateCache.CommonNameToFileName(const ACommonName: AnsiString): string;
var
  I: Integer;
  VName: AnsiString;
  VSubDir: string;
begin
  VName := StrReplaceCharA('*', '#', ACommonName);
  VSubDir := '';
  for I := 1 to Length(VName) do begin
    // use first letter as subfolder name
    if not CharSetMatchCharB(['#', '.'], VName[I]) then begin
      VSubDir := VName[I] + '\';
      Break;
    end;
  end;
  Result := FStoragePath + VSubDir + string(VName);
end;

function TSSLCertificateCache.GetNames(const ACommonName: AnsiString): TNamesRec;
var
  VName: string;
begin
  VName := CommonNameToFileName(ACommonName);
  Result.X509CertFileName := VName + '.crt';

  if FPrivateKeyFileName = '' then begin
    Result.PrivateKeyFileName := VName + '.key';
  end else begin
    Result.PrivateKeyFileName := '';
  end;
end;

end.
