unit u_SSLCertificatePemUtils;

interface

uses
  Classes,
  SysUtils,
  IdSSLOpenSSLHeaders;

type
  EPemCertificateError = class(Exception);

function CertFromPem(const APemCert: AnsiString): PX509; inline; overload;
function CertFromPem(const APemCert: TMemoryStream): PX509; inline; overload;
function CertFromPem(const APemCertPtr: Pointer; const APemCertSize: Integer): PX509; overload;

function CertToPem(const ACert: PX509): AnsiString;

function KeyFromPem(const APemKey: AnsiString; const APass: AnsiString = ''): PEVP_PKEY; inline; overload;
function KeyFromPem(const APemKey: TMemoryStream; const APass: AnsiString = ''): PEVP_PKEY; inline; overload;
function KeyFromPem(const APemKeyPtr: Pointer; const APemKeySize: Integer; const APass: AnsiString = ''): PEVP_PKEY; overload;

function KeyToPem(const APrivateKey: PEVP_PKEY; const APass: AnsiString = ''): AnsiString;

implementation

function CertFromPem(const APemCert: AnsiString): PX509;
begin
  Assert(APemCert <> '');
  Result := CertFromPem(@APemCert[1], Length(APemCert));
end;

function CertFromPem(const APemCert: TMemoryStream): PX509;
begin
  Assert(APemCert <> nil);
  Assert(APemCert.Size > 0);
  Result := CertFromPem(APemCert.Memory, APemCert.Size);
end;

function CertFromPem(const APemCertPtr: Pointer; const APemCertSize: Integer): PX509;
var
  VBio: PBIO;
  VX509: PX509;
begin
  if (APemCertPtr = nil) or (APemCertSize <= 0) then begin
    raise EPemCertificateError.Create('PemCert can''t be empty');
  end;

  VX509 := X509_new();
  if VX509 = nil then begin
    EIdOpenSSLAPICryptoError.RaiseException('X509_new() failed');
  end;
  try
    VBio := BIO_new_mem_buf(APemCertPtr, APemCertSize);
    if VBio = nil then begin
      EIdOpenSSLAPICryptoError.RaiseException('BIO_new_mem_buf() failed');
    end;
    try
      if PEM_read_bio_X509(VBio, @VX509, nil, nil) = nil then begin
        EIdOpenSSLAPICryptoError.RaiseException('PEM_read_bio_X509() failed');
      end;
    finally
      BIO_free(VBio);
    end;
    Result := VX509;
    VX509 := nil;
  finally
    if VX509 <> nil then begin
      X509_free(VX509);
    end;
  end;
end;

function CertToPem(const ACert: PX509): AnsiString;
var
  VBio: PBIO;
  VBufMem: PBUF_MEM;
  VLen: Integer;
  VPem: AnsiString;
begin
  VBio := BIO_new(BIO_s_mem());
  try
    PEM_write_bio_X509(VBio, ACert);
    BIO_get_mem_ptr(VBio, VBufMem);

    VLen := VBufMem.length;
    SetLength(VPem, VLen);

    BIO_read(VBio, @VPem[1], VLen);

    Result := VPem;
  finally
    BIO_free(VBio);
  end;
end;

function KeyFromPem(const APemKey: AnsiString; const APass: AnsiString): PEVP_PKEY;
begin
  Assert(APemKey <> '');
  Result := KeyFromPem(@APemKey[1], Length(APemKey), APass);
end;

function KeyFromPem(const APemKey: TMemoryStream; const APass: AnsiString): PEVP_PKEY;
begin
  Assert(APemKey <> nil);
  Assert(APemKey.Size > 0);
  Result := KeyFromPem(APemKey.Memory, APemKey.Size, APass);
end;

function KeyFromPem(const APemKeyPtr: Pointer; const APemKeySize: Integer; const APass: AnsiString): PEVP_PKEY;
var
  VBio: PBIO;
  VKey: PEVP_PKEY;
begin
  if (APemKeyPtr = nil) or (APemKeySize <= 0) then begin
    raise EPemCertificateError.Create('PemKey can''t be empty');
  end;

  VBio := BIO_new_mem_buf(APemKeyPtr, APemKeySize);
  if VBio = nil then begin
    EIdOpenSSLAPICryptoError.RaiseException('BIO_new_mem_buf() failed');
  end;
  try
    VKey := nil; // !
    if APass <> '' then begin
      VKey := PEM_read_bio_PrivateKey(VBio, @VKey, nil, @APass[1]);
    end else begin
      VKey := PEM_read_bio_PrivateKey(VBio, @VKey, nil, nil);
    end;
    if VKey = nil then begin
      EIdOpenSSLAPICryptoError.RaiseException('PEM_read_bio_PrivateKey() failed');
    end;
    Result := VKey;
  finally
    BIO_free(VBio);
  end;
end;

function KeyToPem(const APrivateKey: PEVP_PKEY; const APass: AnsiString): AnsiString;
var
  b64Key: PBIO;
  bptrKey: PBUF_MEM;
  len: Integer;
  private_key: AnsiString;
begin
  b64Key := BIO_new(BIO_s_mem());
  try
    if APass = '' then begin
      PEM_write_bio_PrivateKey(b64Key, APrivateKey, nil, nil, 0, nil, nil);
    end else begin
      PEM_write_bio_PrivateKey(b64Key, APrivateKey, EVP_aes_256_cbc(), nil, 0, nil, PAnsiChar(APass));
    end;

    BIO_get_mem_ptr(b64Key, bptrKey);

    len := bptrKey.length;
    SetLength(private_key, len);

    BIO_read(b64Key, @private_key[1], len);

    Result := private_key;
  finally
    BIO_free(b64Key);
  end;
end;

end.
