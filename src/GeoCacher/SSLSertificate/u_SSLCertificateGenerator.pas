unit u_SSLCertificateGenerator;

interface

uses
  IdSSLOpenSSLHeaders,
  u_SSLOpenSSLHeadersExtended;

type
  TCertEntries = record
    CountryName: AnsiString;
    StateProvinceName: AnsiString;
    LocalityName: AnsiString;
    OrganizationName: AnsiString;
    OrganizationalUnitName: AnsiString;
    CommonName: AnsiString;
    procedure Clear;
  end;
  PCertEntries = ^TCertEntries;

  TCertExtensions = record
    BasicConstraints: AnsiString;
    KeyUsage: AnsiString;
    ExtKeyUsage: AnsiString;
    SubjectAltName: AnsiString;
    SubjectKeyIdentifier: AnsiString;
    AuthorityKeyIdentifier: AnsiString;
    procedure Clear;
  end;
  PCertExtensions = ^TCertExtensions;

function MakeCertificate(
  const APrivateKey: PEVP_PKEY;
  const ARootCert: PX509;
  const ARootKey: PEVP_PKEY;
  const AEntries: PCertEntries;
  const AExtensions: PCertExtensions;
  const ASerial: AnsiString;
  const ANotBefore: Integer;
  const ANotAfter: Integer
): PX509;

function MakePrivateKey(
  const ABitsKeySize: Integer = 2048
): PEVP_PKEY;

function MakePrivateKeyCopy(
  const APrivateKey: PEVP_PKEY;
  const ADeepCopy: Boolean
): PEVP_PKEY;

implementation

uses
  SysUtils;

procedure TCertEntries.Clear;
const
  cEmptyRec: TCertEntries = ();
begin
  Self := cEmptyRec;
end;

procedure TCertExtensions.Clear;
const
  cEmptyRec: TCertExtensions = ();
begin
  Self := cEmptyRec;
end;

procedure AddEntry(const ASubj: PX509_NAME; const AField, AValue: AnsiString);
var
  VRet: Integer;
begin
  Assert(AField <> '');

  if AValue = '' then begin
    Exit; // ok
  end;

  VRet := X509_NAME_add_entry_by_txt(ASubj, PAnsiChar(AField), MBSTRING_ASC,
    PAnsiChar(AValue), Length(AValue), -1, 0);

  if VRet <> 1 then begin
    EIdOpenSSLAPICryptoError.RaiseException(
      Format('Error adding entry (field=%s; value=%s)', [AField, AValue])
    );
  end;
end;

procedure AddEntries(const ASubj: PX509_NAME; const AEntries: PCertEntries);
begin
  Assert(ASubj <> nil);

  if AEntries = nil then begin
    raise Exception.Create('Entries can''t be empty');
  end;

  AddEntry(ASubj, 'C',  AEntries.CountryName);
  AddEntry(ASubj, 'ST', AEntries.StateProvinceName);
  AddEntry(ASubj, 'L',  AEntries.LocalityName);
  AddEntry(ASubj, 'OU', AEntries.OrganizationalUnitName);
  AddEntry(ASubj, 'O',  AEntries.OrganizationName);
  AddEntry(ASubj, 'CN', AEntries.CommonName);
end;

procedure AddExtention(
  const req: PX509;
  const ext_nid: Integer;
  const value: AnsiString;
  const issuer: PX509 = nil;
  const subject: PX509 = nil
);
var
  ex: PX509_EXTENSION;
  ctx: X509V3_CTX;
begin
  if value = '' then begin
    Exit; // ok
  end;

  X509V3_set_ctx(@ctx, nil, nil, nil, nil, 0);
  X509V3_set_ctx_nodb(ctx);

  if issuer <> nil then begin
    ctx.issuer_cert := issuer;
  end;

  if subject <> nil then begin
    ctx.subject_cert := subject;
  end;

  ex := X509V3_EXT_conf_nid(nil, @ctx, ext_nid, PAnsiChar(value));
  if ex <> nil then begin
    try
      if X509_add_ext(req, ex, -1) = 0 then begin
        EIdOpenSSLAPICryptoError.RaiseException(
          Format('Error adding extention (nid=%d; value="%s")', [ext_nid, value])
        );
      end;
    finally
      X509_EXTENSION_free(ex);
    end;
  end else begin
    EIdOpenSSLAPICryptoError.RaiseException(
      Format('Error create extention with nid=%d; value="%s"', [ext_nid, value])
    );
  end;
end;

procedure AddExtensions(const AExtensions: PCertExtensions; const ACert, ARootCert: PX509);
begin
  if AExtensions <> nil then begin
    AddExtention(ACert, NID_basic_constraints, AExtensions.BasicConstraints);
    AddExtention(ACert, NID_key_usage, AExtensions.KeyUsage);
    AddExtention(ACert, NID_ext_key_usage, AExtensions.ExtKeyUsage);
    AddExtention(ACert, NID_subject_alt_name, AExtensions.SubjectAltName);
    AddExtention(ACert, NID_subject_key_identifier, AExtensions.SubjectKeyIdentifier, nil, ACert);
    AddExtention(ACert, NID_authority_key_identifier, AExtensions.AuthorityKeyIdentifier, ARootCert, nil);
  end;
end;

function HexToAsn1(const AHex: AnsiString): PASN1_INTEGER;
var
  VCount: Integer;
  VBigNum: PBIGNUM;
begin
  VBigNum := BN_new();
  try
    VCount := BN_hex2bn(VBigNum, PAnsiChar(AHex));
    if (VCount = 0) or (Length(AHex) <> VCount) then begin
      EIdOpenSSLAPICryptoError.RaiseException('BN_hex2bn() failed. AHex=' + string(AHex));
    end;
    Result := BN_to_ASN1_INTEGER(VBigNum, nil);
    if Result = nil then begin
      EIdOpenSSLAPICryptoError.RaiseException('BN_to_ASN1_INTEGER() failed');
    end;
  finally
    BN_free(VBigNum);
  end;
end;

function MakeCertificate(
  const APrivateKey: PEVP_PKEY;
  const ARootCert: PX509;
  const ARootKey: PEVP_PKEY;
  const AEntries: PCertEntries;
  const AExtensions: PCertExtensions;
  const ASerial: AnsiString;
  const ANotBefore: Integer;
  const ANotAfter: Integer
): PX509;
var
  VCert: PX509;
  VSubj: PX509_NAME;
  VIssuer: PX509_NAME;
  VDigest: PEVP_MD;
  VSignKey: PEVP_PKEY;
  VIssuerCert: PX509;
  VIsSelfSigned: Boolean;
  VSerialNumber: PASN1_INTEGER;
begin
  Assert(APrivateKey <> nil);

  VCert := X509_new();
  if VCert = nil then begin
    EIdOpenSSLAPICryptoError.RaiseException('X509_new() failed');
  end;

  try
    // set the certificate's public key from the private key object
    if X509_set_pubkey(VCert, APrivateKey) <> 1 then begin
      EIdOpenSSLAPICryptoError.RaiseException('X509 error setting pub key');
    end;

    // set version (value 2 means v3)
    if X509_set_version(VCert, 2) <> 1 then begin
      EIdOpenSSLAPICryptoError.RaiseException('X509 error setting version');
    end;

    // set serial number
    VSerialNumber := HexToAsn1(ASerial);
    try
      if X509_set_serialNumber(VCert, VSerialNumber) <> 1 then begin
        EIdOpenSSLAPICryptoError.RaiseException('X509_set_serialNumber failed');
      end;
    finally
      ASN1_STRING_free(PASN1_STRING(VSerialNumber));
    end;

    // set notBefore
    if X509_gmtime_adj(X509_get_notBefore(VCert), ANotBefore) = nil then begin
      EIdOpenSSLAPICryptoError.RaiseException('X509 error setting notBefore');
    end;

    // set notAfter
    if X509_gmtime_adj(X509_get_notAfter(VCert), ANotAfter) = nil then begin
      EIdOpenSSLAPICryptoError.RaiseException('X509 error setting notAfter');
    end;

    // add entries
    VSubj := X509_NAME_new();
    try
      AddEntries(VSubj, AEntries);
      if X509_set_subject_name(VCert, VSubj) <> 1 then begin
        EIdOpenSSLAPICryptoError.RaiseException('X509 error adding subject to request');
      end;
    finally
      X509_NAME_free(VSubj);
    end;

    VIsSelfSigned := not ( (ARootCert <> nil) and (ARootKey <> nil) );

    if VIsSelfSigned then begin
      VSignKey := APrivateKey;
      VIssuerCert := VCert;
      VIssuer := X509_get_subject_name(VCert);
    end else begin
      VSignKey := ARootKey;
      VIssuerCert := ARootCert;
      VIssuer := X509_get_subject_name(VIssuerCert);
    end;

    if VIssuer = nil then begin
      EIdOpenSSLAPICryptoError.RaiseException('X509_get_subject_name() failed');
    end;

    // set issuer
    if X509_set_issuer_name(VCert, VIssuer) <> 1 then begin
      EIdOpenSSLAPICryptoError.RaiseException('X509_set_issuer_name() failed');
    end;

    // add extensions
    AddExtensions(AExtensions, VCert, VIssuerCert);

    // sign it
    VDigest := EVP_sha256();
    if X509_sign(VCert, VSignKey, VDigest) = 0 then begin
      // return the size of the signature in bytes for success and zero for failure
      EIdOpenSSLAPICryptoError.RaiseException('Error signing request');
    end;

    Result := VCert;
    VCert := nil;
  finally
    if VCert <> nil then begin
      X509_free(VCert);
    end;
  end;
end;

function MakePrivateKey(const ABitsKeySize: Integer = 2048): PEVP_PKEY;
var
  VRSA: PRSA;
  VBigNumber: PBIGNUM;
  VPrivateKey: PEVP_PKEY;
begin
  // Create the RSA key pair object
  VRSA := RSA_new();
  if VRSA = nil then begin
    EIdOpenSSLAPICryptoError.RaiseException('RSA_new() failed');
  end;

  try
    // Create the big number object
    VBigNumber := BN_new();
    if VBigNumber = nil then begin
      EIdOpenSSLAPICryptoError.RaiseException('BN_new() failed');
    end;

    try
      // Set the word
      if BN_set_word(VBigNumber, 65537) <> 1 then begin
        EIdOpenSSLAPICryptoError.RaiseException('BN_set_word() failed');
      end;

      // Generate the key pair (lots of computes here)
      if RSA_generate_key_ex(VRSA, ABitsKeySize, VBigNumber, nil) <> 1 then begin
        EIdOpenSSLAPICryptoError.RaiseException('RSA_generate_key_ex() failed');
      end;

      // Now we need a private key object
      VPrivateKey := EVP_PKEY_new();
      if VPrivateKey = nil then begin
        EIdOpenSSLAPICryptoError.RaiseException('EVP_PKEY_new() failed');
      end;

      try
        if EVP_PKEY_set1_RSA(VPrivateKey, VRSA) <> 1 then begin
          EIdOpenSSLAPICryptoError.RaiseException('EVP_PKEY_set1_RSA() failed');
        end;

        Result := VPrivateKey;
        VPrivateKey := nil;
      finally
        if VPrivateKey <> nil then begin
          EVP_PKEY_free(VPrivateKey);
        end;
      end;

    finally
      BN_free(VBigNumber);
    end;
  finally
    RSA_free(VRSA);
  end;
end;

function MakePrivateKeyCopy(
  const APrivateKey: PEVP_PKEY;
  const ADeepCopy: Boolean
): PEVP_PKEY;

  function _RSAPrivateKey_dup(const ARSA: PRSA): PRSA;
  var
    VBuf, VPtr: PByte;
    VLen: Integer;
  begin
    VLen := i2d_RSAPrivateKey(ARSA, nil);
    VBuf := GetMemory(VLen);
    try
      VPtr := VBuf;
      if i2d_RSAPrivateKey(ARSA, @VPtr) <> VLen then begin
        EIdOpenSSLAPICryptoError.RaiseException('i2d_RSAPrivateKey len mismatch');
      end;
      VPtr := VBuf;
      Result := d2i_RSAPrivateKey(nil, @VPtr, VLen);
    finally
      FreeMem(VBuf);
    end;
  end;

var
  VRSA, VRSACopy: PRSA;
  VPrivateKeyCopy: PEVP_PKEY;
begin
  Assert(APrivateKey <> nil);
  Assert(APrivateKey._type = EVP_PKEY_RSA);

  VPrivateKeyCopy := EVP_PKEY_new();
  if VPrivateKeyCopy = nil then begin
    EIdOpenSSLAPICryptoError.RaiseException('EVP_PKEY_new() failed');
  end;
  try
    VRSA := EVP_PKEY_get1_RSA(APrivateKey);
    if VRSA = nil then begin
      EIdOpenSSLAPICryptoError.RaiseException('EVP_PKEY_get1_RSA() failed');
    end;
    try
      if ADeepCopy then begin
        VRSACopy := _RSAPrivateKey_dup(VRSA);
        if VRSACopy = nil then begin
          EIdOpenSSLAPICryptoError.RaiseException('_RSAPrivateKey_dup failed');
        end;
      end else begin
        VRSACopy := VRSA;
      end;
      try
        if EVP_PKEY_set1_RSA(VPrivateKeyCopy, VRSACopy) <> 1 then begin
          EIdOpenSSLAPICryptoError.RaiseException('EVP_PKEY_set1_RSA() failed');
        end;
        Result := VPrivateKeyCopy;
        VPrivateKeyCopy := nil;
      finally
        if ADeepCopy then begin
          RSA_free(VRSACopy);
        end;
      end;
    finally
      RSA_free(VRSA);
    end;
  finally
    if VPrivateKeyCopy <> nil then begin
      EVP_PKEY_free(VPrivateKeyCopy);
    end;
  end;
end;

end.
