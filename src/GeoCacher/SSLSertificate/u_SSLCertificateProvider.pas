unit u_SSLCertificateProvider;

interface

uses
  i_SSLCertificate,
  u_SSLCertificate,
  u_SSLCertificateCache,
  u_SSLCertificateFactory;

type
  TSSLCertificateProvider = class(TObject)
  private
    FCertPath: string;
    FRootCertificate: IX509Certificate;
    FCertFactory: TSSLCertificateFactory;
    FDummyCertCache: TSSLCertificateCache;
  private
    function CommonNameFromHost(const AHost: string): AnsiString;
    function GetRootCertificate: IX509Certificate;
    function GetDummyPrivateKeyFileName: string;
    function GetStoragePath: string;
  public
    function GetDummyCert(const AHost: string): IX509Certificate;
  public
    constructor Create;
    destructor Destroy; override;
  end;

implementation

uses
  Classes,
  SysUtils,
  libcrc32,
  IdSSLOpenSSLHeaders,
  flcStrings,
  u_FileSystemTools,
  u_SSLCertificatePemUtils,
  u_SSLCertificateGenerator;

const
  cOU: AnsiString = 'Created by GeoCacher';
  cON: AnsiString = 'GC_DO_NOT_TRUST';
  cCN: AnsiString = 'GC_DO_NOT_TRUST_GeoCacherRoot';

const
  cPrivateKeyPass: AnsiString = '';

const
  cMaxCommonNameLen = 63;

{ TSSLCertificateProvider }

constructor TSSLCertificateProvider.Create;
var
  VDummyKeyFileName: string;
begin
  inherited Create;

  FCertPath := PathToFullPath('.\cert\');
  CreateDirIfNotExists(FCertPath);

  VDummyKeyFileName := GetDummyPrivateKeyFileName;

  FCertFactory :=
    TSSLCertificateFactory.Create(
      cON,
      cOU,
      VDummyKeyFileName,
      cPrivateKeyPass
    );

  FRootCertificate := GetRootCertificate;

  FDummyCertCache :=
    TSSLCertificateCache.Create(
      GetStoragePath,
      VDummyKeyFileName,
      cPrivateKeyPass
    );
end;

destructor TSSLCertificateProvider.Destroy;
begin
  FreeAndNil(FCertFactory);
  FreeAndNil(FDummyCertCache);
  FRootCertificate := nil;
  inherited Destroy;
end;

function TSSLCertificateProvider.GetStoragePath: string;
var
  VCrc32: Cardinal;
  VUnique: AnsiString;
begin
  VUnique := AnsiString(FRootCertificate.GetFingerprint);
  Assert(VUnique <> '');
  VCrc32 := crc32(0, PAnsiChar(VUnique), Length(VUnique));
  Result := FCertPath + 'storage\' + LowerCase(IntToHex(VCrc32, 8)) + '\';
end;

function TSSLCertificateProvider.GetDummyPrivateKeyFileName: string;
var
  VKey: PEVP_PKEY;
  VPem: AnsiString;
  VStream: TMemoryStream;
begin
  Result := FCertPath + 'dummy.key';

  if not FileExists(Result) then begin
    VKey := MakePrivateKey(2048);
    try
      VPem := KeyToPem(VKey, cPrivateKeyPass);
      VStream := TMemoryStream.Create;
      try
        VStream.WriteBuffer(VPem[1], Length(VPem));
        VStream.SaveToFile(Result);
      finally
        VStream.Free;
      end;
    finally
      EVP_PKEY_free(VKey);
    end;
  end;
end;

function TSSLCertificateProvider.GetRootCertificate: IX509Certificate;
var
  VNow, VStartDate, VEndDate: TDateTime;
  VCertFileName, VKeyFileName: string;
begin
  VCertFileName := FCertPath + 'rootCA.crt';
  VKeyFileName := FCertPath + 'rootCA.key';

  if not FileExists(VCertFileName) or not FileExists(VKeyFileName) then begin
    Result := FCertFactory.MakeRootCertificate(cCN);
    Result.SaveToFile(VCertFileName, VKeyFileName, cPrivateKeyPass);
  end else begin
    Result := TX509Certificate.Create;
    Result.LoadFromFile(VCertFileName, VKeyFileName, cPrivateKeyPass);

    VStartDate := Result.GetStartDate;
    VEndDate := Result.GetEndDate;
    VNow := Now;
    if not ( (VNow > VStartDate) and (VNow < VEndDate) ) then begin
      raise Exception.Create(
        'Server certificate expired (' + FCertPath + 'rootCA.crt)!'  + #13#10 +
        'Remove it and restart program to create a new one.'
      );
    end;
  end;
end;

function TSSLCertificateProvider.CommonNameFromHost(
  const AHost: string
): AnsiString;
var
  VDotPos: Integer;
begin
  Result := AnsiString(AHost);
  VDotPos := PosCharA('.', Result);
  if VDotPos > 0 then begin
    if PosCharA('.', Result, VDotPos + 1) > 0 then begin
      Result := '*' + CopyFromA(Result, VDotPos);
    end;
  end;
  if Length(Result) > cMaxCommonNameLen then begin
    raise Exception.Create('Host name is too long: ' + AHost);
  end;
end;

function TSSLCertificateProvider.GetDummyCert(const AHost: string): IX509Certificate;
var
  VCommonName: AnsiString;
begin
  VCommonName := CommonNameFromHost(AHost);

  Result := FDummyCertCache.Read(VCommonName);
  if Result = nil then begin
    Result := FCertFactory.MakeDummyCertificate(VCommonName, FRootCertificate);
    FDummyCertCache.Write(VCommonName, Result, False);
  end;
end;

end.
