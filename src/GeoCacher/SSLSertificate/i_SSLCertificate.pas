unit i_SSLCertificate;

interface

uses
  Classes,
  IdSSLOpenSSLHeaders;

type
  IX509Certificate = interface
    ['{EB8F2009-7710-4BB9-B97D-B0C9CDCBC8EA}']

    function GetCertificate: PX509;
    property Certificate: PX509 read GetCertificate;

    function GetPrivateKey: PEVP_PKEY;
    property PrivateKey: PEVP_PKEY read GetPrivateKey;

    procedure SaveToStr(const AKeyPass: AnsiString; out ACert, AKey: AnsiString);
    procedure SaveToFile(const ACertFileName, AKeyFileName: string; const AKeyPass: AnsiString);

    procedure LoadFromMem(const ACert, AKey: TMemoryStream; const AKeyPass: AnsiString = '');
    procedure LoadFromFile(const ACertFileName, AKeyFileName: string; const AKeyPass: AnsiString = '');

    function GetSerialNumber: string;
    function GetFingerprint: string;

    function GetStartDate: TDateTime;
    function GetEndDate: TDateTime;
  end;

implementation

end.
