unit u_SSLOpenSSLHeadersExtended;

interface

uses
  IdSSLOpenSSLHeaders;

procedure LoadOpenSSLLibraryExtended(const APath: string);

var
  BN_to_ASN1_INTEGER: function(const bn: PBIGNUM; ai: PASN1_INTEGER): PASN1_INTEGER cdecl = nil;
  X509_set_serialNumber: function(x: PX509; serial: PASN1_INTEGER): Integer cdecl = nil;

implementation

uses
  Windows,
  SyncObjs,
  SysUtils;

const
  fn_BN_to_ASN1_INTEGER: PAnsiChar = 'BN_to_ASN1_INTEGER';
  fn_X509_set_serialNumber: PAnsiChar = 'X509_set_serialNumber';

var
  GLock: TCriticalSection = nil;
  GHandle: THandle = INVALID_HANDLE_VALUE;

function _LoadProc(const AName: PAnsiChar): Pointer;
begin
  Result := Windows.GetProcAddress(GHandle, AName);
  if Result = nil then begin
    RaiseLastOSError;
  end;
end;

procedure LoadOpenSSLLibraryExtended(const APath: string);
begin
  GLock.Acquire;
  try
    if GHandle = INVALID_HANDLE_VALUE then begin
      GHandle := LoadLibrary(PChar(IncludeTrailingPathDelimiter(APath) + 'libeay32.dll'));

      if GHandle = INVALID_HANDLE_VALUE then begin
        RaiseLastOSError;
      end;

      @BN_to_ASN1_INTEGER := _LoadProc(fn_BN_to_ASN1_INTEGER);
      @X509_set_serialNumber := _LoadProc(fn_X509_set_serialNumber);
    end;
  finally
    GLock.Release;
  end;
end;

procedure UnloadOpenSSLLibraryExtended;
var
  VHandle: THandle;
begin
  GLock.Acquire;
  try
    @BN_to_ASN1_INTEGER := nil;
    @X509_set_serialNumber := nil;
    
    if GHandle <> INVALID_HANDLE_VALUE then begin
      VHandle := GHandle;
      GHandle := INVALID_HANDLE_VALUE;
      if not FreeLibrary(VHandle) then begin
        RaiseLastOSError;
      end;
    end;
  finally
    GLock.Release;
  end;
end;

initialization
  GLock := TCriticalSection.Create;

finalization
  UnloadOpenSSLLibraryExtended;
  FreeAndNil(GLock);

end.
