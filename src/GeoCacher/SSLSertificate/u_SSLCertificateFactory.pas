unit u_SSLCertificateFactory;

interface

uses
  IdSSLOpenSSLHeaders,
  i_SSLCertificate,
  u_SSLCertificateGenerator;

type
  TSSLCertificateFactory = class(TObject)
  private
    FDummyKey: PEVP_PKEY;

    FOrganizationName: AnsiString;
    FOrganizationUnitName: AnsiString;

    function GetDummyKey(
      const ADummyKeyFileName: string;
      const ADummyKeyPass: AnsiString
    ): PEVP_PKEY;

    function MakeCertificate(
      const AIssuerCert: PX509;
      const ASignKey: PEVP_PKEY;
      const ABitsKeySize: Integer;
      const APrivKey: PEVP_PKEY;
      const AEntries: PCertEntries;
      const AExtensions: PCertExtensions;
      const ASerial: AnsiString;
      const ANotBefore: Integer;
      const ANotAfter: Integer
    ): IX509Certificate;
  public
    function MakeRootCertificate(
      const ACommonName: AnsiString
    ): IX509Certificate;

    function MakeDummyCertificate(
      const ACommonName: AnsiString;
      const ARootCA: IX509Certificate
    ): IX509Certificate;
  public
    constructor Create(
      const AOrganizationName: AnsiString;
      const AOrganizationUnitName: AnsiString;
      const ADummyKeyFileName: string;
      const ADummyKeyPass: AnsiString
    );
    destructor Destroy; override;
  end;

implementation

uses
  Classes,
  SysUtils,
  DateUtils,
  u_SSLCertificate,
  u_SSLCertificatePemUtils;

{ TSSLCertificateFactory }

constructor TSSLCertificateFactory.Create(
  const AOrganizationName: AnsiString;
  const AOrganizationUnitName: AnsiString;
  const ADummyKeyFileName: string;
  const ADummyKeyPass: AnsiString
);
begin
  Assert(AOrganizationName <> '');
  Assert(AOrganizationUnitName <> '');

  inherited Create;

  FOrganizationName := AOrganizationName;
  FOrganizationUnitName := AOrganizationUnitName;

  FDummyKey := GetDummyKey(ADummyKeyFileName, ADummyKeyPass);
end;

destructor TSSLCertificateFactory.Destroy;
begin
  if FDummyKey <> nil then begin
    EVP_PKEY_free(FDummyKey);
  end;
  inherited Destroy;
end;

function TSSLCertificateFactory.GetDummyKey(
  const ADummyKeyFileName: string;
  const ADummyKeyPass: AnsiString
): PEVP_PKEY;
var
  VStream: TMemoryStream;
begin
  if not FileExists(ADummyKeyFileName) then begin
    raise Exception.Create('Dummy key file not found: ' + ADummyKeyFileName);
  end;

  VStream := TMemoryStream.Create;
  try
    VStream.LoadFromFile(ADummyKeyFileName);
    Result := KeyFromPem(VStream, ADummyKeyPass);
  finally
    VStream.Free;
  end;
end;

function TSSLCertificateFactory.MakeCertificate(
  const AIssuerCert: PX509;
  const ASignKey: PEVP_PKEY;
  const ABitsKeySize: Integer;
  const APrivKey: PEVP_PKEY;
  const AEntries: PCertEntries;
  const AExtensions: PCertExtensions;
  const ASerial: AnsiString;
  const ANotBefore: Integer;
  const ANotAfter: Integer
): IX509Certificate;
var
  VKey: PEVP_PKEY;
  VCert: PX509;
begin
  if APrivKey <> nil then begin
    VKey := MakePrivateKeyCopy(APrivKey, False);
  end else begin
    Assert(ABitsKeySize > 0);
    VKey := MakePrivateKey(ABitsKeySize);
  end;
  try
    VCert := u_SSLCertificateGenerator.MakeCertificate(VKey, AIssuerCert,
      ASignKey, AEntries, AExtensions, ASerial, ANotBefore, ANotAfter);

    Result := TX509Certificate.CreateWithOwn(VCert, VKey);

    VKey := nil;
  finally
    if VKey <> nil then begin
      EVP_PKEY_free(VKey);
    end;
  end;
end;

function MakeNewSerialNumber: AnsiString;
var
  VGUID: TGUID;
begin
  // SN must be a non-negatve integer value up to 20 bytes len

  if CreateGuid(VGUID) <> S_OK then begin
    raise Exception.Create('CreateGuid failed!');
  end;

  // First 4 bits of TGUID.D3 field is a "GUID Version" (it can be 1..5).
  // We can safely reset sign bit in this field to ensure that result
  // number will be non-negative.

  Result := AnsiString(
    IntToHex(VGUID.D3 and $7FFF, 4) +
    IntToHex(VGUID.D1, 8) +
    IntToHex(VGUID.D2, 4) +
    IntToHex(PInt64(@VGUID.D4[0])^, 16)
  );
end;

function TSSLCertificateFactory.MakeRootCertificate(
  const ACommonName: AnsiString
): IX509Certificate;
var
  VEntries: TCertEntries;
  VExtensions: TCertExtensions;
  VNow, VNotBefore, VNotAfter: TDateTime;
begin
  VEntries.Clear;
  VEntries.OrganizationalUnitName := FOrganizationUnitName;
  VEntries.OrganizationName := FOrganizationName;
  VEntries.CommonName := ACommonName;

  VExtensions.Clear;
  VExtensions.BasicConstraints := 'critical,CA:TRUE,pathlen:0';
  VExtensions.KeyUsage := 'critical,keyCertSign,cRLSign';
  VExtensions.ExtKeyUsage := 'serverAuth';
  VExtensions.SubjectKeyIdentifier := 'hash';

  VNow := Now;
  VNotBefore := StartOfTheYear(IncMonth(VNow, -1));
  VNotAfter := IncYear(VNotBefore, 15);

  Result :=
    MakeCertificate(
      nil,
      nil,
      2048,
      nil,
      @VEntries,
      @VExtensions,
      MakeNewSerialNumber,
      DateTimeToUnix(VNotBefore) - DateTimeToUnix(VNow),
      DateTimeToUnix(VNotAfter) - DateTimeToUnix(VNow)
    );
end;

function TSSLCertificateFactory.MakeDummyCertificate(
  const ACommonName: AnsiString;
  const ARootCA: IX509Certificate
): IX509Certificate;
var
  VEntries: TCertEntries;
  VExtensions: TCertExtensions;
  VNow, VNotBefore, VNotAfter: TDateTime;
begin
  VEntries.Clear;
  VEntries.OrganizationalUnitName := FOrganizationUnitName;
  VEntries.OrganizationName := FOrganizationName;
  VEntries.CommonName := ACommonName;

  VExtensions.Clear;
  VExtensions.KeyUsage := 'critical,digitalSignature,keyEncipherment,dataEncipherment';
  VExtensions.ExtKeyUsage := 'serverAuth';
  VExtensions.SubjectAltName := 'DNS:' + VEntries.CommonName;
  VExtensions.SubjectKeyIdentifier := 'hash';
  VExtensions.AuthorityKeyIdentifier := 'keyid:always';

  VNow := Now;
  VNotBefore := ARootCA.GetStartDate;
  VNotAfter := ARootCA.GetEndDate;

  Result :=
    MakeCertificate(
      ARootCA.Certificate,
      ARootCA.PrivateKey,
      0,
      FDummyKey,
      @VEntries,
      @VExtensions,
      MakeNewSerialNumber,
      DateTimeToUnix(VNotBefore) - DateTimeToUnix(VNow),
      DateTimeToUnix(VNotAfter) - DateTimeToUnix(VNow)
    );
end;

end.
