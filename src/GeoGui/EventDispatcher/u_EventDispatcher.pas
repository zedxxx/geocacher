unit u_EventDispatcher;

interface

uses
  SysUtils,
  ComCtrls,
  i_EventDispatcher,
  u_ClientMonitor,
  u_StatMonitor,
  u_UrlDataMonitor;

type
  TEventDispatcher = class(TInterfacedObject, IEventDispatcher)
  private
    FStatMonitor: TStatMonitor;
    FClientMonitor: TClientMonitor;
    FUrlDataMonitor: TUrlDataMonitor;
  private
    FEnabled: Boolean;
    FLock: IReadWriteSync;
  private
    { IEventDispatcher }
    procedure OnAppClose;
    procedure OnConnect;
    procedure OnDisconnect;
    procedure OnEvent(const AEvent: Pointer);
    function IsVersionReceived: Boolean;
    function IsFirstTimeMonitorStarted: Boolean;
  public
    constructor Create(
      const APanels: TStatusPanels;
      const AOnSetCaption: TOnSetCaptionEvent;
      const AOnUrlDataNotify: TOnUrlDataNotify
    );
    destructor Destroy; override;
  end;

implementation

uses
  i_TimerNotifier,
  u_TimerNotifier,
  u_GuiEvent,
  u_Synchronizer;

{ TEventDispatcher }

constructor TEventDispatcher.Create(
  const APanels: TStatusPanels;
  const AOnSetCaption: TOnSetCaptionEvent;
  const AOnUrlDataNotify: TOnUrlDataNotify
);
var
  VTimer: ITimerNotifier;
  VEventLock: IReadWriteSync;
begin
  inherited Create;

  VTimer := TTimerNotifier.Create;
  VEventLock := GSync.SyncStd.Make;

  FStatMonitor := TStatMonitor.Create(APanels, VTimer, VEventLock);
  FClientMonitor := TClientMonitor.Create(APanels[5], AOnSetCaption, VTimer, VEventLock);
  FUrlDataMonitor := TUrlDataMonitor.Create(AOnUrlDataNotify, VTimer, VEventLock);

  FEnabled := True;
  FLock := GSync.SyncStd.Make;
end;

destructor TEventDispatcher.Destroy;
begin
  OnAppClose;
  inherited Destroy;
end;

procedure TEventDispatcher.OnAppClose;
begin
  FLock.BeginWrite;
  try
    if not FEnabled then begin
      Exit;
    end;
    FEnabled := False;
    FreeAndNil(FStatMonitor);
    FreeAndNil(FClientMonitor);
    FreeAndNil(FUrlDataMonitor);
  finally
    FLock.EndWrite;
  end;
end;

procedure TEventDispatcher.OnConnect;
begin
  FLock.BeginRead;
  try
    if not FEnabled then begin
      Exit;
    end;
    FStatMonitor.OnConnect;
    FClientMonitor.OnConnect;
    FUrlDataMonitor.OnConnect;
  finally
    FLock.EndRead;
  end;
end;

procedure TEventDispatcher.OnDisconnect;
begin
  FLock.BeginRead;
  try
    if not FEnabled then begin
      Exit;
    end;
    FStatMonitor.OnDisconnect;
    FClientMonitor.OnDisconnect;
    FUrlDataMonitor.OnDisconnect;
  finally
    FLock.EndRead;
  end;
end;

procedure TEventDispatcher.OnEvent(const AEvent: Pointer);
begin
  FLock.BeginRead;
  try
    if not FEnabled then begin
      Exit;
    end;
    case TEventMessage.Command(AEvent) of
      EM_STAT: begin
        FStatMonitor.OnEvent(AEvent);
      end;

      EM_VERSION: begin
        FClientMonitor.OnEvent(AEvent);
      end;

      EM_URL_DATA_1,
      EM_URL_DATA_2,
      EM_URL_DATA_3,
      EM_MONITOR_STARTED,
      EM_MONITOR_STOPPED: begin
        FUrlDataMonitor.OnEvent(AEvent);
      end;

    end;
  finally
    FLock.EndRead;
  end;
end;

function TEventDispatcher.IsVersionReceived: Boolean;
begin
  FLock.BeginRead;
  try
    if FEnabled then begin
      Result := FClientMonitor.IsVersionReceived;
    end else begin
      Result := False;
    end;
  finally
    FLock.EndRead;
  end;
end;

function TEventDispatcher.IsFirstTimeMonitorStarted: Boolean;
begin
  FLock.BeginRead;
  try
    if FEnabled then begin
      Result := FUrlDataMonitor.IsFirstTimeMonitorStarted;
    end else begin
      Result := False;
    end;
  finally
    FLock.EndRead;
  end;
end;

end.
