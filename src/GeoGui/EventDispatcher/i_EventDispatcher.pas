unit i_EventDispatcher;

interface

type
  IEventDispatcher = interface
    ['{4934D5A8-62F7-495A-B8E7-773F8297E8A7}']
    procedure OnAppClose;

    procedure OnConnect;
    procedure OnDisconnect;

    procedure OnEvent(const AEvent: Pointer);

    function IsVersionReceived: Boolean;
    function IsFirstTimeMonitorStarted: Boolean;
  end;

implementation

end.
