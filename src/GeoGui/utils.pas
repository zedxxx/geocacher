unit utils;

interface

type
  TValueStruc = record
    Value: string;
    ValNM: string;
  end;

function ValidPort(Field, Text: string): boolean;
function ValidPath(Field, Text: string): boolean;
function ValidIP(Field, Text: string): boolean;
function BytesToReadable(ABytes: Int64): string;
function BytesToReadableStruc(ABytes: Int64): TValueStruc;

implementation

uses Windows, Classes, SysUtils, Math;

const
  INVALID_FILE_ATTRIBUTES = DWORD(-1);

resourcestring
  rsValueError = 'Incorrect value';
  rsPortValueError = 'Incorrect Port number value for "%s".';
  rsPathValueError = 'Incorrect Path value for "%s".';
  rsIPValueError = 'Incorrect IP adress value for "%s".';

//---------------------------------------------------------------------
function IsValidIP(const S: String): Boolean;
var
  j, i  : Integer;
  LTmp  : String;
  SL    : TStringList;
begin
  Result := True;
  SL:=nil;
  try
    LTmp := Trim(S); SL:=TStringList.Create;
    i:=ExtractStrings(['.'],[], PChar(LTmp), SL);
    if i<>4 then begin
      Result := False;
      Exit;
    end;
    for i := 1 to 4 do begin
      j:=StrToInt(SL[i-1]);
      Result := Result and (j > -1) and (j < 256);
      if not Result then begin
        Result := False;
        Exit;
      end;
    end;
  except
    Result := False;
  end;
  if SL<>nil then SL.Free;
end;

function IsValidFilePath(const FileName:string):Boolean;
var
  S:string;
  I:Integer;
begin
  Result:=False;
  S:=FileName;
  repeat
    I:=LastDelimiter('\/',S);
    MoveFile(nil,PChar(S));
    if (GetLastError=ERROR_ALREADY_EXISTS)or(
      (GetFileAttributes(PChar(Copy(S,I+1,MaxInt)))=INVALID_FILE_ATTRIBUTES)
      and(GetLastError=ERROR_INVALID_NAME)) then Exit;
    if I>0 then S:=Copy(S,1,I-1);
  until I=0;
  Result:=True;
end;
//---------------------------------------------------------------------

function ValidPort(Field, Text: string): boolean;
var
  ok: boolean;
  s: string;
  i: longword;
begin
  try
    i := StrToInt(Trim(Text));
    ok:=(i>1024) and (i<=65535);
  except
    ok:=false;
  end;
  if not ok then begin
    s := Format(rsPortValueError, [Field]);
    MessageBox(0, PChar(s), PChar(rsValueError), MB_OK);
  end;
  result:=ok;
end;

function ValidPath(Field, Text: string): boolean;
var
  ok: boolean;
  s: string;
begin
  ok:=IsValidFilePath(Trim(Text));
  if not ok then begin
    s := Format(rsPathValueError, [Field]);
    MessageBox(0, PChar(s), PChar(rsValueError), MB_OK);
  end;
  result:=ok;
end;

function ValidIP(Field, Text: string): boolean;
var
  ok: boolean;
  s: string;
begin
  ok:=IsValidIP(Text);
  if not ok then begin
    s := Format(rsIPValueError, [Field]);
    MessageBox(0, PChar(s), PChar(rsValueError), MB_OK);
  end;
  result:=ok;
end;

function BytesToReadableStruc(ABytes: Int64): TValueStruc;
const LDim: array[0..4] of string = ('b', 'Kb', 'Mb', 'Gb', 'Tb');
var LValue: Double;
    i: Integer;
begin
  LValue := ABytes;
  i := 0;
  while (Trunc(LValue) >= 1024) do begin
    Inc(i);
    LValue := LValue/1024;
  end;
  Result.Value := FloatToStrF(LValue, ffNumber, 6, IfThen(i = 0, 0, 2));
  Result.ValNM := LDim[i];
end;

function BytesToReadable(ABytes: Int64): string;
var
  ValueStr: TValueStruc;
begin
  ValueStr := BytesToReadableStruc(ABytes);
  Result := ValueStr.Value+' ' + ValueStr.ValNM;
end;

end.
