unit config;

interface

uses
  Windows,
  SysUtils,
  Classes,
  IniFiles;

type
  TGUIConf = record
    ServerAddr     : string;  { ����� ���������� � ��������� def = 127.0.0.1}
    GUIPort        : word;    { ���� ��� ������ GUI def = 1025 }

    WindowTop      : Integer;
    WindowLeft     : Integer;
    WindowWidth    : Integer;
    WindowHeight   : Integer;
    Maximized      : boolean;
    AlwaysOnTop    : boolean;

    ShowUrlHint: Boolean;
    ShowFileNameHint: Boolean;

    ReqGridWidth   : array of integer;
    ReqBufLen      : LongInt; { ���������� ����� ������ �������� ��������}

    {$IFDEF DEBUG}
    CmdBufLen      : LongInt; { ���������� ����� ������ ������}
    CmdGridWidth   : array of integer;
    {$ENDIF}

    Lang           : string;
  end;

var
  GUIOPT: TGUIConf;

implementation

const
  CONFIG_PATH = '.\options\';
  CONFIG_FILE = CONFIG_PATH + 'GeoGui.ini';

procedure InitOPTIONS;
var ini   :TINIFile;
    fi,i,j: integer;
begin
   if not fileexists(CONFIG_FILE) then begin
     ForceDirectories(CONFIG_PATH);
     fi:=FileCreate(CONFIG_FILE);
     FileClose(fi);
   end;
     ini:=TIniFile.Create(CONFIG_FILE);
   try
     with ini do begin
       GUIOPT.ServerAddr     := ReadString('gui','ServerAddr','127.0.0.1');
       GUIOPT.GUIPort        := ReadInteger('gui','GUIPort',1025);
       GUIOPT.WindowTop      := ReadInteger('window','Top',0);
       GUIOPT.WindowLeft     := ReadInteger('window','Left',0);
       GUIOPT.WindowWidth    := ReadInteger('window','Width',0);
       GUIOPT.WindowHeight   := ReadInteger('window','Height',0);
       GUIOPT.Maximized      := ReadBool('window','Maximized',false);
       GUIOPT.AlwaysOnTop    := ReadBool('window','OnTop',false);

       GUIOPT.ShowUrlHint := ReadBool('gui', 'ShowUrlHint', True);
       GUIOPT.ShowFileNameHint := ReadBool('gui', 'ShowFileNameHint', True);
       
       {$IFDEF DEBUG}
       GUIOPT.CmdBufLen := ReadInteger('gui','CmdBufLen',300);
       SetLength(GUIOPT.CmdGridWidth,15);
       i:=0; j:=0;
       while (j>=0) and (i<15) do begin
         j:=ReadInteger('cmdwin','Column'+IntToStr(i+1),-1);
         if j>=0 then begin
           GUIOPT.CmdGridWidth[i]:=j;
           Inc(i);
         end;
       end;
       SetLength(GUIOPT.CmdGridWidth,i);
       {$ENDIF}
       GUIOPT.ReqBufLen := ReadInteger('gui','ReqBufLen', 5000);

       SetLength(GUIOPT.ReqGridWidth,15);
       i:=0; j:=0;
       while (j>=0) and (i<15) do begin
         j:=ReadInteger('urlwin','Column'+IntToStr(i+1),-1);
         if j>=0 then begin
           GUIOPT.ReqGridWidth[i]:=j;
           Inc(i);
         end;
       end;
       SetLength(GUIOPT.ReqGridWidth,i);

       GUIOPT.Lang := LowerCase(ReadString('gui','lang','ru'));
     end;
   finally
     ini.Free;
   end;
end;

procedure FinOPTIONS;
var ini:TINIFile;
    fi,i,j:integer;
begin
  if not fileexists(CONFIG_FILE) then begin
    ForceDirectories(CONFIG_PATH);
    fi:=FileCreate(CONFIG_FILE);
    FileClose(fi);
  end;
  if fileexists(CONFIG_FILE) then begin
      ini:=TIniFile.Create(CONFIG_FILE);
    try
      with ini do begin
        WriteString('gui','ServerAddr',GUIOPT.ServerAddr);
        WriteInteger('gui','GUIPort',GUIOPT.GUIPort);
        WriteInteger('window','Top',GUIOPT.WindowTop);
        WriteInteger('window','Left',GUIOPT.WindowLeft);
        WriteInteger('window','Width',GUIOPT.WindowWidth);
        WriteInteger('window','Height',GUIOPT.WindowHeight);
        WriteBool   ('window','Maximized',GUIOPT.Maximized);
        WriteBool   ('window','OnTop',GUIOPT.AlwaysOnTop);

        WriteBool('gui', 'ShowUrlHint', GUIOPT.ShowUrlHint);
        WriteBool('gui', 'ShowFileNameHint', GUIOPT.ShowFileNameHint);

       {$IFDEF DEBUG}
        WriteInteger('gui','CmdBufLen',GUIOPT.CmdBufLen);
        j:=Length(GUIOPT.CmdGridWidth);
        for i:=1 to j do begin
          WriteInteger('cmdwin','Column'+IntToStr(i), GUIOPT.CmdGridWidth[i-1]);
        end;
       {$ENDIF}

        WriteInteger('gui','ReqBufLen',GUIOPT.ReqBufLen);

        j:=Length(GUIOPT.ReqGridWidth);
        for i:=1 to j do begin
          WriteInteger('urlwin','Column'+IntToStr(i), GUIOPT.ReqGridWidth[i-1]);
        end;

        WriteString('gui','lang',GUIOPT.Lang);
      end;
    finally
      ini.Free;
    end;
  end;
end;

initialization
  InitOPTIONS;

finalization
  FinOPTIONS;

end.
