program GeoGui;

uses
  Forms,
  i_AppConfig in '..\GeoCacher\AppConfig\i_AppConfig.pas',
  t_AppConfig in '..\GeoCacher\AppConfig\t_AppConfig.pas',
  u_AppConfig in '..\GeoCacher\AppConfig\u_AppConfig.pas',
  u_AppConfigStatic in '..\GeoCacher\AppConfig\u_AppConfigStatic.pas',
  u_FileSystemTools in '..\GeoCacher\FileSystem\u_FileSystemTools.pas',
  t_GuiEventUrlData in '..\GeoCacher\Gui\t_GuiEventUrlData.pas',
  u_GuiEvent in '..\GeoCacher\Gui\u_GuiEvent.pas',
  u_GuiEventLog in '..\GeoCacher\Gui\u_GuiEventLog.pas',
  u_GuiEventUrlData in '..\GeoCacher\Gui\u_GuiEventUrlData.pas',
  u_GuiSocketStream in '..\GeoCacher\Gui\u_GuiSocketStream.pas',
  t_HttpRequestCounters in '..\GeoCacher\t_HttpRequestCounters.pas',
  i_EventDispatcher in 'EventDispatcher\i_EventDispatcher.pas',
  u_EventDispatcher in 'EventDispatcher\u_EventDispatcher.pas',
  u_BaseMonitor in 'EventMonitors\u_BaseMonitor.pas',
  u_ClientMonitor in 'EventMonitors\u_ClientMonitor.pas',
  u_StatMonitor in 'EventMonitors\u_StatMonitor.pas',
  u_UrlDataMonitor in 'EventMonitors\u_UrlDataMonitor.pas',
  MainForm in 'MainForm.pas' {Form1},
  u_SPSCQueue in 'Queue\u_SPSCQueue.pas',
  i_TimerNotifier in 'Timer\i_TimerNotifier.pas',
  u_TimerNotifier in 'Timer\u_TimerNotifier.pas',
  config in 'config.pas',
  soc_thread in 'soc_thread.pas',
  u_ClipboardFunc in 'u_ClipboardFunc.pas',
  utils in 'utils.pas';

{$R *.res}

const
  IMAGE_DLLCHARACTERISTICS_NX_COMPAT = $0100;
  IMAGE_DLLCHARACTERISTICS_DYNAMIC_BASE = $0040;

{$SetPEOptFlags IMAGE_DLLCHARACTERISTICS_NX_COMPAT} // enables DEP
{$SetPEOptFlags IMAGE_DLLCHARACTERISTICS_DYNAMIC_BASE} // enables ASLR

begin
  {$IFDEF DEBUG}
  //ReportMemoryLeaksOnShutdown := True;
  {$ENDIF}
  
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'GUI for GeoCacher';
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
