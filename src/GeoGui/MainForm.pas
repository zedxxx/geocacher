unit MainForm;

interface

uses
  Windows,
  Messages,
  Classes,
  Controls,
  Forms,
  ScktComp,
  WinSock,
  ExtCtrls,
  ComCtrls,
  AppEvnts,
  StdCtrls,
  Graphics,
  Grids,
  Buttons,
  Mask,
  ImgList,
  Menus,
  i_AppConfig,
  i_EventDispatcher,
  u_UrlDataMonitor,
  config,
  soc_thread;

type
  TForm1 = class(TForm)
    PageControl1: TPageControl;
    StatusBar: TStatusBar;
    tsMonitor: TTabSheet;
    tsOptions: TTabSheet;
    URLGrid: TStringGrid;
    sbUrlStart: TSpeedButton;
    sbURLPause: TSpeedButton;
    sbURLClear: TSpeedButton;
    gbServerOptions: TGroupBox;
    chbCacheOnly: TCheckBox;
    edtServerPort: TMaskEdit;
    gbInterface: TGroupBox;
    Label11: TLabel;
    chbUpdateRoot: TCheckBox;
    chbAnyVer: TCheckBox;
    trMaxZoom: TTrackBar;
    lblMaxZoom: TLabel;
    Label13: TLabel;
    Label15: TLabel;
    edtGUIPort: TMaskEdit;
    Bevel3: TBevel;
    Bevel4: TBevel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Bevel5: TBevel;
    edtTileCachePath: TEdit;
    Label23: TLabel;
    Label24: TLabel;
    edtFileCachePath: TEdit;
    chbTestTiles: TCheckBox;
    Label26: TLabel;
    Label27: TLabel;
    edtServerGUIPort: TMaskEdit;
    edtGUIServerAddr: TEdit;
    lblOptReqBufLen: TLabel;
    edtOptReqBufLen: TMaskEdit;
    btnReload: TBitBtn;
    Label28: TLabel;
    chbAlwaysOnTop: TCheckBox;
    cbbLang: TComboBox;
    lblLang: TLabel;
    pmMonitor: TPopupMenu;
    miCopyUrl: TMenuItem;
    miCopyFilePath: TMenuItem;
    N1: TMenuItem;
    miShowUrlInHint: TMenuItem;
    miShowFilePathInHint: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure edtReqBufLenExit(Sender: TObject);
    procedure URLGridDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure sbUrlStartClick(Sender: TObject);
    procedure sbURLPauseClick(Sender: TObject);
    procedure sbURLClearClick(Sender: TObject);
    procedure WndProc(var AMsg: TMessage); override;
    procedure btnReloadClick(Sender: TObject);
    procedure trMaxZoomChange(Sender: TObject);
    procedure editOptionChanged(Sender: TObject);
    procedure checkOptionChanged(Sender: TObject);
    procedure edtGUITextOptionChanged(Sender: TObject);
    procedure editSetChangeFlag(Sender: TObject);
    procedure chbAlwaysOnTopClick(Sender: TObject);
    procedure cbbLangChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure miCopyUrlClick(Sender: TObject);
    procedure URLGridMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure URLGridMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure miShowUrlInHintClick(Sender: TObject);
    procedure miShowFilePathInHintClick(Sender: TObject);
    procedure miCopyFilePathClick(Sender: TObject);
  private
    FCol, FRow: Integer;
    ActivityTimer: TTimer;
    FSocThread: TSocThread;
    MonitorFirstLine: boolean;
    MonitorRowCounter: Longint;
    CmdBufCleaning: Boolean;
    ReqBufCleaning: Boolean;

    InUpdateOptions: Boolean;
    EditOptionChangedFlag: Boolean;
    NeedSendMaxZoom: Boolean;

    FAppConfig: IAppConfig;
    FEventDispatcher: IEventDispatcher;

    procedure OnSetCaptionEvent(const ACaption: string);
    procedure OnUrlDataEvent(var AUrlData: TUrlDataArray; const ACount: Integer);

    procedure DoSetOptions(const AConfig: IAppConfigStatic);
    procedure DoTimerTick(Sender: TObject);
    procedure WMGetMinMaxInfo(var  Msg: TWMGetMinMaxInfo); message WM_GETMINMAXINFO;

    procedure CreateMonitorGridHeaders;

    function GetUrlString: string;
  public
    constructor Create(AOwner: TComponent); override;
  end;

var
  Form1: TForm1;

resourcestring
  rsConnected = 'connected';
  rsOff = 'off';
  rsReady = 'Ready';
  rsError = 'Error';
  rsDownloading = 'Downloading...';
  rsTime = 'Time';
  rsTotal = 'Total';
  rsInet = 'Inet';
  rsCache = 'Cache';
  rsState = 'State';
  rsResponse = 'Response';

implementation

uses
  Dialogs,
  SysUtils,
  StrUtils,
  gnugettext,
  utils,
  t_AppConfig,
  t_GuiEventUrlData,
  i_LanguageManager,
  i_LanguageListStatic,
  u_AppConfig,
  u_EventDispatcher,
  u_ClipboardFunc;

{$R *.dfm}

type
  TStringGridHack = class(TStringGrid)
  protected
    procedure DeleteRow(ARow: Longint); reintroduce;
    procedure InsertRow(ARow: Longint);
    procedure ClearRows;
  end;

type
  // �������������� ���������� ������� (�����������)
  TURLDataInfo = record
    ReqID     : Longint; // ID �������
    Error     : Boolean; // ������ �������� � ������� (�������� �������)
    Ready     : Boolean; // ������ ��������
    FromInet  : Boolean; // ������ ��������� �� �����
    FromCache : Boolean; // ������ ��������� �� ����
    FileName  : string;  // ��� ����� � ����
  end;
  PURLDataInfo = ^TURLDataInfo;

const
  MaxZoomLevel = 32;

procedure TStringGridHack.DeleteRow(ARow: Longint);
var
  GemRow: Integer;
  VInfo: PURLDataInfo;
begin
  GemRow := Row;
  // � ����� ������� � ������ ������ � 0-�� ������� ����������� ���. ������.
  // �� ��� �� ������, � ��������� �� ��������� ���. ������
  VInfo := PURLDataInfo(Objects[0, ARow]);
  if VInfo <> nil then begin
    Dispose(VInfo);
    Objects[0, ARow] := nil;
  end;

  // ������� ������
  if RowCount > FixedRows + 1 then begin
    Rows[ARow].Clear;
    inherited DeleteRow(ARow);
  end else begin
    Rows[ARow].Clear;
  end;
  if GemRow < RowCount then Row := GemRow;
end;

procedure TStringGridHack.InsertRow(ARow: Longint);
var
  GemRow: Integer;
begin
  GemRow := Row;
  while ARow < FixedRows do Inc(ARow);
  RowCount := RowCount + 1;
  MoveRow(RowCount - 1, ARow);
  Row := GemRow;
  Rows[Row].Clear;
end;

procedure TStringGridHack.ClearRows;
var
  VInfo: PURLDataInfo;
begin
  while RowCount > FixedRows + 1 do DeleteRow(FixedRows);
  // ������ ������ ����� ������������� ������ ���� ������ - ������ ������� �� ������
  Rows[RowCount-1].Clear;
  VInfo := PURLDataInfo(Objects[0, FixedRows + 1]);
  if VInfo<>nil then begin
    Dispose(VInfo);
    Objects[0, FixedRows + 1] := nil;
  end;
end;

constructor TForm1.Create(AOwner: TComponent);
var
  I: Integer;
  VLanguageManager: ILanguageManager;
  VLanguageList: ILanguageListStatic;
begin
  inherited;

  FAppConfig := TAppConfig.Create;

  VLanguageManager := FAppConfig.LanguageManager;
  if Assigned(VLanguageManager) then begin
    VLanguageList := VLanguageManager.LanguageList;
    if VLanguageList.FindCode(GUIOPT.Lang, I) then begin
      VLanguageManager.CurrentLanguageIndex := I;
    end;
  end;
  TranslateComponent(Self);
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  //Application.HintColor := clLime;
  //Application.HintPause := 2000;
  Application.HintHidePause := 10000;

  Self.DoubleBuffered:=True;

  // �������������� ������ �������� ����������
  ActivityTimer:=TTimer.Create(nil);
  ActivityTimer.Interval:=1000;
  ActivityTimer.OnTimer:=DoTimerTick;

  edtGUIServerAddr.Text:=GUIOPT.ServerAddr;
  edtServerGUIPort.Text:=IntToStr(GUIOPT.GUIPort);

  // ��������� ����
  if (GUIOPT.WindowWidth=0) or (GUIOPT.WindowHeight=0) then
    Self.Position:=poDefaultPosOnly
  else begin
    Self.Position:=poDesigned;
    Self.Top:= GUIOPT.WindowTop;
    Self.Left:= GUIOPT.WindowLeft;
    Self.Width:= GUIOPT.WindowWidth;
    Self.Height:= GUIOPT.WindowHeight;
  end;
  if GUIOPT.Maximized then Self.WindowState := wsMaximized;
  if GUIOPT.AlwaysOnTop then Self.FormStyle := fsStayOnTop;
  chbAlwaysOnTop.Checked:=GUIOPT.AlwaysOnTop;
  // ������ ������� ����� ��������
  I:=0;
  while (I<URLGrid.ColCount) and (I<Length(GUIOPT.ReqGridWidth)) do begin
    URLGrid.ColWidths[I]:=GUIOPT.ReqGridWidth[I];
    Inc(I);
  end;

  edtOptReqBufLen.Text := IntToStr(GUIOPT.ReqBufLen);

  miShowUrlInHint.Checked := GUIOPT.ShowUrlHint;
  miShowFilePathInHint.Checked := GUIOPT.ShowFileNameHint;

  // ������ ����� � ���������
  InUpdateOptions:=False;
  EditOptionChangedFlag:=False;
  NeedSendMaxZoom:=False;
  MonitorFirstLine:=True;
  MonitorRowCounter:=0;
  ReqBufCleaning:=False;
  CreateMonitorGridHeaders;
  
  FEventDispatcher :=
    TEventDispatcher.Create(
      StatusBar.Panels,
      Self.OnSetCaptionEvent,
      Self.OnUrlDataEvent
    );

  // ����� � ������� ������/������ � ������
  FSocThread :=
    TSocThread.Create(
      FAppConfig,
      FEventDispatcher
    );

  // ������ ��������� ����������
  PageControl1.ActivePageIndex := 0;
  ActivityTimer.Enabled := True;
end;

procedure TForm1.FormDestroy(Sender: TObject);
var
  i: integer;
begin
  FSocThread.Terminate;

  FEventDispatcher.OnAppClose;
  FEventDispatcher := nil;

  // ������������� ������ ����������, ��������� �����, ...
  ActivityTimer.Enabled:=False;
  ActivityTimer.Free;
  // ��������� ������� ��������� � ������� ���� � GUIOpt, ������� ����������
  // � ini-���� � ������ ����������� ������ Config
  GUIOPT.WindowTop    := Self.Top;
  GUIOPT.WindowLeft   := Self.Left;
  GUIOPT.WindowWidth  := Self.Width;
  GUIOPT.WindowHeight := Self.Height;
  GUIOPT.Maximized    :=(Self.WindowState = wsMaximized);
  GUIOPT.AlwaysOnTop  :=(Self.FormStyle = fsStayOnTop);
  // ������ ������� ����� ��������
  SetLength(GUIOPT.ReqGridWidth,URLGrid.ColCount);
  for i:=0 to URLGrid.ColCount-1 do begin
    GUIOPT.ReqGridWidth[i] := URLGrid.ColWidths[i];
  end;

  {$IFDEF DEBUG}
  TStringGridHack(URLGrid).ClearRows;

  FSocThread.WaitFor;
  FreeAndNil(FSocThread);
  {$ENDIF}
end;

procedure TForm1.WMGetMinMaxInfo(var  Msg: TWMGetMinMaxInfo);
begin
  // ��������� ��������� �������� ����� ������ ������������, ����� �� ����������
  // ������ ������� � ������, ���������� � �� ���������
  inherited;
  with Msg.MinMaxInfo^ do begin
    ptMinTrackSize.x:= 766;
  //ptMaxTrackSize.x:= form1.width;
    ptMinTrackSize.y:= 544;
  //ptMaxTrackSize.y:= form1.height;
  end;
end;

procedure TForm1.FormResize(Sender: TObject);
var
  i, w: integer;
begin
  // � URL ����� ���������������� ������ ������� "URL"
  w:=0;
  for i := 0 to URLGrid.ColCount - 1 do w:=w+URLGrid.ColWidths[i];
  w := w - URLGrid.ColWidths[2];
  URLGrid.ColWidths[2] := URLGrid.Width - w - GetSystemMetrics(SM_CXVSCROLL) - URLGrid.ColCount;
end;

procedure TForm1.FormShow(Sender: TObject);
var
  i: integer;
  VLangMng: ILanguageManager;
  VLangList: ILanguageListStatic;
begin
  cbbLang.Items.Clear;
  VLangMng := FAppConfig.LanguageManager;
  if Assigned(VLangMng) then begin
    VLangList := VLangMng.LanguageList;
    for i := 0 to VLangList.Count - 1 do begin
      cbbLang.Items.Add(VLangMng.GetLangNameByIndex(i));
    end;
    cbbLang.ItemIndex := VLangMng.GetCurrentLanguageIndex;
  end;
end;

function TForm1.GetUrlString: string;
var
  VRow: Integer;
begin
  VRow := URLGrid.Row;
  if VRow > 0 then begin
    Result := URLGrid.Cells[2, VRow];
  end else begin
    Result := '';
  end;
end;

procedure TForm1.miCopyFilePathClick(Sender: TObject);
var
  VRow: Integer;
  VInfo: PURLDataInfo;
begin
  VRow := URLGrid.Row;
  if VRow > 0 then begin
    VInfo := PURLDataInfo(URLGrid.Objects[0, VRow]);
    if VInfo.FileName <> '' then begin
      CopyStringToClipboard(Application.Handle, VInfo.FileName);
    end else begin
      Assert(False);
    end;
  end else begin
    Assert(False);
  end;
end;

procedure TForm1.miCopyUrlClick(Sender: TObject);
var
  VUrl: string;
begin
  VUrl := GetUrlString;
  if VUrl <> '' then begin
    CopyStringToClipboard(Application.Handle, VUrl);
  end;
end;

procedure TForm1.miShowFilePathInHintClick(Sender: TObject);
begin
  GUIOPT.ShowFileNameHint := miShowFilePathInHint.Checked;
  URLGrid.Hint := '';
end;

procedure TForm1.miShowUrlInHintClick(Sender: TObject);
begin
  GUIOPT.ShowUrlHint := miShowUrlInHint.Checked;
  URLGrid.Hint := '';
end;

procedure TForm1.OnSetCaptionEvent(const ACaption: string);
begin
  if Self.Caption <> ACaption then begin
    Self.Caption := ACaption;
  end;
end;

procedure TForm1.URLGridMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  VRow, VCol: Integer;
  VInfo: PURLDataInfo;
begin
  if Button = mbRight then begin
    URLGrid.MouseToCell(X, Y, VCol, VRow);
    VInfo := PURLDataInfo(URLGrid.Objects[0, VRow]);
    miCopyFilePath.Enabled := (VInfo <> nil) and (VInfo.FileName <> '');
    if (VRow > 0) and (GetUrlString <> '') then begin
      URLGrid.Row := VRow;
      URLGrid.PopupMenu := pmMonitor;
      URLGrid.PopupMenu.Popup(Mouse.CursorPos.X, Mouse.CursorPos.Y);
    end;
  end;
end;

procedure TForm1.URLGridMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
var
  VRow: Integer;
  VCol: Integer;
  VHint: string;
  VInfo: PURLDataInfo;
begin
  if not GUIOPT.ShowUrlHint and not GUIOPT.ShowFileNameHint then begin
    Exit;
  end;

  URLGrid.MouseToCell(X, Y, VCol, VRow);
  with URLGrid do begin
    if (VRow > 0) and (VCol = 2) then begin
      if FRow <> VRow then begin
        Application.CancelHint;
        FRow := VRow;
        FCol := VCol;
        VHint := '';

        if GUIOPT.ShowUrlHint then begin
          VHint := URLGrid.Cells[VCol, VRow];
        end;

        if GUIOPT.ShowFileNameHint then begin
          VInfo := PURLDataInfo(URLGrid.Objects[0, VRow]);
          if (VInfo <> nil) and (VInfo.FileName <> '') then begin
            if VHint <> '' then begin
              VHint := VHint + #13#10 + VInfo.FileName;
            end else begin
              VHint := VInfo.FileName;
            end;
          end;
        end;

        URLGrid.Hint := VHint;
      end;
    end else begin
      URLGrid.Hint := '';
    end;
  end;
end;

procedure TForm1.cbbLangChange(Sender: TObject);
var
  VLangMng: ILanguageManager;
begin
  VLangMng := FAppConfig.LanguageManager;
  if Assigned(VLangMng) then begin
    if VLangMng.CurrentLanguageIndex <> cbbLang.ItemIndex then begin
      if GUIOPT.Lang <> VLangMng.LanguageList.Code[cbbLang.ItemIndex] then begin
        GUIOPT.Lang := VLangMng.LanguageList.Code[cbbLang.ItemIndex];
        MessageDlg(
          _('Changes will take effect after restarting the program.'),
          mtInformation,
          [mbOK],
          0
        );
      end;
    end;
  end;
end;

procedure TForm1.btnReloadClick(Sender: TObject);
begin
  FSocThread.OptionsLoaded:=False;
  FSocThread.RequestOptions;
end;

procedure TForm1.CreateMonitorGridHeaders;
begin
  URLGrid.Rows[0].Append(' �');
  URLGrid.Rows[0].Append(' ' + rsTime);
  URLGrid.Rows[0].Append(' URL');
  URLGrid.Rows[0].Append(' ' + rsTotal);
  URLGrid.Rows[0].Append(' ' + rsInet);
  URLGrid.Rows[0].Append(' ' + rsCache);
  URLGrid.Rows[0].Append(' ' + rsState);
  URLGrid.Rows[0].Append(' ' + rsResponse);
  URLGrid.RowHeights[0]:=16;
end;

procedure TForm1.sbURLClearClick(Sender: TObject);
begin
  ReqBufCleaning:=True;
  URLGrid.Options := URLGrid.Options - [goVertline,goHorzLine];
  TStringGridHack(URLGrid).ClearRows;
  URLGrid.Options := URLGrid.Options + [goVertline,goHorzLine];
  MonitorFirstLine:=True;
  ReqBufCleaning:=False;
  URLGrid.Invalidate;
end;

procedure TForm1.sbURLPauseClick(Sender: TObject);
begin
  FSocThread.RequestStopMonitor;
end;

procedure TForm1.sbUrlStartClick(Sender: TObject);
begin
  FSocThread.RequestStartMonitor;
end;

procedure TForm1.trMaxZoomChange(Sender: TObject);
begin
  if InUpdateOptions then Exit;
  //FOptions.AnyVer := trMaxZoom.Position>0;
  InUpdateOptions:=true;
  //chbAnyVer.Checked:=FOptions.AnyVer;
  InUpdateOptions:=false;
end;

procedure TForm1.URLGridDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  X, Y: Integer;
  VText: string;
  VInfo: PURLDataInfo;

  function GetTypeColor(Inet, Cache: Boolean): TColor;
  begin
    if Inet and Cache then begin
      Result := RGB(255,236,255);
    end else
    if Inet then begin
      Result := RGB(236,242,255);
    end else
    if Cache then begin
      Result := RGB(236,255,242);
    end else begin
      Result := RGB(236,236,236);  //RGB(255,255,255);
    end;
  end;

begin
  if ReqBufCleaning then begin
    Exit;
  end;

  VText := URLGrid.Cells[ACol, ARow];

  if ARow = 0 then begin
    // ������ ���������
    X := Rect.Left + 1;
    URLGrid.Canvas.Font.Color := clBlack;
    URLGrid.Canvas.Brush.Color := clWindow; //clBtnFace;
    URLGrid.Canvas.FillRect(Rect);
  end else begin
    // ������ ������ � �������
    if (gdSelected in State) or (gdFocused in State) then begin
      // ���������� ������� ������
      URLGrid.Canvas.Font.Color := clBlue;
      URLGrid.Canvas.Brush.Color := clSkyBlue;
    end else begin
      VInfo := PURLDataInfo(URLGrid.Objects[0, ARow]);
      // ����� ��������� ������
      if VInfo = nil then begin
        URLGrid.Canvas.Font.Color := clBlack;
        URLGrid.Canvas.Brush.Color := clWindow;
      end else
      if VInfo.Error then begin
        // ���� ������
        URLGrid.Canvas.Font.Color := clRed;
        URLGrid.Canvas.Brush.Color := RGB(255,236,236);
      end else begin
        // ��� ������ - ���������� ��� ����������� ������
        if VInfo.Ready then begin
          URLGrid.Canvas.Font.Color := clBlack;
          URLGrid.Canvas.Brush.Color := GetTypeColor(VInfo.fromInet, VInfo.fromCache);
        end else begin
          URLGrid.Canvas.Font.Color := clBlack;
          URLGrid.Canvas.Brush.Color := RGB(255,255,236);
        end;
      end;
    end;

    // ��� ������
    URLGrid.Canvas.FillRect(Rect);

    // ������� 0,3,4,5 - ������������ �� ������� ����
    case ACol of
      0,3,4,5: begin
        X := Rect.Right - URLGrid.Canvas.TextWidth(VText)-2;
        Dec(Rect.Right, 2);
      end;
    else
      X := Rect.Left + 2;
      Dec(Rect.Right, 2);
    end;
  end;

  // ���������� ����� � ������ �� ���������
  Y := Rect.Top + ((Rect.Bottom - Rect.Top - URLGrid.Canvas.TextHeight(VText)) div 2);
  URLGrid.Canvas.TextRect(Rect, X, Y, VText);
end;

procedure TForm1.DoTimerTick(Sender: TObject);
begin
  ActivityTimer.Enabled:=False;
  // ���� ���������� ������� - ��������� ��������� ����������
  if FSocThread.ConnectionActive then begin
    FSocThread.RequestStatisticFromServer;
    if not FEventDispatcher.IsVersionReceived then begin
       FSocThread.RequestVersion;
    end;
    if not FEventDispatcher.IsFirstTimeMonitorStarted then begin
       FSocThread.RequestStartMonitor;
    end;
    if not FSocThread.OptionsLoaded then begin
       FSocThread.RequestOptions;
    end;
  end;
  // ������ ��������� ������� �������� ������ ����� ���� ���������� � ������ ���������
  gbServerOptions.Enabled := FSocThread.ConnectionActive and FSocThread.OptionsLoaded;
  ActivityTimer.Enabled:=True;
end;

procedure TForm1.editOptionChanged(Sender: TObject);
var
  Name: string;
begin
  if not EditOptionChangedFlag then Exit;
  {
  // ���������� �����, ��������� � ����� ��������������
  Name:=TControl(Sender).Name;
  if Name='edtServerPort' then begin
     if not ValidPort(Label11.Caption, edtServerPort.Text)
       then edtServerPort.Text:=IntToStr(FOptions.ServerPort)
       else begin
         FOptions.ServerPort := StrToInt(Trim(edtServerPort.Text));
         UpdateTextOption('OPT_SERVERPORT', edtServerPort.Text);
       end;
  end else
  if Name='edtGUIPort' then begin
     if not ValidPort(Label15.Caption, edtGUIPort.Text)
       then edtGUIPort.Text:=IntToStr(FOptions.GUIPort)
       else begin
         FOptions.GUIPort := StrToInt(Trim(edtGUIPort.Text));
         UpdateTextOption('OPT_GUIPORT', edtGUIPort.Text);
       end;
  end else
  if Name='edtTileCachePath' then begin
     if not ValidPath(Label23.Caption, edtTileCachePath.Text)
       then edtTileCachePath.Text:=FOptions.TileCachePath
       else begin
         FOptions.TileCachePath := edtTileCachePath.Text;
         UpdateTextOption('OPT_TILE_PATH', edtTileCachePath.Text);
       end;
  end else
  if Name='edtFileCachePath' then begin
     if not ValidPath(Label24.Caption, edtFileCachePath.Text)
       then edtFileCachePath.Text:=FOptions.WebCachePath
       else begin
         FOptions.WebCachePath := edtFileCachePath.Text;
         UpdateTextOption('OPT_FILE_PATH', edtFileCachePath.Text);
       end;
  end;
  }
  EditOptionChangedFlag := False;
end;

procedure TForm1.editSetChangeFlag(Sender: TObject);
begin
  if InUpdateOptions then Exit;
  EditOptionChangedFlag:=True;
end;

procedure TForm1.chbAlwaysOnTopClick(Sender: TObject);
begin
  GUIOpt.AlwaysOnTop := chbAlwaysOnTop.Checked;
  if GUIOpt.AlwaysOnTop
    then Self.FormStyle:=fsStayOnTop
    else Self.FormStyle:=fsNormal;
end;

procedure TForm1.checkOptionChanged(Sender: TObject);
var
  Name: string;
begin
  if InUpdateOptions then Exit;
  {
  // ���������� �����, ��������� � ���������
  Name:=TControl(Sender).Name;
  if Name='chbCacheOnly' then begin
    FOptions.CacheOnly := chbCacheOnly.Checked;
    UpdateCheckOption('OPT_CACHEONLY', FOptions.CacheOnly);
    InUpdateOptions:=true;
    if FOptions.CacheOnly then begin
      FOptions.UpdateRoot:=False;
      chbUpdateRoot.Checked:=True;
      chbUpdateRoot.Enabled:=False;
    end else begin
      FOptions.UpdateRoot:=True;
      chbUpdateRoot.Checked:=False;
      chbUpdateRoot.Enabled:=True;
    end;
    UpdateCheckOption('OPT_UPDATEROOT', FOptions.UpdateRoot);
    InUpdateOptions:=False;
  end else
  if Name='chbUpdateRoot' then begin
    FOptions.UpdateRoot := not chbUpdateRoot.Checked;
    UpdateCheckOption('OPT_UPDATEROOT', FOptions.UpdateRoot);
  end else
  if Name='chbAnyVer' then begin
    FOptions.AnyVer := chbAnyVer.Checked;
    UpdateCheckOption('OPT_ANYVER', FOptions.AnyVer);
    InUpdateOptions:=true;
    if not FOptions.AnyVer then begin
      FOptions.AnyVerMaxZoom:=0;
      trMaxZoom.Position:=0;
      lblMaxZoom.Caption := rsOff;
      UpdateTextOption('OPT_MAXZOOM', '0');
    end else begin
      if trMaxZoom.Position=0 then begin
        trMaxZoom.Position := 32;
        lblMaxZoom.Caption := 'z32';
        UpdateTextOption('OPT_MAXZOOM', '32');
      end;
    end;
    InUpdateOptions:=false;
  end else
  if Name='chbTcWrite' then begin
    //Options.TcWrite := chbTcWrite.Checked;
    //UpdateCheckOption('OPT_TCWRITE', Options.TcWrite);
  end else
  if Name='chbTestTiles' then begin
    FOptions.TestTiles := chbTestTiles.Checked;
    UpdateCheckOption('OPT_TESTTILES', FOptions.TestTiles);
  end;
  }
end;

procedure TForm1.edtReqBufLenExit(Sender: TObject);
var
  VLen: Integer;
begin
  VLen := StrToIntDef(Trim(edtOptReqBufLen.Text), 100);
  if VLen < 100 then begin
    VLen := 100;
  end;

  GUIOPT.ReqBufLen := VLen;

  if URLGrid.RowCount > VLen then begin
    while URLGrid.RowCount > VLen do begin
      TStringGridHack(URLGrid).DeleteRow(1);
    end;
  end;
end;

procedure TForm1.OnUrlDataEvent(var AUrlData: TUrlDataArray; const ACount: Integer);
var
  I, J: Integer;
  VData: PGuiEventUrlDataRec;
  VRowIndex: Integer;
  VIsLastRow: Boolean;
  VInfo: PURLDataInfo;
  VRect: TRect;
  VAnswer: string;
  VIsReady: Boolean;
  VInetData: UInt64;
  VCacheRead: UInt64;
begin
  if ReqBufCleaning then begin
    Exit;
  end;

  for J := 0 to ACount - 1 do begin
    VData := AUrlData[J];

    // ���� ������� ������ ��������� - ����������
    VIsLastRow := URLGrid.Row = URLGrid.RowCount - 1;
    VRowIndex:=-1;

    VIsReady := VData.EventType = etDone;

    with VData.Counters do begin
      VInetData := DownBody;
      VCacheRead := CacheRead;
    end;

    if not VIsReady then begin
      // ���� ��������������� ������ - �������� ����� ������
      if not MonitorFirstLine then begin
        URLGrid.RowCount := URLGrid.RowCount + 1;
        Inc(MonitorRowCounter);
      end;
      VRowIndex := URLGrid.RowCount - 1;
    end else begin
      // ������� ������ - ����� ������ � �������
      for I := URLGrid.RowCount - 1 downto 0 do begin
        VInfo := PURLDataInfo(URLGrid.Objects[0, I]);
        // ���� ����� - ���������� ������
        if (VInfo <> nil) and (VInfo.ReqID = VData.RequestId) then begin
          VRowIndex := I;
          Break;
        end;
      end;
      // ���� ������ �� ����� - ��������� ������ (�.�. ��� ��� ���� �� ������)
      if VRowIndex < 0 then begin
        Exit;
      end;
    end;
    // ��������� ������ � ������
    if not VIsReady then begin
      New(VInfo);

      // ��������� ����� ������ ��������������� ������
      URLGrid.Rows[VRowIndex].Append(IntToStr(MonitorRowCounter));
      URLGrid.Rows[VRowIndex].Append(FormatDateTime('HH:mm:ss',Now));
      URLGrid.Rows[VRowIndex].Append(VData.URL);
      URLGrid.Rows[VRowIndex].Append(''); // ����� ������ ������ ��� �� �������
      URLGrid.Rows[VRowIndex].Append(''); // ������ ������ � ����� ��� �� �������
      URLGrid.Rows[VRowIndex].Append(''); // ������ ������ � ���� ��� �� �������
      URLGrid.Rows[VRowIndex].Append(rsDownloading); // ��������� - ���� �� ������ - �����
      URLGrid.Rows[VRowIndex].Append(''); // ������ �� ������� ��� �� ��������

      VInfo.ReqID     := VData.RequestId;
      VInfo.Ready     := False;
      VInfo.Error     := False;
      VInfo.FromInet  := False;
      VInfo.FromCache := False;
      VInfo.FileName  := '';

      URLGrid.Objects[0, VRowIndex] := Pointer(VInfo);

      // ���� ������� ������ ��������� - ����������
      if VIsLastRow then begin
        URLGrid.Row := URLGrid.RowCount - 1;
      end;
    end else begin
      // ����� ����� ����������� ��������������� ������

      VInfo := PURLDataInfo(URLGrid.Objects[0, VRowIndex]);

      VInfo.Error     := (VData.RespNo = 0) or (VData.RespNo >= 400) or (VData.WSAError > 0);
      VInfo.Ready     := True;
      VInfo.FromInet  := VInetData > 0;
      VInfo.FromCache := VCacheRead > 0;
      VInfo.FileName  := VData.Hint;

      // ����� ������, ����� � URL �� ��������

      // Total
      I := VCacheRead + VInetData;
      URLGrid.Cells[3, VRowIndex] := IntToStr(I);

      // Inet
      URLGrid.Cells[4, VRowIndex] := IntToStr(VInetData);

      // Cache
      URLGrid.Cells[5, VRowIndex] := IntToStr(VCacheRead);

      // Status
      if VInfo.Error then begin
        URLGrid.Cells[6, VRowIndex] := rsError;
      end else begin
        URLGrid.Cells[6, VRowIndex] := rsReady;
      end;

      // Response
      if not VInfo.Error or (VData.RespNo >= 400) then begin
        VAnswer := IntToStr(VData.RespNo) + ' ' + VData.RespText;
      end else begin
        if VData.WSAError > 0 then begin
          // https://docs.microsoft.com/ru-ru/windows/desktop/WinSock/windows-sockets-error-codes-2
          if VData.WSAError = WSAECONNABORTED then begin
            VAnswer := 'Connection aborted';
          end else begin
            VAnswer := Format('WinSock error #%d', [VData.WSAError]);
          end;
        end else if VData.RespText <> '' then begin
          VAnswer := VData.RespText;
        end else begin
          VAnswer := 'Unknown error';
        end;
      end;
      URLGrid.Cells[7, VRowIndex] := VAnswer;

      //URLGrid.Invalidate;
      for I := 0 to URLGrid.ColCount - 1 do begin
        VRect := URLGrid.CellRect(I, VRowIndex);
        InvalidateRect(URLGrid.Handle, @VRect, false);
      end;
    end;

    // ���� ���-�� ����� � ������ ��������� ��������� ������ - ������� ������ ������ � �������
    if URLGrid.RowCount > GUIOPT.ReqBufLen then begin
      TStringGridHack(URLGrid).DeleteRow(1);
    end;

    MonitorFirstLine := False;
  end;
end;

procedure TForm1.DoSetOptions(const AConfig: IAppConfigStatic);
var
  VConfig: PAppConfigRec;
begin
  VConfig := AConfig.RecPtr;

  InUpdateOptions:=True;
  try
    edtServerPort.Text := IntToStr(VConfig.ListeningPortHttp);
    edtGUIPort.Text := IntToStr(VConfig.ListeningPortTcp);

    chbCacheOnly.Checked := VConfig.WorkMode = wmCacheOnly;

    chbAnyVer.Checked := VConfig.FlatFileCache.AllowReadAnyVersion;

    edtTileCachePath.Text := VConfig.FlatFileCache.Path;
    edtFileCachePath.Text := VConfig.WebCache.Path;

    chbTestTiles.Checked := VConfig.FlatFileCache.TestContentOnRead;
  finally
    InUpdateOptions:=False;
  end;
end;

procedure TForm1.WndProc(var AMsg: TMessage);
begin
  // ��������� ���������
  case AMsg.Msg of
    WM_Paint: begin
      if CmdBufCleaning or ReqBufCleaning then begin
        AMsg.Msg := 0
      end else begin
        inherited WndProc(AMsg);
      end;
    end;

    WM_EraseBkgnd: AMsg.Msg := 0;
  else
    inherited WndProc(AMsg);
  end;
end;

procedure TForm1.edtGUITextOptionChanged(Sender: TObject);
var
  Name: string;
begin
  // ���������� ��������� ����� GUI
  Name:=TControl(Sender).Name;
  if Name='edtGUIServerAddr' then begin
     if not ValidIP(Label26.Caption, edtGUIServerAddr.Text)
       then edtGUIServerAddr.Text:=GUIOPT.ServerAddr
       else begin
         GUIOPT.ServerAddr := edtGUIServerAddr.Text;
       end;
  end else
  if Name='edtServerGUIPort' then begin
     if not ValidPort(Label27.Caption, edtServerGUIPort.Text)
       then edtServerGUIPort.Text:=IntToStr(GUIOPT.GUIPort)
       else begin
         GUIOPT.GUIPort := StrToInt(Trim(edtServerGUIPort.Text));
       end;
  end;
end;

end.
