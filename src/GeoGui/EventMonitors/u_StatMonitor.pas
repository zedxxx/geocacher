unit u_StatMonitor;

interface

uses
  Classes,
  SysUtils,
  ComCtrls,
  i_TimerNotifier,
  u_BaseMonitor,
  u_GuiEvent;

type
  TStatMonitor = class(TBaseMonitor)
  private
    type
      TStatMonitorInfo = packed record
        InetHead   : UInt64;
        InetData   : UInt64;
        SentHead   : UInt64;
        SentData   : UInt64;
        CacheRead  : UInt64;
        CacheWrite : UInt64;
        InCount    : Integer;
        OutCount   : Integer;
      end;
      PStatMonitorInfo = ^TStatMonitorInfo;
  private
    const CItemsCount = 5;
  private
    FPanels: TStatusPanels;
    FText: array [0..CItemsCount-1] of string;

    FInfo: array [Boolean] of TStatMonitorInfo;
    FIndex: Boolean;
    FDoReadInfo: Boolean;
    FIsInfoReceived: Boolean;
    
    procedure ResetInfo;
    
    procedure UpdatePanels(
      const AInfo: PStatMonitorInfo;
      const AIsInfoReceived: Boolean
    );
  protected
    procedure OnTimer; override;
  public
    procedure OnConnect;
    procedure OnDisconnect;
    procedure OnEvent(const AEvent: Pointer);
  public
    constructor Create(
      const APanels: TStatusPanels;
      const ATimer: ITimerNotifier;
      const ALock: IReadWriteSync
    );
  end;

implementation

uses
  utils,
  config;

resourcestring
  rsConnections = 'Connections';

{ TStatMonitor }

constructor TStatMonitor.Create(
  const APanels: TStatusPanels;
  const ATimer: ITimerNotifier;
  const ALock: IReadWriteSync
);
begin
  Assert(APanels.Count >= CItemsCount);

  inherited Create(ALock, ATimer, 300);

  FPanels := APanels;

  FIndex := False;
  ResetInfo;
end;

procedure TStatMonitor.ResetInfo;
begin
  LockWrite;
  try
    FIsInfoReceived := False;
    FDoReadInfo := True;
  finally
    UnlockWrite;
  end;
end;

procedure TStatMonitor.OnConnect;
begin
  ResetInfo;
end;

procedure TStatMonitor.OnDisconnect;
begin
  ResetInfo;
end;

procedure TStatMonitor.OnEvent(const AEvent: Pointer);
var
  VData: PByte;
  VSize: Integer;
begin
  if TEventMessage.Command(AEvent) <> EM_STAT then begin
    Exit;
  end;

  VData := TEventMessage.Data(AEvent);
  VSize := TEventMessage.DataSize(AEvent);

  if (VData = nil) or (VSize < SizeOf(TStatMonitorInfo)) then begin
    Exit;
  end;

  LockWrite;
  try
    FInfo[FIndex] := PStatMonitorInfo(VData)^;
    FDoReadInfo := True;
    FIsInfoReceived := True;
  finally
    UnlockWrite;
  end;
end;

procedure TStatMonitor.OnTimer;
var
  VInfo: PStatMonitorInfo;
  VIsReceived: Boolean;
begin
  if not FDoReadInfo then begin
    Exit;
  end;

  VInfo := nil;
  VIsReceived := False;

  LockWrite;
  try
    if FDoReadInfo then begin
      VInfo := @FInfo[FIndex];
      FIndex := not FIndex;
      FDoReadInfo := False;
      VIsReceived := FIsInfoReceived;
    end;
  finally
    UnlockWrite;
  end;

  if VInfo <> nil then begin
    UpdatePanels(VInfo, VIsReceived);
  end;
end;

procedure TStatMonitor.UpdatePanels(
  const AInfo: PStatMonitorInfo;
  const AIsInfoReceived: Boolean
);
var
  I: Integer;
begin
  if AIsInfoReceived then begin
    FText[0] := 'D: ' + BytesToReadable(AInfo.InetHead + AInfo.InetData);
    FText[1] := 'U: ' + BytesToReadable(AInfo.SentHead + AInfo.SentData);
    FText[2] := 'R: ' + BytesToReadable(AInfo.CacheRead);
    FText[3] := 'W: ' + BytesToReadable(AInfo.CacheWrite);
    FText[4] := Format('%s: %d/%d', [rsConnections, AInfo.InCount, AInfo.OutCount]);
  end else begin
    for I := 0 to CItemsCount - 1 do begin
      FText[I] := '';
    end;
  end;

  FPanels.BeginUpdate;
  try
    for I := 0 to CItemsCount - 1 do begin
      if FText[I] <> FPanels[I].Text then begin
        FPanels[I].Text := FText[I];
      end;
    end;
  finally
    FPanels.EndUpdate;
  end;
end;

end.
