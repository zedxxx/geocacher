unit u_UrlDataMonitor;

interface

uses
  Classes,
  SysUtils,
  libqueue,
  t_GuiEventUrlData,
  i_TimerNotifier,
  u_BaseMonitor,
  u_GuiEvent,
  u_SPSCQueue;

type
  TUrlDataArray = array of PGuiEventUrlDataRec;

  TOnUrlDataNotify = procedure(var AUrlData: TUrlDataArray; const ACount: Integer) of object;

  TUrlDataMonitor = class(TBaseMonitor)
  private
    FQueue: TSPSCQueue;
    FOnUrlDataNotify: TOnUrlDataNotify;
    FIsFirstTimeMonitorStarted: Boolean;
    procedure ClearQueue;
  protected
    procedure OnTimer; override;
  public
    procedure OnConnect;
    procedure OnDisconnect;
    procedure OnEvent(const AEvent: Pointer);
    property IsFirstTimeMonitorStarted: Boolean read FIsFirstTimeMonitorStarted;
  public
    constructor Create(
      const AOnUrlDataNotify: TOnUrlDataNotify;
      const ATimer: ITimerNotifier;
      const ALock: IReadWriteSync
    );
    destructor Destroy; override;
  end;

implementation

uses
  u_GuiEventUrlData;

{ TUrlDataMonitor }

constructor TUrlDataMonitor.Create(
  const AOnUrlDataNotify: TOnUrlDataNotify;
  const ATimer: ITimerNotifier;
  const ALock: IReadWriteSync
);
begin
  Assert(Assigned(AOnUrlDataNotify));
  inherited Create(ALock, ATimer, 100);

  FOnUrlDataNotify := AOnUrlDataNotify;
  FQueue := TSPSCQueue.Create(1024);

  FIsFirstTimeMonitorStarted := False;
end;

destructor TUrlDataMonitor.Destroy;
begin
  ClearQueue;
  FreeAndNil(FQueue);
  inherited;
end;

procedure TUrlDataMonitor.ClearQueue;
var
  I: Integer;
  VItems: PQueueArray;
  VItemsCount: Integer;
begin
  repeat
    FQueue.Dequeue(VItems, VItemsCount);
    for I := 0 to VItemsCount - 1 do begin
      Assert(VItems^[I] <> nil);
      Dispose(VItems^[I]);
    end;
  until not (VItemsCount > 0);
end;

procedure TUrlDataMonitor.OnConnect;
begin
  FIsFirstTimeMonitorStarted := False;
end;

procedure TUrlDataMonitor.OnDisconnect;
begin
  FIsFirstTimeMonitorStarted := False;
end;

procedure TUrlDataMonitor.OnEvent(const AEvent: Pointer);
var
  VCommand: Integer;
  VUrlData: PGuiEventUrlDataRec;
begin
  VCommand := TEventMessage.Command(AEvent);

  case VCommand of

    EM_URL_DATA_1,
    EM_URL_DATA_2,
    EM_URL_DATA_3: begin

      New(VUrlData);
      try
        TGuiEventUrlData.Unpack(AEvent, VCommand, VUrlData);
        FQueue.Enqueue(VUrlData);
      except
        Dispose(VUrlData);
      end;

      FIsFirstTimeMonitorStarted := True;
    end;

    EM_MONITOR_STARTED: begin
      FIsFirstTimeMonitorStarted := True;
    end;

    EM_MONITOR_STOPPED: begin
      //
    end;
  end;
end;

procedure TUrlDataMonitor.OnTimer;
var
  I: Integer;
  VArr: TUrlDataArray;
  VItems: PQueueArray;
  VItemsCount: Integer;
begin
  FQueue.Dequeue(VItems, VItemsCount);

  if VItemsCount > 0 then begin
    VArr := TUrlDataArray(VItems^);

    FOnUrlDataNotify(VArr, VItemsCount);

    for I := 0 to VItemsCount - 1 do begin
      Dispose(VArr[I]);
    end;
  end;
end;

end.
