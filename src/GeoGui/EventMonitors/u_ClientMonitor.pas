unit u_ClientMonitor;

interface

uses
  SysUtils,
  ComCtrls,
  i_TimerNotifier,
  u_BaseMonitor,
  u_GuiEvent;

type
  TOnSetCaptionEvent = procedure(const ACaption: string) of object;

  TClientMonitor = class(TBaseMonitor)
  private
    type
      TClientMonitorInfo = record
        GeoCurVersion: string;
        GeoAvlVersion: string;
      end;
      PClientMonitorInfo = ^TClientMonitorInfo;
  private
    FPanel: TStatusPanel;
    FOnSetCaption: TOnSetCaptionEvent;
  private
    FInfo: array [Boolean] of TClientMonitorInfo;
    FIndex: Boolean;
    FDoReadInfo: Boolean;

    FIsConnected: Boolean;
    FDoUpdateConnected: Boolean;

    FIsVersionReceived: Boolean;
    procedure UpdateConnected(const AConnected: Boolean);
  protected
    procedure OnTimer; override;
  public
    procedure OnConnect;
    procedure OnDisconnect;
    procedure OnEvent(const AEvent: Pointer);
    function IsVersionReceived: Boolean;
  public
    constructor Create(
      const APanel: TStatusPanel;
      const AOnSetCaption: TOnSetCaptionEvent;
      const ATimer: ITimerNotifier;
      const ALock: IReadWriteSync
    );
  end;

implementation

uses
  u_Synchronizer,
  config;

resourcestring
  rsNoConnection = 'no connection';

{ TClientMonitor }

constructor TClientMonitor.Create(
  const APanel: TStatusPanel;
  const AOnSetCaption: TOnSetCaptionEvent;
  const ATimer: ITimerNotifier;
  const ALock: IReadWriteSync
);
begin
  inherited Create(ALock, ATimer, 100);

  FPanel := APanel;
  FOnSetCaption := AOnSetCaption;

  FIndex := False;
  FDoReadInfo := False;

  FIsConnected := False;
  FDoUpdateConnected := False;
  UpdateConnected(FIsConnected);

  FIsVersionReceived := False;
end;

procedure TClientMonitor.OnConnect;
begin
  LockWrite;
  try
    FIsConnected := True;
    FDoUpdateConnected := True;
  finally
    UnlockWrite;
  end;
end;

procedure TClientMonitor.OnDisconnect;
begin
  FIsVersionReceived := False;

  LockWrite;
  try
    FIsConnected := False;
    FDoUpdateConnected := True;
  finally
    UnlockWrite;
  end;
end;

function _ReadString(var P: PByte; var ASize: Integer; out AStr: string): Boolean; inline;
var
  I: Integer;
begin
  if ASize <= 0 then begin
    Result := False;
    Exit;
  end;
  AStr := string(PAnsiChar(P));
  I := Length(AStr) + 1;
  Inc(P, I);
  Dec(ASize, I);
  Result := ASize >= 0;
end;

procedure TClientMonitor.OnEvent(const AEvent: Pointer);
var
  VData: PByte;
  VSize: Integer;
  VInfo: PClientMonitorInfo;
begin
  if TEventMessage.Command(AEvent) <> EM_VERSION then begin
    Exit;
  end;

  VData := TEventMessage.Data(AEvent);
  VSize := TEventMessage.DataSize(AEvent);

  if (VData = nil) or (VSize <= 0) then begin
    Exit;
  end;

  LockWrite;
  try
    VInfo := @FInfo[FIndex];
    if
      _ReadString(VData, VSize, VInfo.GeoCurVersion) and
      _ReadString(VData, VSize, VInfo.GeoAvlVersion)
    then begin
      FDoReadInfo := True;
      FIsVersionReceived := True;
    end;
  finally
    UnlockWrite;
  end;
end;

procedure TClientMonitor.OnTimer;
var
  VStr: string;
  VInfo: PClientMonitorInfo;
  VIsConnected: Boolean;
  VDoUpdateConnected: Boolean;
begin
  if not FDoReadInfo and not FDoUpdateConnected then begin
    Exit;
  end;

  VStr := '';
  VInfo := nil;
  VIsConnected := False;

  LockWrite;
  try
    VDoUpdateConnected := FDoUpdateConnected;
    if VDoUpdateConnected then begin
      VIsConnected := FIsConnected;
      FDoUpdateConnected := False;
    end;

    if FDoReadInfo then begin
      VInfo := @FInfo[FIndex];
      FIndex := not FIndex;
      FDoReadInfo := False;
    end;
  finally
    UnlockWrite;
  end;

  if VDoUpdateConnected then begin
    if not VIsConnected then begin
      VStr := 'GeoGui';
    end;
    UpdateConnected(VIsConnected);
  end;

  if (VStr = '') and (VInfo <> nil) then begin
    VStr := 'GeoCacher ' + VInfo.GeoCurVersion;
  end;

  if VStr <> '' then begin
    FOnSetCaption(VStr);
  end;
end;

procedure TClientMonitor.UpdateConnected(const AConnected: Boolean);
const
  cAlign = '    ';
begin
  if AConnected then begin
    FPanel.Text := '[' + GUIOPT.ServerAddr + ':' + IntToStr(GUIOPT.GUIPort) + ']' + cAlign;
  end else begin
    FPanel.Text := rsNoConnection + cAlign;
  end;
end;

function TClientMonitor.IsVersionReceived: Boolean;
begin
  Result := FIsVersionReceived;
end;

end.
