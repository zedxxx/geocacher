unit u_BaseMonitor;

interface

uses
  SysUtils,
  i_TimerNotifier;

type
  TBaseMonitor = class
  private
    FLock: IReadWriteSync;
    FTimer: ITimerNotifier;
    FListenerID: TListenerID;
  protected
    procedure LockRead; inline;
    procedure UnlockRead; inline;

    procedure LockWrite; inline;
    procedure UnlockWrite; inline;

    procedure OnTimer; virtual; abstract;
  public
    constructor Create(
      const ALock: IReadWriteSync;
      const ATimer: ITimerNotifier;
      const AInterval: Cardinal
    );
    destructor Destroy; override;
  end;

implementation

{ TBaseMonitor }

constructor TBaseMonitor.Create(
  const ALock: IReadWriteSync;
  const ATimer: ITimerNotifier;
  const AInterval: Cardinal
);
begin
  Assert(ALock <> nil);

  inherited Create;

  FLock := ALock;

  FTimer := ATimer;
  if (FTimer <> nil) and (AInterval > 0) then begin
    FListenerID := FTimer.AddListener(Self.OnTimer, AInterval);
  end else begin
    FListenerID := 0;
  end;
end;

destructor TBaseMonitor.Destroy;
begin
  if (FTimer <> nil) and (FListenerID > 0) then begin
    FTimer.RemoveListener(FListenerID);
    FListenerID := 0;
  end;
  inherited Destroy;
end;

procedure TBaseMonitor.LockRead;
begin
  FLock.BeginRead;
end;

procedure TBaseMonitor.LockWrite;
begin
  FLock.BeginWrite;
end;

procedure TBaseMonitor.UnlockRead;
begin
  FLock.EndRead;
end;

procedure TBaseMonitor.UnlockWrite;
begin
  FLock.EndWrite;
end;

end.
