unit soc_thread;

interface

{.$DEFINE SEND_OK}
{.$DEFINE WRITE_LOG}

uses
  Windows,
  Classes,
  Messages,
  ScktComp,
  WinSock,
  SysUtils,
  libqueue,
  i_AppConfig,
  i_EventDispatcher,
  u_GuiEvent,
  u_GuiEventLog,
  u_SPSCQueue,
  u_GuiSocketStream,
  config;

type
  TSocThread = class(TThread)
  private
    FSendOk: Boolean;
    FLog: IGuiEventLog;
    
    FAppConfig: IAppConfig;
    FEventDispatcher: IEventDispatcher;

    FClientSocket: TClientSocket;

    FSendQueue: TSPSCQueue;

    FLastTick: Cardinal;

    FReceiveBuffer: Pointer;
    FReceiveBufferSize: Integer;

    function DoCheckIoResult(
      const AIoResult: TGuiSocketStreamIoResult
    ): Boolean; inline;

    function DoSendData(
      const ASocketStream: TGuiSocketStream;
      const AData: Pointer;
      const ASize: Integer
    ): Boolean; inline;

    function DoSendEvent(
      const ASocketStream: TGuiSocketStream;
      const AEvent: Pointer
    ): Boolean; inline;

    function DoReceiveData(
      const ASocketStream: TGuiSocketStream;
      const ABuffer: Pointer;
      const ABufferSize: Integer;
      out AReceivedSize: Integer
    ): Boolean; inline;

    function DoReceivePartialEvent(
      const ASocketStream: TGuiSocketStream;
      var AReceivedSize: Integer;
      var AEvent: PByte
    ): Boolean;

    procedure ReadDataFromServer(const ASocketStream: TGuiSocketStream);
    procedure DispatchReceivedData(const AEvent: Pointer);

    procedure Do�onnect(Sender: TObject; Socket: TCustomWinSocket);
    procedure DoDisconnect(Sender: TObject; Socket: TCustomWinSocket);

    procedure AddEventToQueue(const AEvent: Pointer); inline;
    procedure ClearQueue;

    procedure Reset;

    property ClientSocket: TClientSocket read FClientSocket;
  protected
    procedure Execute; override;
  public
    OptionsLoaded: Boolean;

    constructor Create(
      const AAppConfig: IAppConfig;
      const AEventDispatcher: IEventDispatcher
    );
    destructor Destroy; override;

    function ConnectionActive: Boolean;

    procedure RequestStatisticFromServer;
    procedure RequestVersion;
    procedure RequestStartMonitor;
    procedure RequestStopMonitor;
    procedure RequestOptions;

    procedure SendOptions;
  end;

implementation

constructor TSocThread.Create(
  const AAppConfig: IAppConfig;
  const AEventDispatcher: IEventDispatcher
);
begin
  inherited Create(False);

  FreeOnTerminate := {$IFDEF DEBUG} False {$ELSE} True {$ENDIF};

  FAppConfig := AAppConfig;
  FEventDispatcher := AEventDispatcher;

  FReceiveBufferSize := 64*1024;
  GetMem(FReceiveBuffer, FReceiveBufferSize);

  FClientSocket := TClientSocket.Create(nil);
  FClientSocket.Address := GUIOPT.ServerAddr;
  FClientSocket.Port := GUIOPT.GUIPort;
  FClientSocket.ClientType := ctBlocking;
  FClientSocket.OnConnect := Do�onnect;
  FClientSocket.OnDisconnect := DoDisconnect;

  FSendQueue := TSPSCQueue.Create(4*1024);

  FLastTick := 0;

  OptionsLoaded := False;

  FLog :=
  {$IFDEF WRITE_LOG}
    TGuiEventLog.Create(
      'GuiTcpClient',
      TUniqIdGeneratorByDate.Create
    );
  {$ELSE}
    nil;
  {$ENDIF}

  FSendOk := {$IFDEF SEND_OK} True {$ELSE} False {$ENDIF};
end;

destructor TSocThread.Destroy;
begin
  FClientSocket.Close;
  FreeAndNil(FClientSocket);

  ClearQueue;
  FreeAndNil(FSendQueue);

  if FReceiveBuffer <> nil then begin
    FreeMem(FReceiveBuffer);
  end;

  inherited;
end;

procedure TSocThread.Do�onnect(Sender: TObject; Socket: TCustomWinSocket);
begin
  if Assigned(FLog) then begin
    FLog.InfoFmt('Connected: %s:%d, ThreadID: %d',
      [ClientSocket.Address, ClientSocket.Port, GetCurrentThreadId]);
  end;
  
  FEventDispatcher.OnConnect;
  ClearQueue;
  if Suspended then begin
    Resume;
  end;
end;

procedure TSocThread.DoDisconnect(Sender: TObject; Socket: TCustomWinSocket);
begin
  if Assigned(FLog) then begin
    FLog.Info('Disconnected');
  end;
  FEventDispatcher.OnDisconnect;
end;

function TSocThread.ConnectionActive: Boolean;
begin
  Result := FClientSocket.Active and FClientSocket.Socket.Connected;
end;

procedure TSocThread.Reset;
begin
  OptionsLoaded := False;
end;

function TSocThread.DoCheckIoResult(
  const AIoResult: TGuiSocketStreamIoResult
): Boolean;
begin
  Result := False;

  case AIoResult of

    ssrOk: begin
      FLastTick := GetTickCount;
      Result := True;
    end;

    ssrTimeout: begin
      if Assigned(FLog) then begin
        FLog.Error('Client timed out');
      end;
      ClientSocket.Active := False;
    end;

    ssrSocketError: begin
      if Assigned(FLog) then begin
        FLog.Error('Lost connection');
      end;
      ClientSocket.Active := False;
    end;

    ssrDisconnected: begin
      if Assigned(FLog) then begin
        FLog.Error('Client is not connected');
      end;
    end;
  else
    raise Exception.CreateFmt('Unexpected IoResult: %d', [Integer(AIoResult)]);
  end;
end;

function TSocThread.DoSendData(
  const ASocketStream: TGuiSocketStream;
  const AData: Pointer;
  const ASize: Integer
): Boolean;
var
  VBytesCount: Integer;
begin
  Assert(AData <> nil);
  Assert(ASize > 0);
  VBytesCount := ASocketStream.Write(AData^, ASize);
  Result := DoCheckIoResult(ASocketStream.IoResult);
  if Result and (VBytesCount <> ASize) then begin
    Assert(False);
  end;
end;

function TSocThread.DoSendEvent(
  const ASocketStream: TGuiSocketStream;
  const AEvent: Pointer
): Boolean;
begin
  Assert(TEventMessage.Validate(AEvent));
  if Assigned(FLog) then begin
    FLog.EventSend(AEvent);
  end;
  Result := DoSendData(ASocketStream, AEvent, TEventMessage.RawSize(AEvent));
end;

function TSocThread.DoReceiveData(
  const ASocketStream: TGuiSocketStream;
  const ABuffer: Pointer;
  const ABufferSize: Integer;
  out AReceivedSize: Integer
): Boolean;
begin
  AReceivedSize := ASocketStream.Read(ABuffer^, ABufferSize);
  Result := DoCheckIoResult(ASocketStream.IoResult) and (AReceivedSize > 0);
  if Assigned(FLog) then begin
    FLog.DebugFmt('Received: %d byte', [AReceivedSize]);
  end;
end;

procedure TSocThread.Execute;
var
  I: Integer;
  VTick: Cardinal;
  VItems: PQueueArray;
  VItemsCount: Integer;
  VSocketStream: TGuiSocketStream;
begin
  VSocketStream := TGuiSocketStream.Create(ClientSocket.Socket, 30000);
  try
    while not Terminated do begin

      if not ClientSocket.Active then begin
        if ClientSocket.Socket.Connected then begin
          ClientSocket.Close;
        end;
        try
          ClientSocket.Open;
          Reset;
        except
          Sleep(500);
          Continue;
        end;
      end;
      
      try
        FSendQueue.Dequeue(VItems, VItemsCount);
        try
          for I := 0 to VItemsCount - 1 do begin
            if not DoSendEvent(VSocketStream, VItems^[I]) then begin
              Exit;
            end;
            if I mod 128 = 0 then begin
              if VSocketStream.WaitForData(50) then begin
                ReadDataFromServer(VSocketStream);
              end;
            end;
          end;
        finally
          for I := 0 to VItemsCount - 1 do begin
            TEventMessage.Free(VItems^[I]);
          end;
        end;

        if VSocketStream.WaitForData(250) then begin
          ReadDataFromServer(VSocketStream);
        end;

        // send "ping"
        VTick := GetTickCount;
        if (VTick < FLastTick) or (VTick - FLastTick > 30000) then begin
          I := 0;
          if not DoSendData(VSocketStream, @I, 1) then begin
            Exit;
          end;
        end;

        Sleep(20);
      except
        ClientSocket.Close;
      end;
    end;
  finally
    VSocketStream.Free;
  end;
end;

function TSocThread.DoReceivePartialEvent(
  const ASocketStream: TGuiSocketStream;
  var AReceivedSize: Integer;
  var AEvent: PByte
): Boolean;
var
  P: PByte;
  VSize: Integer;
  VHeadOffset: Int64;
  VAvailableSize: Integer;
  VMissingSize: Integer;
begin
  if Assigned(FLog) then begin
    FLog.Debug('Start partial event receive');
  end;

  Result := False;

  VHeadOffset := Int64(AEvent) - Int64(FReceiveBuffer);
  Assert(VHeadOffset >= 0);

  while not Result do begin
    VAvailableSize := AReceivedSize - VHeadOffset;
    Assert(VAvailableSize >= 0);

    if VAvailableSize >= TEventMessage.HeadSize then begin

      if TEventMessage.Magic(AEvent) <> EM_MAGIC then begin
        Assert(False);
        Exit;
      end;

      VMissingSize := TEventMessage.RawSize(AEvent) - VAvailableSize;

      if Assigned(FLog) then begin
        FLog.DebugFmt('Missing size: %d byte', [VMissingSize]);
      end;
    end else begin
      VMissingSize := TEventMessage.HeadSize - VAvailableSize;

      if Assigned(FLog) then begin
        FLog.DebugFmt('Missing head size: %d byte', [VMissingSize]);
      end;
    end;

    // ����������� ������ ������
    if AReceivedSize + VMissingSize > FReceiveBufferSize then begin
      Inc(FReceiveBufferSize, VMissingSize);
      ReallocMem(FReceiveBuffer, FReceiveBufferSize);

      AEvent := FReceiveBuffer;
      Inc(AEvent, VHeadOffset);

      if Assigned(FLog) then begin
        FLog.DebugFmt('ReceiveBuffer increased from %d to %d bytes',
          [AReceivedSize, FReceiveBufferSize]);
      end;
    end;

    P := FReceiveBuffer;
    Inc(P, AReceivedSize);

    // ��������� � ����� ������
    if not DoReceiveData(ASocketStream, P, VMissingSize, VSize) then begin
      Exit;
    end;

    Inc(AReceivedSize, VSize);

    if VSize >= VMissingSize then begin
      Result := True;
      if Assigned(FLog) then begin
        FLog.Debug('Finish partial event receive');
      end;
    end;
  end;
end;

procedure TSocThread.ReadDataFromServer(const ASocketStream: TGuiSocketStream);
var
  P: PByte;
  VSize: Integer;
  VCount: Integer;
  VEvent: Pointer;
  VCommand: Integer;
  VResponse: Pointer;
  VEventSize: Integer;
begin
  P := FReceiveBuffer;

  if not DoReceiveData(ASocketStream, P, 32*1024, VSize) then begin
    Exit;
  end;

  VCount := 0;
  while VCount < VSize do begin
    while (P^ = 0) and (VCount < VSize) do begin
      Inc(P);
      Inc(VCount);
    end;

    if VCount = VSize then begin
      Exit;
    end else if TEventMessage.HeadSize > VSize - VCount then begin
      // ��������� �� ��������� � �����
      if not DoReceivePartialEvent(ASocketStream, VSize, P) then begin
        Assert(False);
        Exit;
      end;
    end;

    VEvent := P;
    if TEventMessage.Magic(VEvent) <> EM_MAGIC then begin
      Assert(False);
      Exit;
    end;

    VCommand := TEventMessage.Command(VEvent);
    VEventSize := TEventMessage.RawSize(VEvent);

    if VCommand = EM_OK then begin
      if Assigned(FLog) then begin
        FLog.EventReceive(VEvent);
      end;
      Inc(P, VEventSize);
      Inc(VCount, VEventSize);
      // ������ ��������� �������
      Continue;
    end;

    if VEventSize > VSize - VCount then begin
      // ������ �� ���������� � �����
      if not DoReceivePartialEvent(ASocketStream, VSize, P) then begin
        Assert(False);
        Exit;
      end;
      // ������ ������� ������� ������
      Continue;
    end;

    if Assigned(FLog) then begin
      FLog.EventReceive(VEvent);
    end;

    // �������� ��������� �� ����� ������
    Inc(P, VEventSize);
    Inc(VCount, VEventSize);

    DispatchReceivedData(VEvent);

    if FSendOk then begin
      VResponse := TEventMessage.Alloc(EM_OK, 4);
      try
        PInteger(TEventMessage.Data(VResponse))^ := TEventMessage.ID(VEvent);
        if not DoSendEvent(ASocketStream, VResponse) then begin
          Exit;
        end;
      finally
        TEventMessage.Free(VResponse);
      end;
    end;
  end;
end;

procedure TSocThread.DispatchReceivedData(const AEvent: Pointer);
var
  P: PByte;
begin
  FEventDispatcher.OnEvent(AEvent);

  case TEventMessage.Command(AEvent) of

    EM_OPTIONS: begin
      // ����� �������: ��������� ������ ������������
      // ToDo
      OptionsLoaded := True;
    end;
  end;
end;

procedure TSocThread.SendOptions;
begin
  //
end;

procedure TSocThread.AddEventToQueue(const AEvent: Pointer);
begin
  FSendQueue.Enqueue(AEvent);
end;

procedure TSocThread.ClearQueue;
var
  I: Integer;
  VEvent: Pointer;
  VItems: PQueueArray;
  VItemsCount: Integer;
begin
  repeat
    FSendQueue.Dequeue(VItems, VItemsCount);
    for I := 0 to VItemsCount - 1 do begin
      VEvent := VItems^[I];
      Assert(TEventMessage.Validate(VEvent));
      TEventMessage.Free(VEvent);
    end;
  until not (VItemsCount > 0);
end;

procedure TSocThread.RequestStatisticFromServer();
begin
  // ������ ���������� �� �������
  AddEventToQueue(TEventMessage.Alloc(EM_GET_STAT, 0));
end;

procedure TSocThread.RequestVersion;
begin
  // ������ ������ �� �������
  AddEventToQueue(TEventMessage.Alloc(EM_GET_VERSION, 0));
end;

procedure TSocThread.RequestOptions;
begin
  // ������ ������������ �� �������
  AddEventToQueue(TEventMessage.Alloc(EM_GET_OPTIONS, 0));
end;

procedure TSocThread.RequestStartMonitor;
begin
  // ������ ����������� �������� ������ � �������
  AddEventToQueue(TEventMessage.Alloc(EM_START_MONITOR, 0));
end;

procedure TSocThread.RequestStopMonitor;
begin
  // ��������� ����������� �������� ������ � �������
  AddEventToQueue(TEventMessage.Alloc(EM_STOP_MONITOR, 0));
end;

end.
