unit u_TimerNotifier;

interface

uses
  ExtCtrls,
  i_TimerNotifier;

const
  CDefaultTimerIntervalMS = 100; // milliseconds

type
  TTimerNotifier = class(TInterfacedObject, ITimerNotifier)
  private
    type
      TListenerRec = record
        FId: TListenerId;
        FOnTimer: TOnTimerEvent;
        FInterval: Cardinal;
        FLastTick: Cardinal;
      end;
      PListenerRec = ^TListenerRec;
  private
    FId: Cardinal;
    FTimer: TTimer;
    FListener: array of PListenerRec;
    procedure DoOnTimer(Sender: TObject);
  private
    { ITimerNotifier }
    function AddListener(
      const AOnTimer: TOnTimerEvent;
      const AIntervalMS: Cardinal = CDefaultTimerIntervalMS
    ): TListenerId;
    procedure RemoveListener(const AListenerID: TListenerId);
  public
    constructor Create;
    destructor Destroy; override;
  end;

implementation

uses
  Windows,
  SysUtils;

{ TTimerNotifier }

constructor TTimerNotifier.Create;
begin
  inherited Create;

  FTimer := TTimer.Create(nil);
  FTimer.Enabled := False;
  FTimer.Interval := CDefaultTimerIntervalMS;
  FTimer.OnTimer := Self.DoOnTimer;

  FId := 0;
  FListener := nil;
end;

destructor TTimerNotifier.Destroy;
var
  I: Integer;
begin
  FreeAndNil(FTimer);

  for I := 0 to Length(FListener) - 1 do begin
    Dispose(FListener[I]);
  end;
  FListener := nil;

  inherited Destroy;
end;

function TTimerNotifier.AddListener(
  const AOnTimer: TOnTimerEvent;
  const AIntervalMS: Cardinal
): TListenerId;
var
  I: Integer;
  VListener: PListenerRec;
begin
  Assert(Assigned(AOnTimer));
  Assert(AIntervalMS > 0);

  Inc(FId);

  Result := FId;

  New(VListener);

  VListener.FId := Result;
  VListener.FOnTimer := AOnTimer;
  VListener.FInterval := AIntervalMS;
  VListener.FLastTick := 0;

  I := Length(FListener);
  SetLength(FListener, I+1);
  FListener[I] := VListener;

  if FTimer.Interval > AIntervalMS then begin
    FTimer.Interval := AIntervalMS;
  end;

  FTimer.Enabled := True;
end;

procedure TTimerNotifier.RemoveListener(const AListenerID: TListenerId);
var
  I: Integer;
  VCount: Integer;
  VIndex: Integer;
begin
  Assert(AListenerID > 0);

  VIndex := -1;
  VCount := Length(FListener);

  for I := 0 to VCount - 1 do begin
    if FListener[I].FId = AListenerID then begin
      VIndex := I;
      Dispose(FListener[I]);
      Break;
    end;
  end;

  if VIndex <> -1 then begin
    I := VCount - VIndex;
    if I > 0 then begin
      Move(FListener[VIndex + 1], FListener[VIndex], SizeOf(Pointer) * I);
    end;
    SetLength(FListener, VCount - 1);
  end;

  FTimer.Enabled := Length(FListener) > 0;
end;

procedure TTimerNotifier.DoOnTimer(Sender: TObject);
var
  I: Integer;
  VTick: Cardinal;
  VItem: PListenerRec;
begin
  VTick := GetTickCount;
  for I := 0 to Length(FListener) - 1 do begin
    VItem := FListener[I];
    if VTick - VItem.FLastTick > VItem.FInterval then begin
      VItem.FOnTimer;
      VTick := GetTickCount;
      VItem.FLastTick := VTick;
    end;
  end;
end;

end.
