unit i_TimerNotifier;

interface

type
  TListenerId = Cardinal;

  TOnTimerEvent = procedure of object;

  ITimerNotifier = interface
    ['{B1BD56CB-864C-45FF-85E9-5D9DC3FEF502}']
    function AddListener(
      const AOnTimer: TOnTimerEvent;
      const AIntervalMS: Cardinal
    ): TListenerId;

    procedure RemoveListener(
      const AListenerID: TListenerId
    );
  end;

implementation

end.
