unit u_ClipboardFunc;

interface

procedure CopyStringToClipboard(const AHandle: THandle; const AStr: string);

implementation

uses
  Windows,
  SysUtils;

procedure _Win32Check(const ARetVal: Boolean); inline;
begin
  if not ARetVal then begin
    RaiseLastOSError;
  end;
end;

procedure CopyStringToClipboard(const AHandle: THandle; const AStr: string);
var
  VStr: WideString;
  hg: THandle;
  P: Pointer;
  VLen: Integer;
begin
  _Win32Check(OpenClipboard(AHandle));
  try
    _Win32Check(EmptyClipBoard);
    if AStr = '' then begin
      Exit;
    end;
    VStr := AStr;
    VLen := (Length(VStr) + 1) * SizeOf(VStr[1]);
    hg := GlobalAlloc(GMEM_DDESHARE or GMEM_MOVEABLE, VLen);
    _Win32Check(hg <> 0);
    try
      P := GlobalLock(hg);
      _Win32Check(P <> nil);
      try
        Move(VStr[1], P^, VLen);
        _Win32Check(SetClipboardData(CF_UNICODETEXT, hg) <> 0);
        // If SetClipboardData succeeds, the system owns the object
        // identified by the hMem parameter.
      finally
        GlobalUnlock(hg);
      end;
    except
      GlobalFree(hg);
      raise
    end;
  finally
    CloseClipboard;
  end;
end;

end.
