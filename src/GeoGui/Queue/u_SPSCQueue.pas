unit u_SPSCQueue;

interface

uses
  SysUtils;

type
  TQueueArray = array of Pointer;
  PQueueArray = ^TQueueArray;

  TSPSCQueue = class
  private
    FArr: TQueueArray;
    FCount: Integer;
    FContext: Pointer;
    procedure _RaiseError;
  public
    procedure Enqueue(const AItem: Pointer); inline;
    procedure Dequeue(out AItems: PQueueArray; out ACount: Integer); inline;
  public
    constructor Create(const ACapacity: Integer);
    destructor Destroy; override;
  end;

  ELockFreeQueue = class(Exception);

implementation

uses
  libqueue;

{ TSPSCQueue }

constructor TSPSCQueue.Create(const ACapacity: Integer);
begin
  inherited Create;
  if not queue_new(FContext, QUEUE_TYPE_SPSC, ACapacity) then begin
    _RaiseError;
  end;
  FCount := ACapacity;
  SetLength(FArr, FCount);
end;

destructor TSPSCQueue.Destroy;
begin
  if not queue_del(FContext) then begin
    // ToDo: log error
  end;
  inherited Destroy;
end;

procedure TSPSCQueue._RaiseError;
var
  VMsg: PAnsiChar;
begin
  VMsg := queue_get_error_message(FContext);
  raise ELockFreeQueue.Create(string(VMsg));
end;

procedure TSPSCQueue.Enqueue(const AItem: Pointer);
begin
  if not queue_enqueue(FContext, AItem) then begin
    _RaiseError;
  end;
end;

procedure TSPSCQueue.Dequeue(
  out AItems: PQueueArray;
  out ACount: Integer
);
var
  VCount: Cardinal;
begin
  VCount := FCount;
  if not queue_dequeue_batch(FContext, FArr[0], VCount) then begin
    _RaiseError;
  end;
  AItems := @FArr;
  ACount := VCount;
end;

end.
