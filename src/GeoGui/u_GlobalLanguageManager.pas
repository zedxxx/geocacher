unit u_GlobalLanguageManager;

interface

uses
  i_LanguageManager;

procedure CreateLangManager;
function GetLanguageManager: ILanguageManager;

implementation

uses
  SysUtils,
  u_LanguageManager;

var
  GlobalLanguageManager: ILanguageManager = nil;

procedure CreateLangManager;
var
  VAppPath: string;
begin
  VAppPath := IncludeTrailingPathDelimiter(ExtractFilePath(ParamStr(0)));
  GlobalLanguageManager := TLanguageManager.Create(VAppPath + 'locale');
end;

function GetLanguageManager: ILanguageManager;
begin
  Result := GlobalLanguageManager;
end;

end.
