#!/usr/bin/bash +ex

src="${1}../src/"
lang="${1}"

function get_translate {
    
    cp -f "${lang}dxgettext/ggexclude.cfg" ${src}
    cp -f "${lang}dxgettext/dxgettext.ini" ${src}
    cp -f "${lang}ignore.po" ${src}

    cd ${src}

    dxgettext -r --delphi --nonascii --useignorepo -o:msgid -o ${lang}

    rm -f ggexclude.cfg dxgettext.ini ignore.po

    cd ${lang}

    msgattrib --output-file=out.po --no-location default.po
    msgremove out.po -i ignore.po -o default.po
    
    rm -f out.po
}

function merge_translate {
    
    cd ${lang}    
    msgmerge --update ru.po default.po    
    rm -f *.po~
}

function set_unix_endline {
    
    cd ${lang}
    dos2unix -u ru.po
}

get_translate
merge_translate
set_unix_endline
