@echo off

set cwd=%~dp0

set USERPROFILE=%cwd%
set COMSPEC=%SystemRoot%\system32\cmd.exe
set PATH=%cwd%..\tools;%cwd%..\tools\dxgettext;%cwd%..\tools\gnugettext;%PATH%

busybox bash %cwd%update.sh %cwd%

cd %cwd%

pause
