unit libcrc32;

{$DEFINE STATIC_LIBCRC32}

interface

const
  libcrc32_dll = 'libcrc32.dll';

{$IFDEF STATIC_LIBCRC32}

function crc32(
  const APrev: Cardinal;
  const AData: Pointer;
  const ALength: Cardinal
): Cardinal; cdecl; external libcrc32_dll;

{$ELSE}

var
  crc32: function(
    const APrev: Cardinal;
    const AData: Pointer;
    const ALength: Cardinal
  ): Cardinal; cdecl;

{$ENDIF}

function init_libcrc32(
  const ALibName: string = libcrc32_dll;
  const ARaiseExceptions: Boolean = True
): Boolean;

implementation

{$IFDEF STATIC_LIBCRC32}

function init_libcrc32(const ALibName: string; const ARaiseExceptions: Boolean): Boolean;
begin
  Result := True;
end;

{$ELSE}

uses
  Windows,
  SyncObjs,
  SysUtils;

var
  GHandle: THandle = 0;
  GLock: TCriticalSection = nil;
  GIsInitialized: Boolean = False;

function init_libcrc32(const ALibName: string; const ARaiseExceptions: Boolean): Boolean;

  function _GetProcAddr(const AName: PAnsiChar): Pointer;
  begin
    Result := GetProcAddress(GHandle, AName);
    if ARaiseExceptions and (Result = nil) then begin
      RaiseLastOSError;
    end;
  end;

begin
  if GIsInitialized then begin
    Result := True;
    Exit;
  end;

  GLock.Acquire;
  try
    if GIsInitialized then begin
      Result := True;
      Exit;
    end;

    if GHandle = 0 then begin
      GHandle := LoadLibrary(PChar(ALibName));
      if ARaiseExceptions and (GHandle = 0) then begin
        RaiseLastOSError;
      end;
    end;

    if GHandle <> 0 then begin
      crc32 := _GetProcAddr('crc32');
    end;

    GIsInitialized := (GHandle <> 0) and (Addr(crc32) <> nil);
    Result := GIsInitialized;
  finally
    GLock.Release;
  end;
end;

procedure free_libcrc32;
begin
  GLock.Acquire;
  try
    GIsInitialized := False;

    if GHandle <> 0 then begin
      FreeLibrary(GHandle);
      GHandle := 0;
    end;

    crc32 := nil;
  finally
    GLock.Release;
  end;
end;

initialization
  GLock := TCriticalSection.Create;

finalization
  free_libcrc32;
  FreeAndNil(GLock);

{$ENDIF}

end.
