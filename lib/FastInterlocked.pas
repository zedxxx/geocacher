unit FastInterlocked;

interface

type
  TFastInterlocked = record
    class procedure Inc(var AValue: Integer); static;
    class procedure Dec(var AValue: Integer); static;
  end;

implementation

class procedure TFastInterlocked.Inc(var AValue: Integer); assembler;
asm
  lock inc [eax]
end;

class procedure TFastInterlocked.Dec(var AValue: Integer); assembler;
asm
  lock dec [eax]
end;

end.
