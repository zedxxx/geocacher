unit CompatibilityIniFiles;

{$I jedi.inc}

interface

{$IFNDEF Compiler12_Up} // Delphi11_Down
uses
  Classes,
  IniFiles,
  Compatibility;
{$ENDIF}

{$IFNDEF Compiler12_Up} // Delphi11_Down
type
  TMemIniFile = class(IniFiles.TMemIniFile)
  private
    FStream: TStream;
    FEncoding: TEncoding;
    procedure LoadValues;
  public
    constructor Create(const FileName: string); overload;
    constructor Create(const FileName: string; Encoding: TEncoding); overload;
    constructor Create(const Stream: TStream; Encoding: TEncoding); overload;
    procedure UpdateFile; override;
    property Encoding: TEncoding read FEncoding write FEncoding;
  end;
{$ENDIF}

implementation

{$IFNDEF Compiler12_Up} // Delphi11_Down
uses
  SysUtils,
  CompatibilityStringList;
{$ENDIF}

{ TMemIniFile }

{$IFNDEF Compiler12_Up} // Delphi11_Down
constructor TMemIniFile.Create(const FileName: string);
begin
  Create(FileName, nil);
end;

constructor TMemIniFile.Create(const FileName: string; Encoding: TEncoding);
begin
  inherited Create('');
  FEncoding := Encoding;
  Rename(FileName, False);
  LoadValues;
end;

constructor TMemIniFile.Create(const Stream: TStream; Encoding: TEncoding);
begin
  inherited Create('');
  FStream := Stream;
  FEncoding := Encoding;
  LoadValues;
end;

procedure TMemIniFile.LoadValues;

  procedure LoadFromStream(const AStream: TStream);
  var
    Size: Integer;
    Buffer: TBytes;
    List: TStringList;
  begin
    // Load file into buffer and detect encoding
    Size := AStream.Size - AStream.Position;
    SetLength(Buffer, Size);
    AStream.Read(Buffer[0], Size);
    Size := TEncoding.GetBufferEncoding(Buffer, FEncoding);

    // Load strings from buffer
    List := TStringList.Create;
    try
      List.Text := FEncoding.GetString(Buffer, Size, Length(Buffer) - Size);
      SetStrings(List);
    finally
      List.Free;
    end;
  end;

var
  Stream: TFileStream;
begin
  if FStream <> nil then
    LoadFromStream(FStream)
  else
  if (FileName <> '') and FileExists(FileName) then
  begin
    Stream := TFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
    try
      LoadFromStream(Stream);
    finally
      Stream.Free;
    end;
  end
  else
    Clear;
end;

procedure TMemIniFile.UpdateFile;
var
  List: TStringList;
begin
  List := TStringList.Create;
  try
    GetStrings(List);
    if FStream <> nil then
    begin
      FStream.Position := 0;
      List.SaveToStream(FStream, FEncoding);
      FStream.Size := FStream.Position
    end
    else
      List.SaveToFile(FileName, FEncoding);
  finally
    List.Free;
  end;
end;
{$ENDIF}

end.
