{******************************************************************************}
{* SAS.Planet (SAS.�������)                                                   *}
{* Copyright (C) 2007-2012, SAS.Planet development team.                      *}
{* This program is free software: you can redistribute it and/or modify       *}
{* it under the terms of the GNU General Public License as published by       *}
{* the Free Software Foundation, either version 3 of the License, or          *}
{* (at your option) any later version.                                        *}
{*                                                                            *}
{* This program is distributed in the hope that it will be useful,            *}
{* but WITHOUT ANY WARRANTY; without even the implied warranty of             *}
{* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *}
{* GNU General Public License for more details.                               *}
{*                                                                            *}
{* You should have received a copy of the GNU General Public License          *}
{* along with this program.  If not, see <http://www.gnu.org/licenses/>.      *}
{*                                                                            *}
{* http://sasgis.ru                                                           *}
{* az@sasgis.ru                                                               *}
{******************************************************************************}

unit u_ListenerByEvent;

interface

uses
  i_Listener;

type
  TNotifyListenerEvent = procedure(const AMsg: IInterface) of object;

  TNotifyEventListener = class(TInterfacedObject, IListener)
  private
    FEvent: TNotifyListenerEvent;
  private
    procedure Notification(const AMsg: IInterface);
  public
    constructor Create(AEvent: TNotifyListenerEvent);
  end;

  TNotifyListenerNoMmgEvent = procedure of object;

  TNotifyNoMmgEventListener = class(TInterfacedObject, IListener)
  private
    FEvent: TNotifyListenerNoMmgEvent;
  private
    procedure Notification(const AMsg: IInterface);
  public
    constructor Create(AEvent: TNotifyListenerNoMmgEvent);
  end;

implementation

{ TNotifyEventListener }

constructor TNotifyEventListener.Create(AEvent: TNotifyListenerEvent);
begin
  inherited Create;
  FEvent := AEvent;
  Assert(Assigned(FEvent));
end;

procedure TNotifyEventListener.Notification(const AMsg: IInterface);
begin
  FEvent(AMsg);
end;

{ TNotifyNoMmgEventListener }

constructor TNotifyNoMmgEventListener.Create(AEvent: TNotifyListenerNoMmgEvent);
begin
  inherited Create;
  FEvent := AEvent;
  Assert(Assigned(FEvent));
end;

procedure TNotifyNoMmgEventListener.Notification(const AMsg: IInterface);
begin
  FEvent;
end;

end.
