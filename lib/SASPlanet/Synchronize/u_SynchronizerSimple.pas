{******************************************************************************}
{* SAS.Planet (SAS.�������)                                                   *}
{* Copyright (C) 2007-2014, SAS.Planet development team.                      *}
{* This program is free software: you can redistribute it and/or modify       *}
{* it under the terms of the GNU General Public License as published by       *}
{* the Free Software Foundation, either version 3 of the License, or          *}
{* (at your option) any later version.                                        *}
{*                                                                            *}
{* This program is distributed in the hope that it will be useful,            *}
{* but WITHOUT ANY WARRANTY; without even the implied warranty of             *}
{* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *}
{* GNU General Public License for more details.                               *}
{*                                                                            *}
{* You should have received a copy of the GNU General Public License          *}
{* along with this program.  If not, see <http://www.gnu.org/licenses/>.      *}
{*                                                                            *}
{* http://sasgis.org                                                          *}
{* info@sasgis.org                                                            *}
{******************************************************************************}

unit u_SynchronizerSimple;

interface

uses
  i_ReadWriteSyncFactory,
  u_SynchronizerBase;

type
  TSynchronizerSimple = class(TSynchronizerBase)
  private
    function MakeSyncVariable: IReadWriteSyncFactory;
    function MakeSyncVariableRecursive: IReadWriteSyncFactory;
    function MakeSyncSymmetrical: IReadWriteSyncFactory;
    function MakeSyncSymmetricalRecursive: IReadWriteSyncFactory;
    function MakeSyncStd: IReadWriteSyncFactory;
    function MakeSyncStdRecursive: IReadWriteSyncFactory;
    function MakeSyncBig: IReadWriteSyncFactory;
    function MakeSyncBigRecursive: IReadWriteSyncFactory;
  public
    constructor Create;
  end;

implementation

uses
  SysUtils,
  u_ReadWriteSyncCriticalSection,
  u_ReadWriteSyncRtlResource,
  u_ReadWriteSyncSRW;

type
  TSynchronizerMREWFactory = class(TInterfacedObject, IReadWriteSyncFactory)
  private
    function Make: IReadWriteSync;
  end;

{ TSynchronizerMREWFactory }

function TSynchronizerMREWFactory.Make: IReadWriteSync;
begin
  Result := TMultiReadExclusiveWriteSynchronizer.Create;
end;

{ TSynchronizerSimple }

constructor TSynchronizerSimple.Create;
begin
  inherited Create(
    MakeSyncVariable,
    MakeSyncVariableRecursive,
    MakeSyncSymmetrical,
    MakeSyncSymmetricalRecursive,
    MakeSyncStd,
    MakeSyncStdRecursive,
    MakeSyncBig,
    MakeSyncBigRecursive
  );
end;

function TSynchronizerSimple.MakeSyncBig: IReadWriteSyncFactory;
begin
  Result := MakeSynchronizerRtlResourceFactory;
  if Result = nil then begin
    Result := TSynchronizerMREWFactory.Create;
  end;
end;

function TSynchronizerSimple.MakeSyncBigRecursive: IReadWriteSyncFactory;
begin
  Result := MakeSynchronizerRtlResourceFactory;
  if Result = nil then begin
    Result := TSynchronizerMREWFactory.Create;
  end;
end;

function TSynchronizerSimple.MakeSyncStd: IReadWriteSyncFactory;
begin
  Result := MakeSynchronizerSRWFactory;
  if Result = nil then begin
    Result := MakeSynchronizerRtlResourceFactory;
  end;
  if Result = nil then begin
    Result := TSynchronizerMREWFactory.Create;
  end;
end;

function TSynchronizerSimple.MakeSyncStdRecursive: IReadWriteSyncFactory;
begin
  Result := MakeSynchronizerRtlResourceFactory;
  if Result = nil then begin
    Result := TSynchronizerMREWFactory.Create;
  end;
end;

function TSynchronizerSimple.MakeSyncSymmetrical: IReadWriteSyncFactory;
begin
  Result := MakeSynchronizerRtlResourceFactory;
  if Result = nil then begin
    Result := MakeSynchronizerSRWFactory;
  end;
  if Result = nil then begin
    Result := TSynchronizerMREWFactory.Create;
  end;
end;

function TSynchronizerSimple.MakeSyncSymmetricalRecursive: IReadWriteSyncFactory;
begin
  Result := MakeSynchronizerRtlResourceFactory;
  if Result = nil then begin
    Result := TSynchronizerMREWFactory.Create;
  end;
end;

function TSynchronizerSimple.MakeSyncVariable: IReadWriteSyncFactory;
begin
  Result := MakeSynchronizerSRWFactory;
  if Result = nil then begin
    Result := TSynchronizerCSSCFactory.Create(4096);
  end;
end;

function TSynchronizerSimple.MakeSyncVariableRecursive: IReadWriteSyncFactory;
begin
  Result := TSynchronizerCSSCFactory.Create(4096);
end;

end.
