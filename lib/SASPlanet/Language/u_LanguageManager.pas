{******************************************************************************}
{* SAS.Planet (SAS.�������)                                                   *}
{* Copyright (C) 2007-2019, SAS.Planet development team.                      *}
{* This program is free software: you can redistribute it and/or modify       *}
{* it under the terms of the GNU General Public License as published by       *}
{* the Free Software Foundation, either version 3 of the License, or          *}
{* (at your option) any later version.                                        *}
{*                                                                            *}
{* This program is distributed in the hope that it will be useful,            *}
{* but WITHOUT ANY WARRANTY; without even the implied warranty of             *}
{* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *}
{* GNU General Public License for more details.                               *}
{*                                                                            *}
{* You should have received a copy of the GNU General Public License          *}
{* along with this program.  If not, see <http://www.gnu.org/licenses/>.      *}
{*                                                                            *}
{* http://sasgis.org                                                          *}
{* info@sasgis.org                                                            *}
{******************************************************************************}

unit u_LanguageManager;

interface

uses
  Windows,
  Classes,
  SysUtils,
  i_LanguageListStatic,
  i_LanguageManager,
  u_ChangeableBase;

type
  TLanguageManager = class(TChangeableBase, ILanguageManager)
  private
    FLock: IReadWriteSync;
    FList: ILanguageListStatic;
    FDefaultLangCode: string;
    FNames: TStringList;
    FLangRootPath: string;
    procedure LoadLangs;
  protected
    procedure DoBeforeChangeNotify; override;
  private
    function GetCurrentLanguageCode: string;
    procedure SetCurrentLanguageCode(const ACode: string);

    function GetCurrentLanguageIndex: Integer;
    procedure SetCurrentLanguageIndex(const AValue: Integer);

    function GetLanguageList: ILanguageListStatic;
    function GetLangNameByIndex(const AIndex: Integer): string;
  public
    constructor Create(const ALangRootPath: string);
    destructor Destroy; override;
  end;

implementation

uses
  Forms,
  gnugettext,
  u_Synchronizer,
  u_LanguagesEx,
  u_LanguageListStatic,
  u_CommonFormAndFrameParents;

const
  cDefLangCode = 'en';
  cLangFileExt = '.mo';

{ TLanguageManager }

constructor TLanguageManager.Create(const ALangRootPath: string);
begin
  inherited Create;
  FLock := GSync.SyncStd.Make;
  FLangRootPath := IncludeTrailingPathDelimiter(ALangRootPath);
  FNames := TStringList.Create;
  FDefaultLangCode := cDefLangCode;
  LoadLangs;
end;

destructor TLanguageManager.Destroy;
begin
  FreeAndNil(FNames);
  inherited;
end;

procedure TLanguageManager.DoBeforeChangeNotify;
var
  I: Integer;
begin
  inherited;
  // force reloading forms with new selection
  for I := 0 to Application.ComponentCount - 1 do begin
    if Application.Components[I] is TCommonFormParent then begin
      TCommonFormParent(Application.Components[I]).RefreshTranslation;
    end;
  end;
end;

function TLanguageManager.GetCurrentLanguageIndex: Integer;
var
  VCurrentCode: string;
begin
  FLock.BeginRead;
  try
    VCurrentCode := DefaultInstance.GetCurrentLocaleName;
  finally
    FLock.EndRead;
  end;
  if not FList.FindCode(VCurrentCode, Result) then begin
    Result := 0;
  end;
end;

function TLanguageManager.GetCurrentLanguageCode: string;
var
  VIndex: Integer;
begin
  VIndex := GetCurrentLanguageIndex;
  if VIndex >= 0 then begin
    Result := FList.Code[VIndex];
  end else begin
    Result := '';
  end;
end;

function TLanguageManager.GetLangNameByIndex(const AIndex: Integer): string;
begin
  Result := FNames[AIndex];
end;

function TLanguageManager.GetLanguageList: ILanguageListStatic;
begin
  Result := FList;
end;

procedure TLanguageManager.LoadLangs;

  procedure Add(const ACodes: TStrings; const AName: string; const ACode: string);
  begin
    FNames.Add(AName);
    ACodes.Add(ACode);
  end;

  procedure GetListOfLanguages(const AList: TStrings);
  var
    VLangCode: string;
    VSearchRec: TSearchRec;
  begin
    if FindFirst(FLangRootPath + '*' + cLangFileExt, faAnyFile, VSearchRec) = 0 then
    try
      repeat
        VLangCode := StringReplace(VSearchRec.Name, cLangFileExt, '', [rfReplaceAll, rfIgnoreCase]);
        AList.Add(VLangCode);
      until FindNext(VSearchRec) <> 0;
    finally
      FindClose(VSearchRec);
    end;
  end;

var
  VCodes: TStringList;
  VLanguagesEx: TLanguagesEx;
  VInstalledLanguages: TStringList;
  I: Integer;
  VLangCodeID: LCID;
  VLangFile: string;
  VCurrentCode: string;
  VCurrentIndex: Integer;
begin
  VCodes := TStringList.Create;
  try
    VLanguagesEx := TLanguagesEx.Create;
    try
      VLangCodeID := VLanguagesEx.GNUGetTextID[FDefaultLangCode];
      Add(VCodes, VLanguagesEx.EngNameFromLocaleID[VLangCodeID], FDefaultLangCode);

      VInstalledLanguages := TStringList.Create;
      try
        GetListOfLanguages(VInstalledLanguages);
        for I := 0 to VInstalledLanguages.Count - 1 do begin
          VLangCodeID := VLanguagesEx.GNUGetTextID[VInstalledLanguages[I]];
          if VLangCodeID <> 0 then begin
            Add(VCodes, VLanguagesEx.EngNameFromLocaleID[VLangCodeID], VInstalledLanguages[I]);
          end;
        end;
      finally
        VInstalledLanguages.Free;
      end;
      FList := TLanguageListStatic.Create(VCodes);

      VCurrentCode := DefaultInstance.GetCurrentLocaleName;
      if not FList.FindCode(VCurrentCode, VCurrentIndex) then begin
        VLangCodeID := VLanguagesEx.GNUGetTextID[VCurrentCode];
        VCurrentCode := VLanguagesEx.GNUGetTextName[VLangCodeID];
        DefaultInstance.UseLanguage(VCurrentCode);
        VLangFile := FLangRootPath + VCurrentCode + cLangFileExt;
        DefaultInstance.bindtextdomainToFile(DefaultTextDomain, VLangFile);
      end;
    finally
      VLanguagesEx.Free;
    end;
  finally
    VCodes.Free;
  end;
end;

procedure TLanguageManager.SetCurrentLanguageIndex(const AValue: Integer);
var
  VLangFile: string;
  VPrevLocaleName: string;
  VCurrLocaleName: string;
  VDoChangeNotify: Boolean;
begin
  VDoChangeNotify := False;

  FLock.BeginWrite;
  try
    VPrevLocaleName := DefaultInstance.GetCurrentLocaleName;
    if AValue >= 0 then begin
      DefaultInstance.UseLanguage(FList.Code[AValue]);
    end else begin
      DefaultInstance.UseLanguage(FDefaultLangCode);
    end;
    VCurrLocaleName := DefaultInstance.GetCurrentLocaleName;
    if VPrevLocaleName <> VCurrLocaleName then begin
      VLangFile := FLangRootPath + VCurrLocaleName + cLangFileExt;
      DefaultInstance.bindtextdomainToFile(DefaultTextDomain, VLangFile);
      VDoChangeNotify := True;
    end;
  finally
    FLock.EndWrite;
  end;

  if VDoChangeNotify then begin
    Self.DoChangeNotify;
  end;
end;

procedure TLanguageManager.SetCurrentLanguageCode(const ACode: string);
var
  VIndex: Integer;
begin
  if FList.FindCode(ACode, VIndex) then begin
    SetCurrentLanguageIndex(VIndex);
  end;
end;

end.
