unit libgepacket;

interface

{$IFDEF DEBUG}
  {.$DEFINE DO_TEST_MEM_ALIGN}
{$ENDIF}

const
  libgepacket_dll = 'libgepacket.dll';

procedure gepacket_encode(
  AData: Pointer;
  const ASize: Cardinal
); cdecl; external libgepacket_dll;

function gepacket_decompress(
  const AData: Pointer;
  const ASize: Cardinal;
  ABuff: Pointer;
  const ABuffSize: Cardinal
): Boolean; cdecl; external libgepacket_dll;

function gepacket_get_decompressed_size(
  const AData: Pointer;
  const ASize: Cardinal;
  out ADecompressedSize: Cardinal
): Boolean; inline;

function gepacket_is_compressed(
  const AData: Pointer;
  const ASize: Cardinal
): Boolean; inline;

implementation

function gepacket_is_compressed(
  const AData: Pointer;
  const ASize: Cardinal
): Boolean; inline;
begin
  Result :=
    (ASize > 8) and // header size (magic + data_size)
    (PCardinal(AData)^ = $7468DEAD); // magic
end;

function gepacket_get_decompressed_size(
  const AData: Pointer;
  const ASize: Cardinal;
  out ADecompressedSize: Cardinal
): Boolean; inline;
var
  P: PCardinal;
begin
  if gepacket_is_compressed(AData, ASize) then begin
    P := AData;
    Inc(P);
    ADecompressedSize := P^;
    Result := ADecompressedSize > 0;
  end else begin
    Result := False;
  end;
end;

{$IFDEF DO_TEST_MEM_ALIGN}
function gepacket_mem_align_test: Boolean;
var
  P: Pointer;
begin
  GetMem(P, SizeOf(Pointer) + 3);

  {$IFDEF WIN32}
  Result := Cardinal(P) and not Cardinal(7) = Cardinal(P);
  {$ELSE}
    {$IFDEF WIN64}
    Result := UInt64(P) and not UInt64(7) = UInt64(P);
    {$ELSE}
    Result := False;
    {$ENDIF}
  {$ENDIF}

  FreeMem(P);
end;

initialization
  Assert(
    gepacket_mem_align_test(),
    'gepacket_encode() assumes that buffer is 64-bit aligned!'
  );
{$ENDIF}

end.
