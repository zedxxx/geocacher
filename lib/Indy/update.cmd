@echo off

if exist Core ( rmdir /s /q Core )
if exist Protocols ( rmdir /s /q Protocols )
if exist System ( rmdir /s /q System )

set indy=.tmp/

if not exist %indy% (
    git clone https://github.com/IndySockets/Indy.git %indy%
)

if exist %indy% (

    cd %indy%

    if exist ".git/" (

        git fetch origin --verbose
        git clean -d -x --force    
        git reset --hard 

        git log > ..\changelog.txt
        
        cd Lib
        
        xcopy /s /e /q .\Core ..\..\Core\
        xcopy /s /e /q .\Protocols ..\..\Protocols\
        xcopy /s /e /q .\System ..\..\System\
        
        cd ..    
    )
    cd ..
)
