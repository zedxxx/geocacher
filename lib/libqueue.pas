unit libqueue;

interface

const
  libqueue_dll = 'libqueue.dll';

type
  {$MINENUMSIZE 4}
  queue_type = (
    // A single-producer, single-consumer lock-free queue.
    // It only supports a two-thread use case (one consuming, and one producing).
    // The threads can't switch roles, though you could use this queue completely
    // from a single thread if you wish (but that would sort of defeat the purpose!).
    // https://github.com/cameron314/readerwriterqueue
    QUEUE_TYPE_SPSC,

    // A multi-producer, multi-consumer lock-free queue.
    // https://github.com/cameron314/concurrentqueue
    QUEUE_TYPE_MPMC
  );

function queue_new(var ctx: Pointer; const qtype: queue_type; const capacity: Cardinal): Boolean; cdecl; external libqueue_dll;
function queue_del(ctx: Pointer): Boolean; cdecl; external libqueue_dll;

function queue_enqueue(ctx: Pointer; const data: Pointer): Boolean; cdecl; external libqueue_dll;
function queue_dequeue(ctx: Pointer; out data: Pointer; out found: Boolean): Boolean; cdecl; external libqueue_dll;
function queue_dequeue_batch(ctx: Pointer; out data: Pointer; var count: Cardinal): Boolean; cdecl; external libqueue_dll;

function queue_get_size_approx(ctx: Pointer; out sz: Cardinal): Boolean; cdecl; external libqueue_dll;

function queue_get_error_message(q: Pointer): PAnsiChar; cdecl; external libqueue_dll;

implementation

end.
